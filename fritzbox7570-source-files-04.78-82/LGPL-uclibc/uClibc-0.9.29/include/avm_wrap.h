/* avm_wrap.h
 * Author: Andr� Raupach
 * Erstellt: November, 2007
 * Beschreibung: Diese H-Datei enth�lt wrapper definitionen und funktionen welche alte 
 *               obsolete Funktionsaufrufe auf neuere wrappen. Dadurch werden Fehler z.B. in den
 *               WLAN-TOOLS vermieden.
 */
#ifndef __AVM_WRAP_H__
#define __AVM_WRAP_H__
static inline char *index(const char *string, int charactar) {
    return strchr(string, charactar);
}

static inline void bzero( void *pointer, size_t counter) {
    memset(pointer, '\0', counter);
}
#endif
