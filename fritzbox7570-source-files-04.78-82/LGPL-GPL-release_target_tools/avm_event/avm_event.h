/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _avm_event_h_
#define _avm_event_h_

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _avm_linux_event_h_
#define MAX_EVENT_CLIENT_NAME_LEN   32
#define MAX_EVENT_SOURCE_NAME_LEN   32

enum _avm_event_id {
    avm_event_id_wlan_client_status      = 0,

    avm_event_id_autoprov                = 7,  
    avm_event_id_usb_status              = 8,

    avm_event_id_dsl_status              = 16,
    avm_event_id_dsl_connect_status      = 17,

    avm_event_id_push_button             = 19,
    avm_event_id_telefon_wlan_command    = 20,
    avm_event_id_capiotcp_startstop      = 21,
    avm_event_id_telefon_up              = 22,
    avm_event_id_reboot_req              = 23,
    avm_event_id_appl_status             = 24,
    avm_event_id_led_status              = 25,

    avm_event_id_ethernet_status         = 32,
    avm_event_id_ethernet_connect_status = 33,

    avm_event_id_user_source_notify      = 63,
    avm_event_last                       = 64
};

struct _avm_event_header {
    enum _avm_event_id id;
};

/*------------------------------------------------------------------------------------------*\
 *  ID Spezifische Datentypen  (id=avm_event_id_user_source_notify)
\*------------------------------------------------------------------------------------------*/
struct _avm_event_user_mode_source_notify {
    struct _avm_event_header header;
    enum _avm_event_id id;
};

#endif /*--- #ifndef _avm_linux_event_h_ ---*/

/*------------------------------------------------------------------------------------------*\
 * Returnwerte
\*------------------------------------------------------------------------------------------*/
#define AVM_EVENT_UNKNOWN_HANDLE        -2
#define AVM_EVENT_RESOURCE_ERROR        -1
#define AVM_EVENT_OK                     0

/*------------------------------------------------------------------------------------------*\
 * Vor allen anderen Aktionen muss man sich registrieren. Der Name kann frei gew�hlt werden.
 * Die L�nge des Namens ist auf MAX_EVENT_CLIENT_NAME_LEN Bytes beschraenkt. Die Maske 
 * beinhaltet die Bits der IDs von denen man Informationen erhalten will.
 *
 * mask |= ((unsigned long long)1 << Id);
 *
 * Wenn man auch Informationen liefern will sollte das Bit 
 *              ((unsigned long long)1 << avm_event_id_user_source_notify);
 * gesetzt sein. Andernfalls erh�lt man keine Notifizierung wenn sich ein Client angemeldet
 * hat der die von uns gelieferte Information erwartet.
 * (siehe avm_event_source_register(), avm_event_source_trigger(), avm_event_source_release()
 *
\*------------------------------------------------------------------------------------------*/
int avm_event_register(char* name, unsigned long long mask);

/*------------------------------------------------------------------------------------------*\
 * Liefert den Filediscriptor des Handles zur�ck. Dieser wird f�r select(), poll() und fnctl()
 * ben�tigt.
\*------------------------------------------------------------------------------------------*/
int avm_event_get_fd(int handle);

/*------------------------------------------------------------------------------------------*\
 * dupliziert einen Handle (wird bei fork() ben�tigt)
\*------------------------------------------------------------------------------------------*/
int avm_event_dup(int handle);

/*------------------------------------------------------------------------------------------*\
 * schlie�t einen mittels 'avm_event_dup' erzeugen handle
\*------------------------------------------------------------------------------------------*/
int avm_event_undup(int handle);

/*------------------------------------------------------------------------------------------*\
 * Abmelden beim Treiber, der Handle ist anschie�end ung�ltig.
\*------------------------------------------------------------------------------------------*/
int avm_event_release(int handle);

/*------------------------------------------------------------------------------------------*\
 * triggern einer Event source
 * ACHTUNG: dadurch dass das 'avm_event_register' impleziet ein trigger ausloesst
 *          sind die angeforderten Daten schon vorhanden, es muss also kein weiteres
 *          trigger ausgefuehrt werden.
\*------------------------------------------------------------------------------------------*/
int avm_event_trigger(int handle, unsigned int id);

/*------------------------------------------------------------------------------------------*\
 * Der Aufruf dieser Funktion sogt daf�r dass der read() nicht mehr blockiert
\*------------------------------------------------------------------------------------------*/
int avm_event_set_non_blocking(int handle);

/*------------------------------------------------------------------------------------------*\
 * Der Aufruf dieser Funktion sogt daf�r dass der read() blockiert (default)
\*------------------------------------------------------------------------------------------*/
int avm_event_set_blocking(int handle);

/*------------------------------------------------------------------------------------------*\
 * Lesen der Eventdaten: 
 *      handle      (siehe avm_event_register)
 *      data        Buffer f�r Daten
 *      length      input: maximal datenlaenge, output: gelesene Bytes
\*------------------------------------------------------------------------------------------*/
int avm_event_read(int handle, void *data, unsigned int *length);

/*------------------------------------------------------------------------------------------*\
 * Anmeldung zum "Liefern" von Informationen. Der Name kann frei gew�hlt werden.
 * Die L�nge des Namens ist auf MAX_EVENT_CLIENT_NAME_LEN Bytes beschraenkt. Die Maske 
 * beinhaltet die Bits der IDs zu denen man Informationen liefern will.
 *
 * mask |= ((unsigned long long)1 << Id);
 *
 * ACHTUNG: ((unsigned long long)1 << avm_event_id_user_source_notify);
 *      darf nicht gesetzt sein.
\*------------------------------------------------------------------------------------------*/
int avm_event_source_register(int handle, char *name, unsigned long long mask);

/*------------------------------------------------------------------------------------------*\
 * Liefern von Informationen. Die Informationesstruktur besteht aus zwei Teilen die direkt
 * aufeinander folgen. Der Erste Teil ist die Struktur _avm_event_header. Der Zweite die
 * zu der ID geh�renden Daten. Die data_length ist die Gesamtl�nge beider Teile.
\*------------------------------------------------------------------------------------------*/
int avm_event_source_trigger(int handle, unsigned int data_length, struct _avm_event_header *data);

/*------------------------------------------------------------------------------------------*\
 * Als Informationslieferand Abmelden !
\*------------------------------------------------------------------------------------------*/
int avm_event_source_release(int handle);


#endif /*--- #ifndef _avm_event_h_ ---*/
