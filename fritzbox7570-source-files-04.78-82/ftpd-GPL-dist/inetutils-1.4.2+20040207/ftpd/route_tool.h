

int route_tool_get_dev(unsigned long ipaddr/*hostorder*/, /*OUT*/unsigned *pdev);

#ifdef USE_IPV6
int route_tool_get_ipv6_dev(unsigned char *ipv6addr /* 16 byte */, unsigned *pdev);
#endif
