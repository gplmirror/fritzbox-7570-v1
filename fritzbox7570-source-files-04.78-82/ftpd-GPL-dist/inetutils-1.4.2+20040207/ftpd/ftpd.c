/* - Ftp Server
 * Copyright (c) 1985, 1988, 1990, 1992, 1993, 1994, 2002
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if 0
static char sccsid[] = "@(#)ftpd.c	8.5 (Berkeley) 4/28/95";
#endif

/*
 * FTP server.
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#if !defined (__GNUC__) && defined (_AIX)
#pragma alloca
#endif
#ifndef alloca /* Make alloca work the best possible way.  */
# ifdef __GNUC__
#  define alloca __builtin_alloca
# else /* not __GNUC__ */
#  if HAVE_ALLOCA_H
#   include <alloca.h>
#  else /* not __GNUC__ or HAVE_ALLOCA_H */
#    ifndef _AIX /* Already did AIX, up at the top.  */
char *alloca ();
#    endif /* not _AIX */
#  endif /* not HAVE_ALLOCA_H */
# endif /* not __GNUC__ */
#endif /* not alloca */

#include <sys/param.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#ifdef HAVE_SYS_WAIT_H
#  include <sys/wait.h>
#endif

#include <netinet/in.h>
#ifdef HAVE_NETINET_IN_SYSTM_H
#  include <netinet/in_systm.h>
#endif
#ifdef HAVE_NETINET_IP_H
#  include <netinet/ip.h>
#endif

#define FTP_NAMES
#include <arpa/ftp.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>


#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h> // TCP_CORK

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <netdb.h>
#include <setjmp.h>
#include <signal.h>
#include <grp.h>
#if defined (HAVE_STDARG_H) && defined (__STDC__) && __STDC__
#  include <stdarg.h>
#else
#  include <varargs.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#ifdef TIME_WITH_SYS_TIME
#  include <sys/time.h>
#  include <time.h>
#else
#  ifdef HAVE_SYS_TIME_H
#    include <sys/time.h>
#  else
#    include <time.h>
#  endif
#endif
#include <unistd.h>
#ifdef HAVE_MMAP
#include <sys/mman.h>
#endif

#include <stdarg.h>


/* Include glob.h last, because it may define "const" which breaks
   system headers on some platforms. */
#include <glob.h>

#include "extern.h"

#ifndef LINE_MAX
# define LINE_MAX 2048
#endif

#ifndef LOG_FTP
# define LOG_FTP LOG_DAEMON     /* Use generic facility.  */
#endif

#ifndef MAP_FAILED
# define MAP_FAILED (void*)-1
#endif

#if !HAVE_DECL_FCLOSE
/* Some systems don't declare fclose in <stdio.h>, so do it ourselves.  */
extern int fclose __P ((FILE *));
#endif

extern char *__progname;

/* Exported to ftpcmd.h.  */
struct  sockaddr_storage data_dest; /* Data port.  */
struct  sockaddr_storage his_addr;  /* Peer address.  */
socklen_t his_addrlen;  /* Peer address len */
int g_extended_passive_all = 0; /* EPSV ALL received ? */
int logging;               /* Enable log to syslog.  */
int type = TYPE_A;         /* Default TYPE_A.  */
int form = FORM_N;                  /* Default FORM_N.  */
int debug;                 /* Enable debug mode if 1.  */
int timeout = 900;         /* Timeout after 15 minutes of inactivity.  */
int maxtimeout = 7200;         /* Don't allow idle time to be set
			          beyond 2 hours.  */
int pdata = -1;            /* For passive mode.  */
char    *hostname = 0;             /* Who we are.  */
int usedefault = 1;        /* For data transfers.  */
char tmpline[7];            /* Temp buffer use in OOB.  */

#if 1 /* FRITZBOX */
int max_clients = 0;        /* restrict max clients. */
int use_userlist = 0;
#endif

/* Requester credentials.  */
struct credentials cred;

static struct  sockaddr_storage ctrl_addr;    /* Control address.  */
static struct  sockaddr_storage data_source;  /* Port address.  */
static struct  sockaddr_storage pasv_addr;    /* Pasv address.  */

static int data = -1;       /* Port data connection socket.  */
static jmp_buf urgcatch;
static int stru = STRU_F;     /* Avoid C keyword.  */
static int stru_mode = MODE_S; /* Default STRU mode stru_mode = MODE_S.  */
static int anon_only;       /* Allow only anonymous login.  */
static int no_version;      /* Don't print version to client.  */
static int daemon_mode;     /* Start in daemon mode.  */
static off_t file_size;
static off_t byte_count;
static sig_atomic_t transflag;   /* Flag where in a middle of transfer.  */
static const char *pid_file = PATH_FTPDPID;
#if !defined (CMASK) || CMASK == 0
#undef CMASK
#define CMASK 027
#endif
static int defumask = CMASK;    /* Default umask value.  */
static int login_attempts;       /* Number of failed login attempts.  */
static int askpasswd;            /* Had user command, ask for passwd.  */
static char curname[10];         /* Current USER name.  */
static char ttyline[20];         /* Line to log in utmp.  */

static int opt_use_sendfile = 1;
int opt_readonly = 0;

static char *tmpstring(size_t siz);


#ifdef _LOG_DEBUG

void _Log(char *fmt, ...)
{
	FILE *fp = fopen("/dev/console", "w");
	if (fp) {
		va_list ap;
		fprintf(fp, "ftpd(%d): ", getpid());
		va_start(ap, fmt);
		vfprintf(fp, fmt, ap);
		va_end(ap);
		fprintf(fp, "\n");
		fclose(fp);
	}
}

void _hex_dump(const char *hint, unsigned char *data, size_t siz)
{
	FILE *fp = fopen("/dev/console", "w");
	if (fp) {
		fprintf(fp, "%s:", hint);
		while(siz) {
			fprintf(fp, "%02x", *data++);
			siz--;
		}
		fprintf(fp, "\n");
		fclose(fp);
	}
}
#endif


unsigned short sockaddr_port(struct sockaddr_storage *ss)
{
	switch(ss->ss_family) {
	case AF_INET: return ntohs( ((struct sockaddr_in *)ss)->sin_port );
#ifdef USE_IPV6
	case AF_INET6: return ntohs( ((struct sockaddr_in6 *)ss)->sin6_port );
#endif
	default: return 0;
	}
}

char *sockaddr_addr2string(struct sockaddr_storage *ss)
{
	switch(ss->ss_family) {
	case AF_INET: 
		return inet_ntoa(((struct sockaddr_in *)ss)->sin_addr);
#ifdef USE_IPV6
	case AF_INET6:
		{
			static char buf[8*5];
			unsigned char *a;
			a = (unsigned char *)&((struct sockaddr_in6 *)ss)->sin6_addr;
			snprintf(buf, sizeof(buf), "%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x",
				a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7],
				a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]);
			buf[sizeof(buf)-1] = '\0';
			return buf;
		}
#endif		
	default:
		return "";
	}
}

static int GetMappedIPv4(struct in6_addr *ipv6, unsigned char *ipv4)
{
	if (ipv6->s6_addr16[0]) return -1;
	if (ipv6->s6_addr16[1]) return -1;
	if (ipv6->s6_addr16[2]) return -1;
	if (ipv6->s6_addr16[3]) return -1;
	if (ipv6->s6_addr16[4]) return -1;
	if (ipv6->s6_addr16[5] != 0xffff) return -1;

	ipv4[0] = ipv6->s6_addr[12];
	ipv4[1] = ipv6->s6_addr[13];
	ipv4[2] = ipv6->s6_addr[14];
	ipv4[3] = ipv6->s6_addr[15];
	return 0;
}

static void GetFamilyAndIPv4(struct sockaddr_storage *ss, int *pfam, unsigned char *ipv4)
{
	*pfam = ss->ss_family;
	if (ss->ss_family == AF_INET) {
		memcpy(ipv4, &((struct sockaddr_in *)ss)->sin_addr, 4);
	}
#ifdef USE_IPV6
	else if (ss->ss_family == AF_INET6) {
		if (0 == GetMappedIPv4(&((struct sockaddr_in6 *)ss)->sin6_addr, ipv4)) *pfam = AF_INET;
	}
#endif
}

int is_same_addr(struct sockaddr_storage *ss1, struct sockaddr_storage *ss2)
{

	if (ss1->ss_family != ss2->ss_family) {
#ifdef USE_IPV6
		if (ss1->ss_family == AF_INET6 && ss2->ss_family == AF_INET) {
			struct sockaddr_storage *h = ss1;
			ss1 = ss2; ss2 = h;
		}
		if (ss1->ss_family == AF_INET && ss2->ss_family == AF_INET6) {
			unsigned char ipv4[4]; // network order 
			if (GetMappedIPv4(&((struct sockaddr_in6 *)ss2)->sin6_addr, ipv4)) return 0;
			if (memcmp( &((struct sockaddr_in *)ss1)->sin_addr, 
					&ipv4, 4)) {
Log(("is_same_addr: NO  AF_INET sin_addr differ to mapped ipv4"));
						return 0;
			}
			return 1;
		}
#endif	

Log(("is_same_addr: NO  fam1=%d fam2=%d", ss1->ss_family, ss2->ss_family));
		return 0;
	}

	switch(ss1->ss_family) {
	case AF_INET:

		if (memcmp( &((struct sockaddr_in *)ss1)->sin_addr, 
					&((struct sockaddr_in *)ss2)->sin_addr, 
					sizeof(((struct sockaddr_in *)ss1)->sin_addr))) {
						return 0;
Log(("is_same_addr: NO  AF_INET sin_addr differ"));
		}
		break;
#ifdef USE_IPV6
	case AF_INET6:
		if (memcmp( &((struct sockaddr_in6 *)ss1)->sin6_addr, 
					&((struct sockaddr_in6 *)ss2)->sin6_addr, 
					sizeof(((struct sockaddr_in6 *)ss1)->sin6_addr))) {
Log(("is_same_addr: NO  AF_INET6 sin6_addr differ"));
hex_dump("addr1",  &((struct sockaddr_in6 *)ss1)->sin6_addr, sizeof(((struct sockaddr_in6 *)ss1)->sin6_addr));
hex_dump("addr2",  &((struct sockaddr_in6 *)ss2)->sin6_addr, sizeof(((struct sockaddr_in6 *)ss2)->sin6_addr));
					return 0;
		}
		break;
#endif
	default:
Log(("is_same_addr: NO  unknown family"));
		return 0;
	}
	return 1;
}


static char *
off_to_str (off_t off)
{
	char *buf = tmpstring(80);

	if (!buf) return "";

	if (sizeof (off) > sizeof (long))
		sprintf (buf, "%qd", off);
	else if (sizeof (off) == sizeof (long))
		sprintf (buf, "%ld", off);
	else
		sprintf (buf, "%d", off);

	return buf;
}

/*
 * Timeout intervals for retrying connections
 * to hosts that don't accept PORT cmds.  This
 * is a kludge, but given the problems with TCP...
 */
#define SWAITMAX        90      /* wait at most 90 seconds */
#define SWAITINT        5       /* interval between retries */

static int swaitmax = SWAITMAX;
static int swaitint = SWAITINT;

#ifdef HAVE_SETPROCTITLE
char proctitle[LINE_MAX];    /* initial part of title */
#endif /* SETPROCTITLE */

#define LOGCMD(cmd, file) \
        if (logging > 1) \
		syslog(LOG_INFO,"%s %s%s", cmd, \
		       *(file) == '/' ? "" : curdir(), file);
#define LOGCMD2(cmd, file1, file2) \
        if (logging > 1) \
		syslog(LOG_INFO,"%s %s%s %s%s", cmd, \
		       *(file1) == '/' ? "" : curdir(), file1, \
		       *(file2) == '/' ? "" : curdir(), file2);
#define LOGBYTES(cmd, file, cnt) \
        if (logging > 1) { \
		if (cnt == (off_t)-1) \
			syslog(LOG_INFO,"%s %s%s", cmd, \
			       *(file) == '/' ? "" : curdir(), file);\
		else \
			syslog(LOG_INFO, "%s %s%s = %s bytes", \
			       cmd, (*(file) == '/') ? "" : curdir(), file, \
			       off_to_str (cnt));\
	}

static void ack __P ((const char *));
static void authentication_setup __P ((const char *));
#ifdef HAVE_LIBWRAP
static int check_host __P ((struct sockaddr *sa));
#endif
static void complete_login __P ((struct credentials *));
static char *curdir __P ((void));
static FILE *dataconn __P ((const char *, off_t, const char *));
static void dolog __P ((struct sockaddr_storage *, struct credentials *));
static void end_login __P ((struct credentials *));
static FILE *getdatasock __P ((const char *));
static char *gunique __P ((const char *, int *pcount));
static void lostconn __P ((int));
static void myoob __P ((int));
static int receive_data __P ((FILE *, FILE *));
static void send_data __P ((FILE *, FILE *, off_t));
static void send_data_ls_filter __P ((FILE *, FILE *, off_t, int, int));
static void sigquit __P ((int));
static void usage __P ((int));

#if 1 /* FRITZBOX */
static const char *short_options = "Aa:Ddlp:qt:T:u:m:h:Usr";
#else
static const char *short_options = "Aa:Ddlp:qt:T:u:";
#endif
static struct option long_options[] =
{
	{ "anonymous-only", no_argument, 0, 'A' },
	{ "auth", required_argument, 0, 'a' },
	{ "daemon", no_argument, 0, 'D' },
	{ "debug", no_argument, 0, 'd' },
	{ "help", no_argument, 0, '&' },
	{ "logging", no_argument, 0, 'l' },
	{ "pidfile", required_argument, 0, 'p' },
	{ "no-version", no_argument, 0, 'q' },
	{ "timeout", required_argument, 0, 't' },
	{ "max-timeout", required_argument, 0, 'T' },
	{ "umask", required_argument, 0, 'u' },
	{ "version", no_argument, 0, 'V' },
#if 1 /* FRITZBOX */
	{ "max-clients", required_argument, 0, 'm' },
	{ "hostname", required_argument, 0, 'h' },
	{ "Users", no_argument, 0, 'U' },
	{ "sendfile", no_argument, 0, 's' },
	{ "readonly", no_argument, 0, 'r' },
#endif
	{ 0, 0, 0, 0 }
};

static void
usage (int err)
{
	if (err != 0)
	{
		fprintf (stderr, "Usage: %s [OPTION] ...\n", __progname);
		fprintf (stderr, "Try `%s --help' for more information.\n", __progname);
	}
	else
	{
		fprintf (stdout, "Usage: %s [OPTION] ...\n", __progname);
		puts ("Internet File Transfer Protocol server.\n\n\
  -A, --anonymous-only      Server configure for anonymous service only\n\
  -D, --daemon              Start the ftpd standalone\n\
  -d, --debug               Debug mode\n\
  -l, --logging             Increase verbosity of syslog messages\n\
  -p, --pidfile=[PIDFILE]   Change default location of pidfile\n\
  -q, --no-version          Do not display version in banner\n\
  -t, --timeout=[TIMEOUT]   Set default idle timeout\n\
  -T, --max-timeout         Reset maximum value of timeout allowed\n\
  -u, --umask               Set default umask(base 8)\n\
      --help                Print this message\n\
  -V, --version             Print version\n\
  -a, --auth=[AUTH]         Use AUTH for authentication, it can be:\n\
                               default     passwd authentication."                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      );
#ifdef WITH_PAM
		puts ("\
                               pam         using pam 'ftp' module."                         );
#endif
#ifdef WITH_KERBEROS
		puts ("\
                               kerberos"                         );
#endif
#ifdef WITH_KERBEROS5
		puts ("\
                               kderberos5"                         );
#endif
#ifdef WITH_OPIE
		puts ("\
                               opie"                         );
#endif

#if 1 /* FRITZBOX */
		fprintf(stdout,"\
  -m, --max-clients         Restrict max. number of connected clients\n\
  -h, --hostname            Set hostname\n\
  -U, --Users               Use linux users (/var/ftpusers_readonly for access restriction)\n\
  -s, --sendfile            DON'T use sendfile() but read/write loop\n\
  -r, --readonly            no write access\n"                                                                                                                                                                                                                                                                                                                              );
#endif

		fprintf (stdout, "\nSubmit bug reports to %s.\n", PACKAGE_BUGREPORT);
	}
	exit (err);
}

int
main(int argc, char *argv[], char **envp)
{
	extern char *localhost __P ((void));
	int option;

	(void) signal (SIGTERM, SIG_DFL);

	setpriority(PRIO_PROCESS, 0, 0); /* set normal prio */

#ifndef HAVE___PROGNAME
	__progname = argv[0];
#endif

#ifdef HAVE_TZSET
	tzset(); /* In case no timezone database in ~ftp.  */
#endif

#ifdef HAVE_INITSETPROCTITLE
	/* Save start and extent of argv for setproctitle.  */
	initsetproctitle (argc, argv, envp);
#endif /* HAVE_INITSETPROCTITLE */

	while ((option = getopt_long (argc, argv, short_options,
	                              long_options, NULL)) != EOF)
	{
		switch (option)
		{
		case 'A': /* Anonymous ftp only.  */
			anon_only = 1;
			break;

		case 'a': /* Authentification method.  */
			if (strcasecmp (optarg, "default") == 0)
				cred.auth_type = AUTH_TYPE_PASSWD;
#ifdef WITH_PAM
			else if (strcasecmp (optarg, "pam") == 0)
				cred.auth_type = AUTH_TYPE_PAM;
#endif
#ifdef WITH_KERBEROS
			else if (stracasecmp (optarg, "kerberos") == 0)
				cred.auth_type = AUTH_TYPE_KERBEROS;
#endif
#ifdef WITH_KERBEROS5
			else if (stracasecmp (optarg, "kerberos5") == 0)
				cred.auth_type = AUTH_TYPE_KERBEROS5;
#endif
#ifdef WITH_OPIE
			else if (stracasecmp (optarg, "opie") == 0)
				cred.auth_type = AUTH_TYPE_OPIE;
#endif
			break;

		case 'D': /* Run ftpd as daemon.  */
			daemon_mode = 1;
			break;

		case 'd': /* Enable debug mode.  */
			debug = 1;
			break;

		case 'l': /* Increase logging level.  */
			logging++; /* > 1 == Extra logging.  */
			break;

		case 'p': /* Override pid file */
			pid_file = optarg;
			break;

		case 'q': /* Don't include version number in banner.  */
			no_version = 1;
			break;

		case 't': /* Set default timeout value.  */
			timeout = atoi (optarg);
			if (maxtimeout < timeout)
				maxtimeout = timeout;
			break;

		case 'T': /* Maximum timeout allowed.  */
			maxtimeout = atoi (optarg);
			if (timeout > maxtimeout)
				timeout = maxtimeout;
			break;

		case 'u': /* Set umask.  */
		{
			long val = 0;

			val = strtol (optarg, &optarg, 8);
			if (*optarg != '\0' || val < 0)
				fprintf (stderr, "%s: bad value for -u", argv[0]);
			else
				defumask = val;
			break;
		}

		case '&': /* Usage.  */
			usage (0);
			/* Not reached.  */

		case 'V': /* Version.  */
			printf ("ftpd (%s) %s\n", PACKAGE_NAME, PACKAGE_VERSION);
			exit (0);

#if 1 /* FRITZBOX */
		case 'm': /* Max Clients */
			max_clients = atoi(optarg);
			break;
		case 'h': /* hostname */
			hostname = optarg;
			break;
		case 'U':
			use_userlist = 1;
			break;

		case 's': /* don't use_sendfile */
			opt_use_sendfile = 0;
			break;
		case 'r': /* read-only */
			opt_readonly = 1;
			break;
#endif

		case '?':
		default:
			usage (1);
			/* Not reached.  */
		}
	}

	/* Bail out, wrong usage */
	argc -= optind;
	if (argc != 0)
		usage (1);

	/* LOG_NDELAY sets up the logging connection immediately,
	   necessary for anonymous ftp's that chroot and can't do it later.  */
	openlog ("ftpd", LOG_PID | LOG_NDELAY, LOG_FTP);
	(void) freopen (PATH_DEVNULL, "w", stderr);

	/* If not running via inetd, we detach and dup(fd, 0), dup(fd, 1) the
	   fd = accept(). tcpd is check if compile with the support  */
	if (daemon_mode)
	{
		if (server_mode (pid_file, &his_addr) < 0)
			exit (1);
	}
	else
	{
		his_addrlen = sizeof (his_addr);
		if (getpeername (STDIN_FILENO, (struct sockaddr *)&his_addr,
		                 &his_addrlen) < 0)
		{
			syslog (LOG_ERR, "getpeername (%s): %m", __progname);
			exit (1);
		}
	}

	(void) signal (SIGHUP, sigquit);
	(void) signal (SIGINT, sigquit);
	(void) signal (SIGQUIT, sigquit);
	(void) signal (SIGTERM, sigquit);
	(void) signal (SIGPIPE, lostconn);
	(void) signal (SIGCHLD, SIG_IGN);
	if (signal (SIGURG, myoob) == SIG_ERR)
		syslog (LOG_ERR, "signal: %m");

	/* Get info on the ctrl connection.  */
	{
		int addrlen = sizeof (ctrl_addr);
		if (getsockname (STDIN_FILENO, (struct sockaddr *)&ctrl_addr,
		                 &addrlen) < 0)
		{
			syslog (LOG_ERR, "getsockname (%s): %m", __progname);
			exit (1);
		}
	}

#if defined (IP_TOS) && defined (IPTOS_LOWDELAY) && defined (IPPROTO_IP)
	/* To  minimize delays for interactive traffic.  */
	{
		int tos = IPTOS_LOWDELAY;
		if (setsockopt (STDIN_FILENO, IPPROTO_IP, IP_TOS,
		                (char *)&tos, sizeof(int)) < 0)
			syslog (LOG_WARNING, "setsockopt (IP_TOS): %m");
	}
#endif

#ifdef SO_OOBINLINE
	/* Try to handle urgent data inline.  */
	{
		int on = 1;
		if (setsockopt (STDIN_FILENO, SOL_SOCKET, SO_OOBINLINE,
		                (char *)&on, sizeof (on)) < 0)
			syslog (LOG_ERR, "setsockopt: %m");
	}
#endif

#ifdef SO_KEEPALIVE
	/* Set keepalives on the socket to detect dropped connections.  */
	{
		int keepalive = 1;
		if (setsockopt (STDIN_FILENO, SOL_SOCKET, SO_KEEPALIVE,
		                (char *)&keepalive, sizeof (keepalive)) < 0)
			syslog (LOG_WARNING, "setsockopt (SO_KEEPALIVE): %m");
	}
#endif

#ifdef  F_SETOWN
	if (fcntl (STDIN_FILENO, F_SETOWN, getpid ()) == -1)
		syslog (LOG_ERR, "fcntl F_SETOWN: %m");
#endif

	dolog (&his_addr, &cred);

	/* Deal with login disable.  */
	if (display_file (PATH_NOLOGIN, 530) == 0)
	{
		reply (530, "System not available.");
		exit (0);
	}

	/* Display a Welcome message if exists,
	   N.B. reply(220,) must follow.  */
	(void) display_file (PATH_FTPWELCOME, 220);

#if 1 /* FRITZBOX */
	if (!hostname) {
#endif
	hostname = localhost ();
	if (!hostname)
		perror_reply (550, "Local resource failure: malloc");
#if 1 /* FRITZBOX */
}
#endif

	/* Tell them we're ready to roll.  */
	if (!no_version)
		reply (220, "%s FTP server (%s %s) ready.",
		       hostname, PACKAGE_NAME, PACKAGE_VERSION);
	else
		reply (220, "%s FTP server ready.", hostname);

	/* Set the jump, if we have an error parsing,
	   come here and start fresh.  */
	(void) setjmp (errcatch);

	/* Roll.  */
	for (;;)
		(void) yyparse ();
	/* NOTREACHED */
}

static char *
curdir (void)
{
	static char *path = 0;
	extern char *xgetcwd __P ((void));
	if (path)
		free (path);
	path = xgetcwd ();
	if (!path)
		return (char *)"";
	if (path[1] != '\0') /* special case for root dir. */
	{
		char *tmp = realloc (path, strlen (path) + 2); /* '/' + '\0' */
		if (!tmp)
		{
			free(path);
			return (char *)"";
		}
		strcat(tmp, "/");
		path = tmp;
	}
	/* For guest account, skip / since it's chrooted */
	return (cred.guest ? path+1 : path);
}

static void
sigquit (int signo)
{
	syslog (LOG_ERR, "got signal %s", strsignal (signo));
	dologout (-1);
}


static void
lostconn (int signo)
{
	(void)signo;
	if (debug)
		syslog (LOG_DEBUG, "lost connection");
	dologout (-1);
}

/* Helper function.  */
char *
sgetsave (const char *s)
{
	char *string;
	size_t len;

	if (s == NULL)
		s = "";

	len = strlen (s) + 1;
	string = malloc (len);
	if (string == NULL)
	{
		perror_reply (421, "Local resource failure: malloc");
		dologout (1);
		/* NOTREACHED */
	}
	/*  (void) strcpy (string, s); */
	memcpy (string, s, len);
	return string;
}

static void
complete_login (struct credentials *pcred)
{
	if (setegid ((gid_t)pcred->gid) < 0)
	{
		reply (550, "Can't set gid.");
		return;
	}

#ifdef HAVE_INITGROUPS
	(void) initgroups (pcred->name, pcred->gid);
#endif

	/* open wtmp before chroot */
	(void)snprintf (ttyline, sizeof (ttyline), "ftp%d", getpid ());
	logwtmp_keep_open (ttyline, pcred->name, pcred->remotehost);

	if (pcred->guest)
	{
		/* We MUST do a chdir () after the chroot. Otherwise
		   the old current directory will be accessible as "."
		   outside the new root!  */
		if (chroot (pcred->rootdir) < 0 || chdir (pcred->homedir) < 0)
		{
			reply (550, "Can't set guest privileges.");
			goto bad;
		}
	}
	else if (pcred->dochroot)
	{
		if (chroot (pcred->rootdir) < 0 || chdir(pcred->homedir) < 0)
		{
			reply (550, "Can't change root.");
			goto bad;
		}
		setenv ("HOME", pcred->homedir, 1);
	}
	else if (chdir (pcred->rootdir) < 0)
	{
#if 1 /* FRITZBOX */
		reply (550, "Can't change to home directory.");
		goto bad;
#else
		if (chdir ("/") < 0)
		{
			reply (530, "User %s: can't change directory to %s.",
			       pcred->display_name, pcred->homedir);
			goto bad;
		}
		else
			lreply (230, "No directory! Logging in with home=/");
#endif
	}

	if (seteuid ((uid_t)pcred->uid) < 0)
	{
		reply (550, "Can't set uid.");
		goto bad;
	}

	/* Display a login message, if it exists.
	   N.B. reply(230,) must follow the message.  */
	(void) display_file (PATH_FTPLOGINMESG, 230);

	if (pcred->guest)
	{
		reply (230, "Guest login ok, access restrictions apply.");
#ifdef HAVE_SETPROCTITLE
		snprintf (proctitle, sizeof (proctitle), "%s: anonymous",
		          pcred->remotehost);
		setproctitle ("%s",proctitle);
#endif /* HAVE_SETPROCTITLE */
		if (logging)
			syslog (LOG_INFO, "ANONYMOUS FTP LOGIN FROM %s",
			        pcred->remotehost);
	}
	else
	{
		reply (230, "User %s logged in.", pcred->display_name);
#ifdef HAVE_SETPROCTITLE
		snprintf (proctitle, sizeof (proctitle),
		          "%s: %s", pcred->remotehost, pcred->name);
		setproctitle ("%s",proctitle);
#endif /* HAVE_SETPROCTITLE */
		if (logging)
			syslog (LOG_INFO, "FTP LOGIN FROM %s as %s",
			        pcred->remotehost, pcred->name);
	}
	(void) umask(defumask);
	return;
bad:
	/* Forget all about it... */
	end_login (pcred);
}

/* USER command.
   Sets global passwd pointer pw if named account exists and is acceptable;
   sets askpasswd if a PASS command is expected.  If logged in previously,
   need to reset state.  */
void
user (const char *name)
{
	if (cred.logged_in)
	{
		if (cred.guest || cred.dochroot)
		{
			reply (530, "Can't change user from guest login.");
			return;
		}
		end_login (&cred);
	}

/* FRITZBOX: 29.1.07
 * Der MS IE7 probiert immer einen anonymous login.
 * Wenn er hier schon ein "530 access denied" ohne Kennwortabfrage erh�lt,
 * scheitert der Zugriff!!
 * Daher wird *immer* mit dem linux-User "ftpuser" authentifiziert!!!
 * Als positiver Nebeneffekt ist jetzt der ftp-Username vom Client total egal.
 */
	/* Non zero means failed.  */
	int bad;
	if (use_userlist) {
// hier wg. IE7 auch bei anonymous oder ftp ein kennwort abfragen.
		if (cred.message) free(cred.message);
		cred.message = 0;
		if (cred.display_name) free(cred.display_name);
		cred.display_name = sgetsave(name);
		if (cred.name) free(cred.name);
		cred.name = 0; // this is a marker, that we have to call auth_user() after the password is entered
		bad = 0;

		cred.access_from_internet = 0;
		{
			unsigned dev;
			unsigned inet_dev;
			int ret;
#ifdef USE_IPV6
			if (his_addr.ss_family != AF_INET) {
				// check routing to DNS root server 2001:503:BA3E::2:30
				unsigned char root_dns[16] = { 0x20,0x01, 0x05,0x03, 0xba,0x3e, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x02, 0x00,0x30 };

 				ret = route_tool_get_ipv6_dev(&root_dns, &inet_dev);
				if (0 == ret) {
					// access to internet exists (default route) -> ftp client may be from internet
					ret = route_tool_get_ipv6_dev(ntohl(((struct sockaddr_in6 *)&his_addr)->sin6_addr.s6_addr), &dev);
					if (0 == ret && dev == inet_dev) {
						// access from internet
						cred.access_from_internet = 1;
					}
				}
			} else {
#endif
				// check routing to DNS root server 207.46.19.190
				ret = route_tool_get_dev((207 << 24) | (46 << 16) | (19 << 8) | 190, &inet_dev);
				if (0 == ret) {
					// access to internet exists (default route) -> ftp client may be from internet
					ret = route_tool_get_dev(ntohl(((struct sockaddr_in *)&his_addr)->sin_addr.s_addr), &dev);
					if (0 == ret && dev == inet_dev) {
						// access from internet
						cred.access_from_internet = 1;
					}
				}
#ifdef USE_IPV6
			}
#endif
		}

	} else {
		cred.access_from_internet = 0;
		bad = auth_user(name, "ftpuser", &cred);
	}
	if (bad) {
		/* If they gave us a reason.  */
		if (cred.message)
		{
			reply (530, "%s", cred.message);
			free (cred.message);
			cred.message = NULL;
		}
		else
			reply (530, "User %s access denied.", name);
		if (logging)
			syslog(LOG_NOTICE, "FTP LOGIN REFUSED FROM %s, %s",
			       cred.remotehost, name);
		return;
	}

	/* If the server is set to serve anonymous service only
	   the request have to come from a guest or a chrooted.  */
	if (anon_only && !cred.guest && !cred.dochroot)
	{
		reply (530, "Sorry, only anonymous ftp allowed");
		return;
	}

	if (logging)
	{
		strncpy (curname, name, sizeof (curname) - 1);
		curname [sizeof (curname) - 1] = '\0'; /* Make sure null terminated.  */
	}

	if (cred.message)
	{
		reply (331, "%s", cred.message);
		free (cred.message);
		cred.message = NULL;
	}
	else
		reply (331, "Password required for %s.", name);

	askpasswd = 1;

	/* Delay before reading passwd after first failed
	   attempt to slow down passwd-guessing programs.  */
	if (login_attempts)
		sleep ((unsigned) login_attempts);
}

/* Terminate login as previous user, if any, resetting state;
   used when USER command is given or login fails.  */
static void
end_login (struct credentials *pcred)
{
	char *remotehost = pcred->remotehost;
	int atype = pcred->auth_type;
	(void) seteuid ((uid_t)0);
	if (pcred->logged_in)
		logwtmp_keep_open (ttyline, "", "");

	if (pcred->name)
		free (pcred->name);
	if (pcred->display_name)
		free (pcred->display_name);
	if (pcred->passwd)
	{
		memset (pcred->passwd, 0, strlen (pcred->passwd));
		free (pcred->passwd);
	}
	if (pcred->homedir)
		free (pcred->homedir);
	if (pcred->rootdir)
		free (pcred->rootdir);
	if (pcred->shell)
		free (pcred->shell);
	if (pcred->pass) /* ??? */
	{
		memset (pcred->pass, 0, strlen (pcred->pass));
		free (pcred->pass);
	}
	if (pcred->message)
		free (pcred->message);
	memset (pcred, 0, sizeof (*pcred));
	pcred->remotehost = remotehost;
	pcred->auth_type = atype;
}

void
pass (const char *passwd)
{
	if (cred.logged_in || askpasswd == 0)
	{
		reply(503, "Login with USER first.");
		return;
	}
	askpasswd = 0;

#if 1 /* FRITZBOX */
	if (!cred.name) {
		// have to call auth_user() now
		char *display_name = sgetsave(cred.display_name);
		int bad = auth_user(display_name, display_name, &cred);
		free(display_name);
		if (cred.message) free(cred.message);
		cred.message = 0;
		if (bad) {
			reply (530, "Login incorrect.");
			return;
		}
	}
#endif


	if (!cred.guest) /* "ftp" is the only account allowed no password.  */
	{

		/* Try to authenticate the user.  Failed if != 0.  */
		if (auth_pass (passwd, &cred) != 0)
		{
			/* Any particular reasons.  */
			if (cred.message)
			{
				reply (530, "%s", cred.message);
				free (cred.message);
				cred.message = NULL;
			}
			else
				reply (530, "Login incorrect.");
			if (logging)
				syslog (LOG_NOTICE, "FTP LOGIN FAILED FROM %s, %s",
				        cred.remotehost, curname);
			if (login_attempts++ >= 5)
			{
				syslog(LOG_NOTICE, "repeated login failures from %s",
				       cred.remotehost);
				exit(0);
			}
			return;
		}
	}
	cred.logged_in = 1; /* Everything seems to be allright.  */
	complete_login (&cred);
	login_attempts = 0; /* This time successful.  */
}

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */

#if 1 /* FRITZBOX */

static char *tmpstring(size_t siz)
{
#define NTMPSTRING 16
	static char *tmps[NTMPSTRING] = { 0, };
	static int index = 0;
	char *p;

	if (tmps[index]) free(tmps[index]);

	p = tmps[index] = malloc(siz);

	index++; if (index == NTMPSTRING) index = 0;

	return p;
}

/* ------------------------------------------------------------------------ */

// like realpath(), but don't follow symbolic links
// this is just a textual operations, the filesystem is not accessed
// (except for getcwd())
static char *my_abspath(const char *path, char *buf, size_t bufsiz)
{
	char *p;

	if (*path == '/') {
		buf[0] = '\0';
	} else {
		if (buf != getcwd(buf, bufsiz)) return 0;
		if (buf[0] != '/') return 0;
		if (buf[1] != '\0') {
			// add a '/'
			strcat(buf, "/");
		}
	}

	if (strlen(path)+1 > (bufsiz - strlen(buf)-1)) return 0;

	strcat(buf, path);
	strcat(buf, "/");

	// remove // remove /./  resolve /../

	p = buf;
	while(*p != '\0') {
		if (*p == '/') {
			p++;
			if (*p == '\0') break;
			if (*p == '/') {
				// remove //
				memcpy(p-1, p, strlen(p)+1);
				p--;
				continue;
			} else if (*p == '.') {
				p++;
				if (*p == '\0') break;
				if (*p == '/') {
					// remove /./
					memcpy(p-2, p, strlen(p)+1);
					p -= 2;
					continue;
				}
				if (*p == '.') {
					p++;
					if (*p == '\0') break;
					if (*p == '/') {
						// resolve /../
						char *p2 = p - 4;
						while(p2 >= buf) {
							if (*p2 == '/') break;
							p2--;
						}
						if (p2 >= buf) {
							memcpy(p2, p, strlen(p)+1);
							p = p2;
						} else {
							memcpy(buf, p,strlen(p)+1);
							p = buf;
						}
						continue;
					}
				}
			}
		}

		p++;
	}

	// remove trailing /
	if (strlen(buf) > 1 && buf[strlen(buf)-1] == '/') {
		buf[strlen(buf)-1] = '\0';
	}
	return buf;
}

/* ------------------------------------------------------------------------ */


static int virt_dir_exists(const char *abs_virt_path, const char *dirname)
{
	if (!cred.rootdir) return 0;

	char *path = tmpstring(strlen(cred.rootdir) + strlen(abs_virt_path) + 1 + strlen(dirname) + 1);

	if (!path) return 0;

	sprintf(path, "%s%s/%s", cred.rootdir, abs_virt_path, dirname);

	struct stat sb;
	if (stat(path, &sb)) return 0;
	return sb.st_mode &  S_IFDIR;
}

static int IsAccessToAbsoluteVirtPath(const char *abs_virt_path, int *pwrite_access)
{
	/*
	   /        <- ro
	   /USB     <- ro
	   /USB/    <- ro
	   /USB/_avm_   <- ro   (no access if access from internet or if /USB/_avm_/fax does not exit)
	   /USB/_avm_/   <- ro  (no access if access from internet or if /USB/_avm_/fax does not exit)
	   /USB/x   <- rw
	 */

	*pwrite_access = 0;

#ifdef USE_AVM_INTERN_DIR
	char *p = abs_virt_path;
	if (*p != '/') return 0;
	p++;
	while(*p != '/' && *p != '\0') p++;
	if (*p == '\0') return 1;

	if (p == strstr(p, "/_avm_")) {
		char *p2 = p + strlen("/_avm_");
		if (*p2 == '\0' || *p2 == '/') {
			if (cred.access_from_internet) return 0;
			char c = *p2;
			*p2 = '\0';
			if (virt_dir_exists(abs_virt_path, "fax")) {
				*p2 = c;
				if (*p2 == '\0' || (*p2 == '/' && *(p2+1) == '\0')) return 1; // /_avm_ is ro
				if (p2 == strstr(p2, "/fax")) {
					p2 += strlen("/fax");
					if (*p2 == '\0' || *p2 == '/') return 1; // /_avm_/fax is ro
				}
				return 0; // e.g. /_avm_/some is hidden
			}
			*p2 = c;
			return 0; // hidden
		}
	}
	// allow  (e.g. /_avm_visable)
#endif  // USE_AVM_INTERN_DIR

	if (cred.write_access) *pwrite_access = 1;

	return 1;
}

static char *virtual_to_real_path(const char *path)
{
	char *real;

	if (*path == '/' && cred.rootdir && strcmp(cred.rootdir, "/")) {
		// absolute client path -> change virt. to real root

		real = tmpstring(strlen(path) + strlen(cred.rootdir) + 1);
		if (!real) return 0;
		strcpy(real, cred.rootdir);
		if (strcmp(path, "/")) {
			strcat(real, path);
		}
	} else {
		// relative virtual paths are autom.
		// relative real paths
		real = tmpstring(strlen(path) + 1);
		if (!real) return 0;
		strcpy(real, path);
	}
	return real;
}

// check if access to a given real path is allowed for the client
// all path components must exist
static int is_allowed_real_path(const char *path, int *pwrite_access)
{
	char buf[1024];
	int len;

	Log(("is_allowed_real_path %s", path));

	if (!cred.logged_in) {
		Log(("is_allowed_real_path - not logged in"));
		return 0;
	}
	if (!cred.rootdir) {
		Log(("is_allowed_real_path - no root dir"));
		return 0;
	}

	if (*path == '\0') {
		/* realpath() with empty string would fail */
		if (0 == getcwd(buf, sizeof(buf))) {
			Log(("is_allowed_real_path - getcwd failed"));
			return 0;
		}
		buf[sizeof(buf)-1] = '\0';
	} else {
		if (buf != realpath(path, buf)) {
			// Check if parent exists
			// and construct realpath
			char *p = &path[strlen(path)];
			while (p > path && *p != '/') p--;
			if (*p != '/') {
				Log(("realpath failed and path=%s has no / p=%s", path, p));
				if (buf != realpath(".", buf)) {
					// parent does not exists
					Log(("realpath of current dir failed"));
					return 0;
				}
				if (buf[strlen(buf)-1] != '/') {
					strcat(buf, "/");
				}
				strcat(buf, path);
			} else {
				if (!strcmp(p+1, "..")) return 0;
				*p = '\0';
				if (buf != realpath(path, buf)) {
					// parent does not exists
					Log(("realpath of parent=%s failed", path));
					*p = '/';
					return 0;
				}
				*p = '/';
				if (buf[strlen(buf)-1] == '/') {
					strcat(buf, p+1);
				} else strcat(buf, p);
			}
			Log(("is_allowed_real_path - constructed realpath %s", buf));
		}
	}

	if (buf != strstr(buf, cred.rootdir)) {
		Log(("is_allowed_real_path - buf=%s not within ftp root=%s", buf, cred.rootdir));
		return 0;
	}

	char *abs_virt_path;

	len = strlen(cred.rootdir);
	char single_slash[2];
	if (len > 1) {
		if (buf[len] != '/' && buf[len] != '\0') return 0;
		if (buf[len] == '\0') {
			strcpy(single_slash, "/");
			abs_virt_path = single_slash;
		} else abs_virt_path = &buf[len];
	} else abs_virt_path = buf;


	if (!IsAccessToAbsoluteVirtPath(abs_virt_path, pwrite_access)) {
		Log(("IsAccessToAbsoluteVirtPath abs_virt_path=%s failed", abs_virt_path));
		return 0;
	}

	return 1;
}

// change the real path to a virtual client path
static void real_to_virtual_path(char **ppath)
{
	if (!cred.rootdir || !strcmp(cred.rootdir, "/")) return;

	if (*ppath == strstr(*ppath, cred.rootdir)) {
		char *p;
		int diff = strlen(*ppath) - strlen(cred.rootdir);
		if (diff) {
			p = tmpstring(diff + 1);
			if (!p) return;
			strcpy(p, (*ppath) + strlen(cred.rootdir));
		} else {
			p = tmpstring(2);
			if (!p) return;
			strcpy(p, "/");
		}
		*ppath = p;
	}
}
#endif  /* FRITZBOX */

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */

static int IsAllowed(const char *path, char **preal_path, int *pwrite_access)
{
	*preal_path = virtual_to_real_path(path);
	if (*preal_path) {
		if (is_allowed_real_path(*preal_path, pwrite_access)) return 1;
	}

	errno = EACCES;
	return 0;
}

static int IsWriteAccess(const char *path, char **preal_path)
{
	int write_access;
	if (!IsAllowed(path, preal_path, &write_access)) {
		Log(("IsWriteAccess failed path=%s", path));
		return 0;
	}
	Log(("IsWriteAccess path=%s real=%s write_access=%d", path, *preal_path, write_access));

	return write_access;
}

static int IsReadAccess(const char *path, char **preal_path)
{
	int write_access;
	return IsAllowed(path, preal_path, &write_access);
}

/* ------------------------------------------------------------------------ */

static FILE *acl_fopen(const char *fn, char *mode)
{
	int is_write = 1;
	char *real;

	if (mode[0] == 'r' && mode[1] != '+') is_write = 0;

	if (is_write) {
		if (!IsWriteAccess(fn, &real)) return 0;
	} else if (!IsReadAccess(fn, &real)) return 0;

	return fopen(real, mode);
}

static int acl_mkdir(const char *dir, mode_t mode)
{
	char *real;
	if (!IsWriteAccess(dir, &real)) {
		return -1;
	}
	return mkdir(real, mode);
}

static int acl_rename(const char *from, const char *to)
{
	char *real_from;
	char *real_to;

	if (!IsWriteAccess(from, &real_from)) {
		return -1;
	}
	if (!IsWriteAccess(to, &real_to)) {
		return -1;
	}
	return rename(real_from, real_to);
}

static int acl_stat(const char *path, struct stat *buf)
{
	char *real_path;
	if (!IsReadAccess(path, &real_path)) return -1;
	return stat(real_path, buf);
}

static int acl_rmdir(const char *pathname)
{
	char *real_path;
	if (!IsWriteAccess(pathname, &real_path)) return -1;
	return rmdir(real_path);
}

static int acl_unlink(const char *pathname)
{
	char *real_path;
	if (!IsWriteAccess(pathname, &real_path)) return -1;
	return unlink(real_path);
}

static int acl_chdir(const char *path)
{
	char *real_path;
	if (!IsReadAccess(path, &real_path)) return -1;
	return chdir(real_path);
}

static DIR *acl_opendir(const char *name)
{
	char *real_path;
	if (!IsReadAccess(name, &real_path)) return 0;
	return opendir(real_path);
}


/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */

static char *filter_ls_options(const char *options)
{
	char *s = tmpstring(strlen(options)+1);
	if (!s) return "";


	// only allow options 1 l R a A
	char *p = s;
	while(*options != '\0') {
		switch(*options) {
		case '-':
		case '1': case 'l': case 'R': case 'a': case 'A':
			*p++ = *options;
		default: ;
		}
		options++;
	}
	*p = '\0';
	if (!strcmp(s, "-")) *s = '\0';

	return s;
}

// remove any options that are not  l a d R
static void remove_unallowed_ls_options(char *buf, int *pis_long, int *pis_recursive)
{
	*pis_long = 0;
	*pis_recursive = 0;

	char *p = buf;

	while(*p != '\0') {
		while(*p == ' ' || *p == '\t') p++;
		if (*p == '\0') break;
		if (*p == '-') {
			char *start = p;
			p++;
			while(*p != ' ' && *p != '\t' && *p != '\0') {
				switch(*p) {
				case 'l': p++; *pis_long = 1; break;
				case 'R': p++; *pis_recursive = 1; break;
				case 'a': case 'd': p++; break;
				default:
					// remove option
				{
					char *p2 = p;
					do {
						*p2 = *(p2+1);
						if (*p2 == '\0') break;
						p2++;
					} while(1);
				}
				}
			}
		} else {
			while(*p != ' ' && *p != '\t' && *p != '\0') p++;
		}
	}
}

void
retrieve (const char *cmd, const char *name)
{
	FILE *fin, *dout;
	struct stat st;
	int (*closefunc)__P ((FILE *));
	size_t buffer_size = 0;
	char line[BUFSIZ];
	int is_long = 0;
	int is_recursive = 0;
	int is_popen = 0;

	Log(("retrieve cmd=%s name=%s", cmd ? cmd : "?", name ? name : "?"));

	if (cmd == 0)
	{
		fin = acl_fopen (name, "r"), closefunc = fclose;
		st.st_size = 0;
	}
	else
	{
		char real_line[BUFSIZ];

		{
/* security - only use the last argument seperated by " \t\n" */
			char *p = name;
			while(*p) {
				if (*p == ' ' || *p == '\t' || *p == '\n') name = p+1;
				p++;
			}
		}
/* note that name can be '-l' or other options for cmd="/bin/ls" */
		char *real_name;
		if (name[0] == '-') { real_name = name; }
		else if (!IsReadAccess(name, &real_name)) {
			perror_reply (550, name);
			return;
		}

		snprintf (real_line, sizeof line, cmd, real_name);
		snprintf (line, sizeof line, cmd, name);
		name = line; /* should not contain the real_name */
		remove_unallowed_ls_options(real_line, &is_long, &is_recursive);
		fin = ftpd_popen (real_line, "r"), closefunc = ftpd_pclose;
		is_popen = 1;
		st.st_size = -1;
		buffer_size = BUFSIZ;
	}

	if (fin == NULL)
	{
		if (errno != 0)
		{
			perror_reply (550, name);
			if (cmd == 0)
			{
				LOGCMD("get", name);
			}
		}
		return;
	}
	byte_count = -1;
	if (cmd == 0 && (fstat (fileno (fin), &st) < 0 || !S_ISREG (st.st_mode)
	                 || !(buffer_size = ST_BLKSIZE (st))))
	{
		reply(550, "%s: not a plain file.", name);
		goto done;
	}
	if (buffer_size < 32*1024) buffer_size = 32*1024; // more speed
	if (restart_point)
	{
		if (type == TYPE_A)
		{
			off_t i, n;
			int c;

			n = restart_point;
			i = 0;
			while (i++ < n)
			{
				c = getc (fin);
				if (c == EOF)
				{
					perror_reply (550, name);
					goto done;
				}
				if (c == '\n')
					i++;
			}
		}
		else if (lseek (fileno (fin), restart_point, SEEK_SET) < 0)
		{
			perror_reply (550, name);
			goto done;
		}
	}
	dout = dataconn (name, st.st_size, "w");
	if (dout == NULL)
		goto done;
	if (is_popen) {
		// assume its ls command
		send_data_ls_filter(fin, dout, buffer_size, is_long, is_recursive);
	} else {
		send_data(fin, dout, buffer_size);
	}
	(void) fclose (dout);
	data = -1;
	pdata = -1;
done:
	if (cmd == 0)
		LOGBYTES ("get", name, byte_count);
	(*closefunc)(fin);
}

void
store (const char *name, const char *mode, int unique)
{
	FILE *fout, *din;
	struct stat st;
	int (*closefunc)__P ((FILE *));

	if (unique && acl_stat(name, &st) == 0) {
		int count;
		char *name2;
		if ((name = gunique(name, &count)) == NULL) {
			LOGCMD (*mode == 'w' ? "put" : "append", name);
			return;
		}
		/* FRITZBOX - gunique adjust name also */
		name2 = tmpstring(strlen(name) + 8);
		if (!name2) {
			LOGCMD (*mode == 'w' ? "put" : "append", name);
			return;
		}
		strcpy(name2, name);
		name = name2;
		sprintf(name + strlen(name), ".%d", count);
	}

	if (restart_point)
		mode = "r+";
	fout = acl_fopen (name, mode);
	closefunc = fclose;
	if (fout == NULL)
	{
		perror_reply (553, name);
		LOGCMD (*mode == 'w' ? "put" : "append", name);
		return;
	}
	byte_count = -1;
	if (restart_point)
	{
		if (type == TYPE_A)
		{
			off_t i, n;
			int c;

			n = restart_point;
			i = 0;
			while (i++ < n)
			{
				c = getc (fout);
				if (c == EOF)
				{
					perror_reply (550, name);
					goto done;
				}
				if (c == '\n')
					i++;
			}
			/* We must do this seek to "current" position
			   because we are changing from reading to
			   writing.  */
			if (fseek (fout, 0L, SEEK_CUR) < 0)
			{
				perror_reply (550, name);
				goto done;
			}
		}
		else if (lseek (fileno(fout), restart_point, SEEK_SET) < 0)
		{
			perror_reply (550, name);
			goto done;
		}
	}
	din = dataconn (name, (off_t)-1, "r");
	if (din == NULL)
		goto done;
	if (receive_data (din, fout) == 0)
	{
		if (unique)
			reply (226, "Transfer complete (unique file name:%s).", name);
		else
			reply (226, "Transfer complete.");
	}
	(void) fclose (din);
	data = -1;
	pdata = -1;
done:
	LOGBYTES (*mode == 'w' ? "put" : "append", name, byte_count);
	(*closefunc)(fout);
}

static FILE *
getdatasock (const char *mode)
{
	int s, t, tries;

	if (data >= 0)
		return fdopen (data, mode);
	(void) seteuid ((uid_t)0);

	unsigned char ipv4[4];
	int family;
	GetFamilyAndIPv4(&ctrl_addr, &family, ipv4);
	s = socket (family /*AF_INET*/, SOCK_STREAM, 0);
	if (s < 0)
		goto bad;

	/* Enables local reuse address.  */
	{
		int on = 1;
		if (setsockopt (s, SOL_SOCKET, SO_REUSEADDR,
		                (char *) &on, sizeof(on)) < 0)
			goto bad;
	}

	/* anchor socket to avoid multi-homing problems */
	memset(&data_source, 0, sizeof(data_source));
	data_source.ss_family = family;
	switch(data_source.ss_family) {
	case AF_INET: 
		memcpy(&((struct sockaddr_in *)&data_source)->sin_addr, ipv4, sizeof(ipv4));
		break;
#ifdef USE_IPV6
	case AF_INET6:
		memcpy(&((struct sockaddr_in6 *)&data_source)->sin6_addr, 
			&((struct sockaddr_in6 *)&ctrl_addr)->sin6_addr, 
			sizeof((struct sockaddr_in6 *)&data_source)->sin6_addr);
#endif
		break;
	default:
		goto bad;
	}
	//data_source.ss_family = AF_INET;
	//data_source.sin_addr = ctrl_addr.sin_addr;
	for (tries = 1; ; tries++)
	{
		if (bind (s, (struct sockaddr *)&data_source, sizeof(data_source)) >= 0)
			break;
		if (errno != EADDRINUSE || tries > 10)
			goto bad;
		sleep (tries);
	}
	(void) seteuid ((uid_t)cred.uid);

#if defined (IP_TOS) && defined (IPTOS_THROUGHPUT) && defined (IPPROTO_IP)
	{
		int on = IPTOS_THROUGHPUT;
		if (setsockopt(s, IPPROTO_IP, IP_TOS, (char *)&on, sizeof(int)) < 0)
			syslog(LOG_WARNING, "setsockopt (IP_TOS): %m");
	}
#endif

	return (fdopen(s, mode));
bad:
	/* Return the real value of errno (close may change it) */
	t = errno;
	(void) seteuid ((uid_t)cred.uid);
	(void) close(s);
	errno = t;
	return NULL;
}

static FILE *
dataconn (const char *name, off_t size, const char *mode)
{
	char sizebuf[32];
	FILE *file;
	int retry = 0;

	file_size = size;
	byte_count = 0;
	if (size != (off_t) -1)
		(void) snprintf(sizebuf, sizeof(sizebuf), " (%s bytes)",
		                off_to_str (size));
	else
		*sizebuf = '\0';
	if (pdata >= 0)
	{
		struct sockaddr_storage from;
		int s, fromlen = sizeof (from);

		(void) signal (SIGALRM, toolong);
		(void) alarm ((unsigned) timeout);
		s = accept (pdata, (struct sockaddr *)&from, &fromlen);
		(void) alarm (0);
		if (s < 0)
		{
			reply(425, "Can't open data connection.");
			(void) close (pdata);
			pdata = -1;
			return NULL;
		}
		(void) close (pdata);
		pdata = s;
#if defined (IP_TOS) && defined (IPTOS_THROUGHPUT) && defined (IPPROTO_IP)
		/* Optimize throughput.  */
		{
			int tos = IPTOS_THROUGHPUT;
			(void) setsockopt (s, IPPROTO_IP, IP_TOS, (char *)&tos, sizeof (int));
		}
#endif
#ifdef SO_KEEPALIVE
		/* Set keepalives on the socket to detect dropped conns.  */
		{
			int keepalive = 1;
			(void) setsockopt (s, SOL_SOCKET, SO_KEEPALIVE,
			                   (char *)&keepalive, sizeof (int));
		}
#endif
		reply (150, "Opening %s mode data connection for '%s'%s.",
		       type == TYPE_A ? "ASCII" : "BINARY", name, sizebuf);
		return fdopen (pdata, mode);
	}
	if (data >= 0)
	{
		reply (125, "Using existing data connection for '%s'%s.",
		       name, sizebuf);
		usedefault = 1;
		return fdopen (data, mode);
	}
	if (usedefault)
		data_dest = his_addr;
	usedefault = 1;
	file = getdatasock (mode);
	if (file == NULL)
	{
		reply (425, "Can't create data socket (%s,%d): %s.",
		       sockaddr_addr2string(&data_source) /*inet_ntoa (data_source.sin_addr)*/,
		       sockaddr_port(&data_source), strerror(errno));
		return NULL;
	}
	data = fileno (file);
	while (connect (data, (struct sockaddr *)&data_dest,
	                sizeof (data_dest)) < 0)
	{
		if (errno == EADDRINUSE && retry < swaitmax)
		{
			sleep ((unsigned) swaitint);
			retry += swaitint;
			continue;
		}
		perror_reply (425, "Can't build data connection");
		(void) fclose (file);
		data = -1;
		return NULL;
	}
	reply (150, "Opening %s mode data connection for '%s'%s.",
	       type == TYPE_A ? "ASCII" : "BINARY", name, sizebuf);
	return file;
}

/* Tranfer the contents of "instr" to "outstr" peer using the appropriate
   encapsulation of the data subject * to Mode, Structure, and Type.

   NB: Form isn't handled.  */
static void
send_data (FILE *instr, FILE *outstr, off_t blksize)
{
	int c, cnt, filefd, netfd;
	char *buf, *bp;
	off_t curpos;
	size_t len, filesize;

	transflag++;
	if (setjmp (urgcatch))
	{
		transflag = 0;
		return;
	}

	netfd = fileno (outstr);
	filefd = fileno (instr);
#ifdef HAVE_MMAP
#error HAVE_MMAP is somewhat slower on F!Box 7270
	if (file_size > 0)
	{
		curpos = lseek (filefd, 0, SEEK_CUR);
		if (curpos >= 0)
		{
			filesize = file_size - curpos;
			buf = mmap (0, filesize, PROT_READ, MAP_SHARED, filefd, curpos);
		}
	}
#endif

	switch (type)
	{

	case TYPE_A:
#ifdef HAVE_MMAP
		if (file_size > 0 && curpos >= 0 && buf != MAP_FAILED)
		{
			len = 0;
			while (len < filesize)
			{
				byte_count++;
				if (buf[len] == '\n')
				{
					if (ferror (outstr))
						break;
					(void) putc ('\r', outstr);
				}
				(void) putc (buf[len], outstr);
				len++;
			}
			fflush (outstr);
			transflag = 0;
			munmap (buf, filesize);
			if (ferror (outstr))
				goto data_err;
			reply (226, "Transfer complete.");
			return;
		}
#endif
		while ((c = getc (instr)) != EOF)
		{
			byte_count++;
			if (c == '\n')
			{
				if (ferror (outstr))
					goto data_err;
				(void) putc ('\r', outstr);
			}
			(void) putc (c, outstr);
		}
		fflush (outstr);
		transflag = 0;
		if (ferror (instr))
			goto file_err;
		if (ferror (outstr))
			goto data_err;
		reply (226, "Transfer complete.");
		return;

	case TYPE_I:
	case TYPE_L:
#ifdef HAVE_MMAP
		if (file_size > 0 && curpos >= 0 && buf != MAP_FAILED)
		{
			bp = buf;
			len = filesize;
			do
			{
				cnt = write (netfd, bp, len);
				len -= cnt;
				bp += cnt;
				if (cnt > 0) byte_count += cnt;
			} while (cnt > 0 && len > 0);
			transflag = 0;
			munmap (buf, (size_t)filesize);
			if (cnt < 0)
				goto data_err;
			reply (226, "Transfer complete.");
			return;
		}
#endif
		if (opt_use_sendfile) {
			if (file_size > 0)
			{
				curpos = lseek (filefd, 0, SEEK_CUR);
				if (curpos >= 0) {
					filesize = file_size - curpos;
				}
			} else { curpos = 0; filesize = 0; }
			off_t offset = curpos;

			{
				int val = 1;
				setsockopt(netfd, IPPROTO_TCP, TCP_CORK, &val, sizeof(val));
			}

			while(filesize > 0) {
				ssize_t size = sendfile(netfd, filefd,  &offset, (size_t)filesize);
				if (size < 0) {
					int sav_errno = errno;
					switch (sav_errno) {
					case EAGAIN:
						break;

					case EINVAL:
					case ENOSYS:
						goto fallback; // use read/write-loop instead

					case EIO:
					case ENOMEM:
						goto file_err;
					default:
						goto data_err;
					}
				} else {
					filesize -= size;
				}
			} // while
		} else {
fallback:
			buf = malloc ((u_int)blksize);
			if (buf == NULL)
			{
				transflag = 0;
				perror_reply (451, "Local resource failure: malloc");
				return;
			}
			{
				int val = 1;
				setsockopt(netfd, IPPROTO_TCP, TCP_CORK, &val, sizeof(val));
			}
#if 0 // TEST DUMMY DATA
			while (byte_count < file_size) {
				cnt = file_size - byte_count; if (cnt > blksize) cnt = blksize;
				if (write(netfd, buf, cnt) != cnt) break;
#else
			while ((cnt = read (filefd, buf, (u_int)blksize)) > 0 &&
			       write(netfd, buf, cnt) == cnt) {
#endif
				byte_count += cnt;
				// loops++;
			}
			transflag = 0;
			(void)free (buf);
			if (cnt != 0)
			{
				if (cnt < 0)
					goto file_err;
				goto data_err;
			}
		} // opt_use_sendfile

		reply (226, "Transfer complete.");
		return;
	default:
		transflag = 0;
		reply (550, "Unimplemented TYPE %d in send_data", type);
		return;
	}

data_err:
	transflag = 0;
	perror_reply (426, "Data connection");
	return;

file_err:
	transflag = 0;
	perror_reply (551, "Error on input file");
}

static void
send_data_ls_filter(FILE *instr, FILE *outstr, off_t blksize, int is_long, int is_recursive)
{
	int c, cnt, filefd, netfd;
	char *buf, *bp;
	off_t curpos;
	size_t len, filesize;

	transflag++;
	if (setjmp (urgcatch))
	{
		transflag = 0;
		return;
	}

	netfd = fileno (outstr);
	filefd = fileno (instr);

	if (type != TYPE_I && type != TYPE_L && type != TYPE_A) {
		transflag = 0;
		reply (550, "Unimplemented TYPE %d", type);
		return;
	}

	char skip_dir = 0;
	char expect_dirline = 0;

	char line[2048];
	char current_dir[2048]; // for not skipped is_recursive

	if (is_recursive) expect_dirline = 1;

	int n = 0;
	current_dir[0] = '\0';
	while ((c = getc (instr)) != EOF) {
		if (ferror (outstr)) goto data_err;
		if (c != '\n') {
			if (n < sizeof(line)-1) line[n++] = c;
		} else {
			// komplette zeile
			line[n] = '\0';
			int skip = 0;
			if (0 == n  && is_recursive) {
				expect_dirline = 1;
			} else if (expect_dirline && line[n-1] == ':') { // assert n != 0
				// scan dirline and set skip_dir accordingly
				char *real;
				line[n-1] = '\0';

				if (!IsReadAccess(line, &real)) skip_dir = 1;
				else {
					strcpy(current_dir, line);
					skip_dir = 0;
				}
				expect_dirline = 1;
				line[n-1] = ':';
			} else if (!skip_dir) {
				// scan line and set skip accordingly
				char *real;
				char *p = line;
				if (is_long) {
					// the filename is the 9th column
					int col = 1;
					while(col != 9 && *p != '\0') {
						while(*p != ' ' && *p != '\t' &&  *p != '\0') p++;
						if (*p == '\0') break;
						while(*p == ' ' || *p == '\t') p++;
						if (*p == '\0') break;
						col++;
					}
					Log(("long listing col=%d p=%s", col, p));
				}
				if (is_recursive) {
					// check with current_dir
					int len = strlen(current_dir);
					if (len && current_dir[len-1] != '/' && len < sizeof(current_dir)-1) {
						current_dir[len++] = '/';
						current_dir[len] = '\0';
					}
					if (sizeof(current_dir) > len+1) strncat(current_dir, p, sizeof(current_dir)-len-1);
					if (!IsReadAccess(current_dir, &real)) skip = 1;
					current_dir[len] = 0;
				} else {
					if (!IsReadAccess(p, &real)) skip = 1;
				}
			}
			if (!skip_dir && !skip) {
				// line ausgeben
				fputs(line, outstr);
				byte_count += n;
				if (type == TYPE_A) { (void) putc ('\r', outstr); byte_count++; }
				(void) putc ('\n', outstr);
				byte_count++;
			}
			n = 0;
		}
	} // while

	fflush (outstr);
	transflag = 0;
	if (ferror (instr)) goto file_err;
	if (ferror (outstr)) goto data_err;
	reply (226, "Transfer complete.");
	return;

data_err:
	transflag = 0;
	perror_reply (426, "Data connection");
	return;

file_err:
	transflag = 0;
	perror_reply (551, "Error on input file");
}


/* Transfer data from peer to "outstr" using the appropriate encapulation of
   the data subject to Mode, Structure, and Type.

   N.B.: Form isn't handled.  */
static int
receive_data (FILE *instr, FILE *outstr)
{
	int c;
	int cnt, bare_lfs = 0;
	char buf[32768];

	transflag++;
	if (setjmp (urgcatch))
	{
		transflag = 0;
		return -1;
	}
	switch (type)
	{
	case TYPE_I:
	case TYPE_L:
		while ((cnt = read (fileno(instr), buf, sizeof(buf))) > 0)
		{
			if (write (fileno (outstr), buf, cnt) != cnt)
				goto file_err;
			byte_count += cnt;
		}
		if (cnt < 0)
			goto data_err;
		transflag = 0;
		return 0;

	case TYPE_E:
		reply (553, "TYPE E not implemented.");
		transflag = 0;
		return -1;

	case TYPE_A:
		while ((c = getc (instr)) != EOF)
		{
			byte_count++;
			if (c == '\n')
				bare_lfs++;
			while (c == '\r')
			{
				if (ferror (outstr))
					goto data_err;
				c = getc (instr);
				if (c != '\n')
				{
					(void) putc ('\r', outstr);
					if (c == '\0' || c == EOF)
						goto contin2;
				}
			}
			(void) putc (c, outstr);
contin2:        ;
		}
		fflush(outstr);
		if (ferror (instr))
			goto data_err;
		if (ferror (outstr))
			goto file_err;
		transflag = 0;
		if (bare_lfs)
		{
			lreply (226, "WARNING! %d bare linefeeds received in ASCII mode",
			        bare_lfs);
			(void)printf ("   File may not have transferred correctly.\r\n");
		}
		return (0);
	default:
		reply (550, "Unimplemented TYPE %d in receive_data", type);
		transflag = 0;
		return -1;
	}

data_err:
	transflag = 0;
	perror_reply (426, "Data Connection");
	return -1;

file_err:
	transflag = 0;
	perror_reply (452, "Error writing file");
	return -1;
}

void
sizecmd(const char *filename)
{
	switch (type) {
	case TYPE_L:
	case TYPE_I: {
		struct stat stbuf;
		if (acl_stat(filename, &stbuf) < 0 || !S_ISREG(stbuf.st_mode))
			reply(550, "%s: not a plain file.", filename);
		else
			reply(213,
			      (sizeof (stbuf.st_size) > sizeof(long)
			       ? "%qu" : "%lu"), stbuf.st_size);
		break;
	}
	case TYPE_A: {
		FILE *fin;
		int c;
		off_t count;
		struct stat stbuf;
		fin = acl_fopen(filename, "r");
		if (fin == NULL) {
			perror_reply(550, filename);
			return;
		}
		if (fstat(fileno(fin), &stbuf) < 0 || !S_ISREG(stbuf.st_mode)) {
			reply(550, "%s: not a plain file.", filename);
			(void) fclose(fin);
			return;
		}

		count = 0;
		while((c=getc(fin)) != EOF) {
			if (c == '\n')  /* will get expanded to \r\n */
				count++;
			count++;
		}
		(void) fclose(fin);

		reply(213, sizeof(count) > sizeof(long) ? "%qd" : "%ld",
		      count);
		break;
	}
	default:
		reply(504, "SIZE not implemented for Type %c.", "?AEIL"[type]);
	}
}

void
statfilecmd (const char *filename)
{
	FILE *fin;
	int c;
	char line[LINE_MAX];

	char *real_name;

	if (!IsReadAccess(filename, &real_name)) {
		perror_reply (550, filename);
		return;
	}

	(void)snprintf (line, sizeof(line), "/bin/ls -lA %s", real_name);
	fin = ftpd_popen (line, "r");
	lreply (211, "status of %s:", filename);
	while ((c = getc (fin)) != EOF)
	{
// TODO -- Filter ls result
		if (c == '\n')
		{
			if (ferror (stdout))
			{
				perror_reply (421, "control connection");
				(void) ftpd_pclose (fin);
				dologout (1);
				/* NOTREACHED */
			}
			if (ferror (fin))
			{
				perror_reply (551, filename);
				(void) ftpd_pclose (fin);
				return;
			}
			(void) putc ('\r', stdout);
		}
		(void) putc (c, stdout);
	}
	(void) ftpd_pclose (fin);
	reply (211, "End of Status");
}

#include "route_tool.h"

void
statcmd (void)
{
	struct sockaddr_storage *sin = 0;

	lreply (211, "%s FTP server status:", hostname);
	if (!no_version)
		printf ("     ftpd (%s) %s\r\n",
		        PACKAGE_NAME, PACKAGE_VERSION);
	printf ("     Connected to %s", cred.remotehost);
	if (!isdigit (cred.remotehost[0]))
		printf (" (%s)", sockaddr_addr2string(&his_addr));
	printf ("\r\n");

	if (cred.logged_in)
	{
		if (cred.guest)
			printf ("     Logged in anonymously\r\n");
		else
			printf ("     Logged in as %s\r\n", cred.name);
	}
	else if (askpasswd)
		printf ("     Waiting for password\r\n");
	else
		printf ("     Waiting for user name\r\n");
	printf ("     TYPE: %s", typenames[type]);
	if (type == TYPE_A || type == TYPE_E)
		printf (", FORM: %s", formnames[form]);
	if (type == TYPE_L)
#ifdef CHAR_BIT
		printf (" %d", CHAR_BIT);
#else
#if NBBY == 8
		printf (" %d", NBBY);
#else
		printf (" %d", bytesize); /* need definition! */
#endif
#endif
	printf ("; STRUcture: %s; transfer MODE: %s\r\n",
	        strunames[stru], modenames[stru_mode]);
	if (data != -1)
		printf ("     Data connection open\r\n");
	else if (pdata != -1)
	{
		printf ("     in Passive mode");
		sin = &pasv_addr;
	}
	else if (usedefault == 0)
	{
		printf ("     PORT");
		sin = &data_dest;
	}
	if (sin) {
		unsigned short port = sockaddr_port(sin);
		int fam;
		unsigned char ipv4[4];
		GetFamilyAndIPv4(sin, &fam, ipv4);
		switch (fam) {
		case AF_INET:
			printf (" (%u,%u,%u,%u,%u,%u)\r\n", 
				ipv4[0],ipv4[1],ipv4[2],ipv4[3], (port >> 8) & 0xff, port & 0xff);
			break;
#ifdef USE_IPV6
		case AF_INET6:
			// not sure if this is the expeceted format for ipv6 address
			printf (" |2|%s|%u|\r\n", sockaddr_addr2string(sin), port);
			break;
#endif
		}
	}
	else
		printf ("     No data connection\r\n");
	reply (211, "End of status");
}

void
fatal (const char *s)
{
	reply (451, "Error in server: %s\n", s);
	reply (221, "Closing connection due to server error.");
	dologout (0);
	/* NOTREACHED */
}

void
reply (int n, const char *fmt, ...)
{
	va_list ap;
#if defined (HAVE_STDARG_H) && defined (__STDC__) && __STDC__
	va_start (ap, fmt);
#else
	va_start (ap);
#endif
	(void)printf ("%d ", n);
	(void)vprintf (fmt, ap);
	(void)printf ("\r\n");
	(void)fflush (stdout);
	if (debug)
	{
		syslog (LOG_DEBUG, "<--- %d ", n);
		vsyslog (LOG_DEBUG, fmt, ap);
	}
}

void
lreply (int n, const char *fmt, ...)
{
	va_list ap;
#if defined (HAVE_STDARG_H) && defined (__STDC__) && __STDC__
	va_start (ap, fmt);
#else
	va_start (ap);
#endif
	(void)printf ("%d- ", n);
	(void)vprintf (fmt, ap);
	(void)printf ("\r\n");
	(void)fflush (stdout);
	if (debug)
	{
		syslog (LOG_DEBUG, "<--- %d- ", n);
		vsyslog (LOG_DEBUG, fmt, ap);
	}
}

static void
ack (const char *s)
{
	reply (250, "%s command successful.", s);
}

void
nack (const char *s)
{
	reply (502, "%s command not implemented.", s);
}

void
delete (const char *name)
{
	struct stat st;

	LOGCMD ("delete", name);

	if (acl_stat(name, &st) < 0)
	{
		perror_reply (550, name);
		return;
	}
	if (S_ISDIR (st.st_mode))
	{
		if (acl_rmdir(name) < 0)
		{
			perror_reply (550, name);
			return;
		}
		goto done;
	}
	if (acl_unlink(name) < 0)
	{
		perror_reply (550, name);
		return;
	}
done:
	ack ("DELE");
}

void
cwd (const char *path)
{
	if (acl_chdir(path) < 0)
		perror_reply (550, path);
	else
		ack ("CWD");
}

void
makedir (const char *name)
{
	extern char *xgetcwd __P ((void));

	LOGCMD ("mkdir", name);
	if (acl_mkdir (name, 0777) < 0) {
		perror_reply (550, name);
		return;
	}

	reply (257, "\"%s\" new directory created.", name);
}

void
removedir (const char *name)
{
	LOGCMD ("rmdir", name);

	if (acl_rmdir (name) < 0)
		perror_reply (550, name);
	else
		ack("RMD");
}

void
pwd (void)
{
	extern char *xgetcwd __P ((void));
	char *path = xgetcwd ();
	if (path)
	{
		char *virt_path = path;
		real_to_virtual_path(&virt_path);
		reply (257, "\"%s\" is current directory.", virt_path);
		free(path);
	}
	else
		reply (550, "%s.", strerror (errno));
}

char *
renamefrom (const char *name)
{
	struct stat st;

	if (acl_stat(name, &st) < 0)
	{
		perror_reply (550, name);
		return ((char *)0);
	}
	reply (350, "File exists, ready for destination name");
	return (char *)(name);
}

void
renamecmd (const char *from, const char *to)
{
	LOGCMD2 ("rename", from, to);

	if (acl_rename (from, to) < 0)
		perror_reply (550, "rename");
	else
		ack ("RNTO");
}


static void
dolog (struct sockaddr_storage *sin, struct credentials *pcred)
{
	const char *name;
#if 1 /* FRITZBOX */
	struct hostent *hp = 0;
#else
	struct hostent *hp = gethostbyaddr ((char *)&sin->sin_addr,
	                                    sizeof (struct in_addr), AF_INET);
#endif

	if (hp)
		name = hp->h_name;
	else
		name = sockaddr_addr2string(sin);

	if (pcred->remotehost)
		free (pcred->remotehost);
	pcred->remotehost = sgetsave (name);

#ifdef HAVE_SETPROCTITLE
	snprintf (proctitle, sizeof (proctitle), "%s: connected", pcred->remotehost);
	setproctitle ("%s",proctitle);
#endif /* HAVE_SETPROCTITLE */

	if (logging)
		syslog (LOG_INFO, "connection from %s", pcred->remotehost);
}

/*  Record logout in wtmp file
    and exit with supplied status.  */
void
dologout (int status)
{
	/* Racing condition with SIGURG: If SIGURG is receive
	   here, it will jump back has root in the main loop
	   David Greenman:dg@root.com.  */
	transflag = 0;

	if (cred.logged_in)
	{
		(void) seteuid ((uid_t)0);
		logwtmp_keep_open (ttyline, "", "");
	}
	/* beware of flushing buffers after a SIGPIPE */
	_exit (status);
}

static void
myoob (int signo)
{
	char *cp;

	(void)signo;
	/* only process if transfer occurring */
	if (!transflag)
		return;
	cp = tmpline;
	if (telnet_fgets (cp, 7, stdin) == NULL)
	{
		reply (221, "You could at least say goodbye.");
		dologout (0);
	}
	upper (cp);
	if (strcmp (cp, "ABOR\r\n") == 0)
	{
		tmpline[0] = '\0';
		reply (426, "Transfer aborted. Data connection closed.");
		reply (226, "Abort successful");
		longjmp (urgcatch, 1);
	}
	if (strcmp (cp, "STAT\r\n") == 0)
	{
		if (file_size != (off_t) -1)
			reply (213, "Status: %s of %s bytes transferred",
			       off_to_str (byte_count), off_to_str (file_size));
		else
			reply (213, "Status: %s bytes transferred",
			       off_to_str (byte_count));
	}
}


static struct sockaddr_storage *numericaladdr2sockaddr_storage(const int family, const char *addr)
{
	struct addrinfo hints, *res;
	int ret;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = family;
	hints.ai_flags = AI_NUMERICHOST;
	ret = getaddrinfo(addr, 0, &hints, &res);
	if (ret != 0) {
Log(("getaddrinfo of addr='%s' failed fam=%d errno=%d", addr, family, errno));
		return 0;
	}

	switch(res->ai_family) {
	case AF_INET:
		if (res->ai_addrlen != sizeof(struct sockaddr_in)) {
Log(("wrong size of ipv4 addr"));
			return 0;
		}
		break;
#ifdef USE_IPV6
	case AF_INET6:
		if (res->ai_addrlen != sizeof(struct sockaddr_in6)) {
Log(("wrong size of ipv6 addr"));
			return 0;
		}
		break;
#endif
	default:
		return 0;
	};

	// take first result
	struct sockaddr_storage *ss = (struct sockaddr_storage *)calloc(sizeof(struct sockaddr_storage), 1);
	if (!ss) return 0;
	switch(res->ai_family) {
	case AF_INET:
		memcpy(ss, res->ai_addr, res->ai_addrlen);
		break;
#ifdef USE_IPV6
	case AF_INET6:
		memcpy(ss, res->ai_addr, res->ai_addrlen);
		break;
#endif
	}
	return ss;
}


/* param from RFC 2428  e.g.:
 *   |1|132.235.1.2|6275|
 *   |2|1080::8:800:200C:417A|5282|
 */
void extended_port(char *param)
{
	struct sockaddr_storage *ss = 0;

	char delim = *param++;
	if (delim < 33 || delim > 126) goto err;
	char net_prot = *param++;
	if (net_prot != '1' && net_prot != '2') goto err_prot;
	int family = AF_INET;
#ifdef USE_IPV6
	if (net_prot == '2') family = AF_INET6;
#else
	if (net_prot == '2')  goto err_prot;
#endif
	if (*param++ != delim) goto err;
	char *addr_string = param;
	while(*param != '\0' && *param != delim) param++;
	if (*param != delim) goto err;
	*param++ = '\0';
	char *port_string = param;
	while(*param != '\0' && *param != delim) param++;
	if (*param != delim) goto err;
	*param++ = '\0';
	if (*param != '\0') goto err;

	ss = numericaladdr2sockaddr_storage(family, addr_string);
	if (!ss) goto err;

	unsigned port;
	if (1 != sscanf(port_string, "%u", &port)) goto err;
Log(("port_string='%s' port=%u, IPPORT_RESERVED=%u", port_string, port, IPPORT_RESERVED));
	if (port > 0xffff || port <= IPPORT_RESERVED) {
Log(("port %u is illegal", port));
		goto illegal;
	}

	// check for same dest ip address of data and control connection
	
	if (!is_same_addr(&his_addr, ss)) {
Log(("EPRT his_addr and req addr do not match"));
		goto illegal;
	}

	memcpy(&data_dest, ss, sizeof(data_dest));
	// data_dest.ss_family = family;
	switch(family) {
	case AF_INET:  ((struct sockaddr_in *)&data_dest)->sin_port = htons(port);
#ifdef USE_IPV6
	case AF_INET6: ((struct sockaddr_in6 *)&data_dest)->sin6_port = htons(port);
#endif
	}
	free(ss);
	reply (200, "EPRT command sucessful.");
	return;

illegal:
	reply(500, "Illegal EPRT command");
	return;	

err:
	if (ss) free(ss);
	reply (501, "Illegal extended address");
	return;

err_prot:
	if (ss) free(ss);
#ifdef USE_IPV6
	reply(522, "Network protocol not supported, use (1,2)");
#else
	reply(522, "Network protocol not supported, use (1)");
#endif
	return;
}

void passive_ex(int extended, int family)
{
	int len;

	if (pdata >= 0) {
		/* AVM Nessus Check (Nessus ID 10085)*/
		/* wg. Zugriff von Firefox 2.0 aus dem Internet d�rfen wir hier kein 'goto done;' machen */
		/* (Firefox 2.0 macht mehrfaches PASV) */
		(void) close(pdata); pdata = -1;
	}

#ifdef USE_IPV6
	if (family == AF_INET6 && !extended) {
		perror_reply (425, "Can't open passive connection");
Log(("passive_ex extended=%d family=%d: not possible", extended, family));
		return;
	}
#endif

	int fam;
	unsigned char ipv4[4];
	GetFamilyAndIPv4(&ctrl_addr, &fam, ipv4);
	if (family != fam) {
		perror_reply (425, "Can't open passive connection");
Log(("passive_ex extended=%d family=%d fam=%d: not possible", extended, family, fam));
		return;
	}

	pdata = socket (family, SOCK_STREAM, 0);
	if (pdata < 0)
	{
		int err = errno;
		perror_reply (425, "Can't open passive connection");
Log(("passive_ex extended=%d family=%d sizeof(pasv_addr)=%u : socket failed errno=%d", extended, family, sizeof(pasv_addr), err));
		return;
	}
	memset(&pasv_addr, 0, sizeof(pasv_addr));
	pasv_addr.ss_family = family;
	switch(pasv_addr.ss_family) {
	case AF_INET: memcpy(&((struct sockaddr_in *)&pasv_addr)->sin_addr, ipv4, 4);
		break;
#ifdef USE_IPV6
	case AF_INET6: memcpy(&((struct sockaddr_in6 *)&pasv_addr)->sin6_addr, 
			&((struct sockaddr_in6 *)&ctrl_addr)->sin6_addr, 
			sizeof(((struct sockaddr_in6 *)&pasv_addr)->sin6_addr)); 
		break;
#endif
	}
	(void) seteuid ((uid_t)0);
	if (bind (pdata, (struct sockaddr *)&pasv_addr, sizeof (pasv_addr)) < 0)
	{
		int err = errno;
		(void) seteuid ((uid_t)cred.uid);
Log(("passive_ex extended=%d family=%d sizeof(pasv_addr)=%u : bind failed errno=%d", extended, family, sizeof(pasv_addr), err));
		goto pasv_error;
	}
	(void) seteuid ((uid_t)cred.uid);
	len = sizeof(pasv_addr);
	if (getsockname (pdata, (struct sockaddr *) &pasv_addr, &len) < 0) {
		int err = errno;
Log(("passive_ex extended=%d family=%d sizeof(pasv_addr)=%u : getsockname failed errno=%d", extended, family, sizeof(pasv_addr), err));
		goto pasv_error;
	}
	if (listen (pdata, 1) < 0) {
		int err = errno;
Log(("passive_ex extended=%d family=%d sizeof(pasv_addr)=%u : listen failed errno=%d", extended, family, sizeof(pasv_addr), err));
		goto pasv_error;
	}

done:
	if (extended) {
		reply (229, "Entering Extended Passive Mode (|||%u|)", sockaddr_port(&pasv_addr));
	} else {
		char *p, *a;
		// assert (pasv_addr.ss_family == AF_INET);
		a = (char *) &((struct sockaddr_in *)&pasv_addr)->sin_addr;
		p = (char *) &((struct sockaddr_in *)&pasv_addr)->sin_port;
#define UC(b) (((int) b) & 0xff)
		reply (227, "Entering Passive Mode (%d,%d,%d,%d,%d,%d)", UC(a[0]),
			   UC(a[1]), UC(a[2]), UC(a[3]), UC(p[0]), UC(p[1]));
	}
	return;

pasv_error:
	(void) close (pdata);
	pdata = -1;
	perror_reply (425, "Can't open passive connection");
	return;
}


/* 
 * net port 0: auto
 *          1: IPv4
 *          2: IPv6
 */
void extended_passive(unsigned net_prot)
{
	int family;
	switch(net_prot) {
	case 0:
		family = ctrl_addr.ss_family;
		break;
	case 1:
		family = AF_INET;
		break;

	case 2:
#ifdef USE_IPV6
		family = AF_INET6;
#else
		goto err;
#endif
		break;

	default:
		goto err;
	}
	passive_ex(1, family);
	return;

err:
	reply (425, "Can't open passive connection");
}

/* Note: a response of 425 is not mentioned as a possible response to
   the PASV command in RFC959. However, it has been blessed as
   a legitimate response by Jon Postel in a telephone conversation
   with Rick Adams on 25 Jan 89.  */
void
passive (void)
{
	passive_ex(0, AF_INET);
}

/* Generate unique name for file with basename "local".
   The file named "local" is already known to exist.
   Generates failure reply on error.  */
static char *
gunique (const char *local, int *pcount)
{
	static char *string = 0;
	struct stat st;
	int count;
	char *cp;

	cp = strrchr (local, '/');
	if (cp)
		*cp = '\0';
	if (acl_stat(cp ? local : ".", &st) < 0)
	{
		perror_reply (553, cp ? local : ".");
		return ((char *) 0);
	}
	if (cp)
		*cp = '/';

	if (string)
		free (string);

	string = malloc (strlen (local) + 5); /* '.' + DIG + DIG + '\0' */
	if (string)
	{
		strcpy (string, local);
		cp = string + strlen (string);
		*cp++ = '.';
		for (count = 1; count < 100; count++)
		{
			(void)sprintf (cp, "%d", count);
			if (stat (string, &st) < 0) {
				if (pcount) *pcount = count;
				return string;
			}
		}
	}

	reply (452, "Unique file name cannot be created.");
	return NULL;
}

/*
 * Format and send reply containing system error number.
 */
void
perror_reply (int code, const char *string)
{
	reply (code, "%s: %s.", string, strerror (errno));
}

static char *onefile[] = {
	"",
	0
};

void
send_file_list (const char *whichf)
{
	struct stat st;
	DIR *dirp = NULL;
	struct dirent *dir;
	FILE *dout = NULL;
	char **dirlist, *dirname;
	int simple = 0;
	int freeglob = 0;
	glob_t gl;
	char *p = NULL;

	if (strpbrk(whichf, "~{[*?") != NULL)
	{
		int flags = GLOB_NOCHECK;

#ifdef GLOB_BRACE
		flags |= GLOB_BRACE;
#endif
#ifdef GLOB_QUOTE
		flags |= GLOB_QUOTE;
#endif
#ifdef GLOB_TILDE
		flags |= GLOB_TILDE;
#endif

		memset (&gl, 0, sizeof (gl));
		freeglob = 1;
		if (glob (whichf, flags, 0, &gl))
		{
			reply (550, "not found");
			goto out;
		}
		else if (gl.gl_pathc == 0)
		{
			errno = ENOENT;
			perror_reply (550, whichf);
			goto out;
		}
		dirlist = gl.gl_pathv;
	}
	else
	{
		p = strdup (whichf);
		onefile[0] = p;
		dirlist = onefile;
		simple = 1;
	}

	if (setjmp (urgcatch))
	{
		transflag = 0;
		goto out;
	}
	while ((dirname = *dirlist++))
	{
		// char *real_dirname;

		/* If user typed "ls -l", etc, and the client
		   used NLST, do what the user meant.  */
		if (dirname[0] == '-' && *dirlist == NULL
		    && transflag == 0)
		{

			retrieve ("/bin/ls %s", dirname); /*dirname is option, no file */
			goto out;
		}

//	real_dirname = make_real_path(dirname);
//	if (!real_dirname) continue;

		if (acl_stat (dirname, &st) < 0)
		{
			perror_reply (550, whichf);
			if (dout != NULL)
			{
				(void) fclose (dout);
				transflag = 0;
				data = -1;
				pdata = -1;
			}
			goto out;
		}

		if (S_ISREG(st.st_mode))
		{
			if (dout == NULL)
			{
				dout = dataconn ("file list", (off_t)-1, "w");
				if (dout == NULL)
					goto out;
				transflag++;
			}
			fprintf (dout, "%s%s\n", dirname, type == TYPE_A ? "\r" : "");
			byte_count += strlen (dirname) + 1;
			continue;
		}
		else if (!S_ISDIR (st.st_mode))
			continue;


		dirp = acl_opendir(dirname);
		if (dirp == NULL)
			continue;

		while ((dir = readdir (dirp)) != NULL) {
			char *virt_nbuf;
			// char *real_nbuf;

			if (dir->d_name[0] == '.' && dir->d_name[1] == '\0')
				continue;
			if (dir->d_name[0] == '.' && dir->d_name[1] == '.' &&
			    dir->d_name[2] == '\0')
				continue;

			virt_nbuf = (char *) alloca (strlen (dirname) + 1 +
			                             strlen (dir->d_name) + 1);
			if (!strlen(dirname) || !strcmp(dirname,"/")) {
				sprintf (virt_nbuf, "%s%s", dirname, dir->d_name);
			} else {
				sprintf (virt_nbuf, "%s/%s", dirname, dir->d_name);
			}
			//  real_nbuf = (char *) alloca (strlen (real_dirname) + 1 +
			//			  strlen (dir->d_name) + 1);
			//  sprintf (real_nbuf, "%s/%s", real_dirname, dir->d_name);

			/* We have to do a stat to insure it's
			   not a directory or special file.  */
			if (simple || (acl_stat (virt_nbuf, &st) == 0
			               && S_ISREG(st.st_mode)))
			{
				if (dout == NULL) {
					dout = dataconn ("file list", (off_t)-1, "w");
					if (dout == NULL)
						goto out;
					transflag++;
				}
				char *real;
				if (IsReadAccess(virt_nbuf, &real)) {
					if (virt_nbuf[0] == '.' && virt_nbuf[1] == '/')
						fprintf (dout, "%s%s\n", &virt_nbuf[2], type == TYPE_A ? "\r" : "");
					else
						fprintf (dout, "%s%s\n", virt_nbuf, type == TYPE_A ? "\r" : "");
					byte_count += strlen (virt_nbuf) + 1;
				}
			}
		}
		(void) closedir (dirp);
	}

	if (dout == NULL)
		reply (550, "No files found.");
	else if (ferror (dout) != 0)
		perror_reply (550, "Data connection");
	else
		reply (226, "Transfer complete.");

	transflag = 0;
	if (dout != NULL)
		(void) fclose (dout);
	data = -1;
	pdata = -1;
out:
	if (p)
		free (p);
	if (freeglob)
	{
		freeglob = 0;
		globfree (&gl);
	}
}



