/*
 * linux/arch/arm/mach-davinci/board-evm.c
 *
 * TI DaVinci EVM board
 *
 * Copyright (C) 2006 Texas Instruments.
 *
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ----------------------------------------------------------------------------
 *
 */

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/root_dev.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <linux/env.h>

#include <asm/setup.h>
#include <asm/io.h>
#include <asm/mach-types.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>

#include <asm/arch/irqs.h>
#include <asm/arch/common.h>
#include <asm/arch/hardware.h>
#include <asm/arch/system_module.h>
#include <asm/arch/remote_keys.h>
#include <asm/arch/hw_control.h>
#include "clock.h"

#if defined(CONFIG_MTD_PHYSMAP) || defined(CONFIG_MTD_PHYSMAP_MODULE)
#define DO_MTD
#endif /*--- #if defined(CONFIG_MTD_PHYSMAP) || defined(CONFIG_MTD_PHYSMAP_MODULE) ---*/

extern struct platform_device davinci_evm_flash_device;
extern struct platform_device davinci_evm_ram_device;

#if defined(CONFIG_FB_DAVINCI) || defined(CONFIG_FB_DAVINCI_MODULE)

static u64 davinci_fb_dma_mask = DMA_32BIT_MASK;

static struct platform_device davinci_fb_device = {
	.name		= "davincifb",
	.id		= -1,
	.dev = {
		.dma_mask		    = &davinci_fb_dma_mask,
		.coherent_dma_mask  = DMA_32BIT_MASK,
	},
	.num_resources = 0,
};
#endif

/*
 * USB
 */
#if defined(CONFIG_USB_MUSB_HDRC) || defined(CONFIG_USB_MUSB_HDRC_MODULE)

#include <linux/usb/musb.h>

static struct musb_hdrc_platform_data usb_data = {
#if     defined(CONFIG_USB_MUSB_OTG)
	/* OTG requires a Mini-AB connector */
	.mode           = MUSB_OTG,
#elif   defined(CONFIG_USB_MUSB_PERIPHERAL)
	.mode           = MUSB_PERIPHERAL,
#elif   defined(CONFIG_USB_MUSB_HOST)
	.mode           = MUSB_HOST,
#endif
	/* irlml6401 switches 5V */
	.power          = 250,          /* sustains 3.0+ Amps (!) */
	.potpgt         = 4,            /* ~8 msec */

	/* REVISIT multipoint is a _chip_ capability; not board specific */
	.multipoint     = 1,
};

static struct resource usb_resources [] = {
	{
		/* physical address */
		.start          = DAVINCI_USB_OTG_BASE,
		.end            = DAVINCI_USB_OTG_BASE + 0x5ff,
		.flags          = IORESOURCE_MEM,
	},
	{
		.start          = IRQ_USBINT,
		.flags          = IORESOURCE_IRQ,
	},
};

static u64 usb_dmamask = DMA_32BIT_MASK;

static struct platform_device usb_dev = {
	.name           = "musb_hdrc",
	.id             = -1,
	.dev = {
		.platform_data		= &usb_data,
		.dma_mask		= &usb_dmamask,
		.coherent_dma_mask      = DMA_32BIT_MASK,
        },
	.resource       = usb_resources,
	.num_resources  = ARRAY_SIZE(usb_resources),
};

static inline void setup_usb(void)
{
	/* REVISIT:  everything except platform_data setup should be
	* shared between all DaVinci boards using the same core.
	*/
	int status;

	status = platform_device_register(&usb_dev);
	if (status != 0)
		pr_debug("setup_usb --> %d\n", status);
	else
		board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_USB, 1);
}

#else
#define setup_usb(void)	do {} while(0)
#endif  /* CONFIG_USB_MUSB_HDRC */


#ifdef	CONFIG_RTC_DRV_DAVINCI_EVM
#define DO_RTC
#endif
#ifdef	CONFIG_RTC_DRV_DAVINCI_EVM_MODULE
#define DO_RTC
#endif

#ifdef	DO_RTC
static struct platform_device rtc_dev = {
	.name           = "rtc_davinci_evm",
	.id             = -1,
};
#endif


static struct platform_device *davinci_evm_devices[] __initdata = {
#ifdef	DO_MTD
	&davinci_evm_flash_device,
    &davinci_evm_ram_device,
#endif
#if defined(CONFIG_FB_DAVINCI) || defined(CONFIG_FB_DAVINCI_MODULE)
	&davinci_fb_device,
#endif
#ifdef	DO_RTC
	&rtc_dev,
#endif
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int power_off_in_progress = 0;
void davinci_key_to_power_control(int keycode) {
    struct system_regs *sys = (struct system_regs *)IO_ADDRESS(DAVINCI_SYSTEM_MODULE_BASE);
    if(power_off_in_progress == 0)
        return;
    switch(keycode & 0xFF) {
        case remote_key_power:
#if defined(CONFIG_AVM_WATCHDOG)
            printk("[davinci_msp_callback] reboot !\n");
            davinci_watchdog_reset();
            break;
#endif /*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/
#if 0
        case remote_key_1:
	        board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_I2C, 0);
            goto _default;
        case remote_key_2:
	        board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VLYNQ, 0);
            goto _default;
        case remote_key_3:
	        board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_UHPI, 0);
            goto _default;
        case remote_key_4:
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_AEMIF, 0);
            goto _default;
        case remote_key_5:
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_EMAC_WRAPPER, 0);
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_EMAC, 0);
            goto _default;
        case remote_key_6:
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VPSSMSTR, 0);
            goto _default;
        case remote_key_7:
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VPSSSLV, 0);
            goto _default;
        case remote_key_8:
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_PWM0, 0);
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_PWM1, 0);
            board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_PWM2, 0);
            goto _default;
        case remote_key_9:
            sys->vdd3p3v_pwdn = 3;
            goto _default;
        case remote_key_0:
	        board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_DDR_EMIF, 0);
            goto _default;
        case remote_key_OK:
	        board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_GPIO, 0);
            goto _default;
#endif
        default:
_default:
            printk("[davinci_msp_callback] receive key 0x%x\n", keycode);
    }
    return;
}

EXPORT_SYMBOL(davinci_key_to_power_control);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void davinci_power_off(void) {
    struct system_regs *sys = (struct system_regs *)IO_ADDRESS(DAVINCI_SYSTEM_MODULE_BASE);
    int davinci_clock_control(unsigned int pll_index, unsigned int output_index, unsigned int frequency);
    void davinci_video_power_off(void);
    unsigned int i = 0;
    printk("[davinci_power_off] \n");
	board_setup_psc(DAVINCI_GPSC_DSPDOMAIN, DAVINCI_LPSC_GEM, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_GEM\n"); ---*/
	board_setup_psc(DAVINCI_GPSC_DSPDOMAIN, DAVINCI_LPSC_IMCOP, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_IMCOP\n"); ---*/
    davinci_clock_control(0, 0, 27000000);
    /*--- printk("[davinci_power_off] PLLI1 ==> 27.000.000 Hz\n"); ---*/
    /*--- davinci_clock_control(1, 0, 27000000); ---*/
    /*--- printk("[davinci_power_off] PLLI2 ==> 27.000.000 Hz\n"); ---*/
    davinci_video_power_off();
    /*--- printk("[davinci_power_off] disable video\n"); ---*/
    gpio_control_hw(enable_5Volt, 0);
    gpio_control_hw(audio_reset, 1);
    gpio_control_hw(non_wlan_reset, 0);
    gpio_control_hw(ephy_reset, 1);
    gpio_control_hw(scart_sig_on, 0);
    gpio_control_hw(ephy_power_down, 1);
    gpio_control_hw(scart_signal_rgb, 0);

#if 0
    sys->vdd3p3v_pwdn = 3;
    /*--- printk("[davinci_power_off] 3.3 Volt off\n"); ---*/
#endif

#if 0
    /*--- sys->vpss_clkctl  = 0; ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_I2C, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_I2C\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VLYNQ, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_VLYNQ\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_UHPI, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_UHPI\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_AEMIF, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_AEMIF\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_EMAC_WRAPPER, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_EMAC_WRAPPER\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_EMAC, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_EMAC\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VPSSMSTR, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_VPSSMSTR\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VPSSSLV, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_VPSSSLV\n"); ---*/

	/*--- board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_GPIO, 0); ---*/
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_GPIO\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_PWM0, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_PWM0\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_PWM1, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_PWM1\n"); ---*/

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_PWM2, 0);
    /*--- printk("[davinci_power_off] DAVINCI_LPSC_PWM2\n"); ---*/

	/*--- board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_DDR_EMIF, 0); ---*/
#endif

    power_off_in_progress = 1;
    while(1) {
#if defined(CONFIG_AVM_WATCHDOG)
        void ar7wdt_hw_trigger(void);
        if((i-- & 0xFFFF) == 0) {
            printk("s");
            ar7wdt_hw_trigger();
        }
#endif /*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/
        schedule();
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void board_init(void)
{
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VPSSMSTR, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_VPSSSLV, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_TPCC, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_TPTC0, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_TPTC1, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_GPIO, 1);

	/* Turn on WatchDog timer LPSC.  Needed for RESET to work */
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_TIMER0, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_TIMER1, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_TIMER2, 1);

	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_UART1, 1);
	board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_UART2, 1);

    pm_power_off = davinci_power_off;

}

static void __init
davinci_evm_map_io(void)
{
	davinci_map_common_io();

	/* Initialize the DaVinci EVM board settigs */
	board_init ();
}

static __init void davinci_evm_init(void)
{
#if defined(CONFIG_BLK_DEV_DAVINCI) || defined(CONFIG_BLK_DEV_DAVINCI_MODULE)
#ifdef	DO_MTD
	printk(KERN_WARNING "WARNING: both IDE and NOR flash are enabled, "
	       "but are pin-muxed.\n\t Disable IDE for NOR support.\n");
#endif
#endif

	platform_add_devices(davinci_evm_devices,
			     ARRAY_SIZE(davinci_evm_devices));

	davinci_serial_init();
	setup_usb();
    gpio_control_hw(enable_5Volt, 1);
}

static __init void davinci_evm_irq_init(void)
{
	davinci_init_common_hw();
	davinci_irq_init(NULL);
}

MACHINE_START(DAVINCI_EVM, "DaVinci EVM")
	/* Maintainer: MontaVista Software <source@mvista.com> */
	.phys_io      = IO_PHYS,
	.io_pg_offst  = (io_p2v(IO_PHYS) >> 18) & 0xfffc,
	.boot_params  = (DAVINCI_DDR_BASE + 0x100),
	.map_io	      = davinci_evm_map_io,
	.init_irq     = davinci_evm_irq_init,
	.timer	      = &davinci_timer,
	.init_machine = davinci_evm_init,
MACHINE_END
