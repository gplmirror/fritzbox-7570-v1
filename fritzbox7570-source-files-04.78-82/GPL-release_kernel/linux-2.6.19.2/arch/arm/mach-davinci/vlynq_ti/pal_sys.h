/*******************************************************************************
**+--------------------------------------------------------------------------+**
**|                            ****                                          |**
**|                            ****                                          |**
**|                            ******o***                                    |**
**|                      ********_///_****                                   |**
**|                      ***** /_//_/ ****                                   |**
**|                       ** ** (__/ ****                                    |**
**|                           *********                                      |**
**|                            ****                                          |**
**|                            ***                                           |**
**|                                                                          |**
**|         Copyright (c) 1998-2004 Texas Instruments Incorporated           |**
**|                        ALL RIGHTS RESERVED                               |**
**|                                                                          |**
**| Permission is hereby granted to licensees of Texas Instruments           |**
**| Incorporated (TI) products to use this computer program for the sole     |**
**| purpose of implementing a licensee product based on TI products.         |**
**| No other rights to reproduce, use, or disseminate this computer          |**
**| program, whether in part or in whole, are granted.                       |**
**|                                                                          |**
**| TI makes no representation or warranties with respect to the             |**
**| performance of this computer program, and specifically disclaims         |**
**| any responsibility for any damages, special or consequential,            |**
**| connected with the use of this program.                                  |**
**|                                                                          |**
**+--------------------------------------------------------------------------+**
*******************************************************************************/

/** \file   pal_sys.h
    \brief  PAL SoC level API header file

    This file defines data types and services (macros as well as functions)
    that are applicable to the abstracted h/w system (SoC/Board).

    (C) Copyright 2004, Texas Instruments, Inc

    \author     PSP Architecture Team
    \version    1.0
 */

#ifndef __PAL_SYS_H__
#define __PAL_SYS_H__


/*****************************************************************************
 * GPIO Control
 *****************************************************************************/

typedef enum PAL_SYS_GPIO_PIN_DIRECTION_tag
{
    GPIO_OUTPUT_PIN = 0,
    GPIO_INPUT_PIN = 1
} PAL_SYS_GPIO_PIN_DIRECTION_T;

typedef enum PAL_SYS_GPIO_PIN_INVERSION_tag
{
    GPIO_NORMAL_PIN = 0,
    GPIO_INVERTED_PIN = 1
} PAL_SYS_GPIO_PIN_INVERSION_T;

typedef enum PAL_SYS_GPIO_PIN_SET_tag
{
    GIO_CLEAR = 0,
    GIO_SET = 1
} PAL_SYS_GPIO_PIN_SET_T;

typedef enum PAL_SYS_GPIO_IRQ_MODE_tag
{
    GIO_IRQ_OFF = 0,
    GIO_IRQ_ON = 1
} PAL_SYS_GPIO_IRQ_MODE_T;

typedef enum PAL_SYS_GPIO_IRQ_EDGE_tag
{
    GIO_IRQ_EDGE_FALL = 0,
    GIO_IRQ_EDGE_ANY = 1
} PAL_SYS_GPIO_IRQ_EDGE_T;

typedef enum PAL_SYS_GPIO_PIN_MODE_tag
{
    GPIO_PIN = 0,
	FUNCTIONAL_PIN = 1
} PAL_SYS_GPIO_PIN_MODE_T;



typedef enum { GPIO_FALSE, GPIO_TRUE } PAL_SYS_GPIO_BOOL_T;


typedef struct
{
	/* GIO_DIR register */
	unsigned short* pGIO_DIR;

    /* GIO_INV register */
	unsigned short* pGIO_INV;

	/* GIO_SET register */
	unsigned short* pGIO_SET;

	/* GIO_CLR register */
	unsigned short* pGIO_CLR;

	/* Bit mask and shift for above registers */
	unsigned short pGIO_DIR_INV_SET_CLR_Mask;
	unsigned short pGIO_DIR_INV_SET_CLR_Shift;



	/* GIO_IRQPORT register */
	unsigned short* pGIO_IRQPORT;

	/* GIO_IRQEDGE register */
	unsigned short* pGIO_IRQEDGE;

	/* Bit mask and shift for above registers */
	unsigned short pGIO_IRQ_Mask;


	/* GIO_FSEL register */
	unsigned short* pGIO_FSEL;
	unsigned short pGIO_FSEL_Mask;

	/* TODO: Add chat and nchat if required */

} PAL_GPIO_MAP_T;

typedef struct
{
	/* GIO_DIR value */
	PAL_SYS_GPIO_PIN_DIRECTION_T	vGIO_DIR;

    /* GIO_INV value */
	PAL_SYS_GPIO_PIN_INVERSION_T	vGIO_INV;

	/* GIO_SET and GIO_CLR value */
	PAL_SYS_GPIO_PIN_SET_T			vGIO_SET_CLR;

	/* GIO_IRQPORT value */
	PAL_SYS_GPIO_IRQ_MODE_T			vGIO_IRQPORT;

	/* GIO_IRQEDGE value */
	PAL_SYS_GPIO_IRQ_EDGE_T			vGIO_IRQEDGE;

	/* GIO_FSEL value */
	PAL_SYS_GPIO_PIN_MODE_T			vGIO_FSEL;


	/* TODO: Add chat and nchat if required */

} PAL_GPIO_VALUE_T;


/** Functions declarations **/

int GPIO_Reg_Map(unsigned int gpioNum, PAL_GPIO_MAP_T* RegStruct);

void PAL_sysGpioInit(void);

int PAL_sysGpioCtrl(PAL_GPIO_MAP_T* GIO_RegStruct, PAL_GPIO_VALUE_T* GIO_RegValue);
int PAL_sysGpioOutBit(PAL_GPIO_MAP_T* GIO_RegStruct, PAL_GPIO_VALUE_T* GIO_RegValue);
int PAL_sysGpioInBit(PAL_GPIO_MAP_T* GIO_RegStruct);

int PAL_sysGpioOutValue(unsigned int out_val, unsigned int set_mask,
                           unsigned int reg_index);
int PAL_sysGpioInValue(unsigned int *in_val, unsigned int reg_index);
void PAL_sysGpioReport(PAL_GPIO_MAP_T* GIO_RegStruct, PAL_GPIO_VALUE_T* GIO_RegValue, const char* FuncName);


#endif
