/*
 * linux/arch/arm/mach-dm320/davinci_vlynq_setup.h
 *
 * header file for the vlynq chain on the morpheus platform.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Original Version:  Eyal Reizer, Texas Instruments Inc
 *
 */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/console.h>

#include <asm/hardware.h>
#include <asm/system.h>
#include <asm/pgtable.h>
#include <asm/mach/map.h>
#include <asm/io.h>

#include "pal.h"
#include "pal_vlynq.h"
#include "pal_vlynqIoctl.h"  

#define WLAN_RESET_BIT  5
#define IN_RESET        1
#define OUT_OF_RESET    0

/* I2C registers definitions (Temoporary until we receive an I2C driver from the PSP)... */
#define PINMUX0			(unsigned int *)(IO_ADDRESS(0x01c40000))

/* General vlynq definitions */
#define DAVINCI_VL_MEM_SIZE  	0x10000000   			/* 256Megs */
#define ARM_VLYNQ_MEM_MAP_BASE 	0x0C000000
#define ARM_SDRAM_BASE 			0x80000000
#define ARM_VLYNQ_CONTROL_BASE  IO_ADDRESS(0x01E01000)

/* VLYNQ registers - For debug only */
#define VLYNQ_VER       (unsigned int *)(ARM_VLYNQ_CONTROL_BASE + 0x00)
#define VLYNQ_CTRL		(unsigned int *)(ARM_VLYNQ_CONTROL_BASE + 0x04)
#define VLYNQ_STAT		(unsigned int *)(ARM_VLYNQ_CONTROL_BASE + 0x08)


#define VLYNQ_CLOCK_NO_DIV 		0x1						/* No clock division */
#define VLYNQ_CLOCK_DIV2		0x2						/* Vlynq clock is divided by 2 */
#define VLYNQ_CLOCK_DIV3		0x3						/* Vlynq clock is divided by 3 */
#define VLYNQ_CLOCK_DIV4		0x4						/* Vlynq clock is divided by 4 */
#define VLYNQ_CLOCK_DIV5		0x5						/* Vlynq clock is divided by 5 */
#define VLYNQ_CLOCK_DIV6		0x6						/* Vlynq clock is divided by 6 */
#define VLYNQ_CLOCK_DIV7		0x7						/* Vlynq clock is divided by 7 */
#define VLYNQ_CLOCK_DIV8		0x8						/* Vlynq clock is divided by 8 */

/* DAVINCI GPIO */
#define DAVINCI_GPIO            IO_ADDRESS(DAVINCI_GPIO_BASE)
#define DAVINCI_GPIO_DIR_REG    (unsigned int *)(DAVINCI_GPIO + 0x10)
#define DAVINCI_GPIO_OUT_DATA   (unsigned int *)(DAVINCI_GPIO + 0x14)

/* WLAN 1350 definition */
#define VLYNQ_TNETW1X50_MEM_OFFSET 			0x00000000  /* Physical address of TNETW1150 memory */
#define VLYNQ_TNETW1X50_MEM_SIZE   			0x00080000  /* Total size of the TNETW1150 memory   */
#define VLYNQ_TNETW1X50_REG_OFFSET 			0x00300000  /* PHYS_ADDR of TNETW1150 control registers   */
#define VLYNQ_TNETW1X50_REG_SIZE   			0x00100000  /* Size of TNETW1150 registers area, MAC+PHY  */
#define VLYNQ_TNETW1X50_VL0_PORTAL_OFFSET	0x60000000  /* Window start address into DM320 using VLYNQ1*/

/* Global pointers to be used later by the driver */
	
Uint32 VlynqBaseAddr;			/* VLYNQ base address */
Uint32 wlan_mem_addr;     	/* WLAN memory base */
Uint32 wlan_reg_addr;     	/* WLAN register area base */

/* GIO registers structures */
PAL_GPIO_MAP_T 		GIO29_RegStruct;
PAL_GPIO_VALUE_T	GIO29_RegValue;

/* Simple delay function */
extern void vlynq_delay_wait(Uint32 count);

/* Vlynq mapping Configuration structurs definition */
typedef struct
{
    signed int    id;
    unsigned int  offset;
    unsigned int  size;
    unsigned char remote;
    
} REGION_CONFIG_T;

/* Vlynq interrupt mapping Configuration structurs definition */
typedef struct
{
	signed char hw_intr_line;
    int  irq;

} IRQ_CONFIG_T;


/* Vlynq mapping configurations of the 1350A */
                                
static REGION_CONFIG_T TNETW1x50_region_config[] = {
{ 0, VLYNQ_TNETW1X50_MEM_OFFSET, VLYNQ_TNETW1X50_MEM_SIZE, 1},		/* TNETW1350A memory area */
{ 1, VLYNQ_TNETW1X50_REG_OFFSET, VLYNQ_TNETW1X50_REG_SIZE, 1},		/* TNETW1350A register area */
{ 0, ARM_SDRAM_BASE,  DAVINCI_VL_MEM_SIZE,0},	
{-1, 0, 0, 0}
	                                 };

static IRQ_CONFIG_T TNETW1x50_irq_config[] = {
								    { 0, 0},
									{-1, 0}
                                   };


/* Vlynq driver local variables */                                  
static PAL_VLYNQ_CONFIG_T  vlynq_1x50_config; 
static PAL_VLYNQ_DEV_HND   *p_vlynq_dev  = NULL;
static PAL_VLYNQ_HND       *p_1x50_vlynq = NULL;
static PAL_VLYNQ_HND       *p_root_vlynq = NULL;

static int reset_flag = 1; 	/* 1: in reset, 0: out of reset */

//void arch_cacheFlushRange(void* startAddr, size_t size);
//void arch_cacheInvalidateRange(void* startAddr, size_t size);
void arch_getTnetResources(unsigned int* tnet_mem_addr, unsigned int* tnet_reg_addr);
