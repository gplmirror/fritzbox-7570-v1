
/*
 * Benchmark tracing utility
 */

#ifndef REG_ACCESS_H

#define REG_ACCESS_H

#define TI_REG_ACCESS
#ifdef TI_REG_ACCESS

#include <linux/stddef.h>

/* Initialization */
int ra_init(void);
void ra_destroy(void);

#else  /* #ifdef TI_PROFILING */

#define ra_init(void)
#define ra_destroy(void)

#endif /* #ifdef TI_PROFILING */

#endif
