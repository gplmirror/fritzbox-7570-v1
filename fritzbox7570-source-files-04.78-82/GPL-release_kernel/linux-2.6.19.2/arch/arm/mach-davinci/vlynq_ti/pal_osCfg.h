/*******************************************************************************
**+--------------------------------------------------------------------------+**
**|                            ****                                          |**
**|                            ****                                          |**
**|                            ******o***                                    |**
**|                      ********_///_****                                   |**
**|                      ***** /_//_/ ****                                   |**
**|                       ** ** (__/ ****                                    |**
**|                           *********                                      |**
**|                            ****                                          |**
**|                            ***                                           |**
**|                                                                          |**
**|         Copyright (c) 1998-2004 Texas Instruments Incorporated           |**
**|                        ALL RIGHTS RESERVED                               |**
**|                                                                          |**
**| Permission is hereby granted to licensees of Texas Instruments           |**
**| Incorporated (TI) products to use this computer program for the sole     |**
**| purpose of implementing a licensee product based on TI products.         |**
**| No other rights to reproduce, use, or disseminate this computer          |**
**| program, whether in part or in whole, are granted.                       |**
**|                                                                          |**
**| TI makes no representation or warranties with respect to the             |**
**| performance of this computer program, and specifically disclaims         |**
**| any responsibility for any damages, special or consequential,            |**
**| connected with the use of this program.                                  |**
**|                                                                          |**
**+--------------------------------------------------------------------------+**
*******************************************************************************/

/** \file   pal_osCfg.h
    \brief  OS Configuration Header File

    This file provides the OS configuration.

    (C) Copyright 2004, Texas Instruments, Inc

    \author     Ajay Singh
    \version    0.1
 */

#ifndef __PAL_OSCFG_H__
#define __PAL_OSCFG_H__

#define INLINE 

#undef PAL_INCLUDE_OSMEM
#undef PAL_INCLUDE_OSBUF
#undef PAL_INCLUDE_OSSEM
#undef PAL_INCLUDE_OSMUTEX
#undef PAL_INCLUDE_OSWAIT
#undef PAL_INCLUDE_OSLIST
#define PAL_INCLUDE_OSPROTECT
#undef PAL_INCLUDE_OSCACHE

#endif
