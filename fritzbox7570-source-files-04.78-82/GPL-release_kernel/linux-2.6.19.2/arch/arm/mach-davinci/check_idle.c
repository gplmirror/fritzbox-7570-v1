#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/check_idle.h>

static void davinci_report_check_idle(unsigned long idle, unsigned long run);

#ifdef CONFIG_PROC_FS
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

static unsigned int shift = 16, schwelle = 100, on = 1;

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int proc_davinci_write_idle(struct file *file, const char __user *buffer, unsigned long count, void *data) {
    extern unsigned int my_atoi(char *p);
    char *p;
    printk("count=%lu buf=[%s]\n", count, buffer);

    if(strstr(buffer, "on")) on = 1;
    if(strstr(buffer, "off")) on = 0;
    if(p = strstr(buffer, "shift=")) shift = my_atoi(p + sizeof("shift=") - 1);
    if(p = strstr(buffer, "delay=")) schwelle = my_atoi(p + sizeof("delay=") - 1);

    if(on) {
        printk("[idle] start (shift=%u, delay=%u ticks)\n", shift, schwelle);

        init_check_idle(
                shift, /*--- int shift, ---*/ 
                schwelle, /*--- int schwelle, ---*/ 
                davinci_report_check_idle, /*--- void (*report_function)(signed int, signed int), ---*/ 
                davinci_get_check_idle_timer /*--- signed int (*timer_function)(void)); ---*/
            );
    } else {
        printk("[idle] stop\n");

        init_check_idle(
                0, /*--- int shift, ---*/ 
                0, /*--- int schwelle, ---*/ 
                NULL, /*--- void (*report_function)(signed int, signed int), ---*/ 
                NULL/*--- signed int (*timer_function)(void)); ---*/
            );
    }
    return count;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int proc_davinci_read_idle(char *page, char **start, off_t off, int count, int *eof, void *data) {
    static int done = 0;
    int len;
    if(done) {
        done = 0;
        return 0;
    }
    /*--- printk("page=%p start=%p *start=%p, count=%d *eof=%u data=%p\n", page, start, *start, count, *eof, data); ---*/
    if(*start == NULL)
        *start = page;
    if(on) {
        sprintf(*start, "[idle] running (shift=%u, delay=%u ticks)\n", shift, schwelle);
    } else {
        sprintf(*start, "[idle] stopped\n");
    }
    len = strlen(*start);
    done = 1;
    *eof = 1;
    return len;
}

void davinci_init_proc_check_idle(void) {
	struct proc_dir_entry *entry;

	entry = create_proc_entry("idle", 0, NULL);
	if (entry) {
		entry->read_proc  = proc_davinci_read_idle;
		entry->write_proc = proc_davinci_write_idle;
    }
	return;
}
#endif /*--- #ifdef CONFIG_PROC_FS ---*/

static void davinci_report_check_idle(unsigned long idle, unsigned long run) {
    printk("[idle %lu%%]\n", idle / ((idle + run) / 100));
}

int davinci_init_check_idle(void) {
#ifdef CONFIG_PROC_FS
    davinci_init_proc_check_idle();
#else /*--- #ifdef CONFIG_PROC_FS ---*/
    init_check_idle(
            16, /*--- int shift, ---*/ 
            100, /*--- int schwelle, ---*/ 
            davinci_report_check_idle, /*--- void (*report_function)(signed int, signed int), ---*/ 
            davinci_get_check_idle_timer /*--- signed int (*timer_function)(void)); ---*/
        );
#endif /*--- #else ---*/ /*--- #ifdef CONFIG_PROC_FS ---*/
    return 0;
}

late_initcall(davinci_init_check_idle);

