/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2007 by Ralf Baechle
 */
#include <linux/clocksource.h>
#include <linux/init.h>

#include <asm/time.h>

static void __init clocksource_set_clock(struct clocksource *cs, unsigned int clock)
{
    u64 temp;
    u32 shift;

    /* Find a shift value */
    for (shift = 32; shift > 0; shift--) {
        temp = (u64) NSEC_PER_SEC << shift;
        do_div(temp, clock);
        if ((temp >> 32) == 0)
            break;
    }
    cs->shift = shift;
    cs->mult = (u32) temp;
}

static cycle_t c0_hpt_read(void)
{
	return read_c0_count();
}

static struct clocksource clocksource_mips = {
	.name		= "MIPS",
	.read		= c0_hpt_read,
	.mask		= CLOCKSOURCE_MASK(32),
	.is_continuous = 1,
};

int __init init_mips_clocksource(void)
{
	if (!cpu_has_counter || !mips_hpt_frequency)
		return -ENXIO;

	/* Calclate a somewhat reasonable rating value */
	clocksource_mips.rating = 200 + mips_hpt_frequency / 10000000;

	clocksource_set_clock(&clocksource_mips, mips_hpt_frequency);

	clocksource_register(&clocksource_mips);

	return 0;
}
