/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications. 
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/init.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/bootmem.h>

#include <asm/addrspace.h>
#include <asm/bootinfo.h>
#include <asm/pmon.h>
#include <linux/initrd.h>

int board_memsize=0;
int board_flash_start,board_flash_size;
extern int adi6843_setup(void);
#define ENABLE_JATAG_DEBUG  0
void __init prom_init(void)
{
#if ENABLE_JATAG_DEBUG 
	arcs_cmdline[0] = '\0';
	strcat(arcs_cmdline,"root=/dev/mtdblock2 rw rootfstype=jffs2 myfs_start=0xbfa60000 rootfstype=jffs2");
        
        board_memsize = 32;
	initrd_start = 0x80000000;
	initrd_end = 0x80000000;
	board_flash_start = 0xBF800000;
	board_flash_size = 0x800000;
#else
	int argc = fw_arg0;
	char **arg = (char **)( fw_arg1);
	char **env = (char **) (fw_arg2);
	int i;
        // printk("argc %d arg %s env %s \n",argc,*arg,*env);
	arcs_cmdline[0] = '\0';
/*	setup default parameter here to save some typings. Chang it if you want.

	Remember you can always pass these parameters from my loader 
*/
	strcat(arcs_cmdline,"root=/dev/mtdblock2 rw rootfstype=jffs2 ");

	for (i = 1; i < argc; i++) {
          //    printk("arg[%d] %s \n",i,arg[i]);
		if (strlen(arcs_cmdline) + strlen(arg[i] + 1)
		    >= sizeof(arcs_cmdline))
			break;
		strcat(arcs_cmdline, arg[i]);
		strcat(arcs_cmdline, " ");
		
	}
	

	while (*env) {
		if (strncmp("memsize", *env, strlen("memsize")) == 0){
			board_memsize =
			    simple_strtol(*env + strlen("memsize="), NULL,
					  10);
                     //   printk("memsize  board_memsize = %d\n",board_memsize);
		}
#ifdef CONFIG_BLK_DEV_INITRD		
		if (strncmp("initrd_start", *env, strlen("initrd_start")) == 0){
			initrd_start =
			    simple_strtol(*env + strlen("initrd_start="), NULL,
					  16)-0x20000000;
                      //  printk("initrd_start %x\n",initrd_start);
		}
		if (strncmp("initrd_size", *env, strlen("initrd_size")) == 0){
			initrd_end = initrd_start +
			    simple_strtol(*env + strlen("initrd_size="), NULL,
					  16);
                       // printk("initrd_size start %x end %x\n",initrd_start,initrd_end);
		}
#endif		
		if (strncmp("flash_start", *env, strlen("flash_start")) == 0){
			board_flash_start =
			    simple_strtol(*env + strlen("flash_start="), NULL,16);
                        //printk("flash_start %x\n",board_flash_start);
		}
		if (strncmp("flash_size", *env, strlen("flash_size")) == 0){
			board_flash_size =
			    simple_strtol(*env + strlen("flash_size="), NULL,
					  16);
                        //printk("board_flash_size %x\n",board_flash_size);
		}
             //printk("env %s \n",*env);

		env++;
	}
#endif
	/* anyone need this ? , OEM board parameters */
	mips_machgroup = 0x1234;
	mips_machtype = 0x5678;


	add_memory_region(0, board_memsize<< 20, BOOT_MEM_RAM);
 	change_c0_status(ST0_BEV,0);
	// adi6843_setup();
	plat_setup();
}

unsigned long __init prom_free_prom_memory(void)
{
	return 0;
}
