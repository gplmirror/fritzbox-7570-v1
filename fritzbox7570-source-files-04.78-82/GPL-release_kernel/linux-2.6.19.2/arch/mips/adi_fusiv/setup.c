/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications. 
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/config.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/console.h>
#include <linux/irq.h>
#include <linux/delay.h>

#include <asm/serial.h>
#include <asm/io.h>
#include <asm/wbflush.h>
#include <asm/time.h>
#include <adi6843.h>

extern void (*_machine_restart)(char *);
extern void (*_machine_halt)(void);
extern void (*pm_power_off)(void);
extern void ad6843_startTimer(struct irqaction *irq);
extern void fusiv_init_IRQ(void);
#ifdef CONFIG_PCI
extern int  fusiv_pci_setup(void);
#endif

void  (*softResetPreparation4Vox150_ptr)( void ) =NULL;
unsigned char glo_uncmp_buffer [512*1024];

#define PCI_RST_CONTROL	*(volatile unsigned long *)(KSEG1ADDR(0x19050700))
#define REBOOT_CTRL	*(volatile unsigned long *)(KSEG1ADDR(0x19000000))

static void ad6843_restart( char *command )
{
	/* Do a good, solid PCI bus reset */
	PCI_RST_CONTROL = 0x1;
	mdelay(1);
	PCI_RST_CONTROL = 0x3;
	mdelay(1);
	PCI_RST_CONTROL = 0x1;
	mdelay(1);
#if 0 // Linux-2.6.18
        if(softResetPreparation4Vox150_ptr == NULL)
        {
                printk("\n Error:softResetPreparation4Vox150_ptr. Please restart MANUALLY...\n");
        }
        else
        {
                (*softResetPreparation4Vox150_ptr)( );
        }
#endif
	REBOOT_CTRL = 0x1;
	while ( 1 );
}

static void ad6843_halt( void )
{
	printk("AD6843 Halted\n");
	while ( 1 );
}

/* Do AD6843-specific initialization in here */
// int adi6843_setup(void)
int plat_setup(void)
{
	struct uart_port req;


//	board_timer_setup = ad6843_startTimer;
	_machine_restart = ad6843_restart;
	_machine_halt = ad6843_halt;
	pm_power_off = ad6843_halt;   

	/* Setup serial port */
	memset(&req, 0, sizeof(req));
	req.line = 0;
	req.type = PORT_16450;
	req.irq = UART1_INT;
	req.flags = STD_COM_FLAGS;
	req.uartclk = 100*1000*1000 / 16 /6;
	req.iotype = SERIAL_IO_MEM;
	req.membase = (unsigned char *)((ADI_6843_UART0_ADDR));
	req.mapbase = (unsigned long)(ADI_6843_UART0_ADDR);
	req.regshift = 2;
	req.fifosize = 1;
	early_serial_setup(&req);
#ifdef CONFIG_PCI	
	set_io_port_base(0);
#endif 
	return 0;
}


const char *get_system_type(void)
{
	return "ADI Fusiv Core";
}

void __init arch_init_irq(void)
{
	fusiv_init_IRQ();
}

#if CONFIG_FUSIV_KERNEL_PROFILER_MODULE

int (*loggerFunction)(unsigned long event) = NULL;

int loggerProfile(unsigned long event)
{
  if(loggerFunction != NULL)
    return loggerFunction(event);
  return -1;
}
void  loggerRegFunction( int (*func)(unsigned long event))
{
  loggerFunction = func;
}

EXPORT_SYMBOL(loggerRegFunction);
#endif
EXPORT_SYMBOL(softResetPreparation4Vox150_ptr);
EXPORT_SYMBOL(glo_uncmp_buffer);
