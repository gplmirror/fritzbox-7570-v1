/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications. 
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */


/*
 * Platform device support for ADI's VoX160 SoCs.
 *
 * Copyright 2004, Matt Porter <mporter@kernel.crashing.org>
 * Modified  2005, Vivek Dharmadhikari <mporter@kernel.crashing.org>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/resource.h>

#include <asm/mach-adi_fusiv/vox160usb.h>

static struct resource vox160_usb_ohci_hcd_resources[] = {
	[0] = {
	       .start = VOX160_USB_OHCI_BASE,
	       .end = VOX160_USB_OHCI_BASE + VOX160_USB_OHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = VOX160_USB_HOST_INT,
	       .end = VOX160_USB_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

static struct resource vox160_usb_ehci_hcd_resources[] = {
	[0] = {
	       .start = VOX160_USB_EHCI_BASE,
	       .end = VOX160_USB_EHCI_BASE + VOX160_USB_EHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = VOX160_USB_HOST_INT,
	       .end = VOX160_USB_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

static u64 vox160_dmamask = ~(u32) 0;

static struct platform_device vox160_usb_ohci_hcd_device = {
	.name = "vox160-ohci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &vox160_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(vox160_usb_ohci_hcd_resources),
	.resource = vox160_usb_ohci_hcd_resources,
};

static struct platform_device vox160_usb_ehci_hcd_device = {
	.name = "vox160-ehci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &vox160_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(vox160_usb_ehci_hcd_resources),
	.resource = vox160_usb_ehci_hcd_resources,
};

/* Declare all the vox160 USBD related resources */
static struct resource vox160_usbd_resources[] = {
	[0] = {
	       .start = VOX160_USBD_INT,
	       .end = VOX160_USBD_INT,
	       .flags = IORESOURCE_IRQ,
	       },
	[1] = {
	       .start = VOX160_USBD_GENERAL_REG_BASE,
	       .end =
	       VOX160_USBD_GENERAL_REG_BASE + VOX160_USBD_GENERAL_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[2] = {
	       .start = VOX160_USBD_DMA_REG_BASE,
	       .end = VOX160_USBD_DMA_REG_BASE + VOX160_USBD_DMA_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[3] = {
	       .start = VOX160_USBD_INTRN_REG_BASE,
	       .end = VOX160_USBD_INTRN_REG_BASE + VOX160_USBD_EP_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[4] = {
	       .start = VOX160_USBD_MASKN_REG_BASE,
	       .end = VOX160_USBD_MASKN_REG_BASE + VOX160_USBD_EP_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[5] = {
	       .start = VOX160_USBD_EPCFGN_REG_BASE,
	       .end = VOX160_USBD_EPCFGN_REG_BASE + VOX160_USBD_EP_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[6] = {
	       .start = VOX160_USBD_EPADRN_REG_BASE,
	       .end = VOX160_USBD_EPADRN_REG_BASE + VOX160_USBD_EP_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[7] = {
	       .start = VOX160_USBD_EPLEN_REG_BASE,
	       .end = VOX160_USBD_EPLEN_REG_BASE + VOX160_USBD_EP_REG_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
};

static u64 vox160_usbd_dmamask = ~(u32) 0;

static struct platform_device vox160_usbd_device = {
	.name = "vox160-udc",
	.id = 0,
	.dev = {
		.dma_mask = &vox160_usbd_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(vox160_usbd_resources),
	.resource = vox160_usbd_resources,
};

static struct platform_device *vox160_platform_devices[] __initdata = {
	&vox160_usb_ohci_hcd_device,
	&vox160_usb_ehci_hcd_device,
	&vox160_usbd_device,
};

int
vox160_platform_init(void)
{
	return platform_add_devices(vox160_platform_devices,
				    ARRAY_SIZE(vox160_platform_devices));
}

arch_initcall(vox160_platform_init);
