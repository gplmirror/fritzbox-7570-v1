/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/init.h>
#include <linux/sched.h>
#include <linux/pci.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/serial.h>
#include <linux/serial_core.h>
#include <linux/console.h>
#include <linux/irq.h>
#include <linux/delay.h>

#include <asm/serial.h>
#include <asm/io.h>
#include <asm/wbflush.h>
#include <asm/time.h>

#if defined(CONFIG_MIPS_FUSIV)
#include <asm/mach_avm.h>
#include <asm/mach-ikan_mips/vx180.h>
#endif

#include <ikan6850.h>

extern void (*_machine_restart)(char *);
extern void (*_machine_halt)(void);
extern void (*pm_power_off)(void);
extern void ad6843_startTimer(struct irqaction *irq);
extern void fusiv_init_IRQ(void);
#ifdef CONFIG_PCI
extern int  fusiv_pci_setup(void);
#endif

void  (*softResetPreparation4Vox150_ptr)( void ) =NULL;
#ifdef CONFIG_FUSIV_VX180
void  (*softResetVoiceDriver_ptr)( void ) =NULL;
#endif
#ifdef CONFIG_ATM
unsigned char glo_uncmp_buffer [512*1024];
#endif

#define PCI_RST_CONTROL	*(volatile unsigned long *)(KSEG1ADDR(0x19050700))
#define REBOOT_CTRL	*(volatile unsigned long *)(KSEG1ADDR(0x19000000))

#define GPIO_USB_POWER 10
#if defined(CONFIG_MIPS_FUSIV)
static struct resource fusiv_gpioressource[] = {
{
    .name  = "DSP2_PF3 (used as USB_POWER)",
    .flags = IORESOURCE_IO,
    .start = GPIO_USB_POWER,
    .end   = GPIO_USB_POWER
},
};
#endif


static void ad6843_restart( char *command )
{
	/* Do a good, solid PCI bus reset */
	PCI_RST_CONTROL = 0x1;
	mdelay(1);
	PCI_RST_CONTROL = 0x3;
	mdelay(1);
	PCI_RST_CONTROL = 0x1;
	mdelay(1);
#if 0 // Linux-2.6.18
        if(softResetPreparation4Vox150_ptr == NULL)
        {
                printk("\n Error:softResetPreparation4Vox150_ptr. Please restart MANUALLY...\n");
        }
        else
        {
                (*softResetPreparation4Vox150_ptr)( );
        }
#endif
#if defined(CONFIG_FUSIV_MIPS_BASED_VOICE) || defined(CONFIG_FUSIV_DSP_BASED_VOICE)
#ifdef CONFIG_FUSIV_VX180
        if(softResetVoiceDriver_ptr != NULL)
        {
	  (*softResetVoiceDriver_ptr)();
        }
        mdelay(10);
#endif
#endif
     if((*(unsigned int*)0xb900003c) == 0x6850) // vx180
     {
       int reset_value = 0x1 << 15;

       printk("\ndisabling IRQ's and enabling reset bit in all AP's\n");

        /* reset all AP's -- enable reset bit in AP control reg's */
       *(volatile unsigned int *)0xb9110300 = reset_value; /* GEMAC1 AP */
       *(volatile unsigned int *)0xb9150300 = reset_value; /* GEMAC2 AP */
       *(volatile unsigned int *)0xb9190300 = reset_value; /* VDSL AP   */
#if defined(CONFIG_FUSIV_KERNEL_PERI_AP) || defined(CONFIG_FUSIV_KERNEL_PERI_AP_MODULE)
       *(volatile unsigned int *)0xb91a0300 = reset_value; /* PERI AP   */
#endif
       *(volatile unsigned int *)0xb9100300 = reset_value; /* SEC AP    */
       mdelay(5);
       *(volatile unsigned int *)0xb9210300 = reset_value; /* BMU AP    */
       mdelay(5);

        /* Disable Interrupt Request (Timer 0) -- simple reset will do this*/
       *(volatile unsigned int *)0xb9070004 = 0x0;
        /* Disable Timer (TIMER0) */
       *(volatile unsigned int *)0xb9070000 = 0x1 << 9;
       mdelay(2);
     }
	REBOOT_CTRL = 0x1;
	while ( 1 );
}

static void ad6843_halt( void )
{
	printk("AD6843 Halted\n");
	while ( 1 );
}
// extern void register_console(struct console *);
// extern struct console ad6843_console;

void __init ikan_time_init(void)
{
	unsigned int est_freq;

	est_freq = ikan_get_clock(avm_clock_id_cpu);
	printk("CPU frequency %d.%02d MHz\n", est_freq/1000000,
	       (est_freq%1000000)*100/1000000);
	mips_hpt_frequency = est_freq / 2;
}

/* Do AD6843-specific initialization in here */
// int adi6843_setup(void)
int plat_setup(void)
{
	struct uart_port req;

	board_time_init = ikan_time_init;

//	board_timer_setup = ad6843_startTimer;
	_machine_restart = ad6843_restart;
	_machine_halt = ad6843_halt;
	pm_power_off = ad6843_halt;   

#if CONFIG_SERIAL_8250_CONSOLE_UART == 0
#define CONSOLE_UART_INT  UART1_INT    
#define CONSOLE_UART_ADDR ADI_6843_UART0_ADDR
#define CONSOLE_UART_FIFO 1
#else     
#define CONSOLE_UART_INT  UART2_INT    
#define CONSOLE_UART_ADDR ADI_6843_UART1_ADDR
#define CONSOLE_UART_FIFO 16
#endif
	/* Setup serial port */
	memset(&req, 0, sizeof(req));
	req.line = 0;
	req.type = PORT_16450;
	req.irq  = CONSOLE_UART_INT;
	req.flags = STD_COM_FLAGS;
	/* req.uartclk = 4*1000*1000 /16/6; */
	req.uartclk = 166000000 /16/6;
	req.iotype = SERIAL_IO_MEM;
	req.membase = (unsigned char *)((CONSOLE_UART_ADDR));
	req.mapbase = (unsigned long)(CONSOLE_UART_ADDR);
	req.regshift = 2;
	req.fifosize = CONSOLE_UART_FIFO;
	early_serial_setup(&req);

#if CONFIG_SERIAL_8250_NR_UARTS > 1
	memset(&req, 0, sizeof(req));
	req.line = 1;
	req.type = PORT_16550A;
	req.irq = UART2_INT;
	req.flags = STD_COM_FLAGS;
	/* req.uartclk = 4*1000*1000 /16/6; */
	req.uartclk = 166000000 /16/6;
	req.iotype = SERIAL_IO_MEM;
	req.membase = (unsigned char *)((ADI_6843_UART1_ADDR));
	req.mapbase = (unsigned long)(ADI_6843_UART1_ADDR);
	req.regshift = 2;
	req.fifosize = 16;
	early_serial_setup(&req);
#endif

#ifdef CONFIG_PCI	
	set_io_port_base(0);
#endif 
//	register_console( &ad6843_console );

#if defined(CONFIG_MIPS_FUSIV)
	/* 20082027 WK: GPIO init for USB */
	if(request_resource(&gpio_resource, &fusiv_gpioressource[0])) {
		printk(KERN_ERR"[ikan_setup]ERROR: fusiv-resource %d gpio %u-%u not available\n", 0, (unsigned int)fusiv_gpioressource[0].start, (unsigned int)fusiv_gpioressource[0].end);  
	} else {
		printk(KERN_INFO"[ikan_setup] USB_POWER on\n");  
		VX180_GPIO_SETDIR(GPIO_USB_POWER, VX180_GPIO_AS_OUTPUT);
		VX180_GPIO_OUTPUT(GPIO_USB_POWER, 1);
	}
#endif	

	return 0;
}

const char *get_system_type(void)
{
	return "Ikanos Fusiv Core";
}

void __init arch_init_irq(void)
{
	fusiv_init_IRQ();
}

#if CONFIG_FUSIV_KERNEL_PROFILER_MODULE

int (*loggerFunction)(unsigned long event) = NULL;

int loggerProfile(unsigned long event)
{
  if(loggerFunction != NULL)
    return loggerFunction(event);
  return -1;
}
void  loggerRegFunction( int (*func)(unsigned long event))
{
  loggerFunction = func;
}

EXPORT_SYMBOL(loggerRegFunction);
#endif
EXPORT_SYMBOL(softResetPreparation4Vox150_ptr);
#ifdef CONFIG_FUSIV_VX180
EXPORT_SYMBOL(softResetVoiceDriver_ptr);
#endif
#ifdef CONFIG_ATM
EXPORT_SYMBOL(glo_uncmp_buffer);
#endif

/*--- Kernel-Schnittstelle für das neue LED-Modul ---*/
enum _led_event { /* DUMMY DEFINITION */ LastEvent = 0 };
int (*led_event_action)(int, enum _led_event , unsigned int ) = NULL;
EXPORT_SYMBOL(led_event_action);

