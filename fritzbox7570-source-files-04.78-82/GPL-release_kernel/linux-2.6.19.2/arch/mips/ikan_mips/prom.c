/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 */

#include <linux/init.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/bootmem.h>
#include <linux/module.h>

#include <asm/addrspace.h>
#include <asm/bootinfo.h>
#include <asm/pmon.h>
#include <linux/initrd.h>
#include <linux/env.h>
#include <asm/mips-boards/prom.h>
#include <ikan6850.h>

/*--- #define DEBUG_WLAN_DECT_CONFIG ---*/
/*--- #define DEBUG_PROM_INIT_ENV ---*/
#if defined(DEBUG_WLAN_DECT_CONFIG)
#define PRINTK(args...)  printk(args)
#else
#define PRINTK(args...)
#endif

unsigned int xtensa_mem_start = 0;
unsigned int xtensa_mem_size  = 0;
int board_flash_start,board_flash_size;


char * __init prom_getcmdline(void)
{
	return &(arcs_cmdline[0]);
}


#ifdef        CONFIG_DEBUG_LL
int prom_putchar(char c) {
	volatile unsigned char *SIO = (volatile unsigned char)(ADI_6843_UART1_ADDR);
    while((*(SIO + 0x14) & (1 << 6)) == 0)
        ;
    *(SIO) = c;
}

void printascii(char *c) {
    while (*c) {
        prom_putchar(*c++);
    }
}
#endif

extern int adi6843_setup(void);

#define ENABLE_JATAG_DEBUG  0

static unsigned int wlan_dect_config[IKANOS_MAX_CONFIG_ENTRIES];
void __init set_wlan_dect_config_address(unsigned int *pConfig) {

    int i = 0;

    while (pConfig[i]) {
        wlan_dect_config[i] = pConfig[i];
#if defined(DEBUG_WLAN_DECT_CONFIG)
        prom_printf("[set_wlan_dect_config] pConfig[%d] 0x%x\n", i, wlan_dect_config[i]);
#endif
        i++;
    }

}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned short len) {

    int i;
    struct wlan_dect_config config;

    PRINTK("[get_wlan_dect_config] buffer 0x%p len %d\n", buffer , len);

    for (i=0;i<IKANOS_MAX_CONFIG_ENTRIES;i++) {
        PRINTK("[get_wlan_dect_config] wlan_dect_config[%d] 0x%x\n", i , wlan_dect_config[i]);
        if (wlan_dect_config[i]) {    /*--- Eintrag vorhanden und nicht leer ---*/
            memcpy(&config, (char *)wlan_dect_config[i], sizeof(struct wlan_dect_config));
            switch (config.Version) {
	        case 1:
                case 2:
                    PRINTK("[get_wlan_dect_config] Type %d Len 0x%x\n", config.Type, config.Len);
                    if (Type != config.Type) {
                        break;                      /*--- n�chster Konfigeintrag ---*/
                    }
                    if (!(len >= config.Len + sizeof(struct wlan_dect_config)))
                        return -2;                  /*--- buffer zu klein ---*/
                    PRINTK("[get_wlan_dect_config] read ");
                    switch (config.Type) {
                        case WLAN:
                        case WLAN2:
                            PRINTK("WLAN\n");
                            memcpy(buffer, (char *)wlan_dect_config[i], config.Len + sizeof(struct wlan_dect_config));
#if defined(DEBUG_WLAN_DECT_CONFIG)
                            {
                                int x;
                                for (x=0;x<(config.Len+sizeof(struct wlan_dect_config));x++)
                                    printk("0x%x ", buffer[x]);
                                printk("\n");
                            }
#endif
                            return 0;
                        case DECT:
                            PRINTK("DECT\n");
                            memcpy(buffer, (char *)wlan_dect_config[i], config.Len + sizeof(struct wlan_dect_config));
#if defined(DEBUG_WLAN_DECT_CONFIG)
                            {
                                int x;
                                for (x=0;x<(config.Len+sizeof(struct wlan_dect_config));x++)
                                    printk("0x%x ", buffer[x]);
                                printk("\n");
                            }
#endif
                            return 0;
                        default:
                            PRINTK("Type unknown\n");
                            return -3;
                    }
                    break;
                default:
                    PRINTK("[get_wlan_dect_config] unknown Version %x\n", config.Version);
                    return -3;
            }
        }
    }
    return -1;
}
EXPORT_SYMBOL(set_wlan_dect_config_address);
EXPORT_SYMBOL(get_wlan_dect_config);


void __init prom_init(void)
{
#if ENABLE_JATAG_DEBUG 

    arcs_cmdline[0] = '\0';
    /* strcat(arcs_cmdline,"root=/dev/mtdblock2 rw rootfstype=jffs2 myfs_start=0xbfa60000 rootfstype=jffs2"); */
    strcat(arcs_cmdline,"root=/dev/mtdblock3 rw rootfstype=jffs2 myfs_start=0xbfe00000 rootfstype=jffs2");
        
    xtensa_mem_start = 0;  /*--- sinnvolle were ggf. einsetzen ---*/
    initrd_start = 0x80000000;
    initrd_end = 0x80000000;
    board_flash_start = 0xBF800000;
    board_flash_size = 0x800000;
#else
    int argc = fw_arg0;
    char **arg = (char **)( fw_arg1);
    char **env = (char **) (fw_arg2);
    int i;
    unsigned int usable_memsize = 0;

#if defined(DEBUG_PROM_INIT_ENV)
    printk("argc %d arg %s env %s \n",argc, arg ? *arg : "arg=NULL", env ? *env : "env=NULL" );
#endif

    arcs_cmdline[0] = '\0';
/*	setup default parameter here to save some typings. Chang it if you want.

    Remember you can always pass these parameters from my loader 
*/
    if(env == NULL) {
        char *__env[] = { "memsize=128", "flash_start=0xbe000000", "flash_size=32", NULL };
        env = (char **)__env;
    }

    board_flash_start = 0x1e000000;

    while (env && *env) {
        if (strcmp("memsize", *env) == 0){
#define PSB_CTL                     0xb90000EC
            unsigned int psb_value = *(volatile unsigned int *)PSB_CTL;
            env++;

            xtensa_mem_start = psb_value & 0x1FFF0000;
            xtensa_mem_size  = ((psb_value << 16) | 0xFFFF) + 1;

            usable_memsize = simple_strtol(*env, NULL, 16) & 0x1FFFFFFF;

#if defined(DEBUG_PROM_INIT_ENV)
            printk("memsize = 0x%08x xtensa_mem_start = 0x%08x xtensa_mem_size %d MBytes\n",  usable_memsize, xtensa_mem_start, xtensa_mem_size >> 20);
#endif

            if(xtensa_mem_start + xtensa_mem_size  > usable_memsize) {
                panic("xtensa memory not in avalible memory space, update boot loader\n");
            }

        }
        if (strcmp("flashsize", *env) == 0){
            env++;
            board_flash_size = simple_strtol(*env, NULL, 16) >> 20;
#if defined(DEBUG_PROM_INIT_ENV)
            printk("board_flash_size %d MByte\n", board_flash_size);
#endif
        }
#if defined(DEBUG_PROM_INIT_ENV)
        printk("env %s \n",*env);
#endif
        env++;
    }

#if 0
    if(board_flash_start == 0xbe000000)
        strcat(arcs_cmdline,"root=/dev/mtdblock3 rw rootfstype=jffs2 ");
    else
        strcat(arcs_cmdline,"root=/dev/mtdblock2 rw rootfstype=jffs2 ");
#endif

    for (i = 1; i < argc; i++) {
#if defined(DEBUG_PROM_INIT_ENV)
        printk("arg[%d] %s \n", i, arg[i]);
#endif
        if (strlen(arcs_cmdline) + strlen(arg[i] + 1)
            >= sizeof(arcs_cmdline))
            break;
        strcat(arcs_cmdline, arg[i]);
        strcat(arcs_cmdline, " ");
    }
    if(argc == 0) {
        strcat(arcs_cmdline, " ");
        strcat(arcs_cmdline, CONFIG_CMDLINE);
    }
	
#endif
	/* anyone need this ? , OEM board parameters */
	mips_machgroup = 0x1234;
	mips_machtype = 0x5678;

#if defined(CONFIG_FUSIV_KERNEL_BME_DRIVER_VX180_MODULE) || defined(CONFIG_FUSIV_KERNEL_BME_DRIVER_VX180)
	add_memory_region(0, xtensa_mem_start, BOOT_MEM_RAM);
    if(xtensa_mem_start + xtensa_mem_size < usable_memsize) {
        add_memory_region(xtensa_mem_start + xtensa_mem_size, 
                          usable_memsize - (xtensa_mem_start + xtensa_mem_size), BOOT_MEM_RAM); /*--- normal RAM ---*/
    }
#else /*--- #if defined(CONFIG_FUSIV_KERNEL_BME_DRIVER_VX180_MODULE) || defined(CONFIG_FUSIV_KERNEL_BME_DRIVER_VX180) ---*/
	add_memory_region(0, usable_memsize, BOOT_MEM_RAM);
#endif
 	change_c0_status(ST0_BEV,0);
	// adi6843_setup();
    env_init(fw_arg2, ENV_LOCATION_PHY_RAM);
    set_wlan_dect_config_address((unsigned int *)fw_arg3);
    
	plat_setup();
}

unsigned long __init prom_free_prom_memory(void)
{
	return 0;
}
EXPORT_SYMBOL(xtensa_mem_start);
EXPORT_SYMBOL(xtensa_mem_size);
