/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications.
 */


/*
 * Platform device support for Ikanos's  IKF68XX SoCs.
 *
 * Copyright 2004, Matt Porter <mporter@kernel.crashing.org>
 * Modified  2005, Vivek Dharmadhikari <mporter@kernel.crashing.org>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/resource.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/nand-direct-avm.h>
#include <asm/mach_avm.h>

#include <asm/mach-ikan_mips/ikf68xx_usb.h>

/*------------------------------------------------------------------------------------------*\
 * USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB USB 
\*------------------------------------------------------------------------------------------*/
static struct resource ikf68xx_usb_ohci_hcd_resources[] = {
	[0] = {
	       .start = IKF68XX_USB_OHCI_BASE,
	       .end = IKF68XX_USB_OHCI_BASE + IKF68XX_USB_OHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = IKF68XX_USB_HOST_INT,
	       .end = IKF68XX_USB_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

static struct resource ikf68xx_usb_ehci_hcd_resources[] = {
	[0] = {
	       .start = IKF68XX_USB_EHCI_BASE,
	       .end = IKF68XX_USB_EHCI_BASE + IKF68XX_USB_EHCI_LEN - 1,
	       .flags = IORESOURCE_MEM,
	       },
	[1] = {
	       .start = IKF68XX_USB_HOST_INT,
	       .end = IKF68XX_USB_HOST_INT,
	       .flags = IORESOURCE_IRQ,
	       },
};

static u64 ikf68xx_dmamask = ~(u32) 0;

static struct platform_device ikf68xx_usb_ohci_hcd_device = {
	.name = "ikf68xx-ohci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &ikf68xx_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(ikf68xx_usb_ohci_hcd_resources),
	.resource = ikf68xx_usb_ohci_hcd_resources,
};

static struct platform_device ikf68xx_usb_ehci_hcd_device = {
	.name = "ikf68xx-ehci-hcd",
	.id = 0,
	.dev = {
		.dma_mask = &ikf68xx_dmamask,
		.coherent_dma_mask = 0xffffffff,
		},
	.num_resources = ARRAY_SIZE(ikf68xx_usb_ehci_hcd_resources),
	.resource = ikf68xx_usb_ehci_hcd_resources,
};

static struct platform_device *ikf68xx_platform_devices[] __initdata = {
	&ikf68xx_usb_ohci_hcd_device,
	&ikf68xx_usb_ehci_hcd_device,
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int ikf68xx_platform_init(void)
{
	return platform_add_devices(ikf68xx_platform_devices,
				    ARRAY_SIZE(ikf68xx_platform_devices));
}

arch_initcall(ikf68xx_platform_init);

/*------------------------------------------------------------------------------------------*\
 * NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND NAND 
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_MTD_NAND_DIRECT_AVM

#define DIRECT_NAND_SIZE    (unsigned int)((2 * 1024) << 20)   /*--- 2 GByte ---*/
static struct mtd_partition direct_avm_nand_partitions[] = {
    {
        .offset = 0,
        .size   = 100 * (1 << 20),
        .name   = "nand-swap"
    },
    {
        .offset = 100 * (1 << 20), /*--- direct_avm_nand_partitions[0].size, ---*/
        .size   = 100 * (1 << 20),
        .name   = "reserved"
    },
    {
        .offset = 100 * (1 << 20) + 100 * (1 << 20), /*--- direct_avm_nand_partitions[0].size + direct_avm_nand_partitions[1].size, ---*/
        .size   = DIRECT_NAND_SIZE - (100 * (1 << 20) + 100 * (1 << 20)), /*--- direct_avm_nand_partitions[0].size + direct_avm_nand_partitions[1].size, ---*/
        .name   = "nand-filesystem"
    }
};

static void direct_avm_nand_adjust_partitions(struct direct_avm_nand_platdata *, size_t);

static struct direct_avm_nand_platdata direct_avm_nand_platform_data = {
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
	.addr_nce = (1 << 16),
	.addr_nwp = (1 << 18),
	.addr_cle = (1 << 17),
	.addr_ale = (1 << 15),
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/
	.gpio_rdy = CONFIG_MTD_NAND_DIRECT_AVM_READY,
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
    .gpio_nwp = CONFIG_MTD_NAND_DIRECT_AVM_NWP,
    .gpio_nce = CONFIG_MTD_NAND_DIRECT_AVM_NCE,
    .gpio_cle = CONFIG_MTD_NAND_DIRECT_AVM_CLE,
    .gpio_ale = CONFIG_MTD_NAND_DIRECT_AVM_ALE,
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/
    .adjust_parts = direct_avm_nand_adjust_partitions,
	.parts = direct_avm_nand_partitions,
	.num_parts ARRAY_SIZE(direct_avm_nand_partitions),
	.options = 0, /*--- NAND_USE_FLASH_BBT | NAND_BBT_SCANALLPAGES | NAND_BBT_SCANEMPTY, ---*/
	.chip_delay = 0
};

static struct resource direct_nand_resources[] = {
	[0] = {
	       .start = CONFIG_MTD_NAND_DIRECT_AVM_DATA,
	       .end = CONFIG_MTD_NAND_DIRECT_AVM_DATA + (16 << 20) - 1,
	       .flags = IORESOURCE_MEM,
	       },
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
	[1] = {
	       .start = CONFIG_MTD_NAND_DIRECT_AVM_CONTROL,
	       .end = CONFIG_MTD_NAND_DIRECT_AVM_CONTROL + (16 << 20) - 1,
	       .flags = IORESOURCE_MEM,
	       },
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/
};

static struct platform_device direct_nand_device = {
	.name = "direct-avm-nand",
	.id = 0,
	.dev = {
        .platform_data = &direct_avm_nand_platform_data
		},
	.num_resources = ARRAY_SIZE(direct_nand_resources),
	.resource = direct_nand_resources
};

static struct platform_device *direct_nand_platform_devices[] __initdata = {
	&direct_nand_device
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void direct_avm_nand_adjust_partitions(struct direct_avm_nand_platdata *plat, size_t size) {
    size_t old_size = 0;
    unsigned int i;
    printk(KERN_ERR "[direct_avm_nand_adjust_partitions] adjust to size 0x%x (%d)\n", size, size);
    for(i = 0 ; i < plat->num_parts ; i++) {
        old_size += plat->parts[i].size;
    }
    printk(KERN_ERR "[direct_avm_nand_adjust_partitions] old size s 0x%x (%d)\n", size, size);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int direct_nand_platform_init(void) {
	return platform_add_devices(direct_nand_platform_devices, ARRAY_SIZE(direct_nand_platform_devices));
}

arch_initcall(direct_nand_platform_init);



#endif /*--- #ifdef CONFIG_MTD_NAND_DIRECT_AVM ---*/
