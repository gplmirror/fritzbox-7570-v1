/*
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ----------------------------------------------------------------------------
 *
 */

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/env.h>
#include <linux/root_dev.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <asm/mach-ikan_mips/vx180.h>

#include <asm/setup.h>
#include <asm/io.h>

#if defined(CONFIG_MTD_PHYSMAP) || defined(CONFIG_MTD_PHYSMAP_MODULE)
#define DO_MTD

#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/mtd/plat-ram.h>
#include <asm/mips-boards/prom.h>
#include <linux/squashfs_fs.h>
#include <linux/jffs2.h>

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MAX_FLASH_MTD   6
#define JFFS2_MIN_SIZE  6

static struct mtd_partition ikan_partitions[MAX_FLASH_MTD];
/*-------------------------------------------------------------------------------------*\
 * Zuerst wird das JFFS2 gesucht, dann das Squash-FS!
\*-------------------------------------------------------------------------------------*/
static const char *probes[] = { "find_jffs2", "find_squashfs" , NULL };
static unsigned int flash_erase_block_size = 0;

static struct physmap_flash_data ikan_flash_data = {
	.width		= 2,
	.parts		= ikan_partitions,
	.nr_parts	= ARRAY_SIZE(ikan_partitions),
    .probes     = probes
};

/* NOTE: CFI probe will correctly detect flash part as 32M, but EMIF
 * limits addresses to 16M, so using addresses past 16M will wrap */
static struct resource ikan_flash_resource[2] = {
    {
        .start		= VX180_ASYNC_BANK1_START,
        .end		= VX180_ASYNC_BANK1_START + (32 << 20),    /* 32 MB */
        .flags		= IORESOURCE_MEM,
    },
    {   /* f�r ins RAM geladenes Filesystem */
        .start		= VX180_ASYNC_BANK1_START,
        .end		= VX180_ASYNC_BANK1_START + (32 << 20),
        .flags		= IORESOURCE_MEM,
    }
};

void ikan_ram_mtd_set_rw(struct device *pdev, int);

struct platform_device ikan_flash_device = {
	.name		= "physmap-flash",
	.id		    = 1,
	.dev		= {
		.platform_data	= &ikan_flash_data,
	},
	.num_resources	= 1,
	.resource	= &ikan_flash_resource[0],
};

#ifdef CONFIG_MTD_SPI
struct platform_device ikan_spiflash_device = {
	.name		= "spimap-flash",
	.id		    = 1,
	.dev		= {
		.platform_data	= &ikan_flash_data,
	},
	.num_resources	= 1,
	.resource	= &ikan_flash_resource[0],
};
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static struct mtd_partition ikan_ram_partitions[3];

static struct platdata_mtd_ram ikan_ram_data = {
	.mapname       = "ram-filesystem",
	.bankwidth	   = 4,
	.partitions    = ikan_ram_partitions,
	.nr_partitions = ARRAY_SIZE(ikan_ram_partitions),
    .set_rw        = ikan_ram_mtd_set_rw,
    .probes        = probes
};

struct platform_device ikan_ram_device = {
	.name		= "mtd-ram",
	.id		    = 2,
	.dev		= {
		.platform_data	= &ikan_ram_data,
	},
	.num_resources	= 1,
	.resource	= &ikan_flash_resource[1],
};

struct platform_device *ikan_platform_devices[] = {
#ifdef CONFIG_MTD_SPI
    &ikan_spiflash_device,
#else
    &ikan_flash_device,
#endif
    &ikan_ram_device 
};

/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
void ikan_init_platform_devices(void) {
	platform_add_devices(ikan_platform_devices, ARRAY_SIZE(ikan_platform_devices));
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline unsigned int get_flash_base(unsigned int flash_size) {
    if(flash_size > ((1 << 20) * 16))
        return 0xA0000000 - ((1 << 20) * 32);
    return 0xA0000000 - ((1 << 20) * 16);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ikan_ram_mtd_set_rw(struct device *pdev, int mode) {
    if(mode == PLATRAM_RO) {
        printk(KERN_ERR "[ikan_ram_mtd_set_rw] PLATRAM_RO\n");
    } else if(mode == PLATRAM_RW) {
        printk(KERN_ERR "[ikan_ram_mtd_set_rw] PLATRAM_RW\n");
    }
}

enum _flash_map_enum {
    MAP_UNKNOWN,
    MAP_RAM,
    MAP_FLASH
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ikan_squashfs_parser_function(struct mtd_info *mtd, struct mtd_partition **p_mtd_pat, unsigned long param) {
    enum _flash_map_enum maptype = MAP_UNKNOWN;
    unsigned count = 1, maxcount = 0;
    
    printk("[ikan_squashfs_parser_function] mtd_info->name %s mtd_info->index %u param=%lu p_mtd_pat=0x%p\n", mtd->name, mtd->index, param, p_mtd_pat);
    
    if (!strcmp(mtd->name, "ram-filesystem")) {
        maptype = MAP_RAM;
    } else if (!strcmp(mtd->name, "physmap-flash.1")) {
        maptype = MAP_FLASH;
        flash_erase_block_size = mtd->erasesize;
    } else {
        printk(KERN_WARNING "[ikan_squashfs_parser_function] with unknown mtd type %s\n", mtd->name);
        return 0;
    }

    if(p_mtd_pat) {
        unsigned int magic = 0, readlen = 0;
        char* p;
        unsigned int jffs2_size, jffs2_env_size;
        loff_t pos, start_offset;

        if(*p_mtd_pat) 
            printk("[ikan_squashfs_parser_function] *p_mtd_pat->name %s\n", (*p_mtd_pat)->name);

        switch (maptype) {
            case MAP_FLASH:
                if(*p_mtd_pat == NULL) {
                    *p_mtd_pat = ikan_partitions;
                }
                maxcount = ARRAY_SIZE(ikan_partitions);
                break;
            case MAP_RAM:
                if(*p_mtd_pat == NULL) {
                    *p_mtd_pat = ikan_ram_partitions;
                }
                maxcount = ARRAY_SIZE(ikan_ram_partitions);
                break;
            default:
                break;
        }
        {
            int i;
            for(i = 0 ; i < maxcount ; i++)
                printk("[mtd%d] %20s: 0x%08x - 0x%08x (size 0x%x)\n",
                        i,
                        (*p_mtd_pat)[i].name,
                        (*p_mtd_pat)[i].offset,
                        (*p_mtd_pat)[i].offset + (*p_mtd_pat)[i].size,
                        (*p_mtd_pat)[i].size);
        }

        printk("[ikan_squashfs_parser_function] try partition %s (offset 0x%x len %u blocksize=%x)\n", 
                (*p_mtd_pat)[count].name,
                (*p_mtd_pat)[count].offset,
                (*p_mtd_pat)[count].size,
                mtd->erasesize);

        start_offset = pos = (*p_mtd_pat)[count].offset;
        while(pos < (*p_mtd_pat)[1].offset + (*p_mtd_pat)[count].size) {
            mtd->read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&magic);
            /*--- printk("[ikan_squashfs_parser_function] read %u bytes, magic = 0x%08x index %u pos 0x%x\n", readlen, magic, mtd->index, (unsigned int)pos); ---*/
            if(magic == 0x73717368) {
                /*-------------------------------------------------------------------------------------*\
                 *
                 *    +---+---------------------+-----------------------+--------------------+
                 *    |   |     Kernel          |     SquashFS          |      JFFS2         |
                 *    +---+---------------------+-----------------------+--------------------+
                 *        A                     ^_pos                                        E
                 *
                 *    Zu Beginn ist das Layout obiges:
                 *    start_offset = A
                 *    MTD1 mit Kernel reicht von A bis E
                 *    MTD5 f�r JFFS2 kann gesetzt sein, wenn JFFS2 Parser vorher schon was gefunden hat
                 *
                 *    Wenn SquashFS gefunden wird, wird MTD1 auf den Kernel verkleinert,
                 *    MTD0 f�r das FS wird von pos bis E angelegt
                 *    Wenn noch kein MTD5 mit JFFS2 existiert wird dieses innerhalb von MTD0 angelegt
                 *
                \*-------------------------------------------------------------------------------------*/
                (*p_mtd_pat)[0].offset = pos;
                (*p_mtd_pat)[0].size	 = (u_int32_t)start_offset + (u_int32_t)(*p_mtd_pat)[1].size - (u_int32_t)pos;
                (*p_mtd_pat)[0].name	 = "rootfs";
                (*p_mtd_pat)[1].size	 = (u_int32_t)pos - (u_int32_t)start_offset;
                (*p_mtd_pat)[1].name     = "kernel";
                printk("[ikan_squashfs_parser_function] magic found @pos 0x%x\n", (unsigned int)pos);
                if ((maptype == MAP_FLASH) && (memcmp(ikan_partitions[5].name, "jffs2", 4) != 0)) {
                    /* JFFS2 nicht gefunden: Wenn jffs2_size gesetzt ist, ggf. verkleinern */
                    /* sonst anlegen mit der verbleibenden Flash Gr�sse nach Filesystem % 64k */
                    u_int32_t   jffs2_size, jffs2_start, jffs2_earliest_start;
                    struct squashfs_super_block squashfs_sb;

                    p = prom_getenv((char*)"jffs2_size");
                    /*--- printk("jffs2_size not set\n"); ---*/
                    mtd->read(mtd, (loff_t)pos, sizeof(struct squashfs_super_block), &readlen, (u_char*)&squashfs_sb);
                    jffs2_earliest_start = (u_int32_t)pos + (u_int32_t)squashfs_sb.bytes_used;
                    /*--- printk("squashfs pos: %x\n", (u_int32_t)pos); ---*/
                    /*--- printk("squashfs size: %x\n", (u_int32_t)squashfs_sb.bytes_used); ---*/
                    /*--- printk("jffs2_start (squashfs pos + len) = %x\n", (u_int32_t)jffs2_earliest_start); ---*/
                    if (jffs2_earliest_start & (mtd->erasesize-1)) {
                        /*--- printk("align jffs: start: %x\n", jffs2_earliest_start); ---*/
                        jffs2_earliest_start = (jffs2_earliest_start & ~(mtd->erasesize-1)) + mtd->erasesize;
                    }
                    /*--- printk("jffs2_earliest_start (aligned) = %x\n", jffs2_earliest_start); ---*/
                    jffs2_size = ((*p_mtd_pat)[0].offset + (*p_mtd_pat)[0].size - jffs2_earliest_start) >> 16;
                    /* jffs2_size in 64k Bl�cken. Muss ggf. um 1 veringert werden f�r 128k Block Flash */
                    /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/
                    jffs2_size = jffs2_size & ~((mtd->erasesize / 0x10000)-1);
                    /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/
                    if (jffs2_size < (JFFS2_MIN_SIZE * (mtd->erasesize/0x10000))) {
                        printk(KERN_WARNING "[ikan_squashfs_parser_function]: not enough space for JFFS2!\n");
                    } else {
                        char *p;
                        int flashsize;
                        p = prom_getenv((char*)"flashsize");
                        if(p) {
                            flashsize = (unsigned int)simple_strtoul(p, NULL, 16);
                        } else {
                            flashsize = 0x800000;
                        }
                        printk("[ikan_squashfs_parser_function] flashsize=%x\n", flashsize);
                        if ((flashsize <= 0x800000) && (jffs2_size > (JFFS2_MIN_SIZE * (mtd->erasesize/0x10000)))) {
                            /* F�r 7270 und W920V mit nur 8MB Flash das JFFS2 auf Minimalgr��e halten/verringern */
                            jffs2_start = jffs2_earliest_start + ((jffs2_size - JFFS2_MIN_SIZE) * mtd->erasesize);
                            jffs2_size = JFFS2_MIN_SIZE * (mtd->erasesize/0x10000);
                        } else {
                            jffs2_start = jffs2_earliest_start;
                        }
                        while(jffs2_start & (mtd->erasesize - 1)) {
                            printk("[vx180:mtd:jffs2:erase] jffs2_start not on erase boundary add 64KBytes\n");
                            jffs2_start += 0x10000;
                        }
                        ikan_partitions[5].offset = jffs2_start;
                        ikan_partitions[5].size   = jffs2_size * 0x10000;
                        ikan_partitions[5].name   = "jffs2";
                        printk("[ikan_squashfs_parser_function] jffs2_start@%x size: %d\n", jffs2_start, jffs2_size); 
#if 0
                        {
                            struct erase_info instr;
                            int ret;
                            int i;
                            int blocks = mtd->erasesize == 0x10000 ? jffs2_size : jffs2_size >> 1;
                            for(i = 0 ; i < blocks ; i++) {
                                static unsigned char tmp_buffer[4096];
                                unsigned int ii, tmp_readlen = sizeof(tmp_buffer);
                                for(ii = 0 ; ii < mtd->erasesize / sizeof(tmp_buffer) ; ii++) {
                                    unsigned int iii;
                                    mtd->read(mtd, (loff_t)jffs2_start + i * mtd->erasesize + ii * sizeof(tmp_buffer), 
                                            sizeof(tmp_buffer), &tmp_readlen, tmp_buffer);
                                    for(iii = 0 ; iii < tmp_readlen ; iii++) {
                                        if(tmp_buffer[iii] != 0xFF) {
                                            goto need_to_erase_block;
                                        }
                                    }
                                }
                                continue;
need_to_erase_block:

                                memset(&instr, 0, sizeof(instr));
                                instr.mtd = mtd;
                                instr.addr = jffs2_start + i * mtd->erasesize;
                                instr.len = mtd->erasesize;
                                instr.callback = NULL;
                                instr.fail_addr = 0xffffffff;

                                printk("[vx180:mtd:jffs2:erase] erase from 0x%x to 0x%x\n", instr.addr, instr.addr + instr.len);
                                ret = mtd->erase(mtd, &instr);
                                if (ret) {
                                    printk(KERN_ERR "jffs mtd erase failed %d\n", ret);
                                    break;
                                }
                            }
                        }
#endif
                    }
                }
                return maxcount;
            }
            pos += 256;
        }
        
    }
    return 0;
}

#define JFFS_NODES ( JFFS2_NODETYPE_DIRENT | JFFS2_NODETYPE_INODE | JFFS2_NODETYPE_CLEANMARKER | JFFS2_NODETYPE_PADDING | JFFS2_NODETYPE_SUMMARY | JFFS2_NODETYPE_XATTR | JFFS2_NODETYPE_XREF) 
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ikan_jffs2_parser_function(struct mtd_info *mtd, struct mtd_partition **p_mtd_pat, unsigned long param) {
    enum _flash_map_enum maptype = MAP_UNKNOWN;
    unsigned count = 1, maxcount = 0;
    /*--- static unsigned int erasesize = 0; ---*/
    
    printk("[ikan_jffs2_parser_function] mtd_info->name %s mtd_info->index %u param=%lu p_mtd_pat=0x%p\n", mtd->name, mtd->index, param, p_mtd_pat);
    
    if (!strcmp(mtd->name, "ram-filesystem")) {
        maptype = MAP_RAM;
        /*--- if(erasesize) { ---*/
            /*--- printk(KERN_WARNING "[ikan_jffs2_parser_function] set mtd-ram erase size from 0x%x to 0x%x\n", mtd->erasesize, erasesize); ---*/
            /*--- mtd->erasesize = erasesize; ---*/
        /*--- } ---*/
    } else if (!strcmp(mtd->name, "physmap-flash.1")) {
        /*--- erasesize = mtd->erasesize; ---*/
        maptype = MAP_FLASH;
    } else {
        printk(KERN_WARNING "[ikan_jffs2_parser_function] with unknown mtd type %s\n", mtd->name);
        return 0;
    }

    if(p_mtd_pat) {
        unsigned int magic = 0, readlen = 0;
        loff_t pos;
        if(*p_mtd_pat) 
            printk("[ikan_jffs2_parser_function] *p_mtd_pat->name %s\n", (*p_mtd_pat)->name);

        switch (maptype) {
            case MAP_FLASH:
                if(*p_mtd_pat == NULL) {
                    *p_mtd_pat = ikan_partitions;
                }
                maxcount = ARRAY_SIZE(ikan_partitions);
                break;
            case MAP_RAM:
                count = 2;
                if(*p_mtd_pat == NULL) {
                    *p_mtd_pat = ikan_ram_partitions;
                }
                maxcount = ARRAY_SIZE(ikan_ram_partitions);
                /*--- return 0; ---*/   /* nicht im RAM suchen */
                break;
            default:
                break;
        }

        printk("[ikan_jffs2_parser_function] try partition %s (offset 0x%x len %u)\n", 
                (*p_mtd_pat)[count].name,
                (*p_mtd_pat)[count].offset,
                (*p_mtd_pat)[count].size);

        pos = (*p_mtd_pat)[count].offset;
        while(pos < (*p_mtd_pat)[count].offset + (*p_mtd_pat)[count].size) {
            mtd->read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&magic);
            /*--- printk("[ikan_jffs2_parser_function] read %u bytes, magic = 0x%08x index %u pos 0x%x\n", readlen, magic, mtd->index, pos); ---*/
            if ((((magic >> 16) & ~JFFS_NODES) == 0) && ((magic & 0xFFFF) == JFFS2_MAGIC_BITMASK)) {
                switch (maptype) {
                    case MAP_FLASH:
                        (*p_mtd_pat)[5].size	 = (*p_mtd_pat)[1].offset + (*p_mtd_pat)[1].size - pos;
                        (*p_mtd_pat)[5].offset   = pos;
                        (*p_mtd_pat)[5].name	 = "jffs2";
                        /*--- printk("mtd1: size %d\n", (*p_mtd_pat)[1].size); ---*/
                        printk("[ikan_jffs2_parser_function] magic %04x found @pos 0x%x, size %d\n", magic, (unsigned int)pos, (*p_mtd_pat)[5].size);
                        break;
                    case MAP_RAM:
                        (*p_mtd_pat)[2].size	 = (*p_mtd_pat)[count].offset + (*p_mtd_pat)[count].size - pos;
                        (*p_mtd_pat)[2].offset   = pos;
                        (*p_mtd_pat)[2].name	 = "ram-jffs2";
                        /*--- printk("mtd1: size %d\n", (*p_mtd_pat)[1].size); ---*/
                        printk("[ikan_jffs2_parser_function] magic %04x found @pos 0x%x, size %d\n", magic, (unsigned int)pos, (*p_mtd_pat)[2].size);
                        break;
                    default:
                        break;
                }
                return 0;
            }
            pos += mtd->erasesize;
        }
    }
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int get_erase_block_size_on_ram_device(struct mtd_info *mtd) {
    unsigned int readlen = 0;
    unsigned int pos = 0;
    unsigned int value1, value2;

    mtd->read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value1);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- printk("[get_erase_block_size_on_ram_device] name=%s pos=0x%x value=0x%x\n", mtd->name, pos, value1); ---*/

    pos += 0x10000;
    mtd->read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- printk("[get_erase_block_size_on_ram_device] name=%s pos=0x%x value2=0x%x\n", mtd->name, pos, value2); ---*/

    if(value1 == value2) {
        pos += 0x10000;
        mtd->read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
        if(readlen != sizeof(unsigned int))
            return 0;
        /*--- printk("[get_erase_block_size_on_ram_device] name=%s pos=0x%x value2=0x%x (check)\n", mtd->name, pos, value2); ---*/

        if(value1 == value2) {
            printk("[get_erase_block_size_on_ram_device] eraseblocksize=0x10000\n");
            return 0x10000;
        }
        return 0;
    }

    pos += 0x10000;
    mtd->read(mtd, (loff_t)pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- printk("[get_erase_block_size_on_ram_device] name=%s pos=0x%x value2=0x%x\n", mtd->name, pos, value2); ---*/

    if(value1 == value2) {
        printk("[get_erase_block_size_on_ram_device] eraseblocksize=0x20000\n");
        return 0x20000;
    }
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct mtd_part_parser ikan_squashfs_parser = {
	.name     = "find_squashfs",
	.parse_fn = ikan_squashfs_parser_function
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct mtd_part_parser ikan_jffs2_parser = {
	.name     = "find_jffs2",
	.parse_fn = ikan_jffs2_parser_function
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ikan_mtd_add_notifier(struct mtd_info *mtd) {
    printk("[ikan_mtd_add_notifier] name %s\n", mtd->name);
    if(!strcmp(mtd->name, "rootfs")) {
        extern struct mtd_info *mtd_table[MAX_MTD_DEVICES];
        unsigned int i;
        printk("[ikan_mtd_add_notifier] use %s\n", mtd->name);
        for(i = 0 ; i < MAX_MTD_DEVICES ; i++) {
            if(mtd_table[i] == mtd) {
                extern int __init root_dev_setup(char *line);
                static char root_device[64];
                strcpy(root_device, "/dev/mtdblock_");
                *strchr(root_device, '_') = '0' + i;
                printk("[ikan_mtd_add_notifier] root device: %s (%s)\n", root_device, mtd_table[i]->name);
                root_dev_setup(root_device);
                return;
            } else {
                printk("[ikan_mtd_add_notifier] %s is not my root device\n", 
                        mtd_table[i] ? mtd_table[i]->name : "<NULL>");
            }
        }
    } else if(!strcmp(mtd->name, "ram-jffs2")) {
        mtd->erasesize = get_erase_block_size_on_ram_device(mtd);
        if(mtd->erasesize == 0)
            mtd->erasesize = flash_erase_block_size;
        printk("[ikan_mtd_add_notifier] %s: set erasesize to 0x%x\n", mtd->name, flash_erase_block_size);
    } else if(!strcmp(mtd->name, "ram-filesystem")) {
        mtd->erasesize = get_erase_block_size_on_ram_device(mtd);
        if(mtd->erasesize == 0)
            mtd->erasesize = flash_erase_block_size;
        printk("[ikan_mtd_add_notifier] %s: set erasesize to 0x%x\n", mtd->name, flash_erase_block_size);
    } else {
        printk("[ikan_mtd_add_notifier] skip %s\n", mtd->name);
    }
}

void ikan_mtd_rm_notifier(struct mtd_info *mtd) {
    printk("[ikan_mtd_rm_notifier] ignore %s\n", mtd->name);
}

struct mtd_notifier ikan_mtd_notifier = {
    add: ikan_mtd_add_notifier,
    remove: ikan_mtd_rm_notifier
	/*--- void (*remove)(struct mtd_info *mtd); ---*/
	/*--- struct list_head list; ---*/
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int __init ikan_mtd_init(void) {
    char *p;
    unsigned int flashsize = 0;
    struct _my_mtd {
        unsigned int start, end;
        char *Name;
    } mtd[MAX_MTD_DEVICES];
    unsigned int i;
     
    printk("[ikan_mtd_init]\n");
    p = prom_getenv((char*)"flashsize");
    if(p) {
        flashsize = (unsigned int)simple_strtoul(p, NULL, 16);
        printk("[ikan_mtd_init] flashsize = %s Byte %u MBytes\n", p, flashsize >> 20);
        ikan_flash_resource[0].start = get_flash_base(flashsize) & ~0xE0000000;
        ikan_flash_resource[0].end   = ikan_flash_resource[0].start + flashsize;
        /*--- printk("redurce flashsize to 512 KByte\n"); ---*/
        /*--- flashsize = 512 * 1024; ---*/
        /*--- ikan_flash_resource.end = ikan_flash_resource.start + flashsize; ---*/
    }
    memset(&mtd[0], 0, sizeof(mtd));

    for(i = 0 ; i < MAX_FLASH_MTD ; i++) {
        switch(i) {
            case 0:
                p = prom_getenv("mtd0");
                mtd[i].Name = (char*)"filesystem";
                break;
            case 1:
                p = prom_getenv("mtd1");
                mtd[i].Name = (char*)"kernel";
                break;
            case 2:
                p = prom_getenv("mtd2");
                mtd[i].Name = (char*)"urlader";
                break;
            case 3:
                p = prom_getenv("mtd3");
                mtd[i].Name = (char*)"tffs (1)";
                break;
            case 4:
                p = prom_getenv("mtd4");
                mtd[i].Name = (char*)"tffs (2)";
                break;
            case 5:
                p = prom_getenv("mtd0");
                mtd[i].Name = (char*)"reserved";
                break;
        }
        if(p == NULL)
            continue;
        printk("[ikan_mtd_init] mtd[%u] = %s\n", i, p);
        mtd[i].start  = ((unsigned int)simple_strtoul(p, NULL, 16) & ~0xE0000000);
        if(mtd[i].start > flashsize)
            mtd[i].start -= (get_flash_base(flashsize) & ~0xE0000000);
        p = strchr(p, ',');
        if(p == NULL) {
            mtd[i].start = 0;
            continue;
        }
        p++;
        mtd[i].end  = (unsigned int)simple_strtoul(p, NULL, 16);
        if(mtd[i].end) {
            mtd[i].end &= ~0xE0000000;
            if(mtd[i].end > flashsize) {
                mtd[i].end -= (get_flash_base(flashsize) & ~0xE0000000);
            } else {
                if(mtd[i].end == 0) {
                    mtd[i].end = flashsize;
                }
            }
        }
        printk("[ikan_mtd_init] mtd[%u] = 0x%08x - 0x%08x\n", i, mtd[i].start, mtd[i].end);

    }
    for(i = 0 ; i < MAX_FLASH_MTD ; i++) {
        ikan_partitions[i].name		 = mtd[i].Name;
        ikan_partitions[i].offset	 = mtd[i].start;
        ikan_partitions[i].size		 = mtd[i].end - mtd[i].start;
        ikan_partitions[i].mask_flags = 0;
    }
        {
            int i;
            for(i = 0 ; i < MAX_FLASH_MTD ; i++)
                printk("[ikan_mtd_init] mtd%d: %20s: 0x%08x - 0x%08x (size 0x%x)\n",
                        i,
                        ikan_partitions[i].name,
                        ikan_partitions[i].offset,
                        ikan_partitions[i].offset + ikan_partitions[i].size,
                        ikan_partitions[i].size);
        }

    register_mtd_parser(&ikan_squashfs_parser);
    register_mtd_parser(&ikan_jffs2_parser);
    register_mtd_user(&ikan_mtd_notifier);
    ikan_init_platform_devices();
    return 0;
}
arch_initcall(ikan_mtd_init);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init mtdram_setup(char *p) {
    extern int __init root_dev_setup(char *line);
    printk("[mtdram_setup] str=\"%s\"\n", p);
    if(p) {
        printk("[ikan_mtd_init] mtdram1 %s\n", p);
        ikan_flash_resource[1].start  = (unsigned int)simple_strtoul(p, NULL, 16);
        ikan_flash_resource[1].start &= ~0xE0000000;
        ikan_flash_resource[1].flags  = IORESOURCE_MEM,
        p = strchr(p, ',');
        if(p) {
            p++;
            ikan_flash_resource[1].end  = (unsigned int)simple_strtoul(p, NULL, 16);
            ikan_flash_resource[1].end &= ~0xE0000000;
            /*--- ikan_flash_resource[1].end -= 1; ---*/
        } else {
            ikan_flash_resource[1].start = 0;
        }
        printk("[ikan_mtd_init] mtdram1 0x%08x - 0x%08x\n", ikan_flash_resource[1].start, ikan_flash_resource[1].end );
        ikan_ram_partitions[0].name		 = "filesystem";
        ikan_ram_partitions[0].offset	 = 0;
        ikan_ram_partitions[0].size		 = ikan_flash_resource[1].end - ikan_flash_resource[1].start;
        ikan_ram_partitions[0].mask_flags = MTD_ROM;
        ikan_ram_partitions[1].name		 = "unused";
        ikan_ram_partitions[1].offset	 = 0;
        ikan_ram_partitions[1].size		 = ikan_flash_resource[1].end - ikan_flash_resource[1].start;
        ikan_ram_partitions[1].mask_flags = MTD_ROM;
        ikan_ram_partitions[2].name		 = "extra";
        ikan_ram_partitions[2].offset	 = 0;
        ikan_ram_partitions[2].size		 = ikan_flash_resource[1].end - ikan_flash_resource[1].start;
        ikan_ram_partitions[2].mask_flags = MTD_ROM;
    }
    /*--- register_mtd_user(&ikan_mtd_notifier); ---*/
    /*--- register_mtd_parser(&ikan_squashfs_parser); ---*/
    return 0;
}

__setup("mtdram1=", mtdram_setup);

#endif


