/*
 * Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 2002 MIPS Technologies, Inc.  All rights reserved.
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * UR8 specific setup.
 */
#include <linux/autoconf.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/ioport.h>
#include <linux/tty.h>
#include <linux/console.h>
#include <linux/serial.h>
#include <linux/serial_core.h>

#include <asm/cpu.h>
#include <asm/bootinfo.h>
#include <asm/irq.h>
#include <asm/mips-boards/generic.h>
#include <asm/mips-boards/prom.h>
#include <asm/mips-boards/ur8.h>
#include <asm/mips-boards/ur8int.h>
#include <asm/mach_avm.h>
#include <asm/time.h>
#include <linux/delay.h>


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <asm/mach-ur8/hw_bbif.h>
#include <asm/mach-ur8/hw_boot.h>
#include <asm/mach-ur8/hw_clock.h>
#include <asm/mach-ur8/hw_emif.h>
#include <asm/mach-ur8/hw_gpio.h>
#include <asm/mach-ur8/hw_i2c.h>
#include <asm/mach-ur8/hw_irq.h>
#include <asm/mach-ur8/hw_mdio.h>
#include <asm/mach-ur8/hw_reset.h>
#include <asm/mach-ur8/hw_stack.h>
#include <asm/mach-ur8/hw_timer.h>
#include <asm/mach-ur8/hw_uart.h>
#include <asm/mach-ur8/hw_usb.h>
#include <asm/mach-ur8/hw_vbus.h>

struct _hw_boot *UR8_hw_boot  = (struct _hw_boot *)UR8_DEVICE_CONFIG_BASE;
struct _hw_clock *UR8_hw_clock  = (struct _hw_clock *)UR8_CLOCK_BASE;
struct EMIF_register_memory_map *UR8_EMIF_register_memory_map = (struct EMIF_register_memory_map *)UR8_EMIF_BASE;
struct _hw_gpio *UR8_hw_gpio  = (struct _hw_gpio *)UR8_GPIO_BASE;
/*--- struct _hw_i2c *UR8_hw_i2c = (struct _hw_i2c *)UR8_I2C_BASE; ---*/
struct _irq_hw *UR8_irq_hw = (struct _irq_hw *)UR8_IRQ_CTRL_BASE;
union _hw_non_reset *UR8_hw_non_reset = (union _hw_non_reset *)UR8_RESET_BASE;
struct _timer_hw *UR8_timer_hw[2] = { (struct _timer_hw  *)UR8_TIMER1_BASE, (struct _timer_hw  *)UR8_TIMER2_BASE };
struct _hw_uart *UR8_hw_uart = (struct _hw_uart *)UR8_UART0_BASE;
struct _usb_hw *UR8_usb_hw = (struct _usb_hw *)UR8_USB_BASE;
/*--- struct _usbdma_hw *UR8_usbdma_hw = (struct _usbdma_hw *)UR8_USBDMA_BASE; ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_VLYNQ_SUPPORT)
#include <asm/mach-ur8/hw_vlynq.h>
#include <asm/mips-boards/ur8_vlynq.h>
struct _vlynq_registers *UR8_vlynq_registers = (struct _vlynq_registers *)UR8_VLYNQ0_CTRL_BASE;
#endif /*--- #if defined(CONFIG_VLYNQ_SUPPORT) ---*/

#if defined(CONFIG_PCI)
#include <asm/mach-ur8/hw_pci.h>
#include <asm/mips-boards/ur8_pci.h>
struct _pci_registers *UR8_pci_registers = (struct _pci_registers *)UR8_PCI_CFG_BASE;
#endif /*--- #if defined(CONFIG_PCI) ---*/

/*
 * USB
 */
#if defined(CONFIG_USB_MUSB_HDRC) || defined(CONFIG_USB_MUSB_HDRC_MODULE)

#include <asm/mips-boards/ur8.h>
#include <asm/mips-boards/ur8int.h>
#include <linux/platform_device.h>
#include <linux/dma-mapping.h>
#include <linux/usb/musb.h>


static struct musb_hdrc_platform_data usb_data = {
#if     defined(CONFIG_USB_MUSB_OTG)
    /* OTG requires a Mini-AB connector */
    .mode           = MUSB_OTG,
#elif   defined(CONFIG_USB_MUSB_PERIPHERAL)
    .mode           = MUSB_PERIPHERAL,
#elif   defined(CONFIG_USB_MUSB_HOST)
    .mode           = MUSB_HOST,
#endif
    /* irlml6401 switches 5V */
    .power          = 250,          /* sustains 3.0+ Amps (!) */
    .potpgt         = 4,            /* ~8 msec */

    /* REVISIT multipoint is a _chip_ capability; not board specific */
    .multipoint     = 1,
};

static struct resource usb_resources [] = {
    {
        /* physical address */
        .start          = UR8_USB_BASE,
        .end            = UR8_USB_BASE + 0x5ff,
        .flags          = IORESOURCE_MEM,
    },
    {
        .start          = UR8INT_USB,
        .flags          = IORESOURCE_IRQ,
    },
    {
        .start          = UR8INT_USB_DMA,
        .flags          = IORESOURCE_IRQ,
    },
};

static u64 usb_dmamask = DMA_32BIT_MASK;

static struct platform_device usb_dev = {
    .name           = "musb_hdrc",
    .id             = -1,
    .dev = {
        .platform_data      = &usb_data,
        .dma_mask       = &usb_dmamask,
        .coherent_dma_mask      = DMA_32BIT_MASK,
        },
    .resource       = usb_resources,
    .num_resources  = ARRAY_SIZE(usb_resources),
};

static inline void ur8_musb_setup(void) {
    /* REVISIT:  everything except platform_data setup should be
    * shared between all DaVinci boards using the same core.
    */
    int status;

    status = platform_device_register(&usb_dev);
    if (status != 0)
        pr_debug("setup_usb --> %d\n", status);
    else
        //board_setup_psc(DAVINCI_GPSC_ARMDOMAIN, DAVINCI_LPSC_USB, 1);
        printk ("MUSB UR8 pdev active!\n");
}
#endif /* (CONFIG_USB_MUSB_HDRC) || (CONFIG_USB_MUSB_HDRC_MODULE)*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
extern void mips_reboot_setup(void);
extern void mips_time_init(void);
/*--- extern void mips_timer_setup(struct irqaction *irq); ---*/    /* TODO */
extern void ur8_clk_init(void);
extern int  ur8_pci_init(void);

static void __init ur8_serial_init(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
const char *get_system_type(void) {
	return "MIPS UR8";
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- static int __init ur8_setup(void) { ---*/
void __init plat_mem_setup(void) {
	ioport_resource.end = 0x7fffffff;
    /*--- { ---*/
        /*--- unsigned char test[] = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B"; ---*/

        /*--- printk("%% 12B:  % 12B\n", test); ---*/
        /*--- printk("%% 12.2B:  % 12.2B\n", test); ---*/
        /*--- printk("%%# 12.4B:  %# 12.4B\n", test); ---*/
        /*--- printk("little endian %%#- 12.4B:  %#- 12.4B\n", test); ---*/
        /*--- printk("big endian %%#+- 12.4B:  %#+- 12.4B\n", test); ---*/
        /*--- printk("%%-:12B:  %-:12B\n", test); ---*/
    /*--- } ---*/

    ur8_pre_init();
    ur8_reset_init();
    ur8_clk_init();
    ur8_serial_init();
#if defined(CONFIG_VLYNQ_SUPPORT)
    prom_printf("(ur8_vlynq_init)\n");
    ur8_vlynq_init();
#endif /*--- #if defined(CONFIG_VLYNQ_SUPPORT) ---*/
    prom_printf("(ur8_gpio_init)\n");
    ur8_gpio_init();
#if defined(CONFIG_PCI)
	ur8_pci_init();
#endif	
	board_time_init = mips_time_init;
    prom_printf("(mips_reboot_setup)\n");
	mips_reboot_setup();
    ur8_vbus_init();
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void __init ur8_serial_init(void) {
#ifdef CONFIG_SERIAL_8250
	struct uart_port s;

	/*--- prom_printf("[ur8_serial_init]\n"); ---*/
	memset(&s, 0, sizeof(s));

#ifdef CONFIG_CPU_LITTLE_ENDIAN
	/*--- s.membase   = CPHYSADDR(UR8_UART0_BASE); ---*/
	s.membase   = (unsigned char *)CKSEG1ADDR(UR8_UART0_BASE);
#else
	s.membase   = CPHYSADDR(UR8_UART0_BASE+3);
#endif
	s.irq      = UR8INT_UART0;
	s.uartclk  = ur8_get_clock(avm_clock_id_peripheral);
	/*--- s.fifosize = 1; ---*/
	s.fifosize = 16;
	s.x_char   = 'S' - ' ';
	/*--- s.flags    = ASYNC_BOOT_AUTOCONF | ASYNC_SKIP_TEST | ASYNC_AUTO_IRQ; ---*/
	s.flags    = 0;
	s.line     = 0; /*--- erster UART ---*/
	s.type     = PORT_UR8; 
	s.iotype   = UPIO_MEM32;
	s.regshift = 2;

	if (early_serial_setup(&s) != 0) {
		prom_printf("Serial setup failed!\n");
	}	
#endif
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_UR8_CLOCK_SWITCH)
unsigned int ur8_pre_init_system_clock_change(enum _avm_clock_id clock_id, unsigned int new_clk) {
    struct _hw_uart *U = (struct _hw_uart *)UR8_UART0_BASE;
    unsigned int baud = ((new_clk / 2 / 38400) + 8) >> 4;

    /*--- printk("[ur8_pre_init_system_clock_change] called new_clk = %u start\n", new_clk); ---*/

    U->lc.Register |=  (1 << 7);  /* dlab bit setzen */
    U->data.tx.data = baud;
    U->ie.Register  = baud >> 8;
    U->lc.Register &= ~(1 << 7);  /* dlab bit setzen */

    /*--- printk("[ur8_pre_init_system_clock_change] called new_clk = %u done\n", new_clk); ---*/
    return 0;
}

#endif /*--- #if defined(CONFIG_UR8_CLOCK_SWITCH) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int ur8_late_init(void) {

#if defined(CONFIG_UR8_CLOCK_SWITCH)
    ur8_get_clock_notify(avm_clock_id_system, ur8_pre_init_system_clock_change);
#endif /*--- #if defined(CONFIG_UR8_CLOCK_SWITCH) ---*/

#if defined (CONFIG_USB_MUSB_HDRC) || defined(CONFIG_USB_MUSB_HDRC_MODULE)
    ur8_musb_setup();
#endif /* (CONFIG_USB_MUSB_HDRC) ||(CONFIG_USB_MUSB_HDRC_MODULE)*/

    return 0;
}

late_initcall(ur8_late_init);

/*------------------------------------------------------------------------------------------*\
 *
 *  GPIO  0(0x00): USB_5V_ENABLE
 *  GPIO  1(0x01): LED5 POWER_DSL_LED
 *  GPIO  2(0x02): JEMU1
 *  GPIO  3(0x03): TDMRD 
 *  GPIO  4(0x04): TDMCLK
 *  GPIO  5(0x05): TDMFS
 *  GPIO  6(0x06): TDMTD
 *  GPIO  7(0x07): UART_TXD (DSP)
 *  GPIO  8(0x08): UART_RXD (DSP)
 *  GPIO  9(0x09): LED4 USB/LAN_LED VOIP_LED
 *  GPIO 10(0x0A): SPICS0
 *  GPIO 11(0x0B): SPICS1
 *  GPIO 12(0x0C): SPICLK
 *  GPIO 13(0x0D): SPITX
 *  GPIO 14(0x0E): SPIRX
 *  GPIO 15(0x0F): UART_RXD (MIPS)
 *  GPIO 16(0x10): UART_TXD (MIPS)
 *  GPIO 17(0x11): LED1 INFO_LED
 *  GPIO 18(0x12): UART_RXD (DECT)
 *  GPIO 19(0x13): UART_TXD (DECT)
 *  GPIO 20(0x14): DECT_RESET
 *  GPIO 21(0x15): LED_ROT5
 *  GPIO 22(0x16): LED_ROT4
 *  GPIO 23(0x17): WLAN_ONOFF
 *  GPIO 24(0x18): 40V_ENABLE
 *  GPIO 25(0x19): DECT_TASTER
 *  GPIO 26(0x1A): free
 *  GPIO 27(0x1B): LED3 FON_LED
 *  GPIO 28(0x1C): LED2 WLAN_LED
 *  GPIO 29(0x1D): free
 *  GPIO 30(0x1E): free
 *  GPIO 31(0x1F): Testpunkt
 *  GPIO 32(0x20): LED_ROT3
 *  GPIO 33(0x21): free
 *  GPIO 34(0x22): free
 *  GPIO 35(0x23): LED_ROT1
 *  GPIO 36(0x24): LED_ROT2
 *  GPIO 37(0x25): FPGA_INTR ENET_INTR
 *  GPIO 38(0x26): WLAN_RESET
 *  GPIO 39(0x27): PROG_B
 *  GPIO 40(0x28): free
 *  GPIO 41(0x29): DONE
 *  GPIO 42(0x2A): free
 *  GPIO 43(0x2B): free

\*------------------------------------------------------------------------------------------*/
// Aufruf in init des Kernels
//
static struct _pci_pin_config {
   struct resource pci_resource;
   unsigned int gpio;
   unsigned int output;
   unsigned int function;
   char *Name;
} pci_pin_config[] = {
    {
        gpio: _GPIO_BIT_UARTA_TD,
        Name: "MIPS uarta_td (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_UARTA_RD,
        Name: "MIPS uarta_rd (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_UARTC_TD,
        Name: "DSP uartc_td (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_UARTC_RD,
        Name: "DSP uartc_rd (function)",
        output: 1,
        function: 1
    },
#if CONFIG_AVM_PCI_DEVICE_COUNT > 0

#if defined (CONFIG_AVM_PCI_REQ_AND_GNT_1)
    {
        gpio: _GPIO_BIT_PREQ1,
        Name: "preq1 (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PGNT1,
        Name: "pgnt1 (function)",
        output: 1,
        function: 1
    },
#else    
    {
        gpio: _GPIO_BIT_PREQ1,
        Name: "preq1 (wlan_radio_reset)",
        output: 1,
        function: 0
    },
#endif

#if defined (CONFIG_AVM_PCI_REQ_AND_GNT_2)
    {
        gpio: _GPIO_BIT_PREQ2,
        Name: "preq2 (function)",
        output: 0,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PGNT2,
        Name: "pgnt2 (function)",
        output: 0,
        function: 1
    },
#endif

#if defined (CONFIG_AVM_PCI_INT_B)
    {
        gpio: _GPIO_BIT_PINTB,
        Name: "pintb (function)",
        output: 0,
        function: 1
    },
#endif

#if defined (CONFIG_AVM_PCI_INT_C)
    {
        gpio: _GPIO_BIT_PINTC,
        Name: "pintc (function)",
        output: 1,
        function: 1
    },
#endif

#endif /* CONFIG_AVM_PCI_DEVICE_COUNT > 0 */

#ifdef CONFIG_UR8_EVM 
    {
        gpio: _GPIO_BIT_PREQ1,
        Name: "preq1 (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PGNT1,
        Name: "pgnt1 (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PREQ2,
        Name: "preq2 (function)",
        output: 0,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PGNT2,
        Name: "pgnt2 (function)",
        output: 0,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PINTC,
        Name: "pintc (function)",
        output: 1,
        function: 1
    },
    {
        gpio: _GPIO_BIT_PINTB,
        Name: "pintb (function)",
        output: 0,
        function: 1
    },
#endif /*--- #ifdef CONFIG_UR8_EVM ---*/ 

    {
        Name: NULL
    }
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ur8_pre_init(void) {
    volatile struct _hw_uart *U = (struct _hw_uart *)UR8_UART0_BASE;
    volatile struct _hw_gpio *G = (struct _hw_gpio *)UR8_GPIO_BASE;
    volatile union _hw_non_reset *R = (union _hw_non_reset *)UR8_RESET_BASE;
    volatile struct _hw_clock *C = (struct _hw_clock *)UR8_CLOCK_BASE;
    unsigned int baud = ((ur8_get_clock(avm_clock_id_peripheral) / 38400) + 8) >> 4;
    char *p;
    static struct _pci_pin_config *P;

    prom_printf("[ur8_pre_init] System Clk = %u Hz               \n", ur8_get_clock(avm_clock_id_peripheral));
    

    C->PDCR.Bits.uartp = 0;
    C->PDCR.Bits.gpiop = 0;
    C->PDCR.Bits.pcip = 0;
    C->PDCR.Bits.vlynqp = 1;
    C->PDCR.Bits.spip = 1;
    C->PDCR.Bits.c55xxp = 1;


    R->Bits.uart0_unreset = 1;
    R->Bits.gpio_unreset = 1;
    R->Bits.mcsp_unreset = 1;

    for(P = &pci_pin_config[0] ; P->Name ; P++) {
        P->pci_resource.start = P->gpio;
        P->pci_resource.end   = P->gpio;
        P->pci_resource.name  = P->Name;
        if(request_resource(&gpio_resource, &(P->pci_resource))) {
            printk(KERN_ERR "GPIO %u: already in use, PCI init failed (Name: %s)\n", P->gpio,P->Name);
            continue;
        }
        if(P->gpio < 32) {
            if(P->output == 1) {
                G->Direction.Register &= ~(1 << P->gpio); /* 0 ist output */
            } else {
                G->Direction.Register |=  (1 << P->gpio);
            }
            if(P->function == 1) {
                G->Enable.Register  &= ~(1 << P->gpio); /* 0 ist function */
            } else {
                G->Enable.Register  |=  (1 << P->gpio); /* 1 ist gpio */
            }
        } else {
            if(P->output == 1) {
                G->Direction2.Register &= ~(1 << (P->gpio - 32)); /* 0 ist output */
            } else {
                G->Direction2.Register |=  (1 << (P->gpio - 32));
            }
            if(P->function == 1) {
                G->Enable2.Register  &= ~(1 << (P->gpio - 32)); /* 0 ist function */
            } else {
                G->Enable2.Register  |=  (1 << (P->gpio - 32)); /* 1 ist gpio */
            }
        }
    }

#ifdef CONFIG_UR8_EVM 
//evm
    prom_printf("[kernel for TI-EVM]\n");
    /*--- G->Direction2.Bits.preq1 = 1; ---*/ /*--- input ---*/  // 38
    /*--- G->Enable2.Bits.preq1 = 0; ---*/ /*--- function ---*/  // 38
#else 
//7270

#ifdef PCIPRIO
    p = prom_getenv("wlan_cal");
    if (p && strncmp(p,"disabled",8)) {
	prom_printf("MIPS-VBUS-PRIO level: %x\n",UR8_hw_boot->MIPS_Prio.Bits.prio_level);
    	prom_printf("MIPS-VBUS-PRIO prio_escalation_count: %x\n",UR8_hw_boot->MIPS_Prio.Bits.prio_escalation_count);
    	prom_printf("MIPS-VBUS-PRIO lowest_priority_value_achieveable: %x\n",UR8_hw_boot->MIPS_Prio.Bits.lowest_priority_value_achieveable);
    	prom_printf("MIPS-VBUS-PRIO enable: %x\n",UR8_hw_boot->MIPS_Prio.Bits.enable);
//    UR8_hw_boot->MIPS_Prio.Bits.prio_level=6;
//    UR8_hw_boot->MIPS_Prio.Bits.enable=1;
	
    	prom_printf("CPPI-VBUS-PRIO level: %x\n",UR8_hw_boot->CPPI_Prio.Bits.prio_level);
    	prom_printf("CPPI-VBUS-PRIO prio_escalation_count: %x\n",UR8_hw_boot->CPPI_Prio.Bits.prio_escalation_count);
    	prom_printf("CPPI-VBUS-PRIO lowest_priority_value_achieveable: %x\n",UR8_hw_boot->CPPI_Prio.Bits.lowest_priority_value_achieveable);
    	prom_printf("CPPI-VBUS-PRIO enable: %x\n",UR8_hw_boot->CPPI_Prio.Bits.enable);
    	UR8_hw_boot->CPPI_Prio.Bits.prio_level=2;   
    	UR8_hw_boot->CPPI_Prio.Bits.enable=1;

	prom_printf("BufferManager-VBUS-PRIO level: %x\n",UR8_hw_boot->BufferManager_Prio.Bits.prio_level);
    	prom_printf("BufferManager-VBUS-PRIO prio_escalation_count: %x\n",UR8_hw_boot->BufferManager_Prio.Bits.prio_escalation_count);
    	prom_printf("BufferManager-VBUS-PRIO lowest_priority_value_achieveable: %x\n",UR8_hw_boot->BufferManager_Prio.Bits.lowest_priority_value_achieveable);
    	prom_printf("BufferManager-VBUS-PRIO enable: %x\n",UR8_hw_boot->BufferManager_Prio.Bits.enable);
    	UR8_hw_boot->BufferManager_Prio.Bits.prio_level=1;
    	UR8_hw_boot->BufferManager_Prio.Bits.enable=1;

    	prom_printf("PCI-VBUS-PRIO level: %x\n",UR8_hw_boot->PCI_Prio.Bits.prio_level);
    	prom_printf("PCI-VBUS-PRIO prio_escalation_count: %x\n",UR8_hw_boot->PCI_Prio.Bits.prio_escalation_count);
    	prom_printf("PCI-VBUS-PRIO lowest_priority_value_achieveable: %x\n",UR8_hw_boot->PCI_Prio.Bits.lowest_priority_value_achieveable);
    	prom_printf("PCI-VBUS-PRIO enable: %x\n",UR8_hw_boot->PCI_Prio.Bits.enable);
    	UR8_hw_boot->PCI_Prio.Bits.prio_level=1;
    	UR8_hw_boot->PCI_Prio.Bits.enable=1;

//    UR8_hw_boot->SAR_Prio.Bits.prio_level=6;
//    UR8_hw_boot->SAR_Prio.Bits.enable=1;

//    UR8_hw_boot->USB_Prio.Bits.prio_level=6;
//    UR8_hw_boot->USB_Prio.Bits.enable=1;

//    UR8_hw_boot->VLYNQ_Prio.Bits.prio_level=6;
//    UR8_hw_boot->VLYNQ_Prio.Bits.enable=1;

    
//    UR8_hw_boot->C55_Prio.Bits.prio_level=6;
//    UR8_hw_boot->C55_Prio.Bits.enable=1;
    
//    UR8_hw_boot->SBRM_Prio.Bits.prio_level=6;
//    UR8_hw_boot->SBRM_Prio.Bits.enable=1;
    }
#endif
#endif//EVM

#if 0
    G->Direction2.Bits.preq1 = 0; /*--- output ---*/  // 38
    G->Enable2.Bits.preq1 = 1; /*--- gpio ---*/  // 38

    /*--- prom_printf("Before: Output->Preq1: %i\n",G->OutputData2.Bits.preq1); ---*/
    /*--- prom_printf("Before: Input->Preq1: %i\n",G->InputData2.Bits.preq1); ---*/

    udelay(2000);
    G->OutputData2.Bits.preq1 = 1; /*--- enable ---*/  // 38
#endif


#if 0
    G->Direction2.Bits.preq2 = 1; /*--- input ---*/   
    G->Enable2.Bits.preq2 = 0; /*--- function ---*/   

    G->Direction.Bits.pintc = 0; /*--- output ---*/
    G->Enable.Bits.pintc = 0;  /*--- function ---*/

    G->Direction.Bits.pgnt1 = 0; /*--- output ---*/
    G->Enable.Bits.pgnt1 = 0;  /*--- function ---*/
    G->Direction.Bits.pgnt2 = 0; /*--- output ---*/
    G->Enable.Bits.pgnt2 = 0;  /*--- function ---*/
    G->Direction.Bits.pintb = 0; /*--- output ---*/
    G->Enable.Bits.pintb = 0;  /*--- function ---*/
#endif

/*
    printk(KERN_ERR "%s: resetting wlan radio\n",__func__);
    G->OutputData2.Bits.preq1=0;
    mdelay(4);
    G->OutputData2.Bits.preq1=1;
*/
    
    U->lc.Bits.ws = 3;   /*--- 8 Bit ---*/
    U->lc.Bits.dlab = 1;
    U->data.tx.data = baud;
    U->ie.Register = baud >> 8;
    U->lc.Bits.dlab = 0;

	board_time_init = mips_time_init;
	/*--- board_timer_setup = mips_timer_setup; ---*/   /* TODO */

    memset((char *)0xA0000000, 0, 0x4096);  /*--- internal ram loeschen ---*/
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #define DEBUG_WLAN_DECT_CONFIG ---*/
#if defined(DEBUG_WLAN_DECT_CONFIG)
#define PRINTK(args...)  printk(args)
#else
#define PRINTK(args...)
#endif

static unsigned int wlan_dect_config[UR8_MAX_CONFIG_ENTRIES];
void __init set_wlan_dect_config_address(unsigned int *pConfig) {

    int i = 0;

    while (pConfig[i]) {
        wlan_dect_config[i] = pConfig[i];
#if defined(DEBUG_WLAN_DECT_CONFIG)
        prom_printf("[set_wlan_dect_config] pConfig[%d] 0x%x\n", i, wlan_dect_config[i]);
#endif
        i++;
    }

}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned short len) {

    int i;
    struct wlan_dect_config config;

    PRINTK("[get_wlan_dect_config] buffer 0x%p len %d\n", buffer , len);

    for (i=0;i<UR8_MAX_CONFIG_ENTRIES;i++) {
        PRINTK("[get_wlan_dect_config] wlan_dect_config[%d] 0x%x\n", i , wlan_dect_config[i]);
        if (wlan_dect_config[i]) {    /*--- Eintrag vorhanden und nicht leer ---*/
            memcpy(&config, (char *)wlan_dect_config[i], sizeof(struct wlan_dect_config));
            switch (config.Version) {
	        case 1:
                case 2:
                    PRINTK("[get_wlan_dect_config] Type %d Len 0x%x\n", config.Type, config.Len);
                    if (Type != config.Type) {
                        break;                      /*--- n�chster Konfigeintrag ---*/
                    }
                    if (!(len >= config.Len + sizeof(struct wlan_dect_config)))
                        return -2;                  /*--- buffer zu klein ---*/
                    PRINTK("[get_wlan_dect_config] read ");
                    switch (config.Type) {
                        case WLAN:
                        case WLAN2:
                            PRINTK("WLAN\n");
                            memcpy(buffer, (char *)wlan_dect_config[i], config.Len + sizeof(struct wlan_dect_config));
#if defined(DEBUG_WLAN_DECT_CONFIG)
                            {
                                int x;
                                for (x=0;x<(config.Len+sizeof(struct wlan_dect_config));x++)
                                    printk("0x%x ", buffer[x]);
                                printk("\n");
                            }
#endif
                            return 0;
                        case DECT:
                            PRINTK("DECT\n");
                            memcpy(buffer, (char *)wlan_dect_config[i], config.Len + sizeof(struct wlan_dect_config));
#if defined(DEBUG_WLAN_DECT_CONFIG)
                            {
                                int x;
                                for (x=0;x<(config.Len+sizeof(struct wlan_dect_config));x++)
                                    printk("0x%x ", buffer[x]);
                                printk("\n");
                            }
#endif
                            return 0;
                        default:
                            PRINTK("Type unknown\n");
                            return -3;
                    }
                    break;
                default:
                    PRINTK("[get_wlan_dect_config] unknown Version %x\n", config.Version);
                    return -3;
            }
        }
    }
    return -1;
}

EXPORT_SYMBOL(set_wlan_dect_config_address);
EXPORT_SYMBOL(get_wlan_dect_config);



/*--- Kernel-Schnittstelle f�r das neue LED-Modul ---*/
enum _led_event { /* DUMMY DEFINITION */ LastEvent = 0 };
int (*led_event_action)(int, enum _led_event , unsigned int ) = NULL;
EXPORT_SYMBOL(led_event_action);

