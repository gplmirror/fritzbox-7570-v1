/*
 * Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 2002 MIPS Technologies, Inc.  All rights reserved.
 * Copyright (C) 2003 Ralf Baechle (ralf@linux-mips.org)
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * Routines for generic manipulation of the interrupts found on the MIPS
 * Sead board.
 */
#include <linux/init.h>
#include <linux/irq.h>
#include <asm/irq.h>
#include <linux/interrupt.h>
#include <linux/kernel_stat.h>
#include <linux/module.h>

#include <asm/mips-boards/ur8.h>
#include <asm/mips-boards/ur8int.h>
#include <asm/mach_avm.h>
#if defined(CONFIG_VLYNQ_SUPPORT)
#include <asm/mips-boards/ur8_vlynq.h>
#endif /*--- #if defined(CONFIG_VLYNQ_SUPPORT) ---*/

#if defined(CONFIG_PCI)
//#include <asm/mips-boards/ur8_pci.h>
#endif /*--- #if defined(CONFIG_PCI) ---*/

#include <asm/mach-ur8/hw_irq.h>
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
#include <asm-mips/mipsregs.h>
#include <linux/avm_profile.h>
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 

extern irqreturn_t mips_timer_interrupt(int irq, void *dev_id);
extern asmlinkage void handle_int(void);    /* genex.S */
static void __init ur8_init_irq(void);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
asmlinkage void ur8_hw0_irqdispatch(void) {  /* TODO */
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
    struct pt_regs regs;
    regs.cp0_epc = read_c0_epc();
#if 0
    void DO_IRQ(unsigned int i) {
        struct irq_desc *desc = irq_desc + i;
        avm_simple_profiling_log(avm_profile_data_type_hw_irq_begin, (unsigned int)desc, i);
        do_IRQ(i);
        avm_simple_profiling_log(avm_profile_data_type_hw_irq_end, (unsigned int)desc, i);
    }
#else
#define DO_IRQ(i)   do_IRQ(i)
#endif
#else /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 
#define DO_IRQ(i)   do_IRQ(i)
#endif /*--- #else ---*/ /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 

    struct _irq_hw *IRQ = (struct _irq_hw *)UR8_IRQ_CTRL_BASE;
    unsigned int priority_index_reg = IRQ->priority_index_reg;
    unsigned int irq      = (priority_index_reg >> 16) & 0x3F;
    extern int cpu_wait_end(void);
    cpu_wait_end();

    irq += MIPS_EXCEPTION_OFFSET;
    //printk("[ur8_hw0_irqdispatch] irq=%u\n", irq); 
    if(irq == MIPS_EXCEPTION_OFFSET) {
        unsigned int _irq;
        for(_irq = 0 ; _irq < 32 ; _irq++) {
            if(IRQ->exception_clear_status_reg & (1 << _irq))
                break;
        }
        if(_irq != 32) {
            DO_IRQ(_irq);
        }
    }
#if defined(CONFIG_VLYNQ_SUPPORT)
    else if(irq == UR8INT_VLYNQ0) {
        unsigned int vlynq_irq = vlynq_get_irq(0) + UR8_INT_START_VIRTUAL;
        if(vlynq_irq != (unsigned int)-1) {
            DO_IRQ(UR8INT_VLYNQ0);
            DO_IRQ(vlynq_irq);
            kstat_this_cpu.irqs[MIPS_EXCEPTION_OFFSET - 2]++;
            return;
        }
    }
#endif /*--- #if defined(CONFIG_VLYNQ_SUPPORT) ---*/
    kstat_this_cpu.irqs[MIPS_EXCEPTION_OFFSET - 2]++;
    DO_IRQ(irq);

/*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 
    /*--- avm_simple_profiling(&regs, irq); ---*/
/*--- #endif ---*/ /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
asmlinkage void ur8_hw1_irqdispatch(void)   /* TODO */
{
	/*--- do_IRQ(UR8INT_UART1, regs); ---*/
    /*--- struct irq_desc *desc; ---*/
	/*--- int cpu = smp_processor_id(); ---*/
    struct _irq_hw *IRQ = (struct _irq_hw *)UR8_IRQ_CTRL_BASE;
    unsigned int priority_index_reg = IRQ->priority_index_reg;
    unsigned int irq      = (priority_index_reg >> 16) & 0x3F;
    unsigned int chan_nr  =  priority_index_reg & 0x3F;

    printk("[ur8_hw1_irqdispatch] irq=%u chan_nr=%u\n", irq, chan_nr);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static struct _ur8_nsec_timer {
    unsigned long long offset;
    unsigned int last;
    unsigned long clock_faktor;
} nT = { 0ULL, 0U, 0UL };

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned long long ur8_nsec_timer(void) {
    unsigned int count;
    if(nT.clock_faktor == 0) {  /*--- wie viele n-sec pro clock ---*/
        nT.clock_faktor = (unsigned long)(1000000000 / (ur8_get_clock(avm_clock_id_cpu) / 2));
    }
    local_irq_disable();
	count = read_c0_count() * nT.clock_faktor;
    if(count < nT.last)
        nT.offset += 0xFFFFFFFF *  nT.clock_faktor;
	nT.last = count;
    local_irq_enable();
    return (unsigned long long)count + nT.offset;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ur8_timer_interrupt(void) {
    unsigned int count;
	count = read_c0_count();
    if(count < nT.last) {
        nT.offset += ((unsigned long long)1 << 32);
        nT.offset = count;
    }
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
extern void mips_cpu_irq_init(int irq_base);
extern char except_vec3_generic;

void __init arch_init_irq(void)   /* TODO */
{
    /*--- return; ---*/
    ur8_init_irq();
    /*
         * Mask out all interrupt
	*/
	clear_c0_status(0x0000ff00);

	/* Now safe to set the exception vector. */
	/*--- set_except_vector(0, &except_vec3_generic); ---*/       /* in genex.S */
    /*--- set_handler(0x200, &except_vec3_generic, 0x8); ---*/

	mips_cpu_irq_init(0);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
irqreturn_t dummy_timer_irq(int irq, void *context) {
    panic("dummy_timer_irq must not be called\n");
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
irqreturn_t dummy_system_irq_2(int irq, void *context) {
    panic("dummy_system_irq_2 must not be called\n");
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init ur8_install_dummy_irq_functions(void) {
    request_irq(MIPS_EXCEPTION_OFFSET - 7, dummy_timer_irq, 0, "system timer", NULL);
    request_irq(MIPS_EXCEPTION_OFFSET - 2, dummy_system_irq_2, 0, "UR8 primary", NULL);
    return 0;
}

late_initcall(ur8_install_dummy_irq_functions);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void __init ur8_init_irq(void) {
    struct _irq_hw *IRQ = (struct _irq_hw *)UR8_IRQ_CTRL_BASE;
    struct _hw_clock *CLOCK = (struct _hw_clock *)UR8_CLOCK_BASE;
    unsigned int irq;

    IRQ->enable_clear_reg[0]        = 0xFFFFFFFF;
    IRQ->enable_clear_reg[1]        = 0xFFFFFFFF;
    IRQ->status_clear_reg[0]        = 0xFFFFFFFF;
    IRQ->status_clear_reg[1]        = 0xFFFFFFFF;
    /*--- IRQ->enable_set_reg[0]          = ---*/
    /*--- IRQ->enable_set_reg[1]          = ---*/
    /*--- IRQ->polarity_reg[0]            = ---*/
    /*--- IRQ->polarity_reg[1]            = ---*/
    /*--- IRQ->type_reg[0]                = ---*/
    /*--- IRQ->type_reg[1]                = ---*/
    /*--- IRQ->exception_set_status_reg   = ---*/
    IRQ->exception_enable_clear_reg = 0xFFFFFFFF;
    IRQ->exception_clear_status_reg = 0xFFFFFFFF;
    IRQ->priority_mask_index_reg    = 40;
    /*--- IRQ->exception_enable_set_reg   = ---*/
    /*--- IRQ->pacing_prescale_reg        = ---*/
    /*--- IRQ->pacing_map_reg             = ---*/
    /*--- IRQ->pacing_max_reg             = ---*/

    // bk: DSLSS to MIPS interrupt is Active Low. (Active High = 0 ; Active Low = 1)
    IRQ->polarity_reg[0] = 0 | (1 << 28);
    IRQ->polarity_reg[1] = 0;
    IRQ->type_reg[0]     = 0
                           | (1 << 7)
                           | (1 << 8)
                           | (1 << 22)
                           | (1 << 23)
                           | (1 << 24)
			   | (1 << 25)
                           | (1 << 28)
                           | (1 << 29)
                           ;
    IRQ->type_reg[1]     = 0x00000000;

    for(irq = 0 ; irq < sizeof(IRQ->channel_number_reg) / sizeof(IRQ->channel_number_reg[0]) ; irq++) {
        IRQ->channel_number_reg[irq] = irq;
    }

    /*--- printk("[ur8_init_irq] ebase = 0x%x\n", read_c0_ebase()); ---*/
}


/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
static inline int clz(unsigned long x)
{
	__asm__ (
	"	.set	push					\n"
	"	.set	mips32					\n"
	"	clz	%0, %1					\n"
	"	.set	pop					\n"
	: "=r" (x)
	: "r" (x));

	return x;
}


/*-------------------------------------------------------------------------------------*\
 * Version of ffs that only looks at bits 12..15.
\*-------------------------------------------------------------------------------------*/
static inline unsigned int irq_ffs(unsigned int pending)
{
#if defined(CONFIG_CPU_MIPS32) || defined(CONFIG_CPU_MIPS64)
	return -clz(pending) + 31 - CAUSEB_IP;
#else
	unsigned int a0 = 7;
	unsigned int t0;

	t0 = pending & 0xf000;
	t0 = t0 < 1;
	t0 = t0 << 2;
	a0 = a0 - t0;
	pending = pending << t0;

	t0 = pending & 0xc000;
	t0 = t0 < 1;
	t0 = t0 << 1;
	a0 = a0 - t0;
	pending = pending << t0;

	t0 = pending & 0x8000;
	t0 = t0 < 1;
	//t0 = t0 << 2;
	a0 = a0 - t0;
	//pending = pending << t0;

	return a0;
#endif
}

/*
 * IRQs on the Malta board look basically (barring software IRQs which we
 * don't use at all and all external interrupt sources are combined together
 * on hardware interrupt 0 (MIPS IRQ 2)) like:
 *
 *	MIPS IRQ	Source
 *      --------        ------
 *             0	Software (ignored)
 *             1        Software (ignored)
 *             2        Combined hardware interrupt (hw0)
 *             3        Hardware (ignored)
 *             4        Hardware (ignored)
 *             5        Hardware (ignored)
 *             6        Hardware (ignored)
 *             7        R4k timer (what we use)
 *
 * We handle the IRQ according to _our_ priority which is:
 *
 * Highest ----     R4k Timer
 * Lowest  ----     Combined hardware interrupt
 *
 * then we just return, if multiple IRQs are pending then we will just take
 * another exception, big deal.
 */

/*-------------------------------------------------------------------------------------*\
 * generischer IRQ dispatcher, aufgerufen aus genex.S (handle_int)
\*-------------------------------------------------------------------------------------*/
asmlinkage void plat_irq_dispatch(void)
{
	unsigned int pending = read_c0_cause() & read_c0_status() & ST0_IM;
	int irq;
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
    struct pt_regs regs;
    regs.cp0_epc = read_c0_epc();
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 

	irq = irq_ffs(pending);
    /*--- prom_printf("platirq %d\n", irq); ---*/

#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
    avm_simple_profiling(&regs, irq);
    avm_simple_profiling_log(avm_profile_data_type_hw_irq_begin, (unsigned int)(irq_desc + irq), irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 

	if (irq == 2) {
		ur8_hw0_irqdispatch();
    } else if (irq == 5) {
		ur8_hw1_irqdispatch();
    } else if (irq == 7) {
        mips_timer_interrupt(irq, NULL);
    } else {
		spurious_interrupt();
    }
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
    avm_simple_profiling_log(avm_profile_data_type_hw_irq_end, (unsigned int)(irq_desc + irq), irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 
}

EXPORT_SYMBOL (ur8_nsec_timer);
