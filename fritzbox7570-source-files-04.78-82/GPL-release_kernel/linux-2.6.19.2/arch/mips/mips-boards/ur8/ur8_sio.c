/*
 * Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 1999,2000 MIPS Technologies, Inc.  All rights reserved.
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * Putting things on the screen/serial line using YAMONs facilities.
 */

#include <linux/autoconf.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/serial_reg.h>
#include <linux/spinlock.h>
#include <asm/io.h>
#include <asm/mips-boards/ur8.h>
#include <asm/mach-ur8/hw_uart.h>

int prom_putchar(char c) {
    struct _hw_uart *U = (struct _hw_uart *)UR8_UART0_BASE;

    switch(c) {
        case '\n':
            prom_putchar('\r');
            /*--- kein break ---*/
        default:
            while(U->ls.Bits.te == 0)
                ;

            U->data.tx.data = c;
    }
	return 1;
}

char prom_getchar(void) {
    struct _hw_uart *U = (struct _hw_uart *)UR8_UART0_BASE;

    while(U->ls.Bits.rx == 0)
		;

    return U->data.rx.data;
}

#ifdef        CONFIG_DEBUG_LL
void printascii(char *c) {
    while (*c) {
        prom_putchar(*c++);
    }
}
#endif

