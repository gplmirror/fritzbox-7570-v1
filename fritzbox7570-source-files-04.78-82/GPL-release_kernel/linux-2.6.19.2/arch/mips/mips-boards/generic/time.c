/*
 * Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 1999,2000 MIPS Technologies, Inc.  All rights reserved.
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * Setting up the clock on the MIPS boards.
 */

#include <linux/types.h>
#include <linux/init.h>
#include <linux/kernel_stat.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/time.h>
#include <linux/timex.h>
#include <linux/mc146818rtc.h>

#include <asm/mipsregs.h>
#include <asm/mipsmtregs.h>
#include <asm/hardirq.h>
#include <asm/irq.h>
#include <asm/div64.h>
#include <asm/cpu.h>
#include <asm/time.h>
#include <asm/bitops.h>
#include <asm/mc146818-time.h>
#include <asm/msc01_ic.h>

#include <asm/mips-boards/generic.h>
#include <asm/mips-boards/prom.h>
#if defined(CONFIG_MIPS_OHIO)
#include <asm/mips-boards/ohio.h>
#include <asm/mips-boards/ohioint.h>
#include <asm/mach_avm.h>
#elif defined(CONFIG_MIPS_AR7)
#include <asm/mips-boards/ar7.h>
#include <asm/mips-boards/ar7int.h>
#include <asm/mach_avm.h>
#elif defined(CONFIG_MIPS_UR8)
#include <asm/mips-boards/ur8.h>
#include <asm/mips-boards/ur8int.h>
#include <asm/mach_avm.h>
#else
#error unknown CONFIG_MIPS_...
#endif /*--- #if defined(CONFIG_MIPS_UR8) ---*/

unsigned long cpu_khz;

#if defined(CONFIG_MIPS_SEAD) || defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) || defined(CONFIG_MIPS_UR8)
#define ALLINTS (IE_IRQ0 | IE_IRQ1 | IE_IRQ5)
#else
#define ALLINTS (IE_IRQ0 | IE_IRQ1 | IE_IRQ2 | IE_IRQ3 | IE_IRQ4 | IE_IRQ5)
#endif

#if defined(CONFIG_MIPS_ATLAS)
static char display_string[] = "        LINUX ON ATLAS       ";
#endif
#if defined(CONFIG_MIPS_MALTA)
static char display_string[] = "        LINUX ON MALTA       ";
#endif
#if defined(CONFIG_MIPS_SEAD)
static char display_string[] = "        LINUX ON SEAD       ";
#endif
#if defined(CONFIG_MIPS_OHIO)
static char display_string[] = "        LINUX ON OHIO       ";
#endif
#if defined(CONFIG_MIPS_AR7)
static char display_string[] = "        LINUX ON AR7       ";
#endif
#if defined(CONFIG_MIPS_UR8)
static char display_string[] = "        LINUX ON UR8       ";
#endif
static unsigned int display_count;
#define MAX_DISPLAY_COUNT (sizeof(display_string) - 8)

#if defined(CONFIG_MIPS_UR8)
#define MIPS_CPU_TIMER_IRQ 7
#else
#define MIPS_CPU_TIMER_IRQ (NR_IRQS-1)
#endif

#define CPUCTR_IMASKBIT (0x100 << MIPSCPU_INT_CPUCTR)

static unsigned int timer_tick_count;
static int mips_cpu_timer_irq;

#if defined(CONFIG_OHIO_CLOCK_SWITCH) || defined(CONFIG_UR8_CLOCK_SWITCH) || defined(CONFIG_AR7_CLOCK_SWITCH) 
    static unsigned int mips_time_cpu_frequenz_change(enum _avm_clock_id, unsigned int);
#endif

extern void smtc_timer_broadcast(int);

#if 0
static inline void scroll_display_message(void)
{
	if ((timer_tick_count++ % HZ) == 0) {
		mips_display_message(&display_string[display_count++]);
		if (display_count == MAX_DISPLAY_COUNT)
			display_count = 0;
	}
}
#endif

static void mips_timer_dispatch(void)
{
	do_IRQ(mips_cpu_timer_irq);
}

/*
 * Redeclare until I get around mopping the timer code insanity on MIPS.
 */
extern int null_perf_irq(void);

extern int (*perf_irq)(void);

irqreturn_t mips_timer_interrupt(int irq, void *dev_id)
{
    /*--- prom_printf("mips_timer_interrupt\n"); ---*/
    extern int cpu_wait_end(void);
    cpu_wait_end();

#if defined(CONFIG_MIPS_OHIO)
	kstat_this_cpu.irqs[MIPS_EXCEPTION_OFFSET - 7]++;
    ohio_timer_interrupt();
#elif defined(CONFIG_MIPS_AR7)
	kstat_this_cpu.irqs[MIPS_EXCEPTION_OFFSET - 7]++;
    ar7_timer_interrupt();
#elif defined(CONFIG_MIPS_UR8) /*--- #if defined(CONFIG_MIPS_OHIO) ---*/
	kstat_this_cpu.irqs[MIPS_EXCEPTION_OFFSET - 7]++;
    ur8_timer_interrupt();
#endif /*--- #elif ---*/ /*--- #if defined(CONFIG_MIPS_OHIO) ---*/
	ll_timer_interrupt(MIPS_CPU_TIMER_IRQ);
	return IRQ_HANDLED;
}

/*
 * Estimate CPU frequency.  Sets mips_hpt_frequency as a side-effect
 */
static unsigned int __init estimate_cpu_frequency(void)
{
	unsigned int prid = read_c0_prid() & 0xffff00;
	unsigned int count;

#if defined(CONFIG_MIPS_SEAD) || defined(CONFIG_MIPS_SIM)
	/*
	 * The SEAD board doesn't have a real time clock, so we can't
	 * really calculate the timer frequency
	 * For now we hardwire the SEAD board frequency to 12MHz.
	 */

	if ((prid == (PRID_COMP_MIPS | PRID_IMP_20KC)) ||
	    (prid == (PRID_COMP_MIPS | PRID_IMP_25KF)))
		count = 12000000;
	else
		count = 6000000;
#endif
#if defined(CONFIG_MIPS_ATLAS) || defined(CONFIG_MIPS_MALTA)
	unsigned int flags;

	local_irq_save(flags);

	/* Start counter exactly on falling edge of update flag */
	while (CMOS_READ(RTC_REG_A) & RTC_UIP);
	while (!(CMOS_READ(RTC_REG_A) & RTC_UIP));

	/* Start r4k counter. */
	write_c0_count(0);

	/* Read counter exactly on falling edge of update flag */
	while (CMOS_READ(RTC_REG_A) & RTC_UIP);
	while (!(CMOS_READ(RTC_REG_A) & RTC_UIP));

	count = read_c0_count();

	/* restore interrupts */
	local_irq_restore(flags);
#endif
#if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) || defined(CONFIG_MIPS_UR8)
#if defined(CONFIG_OHIO_CLOCK_SWITCH) || defined(CONFIG_UR8_CLOCK_SWITCH) || defined(CONFIG_AR7_CLOCK_SWITCH) 
    count = avm_get_clock_notify(avm_clock_id_cpu, mips_time_cpu_frequenz_change);
#else /*--- #if defined(CONFIG_OHIO_CLOCK_SWITCH) || defined(CONFIG_UR8_CLOCK_SWITCH) || defined(CONFIG_AR7_CLOCK_SWITCH)  ---*/
    count = avm_get_clock(avm_clock_id_cpu);
#endif /*--- #else  ---*//*--- #if defined(CONFIG_OHIO_CLOCK_SWITCH) || defined(CONFIG_UR8_CLOCK_SWITCH) || defined(CONFIG_AR7_CLOCK_SWITCH)  ---*/
	mips_hpt_frequency = count / 2;
#endif

#if !defined(CONFIG_MIPS_OHIO) && !defined(CONFIG_MIPS_AR7) &&  !defined(CONFIG_MIPS_UR8) 
	if ((prid != (PRID_COMP_MIPS | PRID_IMP_20KC)) &&
	    (prid != (PRID_COMP_MIPS | PRID_IMP_25KF)))
		count *= 2;

	mips_hpt_frequency = count;
#endif /*--- #if !defined(CONFIG_MIPS_OHIO) && !defined(CONFIG_MIPS_AR7) &&  !defined(CONFIG_MIPS_UR8)  ---*/

	count += 5000;    /* round */
	count -= count%10000;

	return count;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_OHIO_CLOCK_SWITCH) || defined(CONFIG_AR7_CLOCK_SWITCH) || defined(CONFIG_UR8_CLOCK_SWITCH) 
static unsigned int mips_time_cpu_frequenz_change(enum _avm_clock_id clock_id, unsigned int new_clk) {
    printk("[mips_time_cpu_frequenz_change] change to %u Hz\n", new_clk);
    return 0;
}
#endif /*--- #if defined(CONFIG_OHIO_CLOCK_SWITCH) || defined(CONFIG_AR7_CLOCK_SWITCH) || defined(CONFIG_UR8_CLOCK_SWITCH)  ---*/

#if defined(CONFIG_MIPS_RTC)
unsigned long __init mips_rtc_get_time(void)
{
	return mc146818_get_cmos_time();
}
#endif /*--- #if defined(CONFIG_MIPS_RTC) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init mips_time_init(void)
{
	unsigned int est_freq;

#if defined(CONFIG_MIPS_ATLAS) || defined(CONFIG_MIPS_MALTA)
        /* Set Data mode - binary. */
        CMOS_WRITE(CMOS_READ(RTC_CONTROL) | RTC_DM_BINARY, RTC_CONTROL);
#endif

	est_freq = estimate_cpu_frequency ();

	printk("CPU frequency %d.%02d MHz\n", est_freq/1000000,
	       (est_freq%1000000)*100/1000000);

        cpu_khz = est_freq / 1000;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void __init plat_timer_setup(struct irqaction *irq)
{
    prom_printf("plat_timer_setup TIMER IRQ: %d\n", MIPS_CPU_TIMER_IRQ);
#if 0
	if (cpu_has_veic) {
		set_vi_handler (MSC01E_INT_CPUCTR, mips_timer_dispatch);
		mips_cpu_timer_irq = MSC01E_INT_BASE + MSC01E_INT_CPUCTR;
	}
	else {
		if (cpu_has_vint)
			set_vi_handler (MIPSCPU_INT_CPUCTR, mips_timer_dispatch);
		mips_cpu_timer_irq = MIPSCPU_INT_BASE + MIPSCPU_INT_CPUCTR;
	}
#endif

	/* we are using the cpu counter for timer interrupts */
	irq->handler = no_action;     /* we use our own handler */
	setup_irq(MIPS_CPU_TIMER_IRQ, irq);
	/*--- setup_irq(mips_cpu_timer_irq, irq); ---*/

    /* to generate the first timer interrupt */
	write_c0_compare (read_c0_count() + mips_hpt_frequency/HZ);
	set_c0_status(ALLINTS);
}
