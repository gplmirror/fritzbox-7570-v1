/*
 * Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 1999,2000 MIPS Technologies, Inc.  All rights reserved.
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * PROM library functions for acquiring/using memory descriptors given to
 * us from the YAMON.
 */
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/bootmem.h>
#include <linux/pfn.h>
#include <linux/string.h>
#include <linux/module.h>

#include <asm/bootinfo.h>
#include <asm/page.h>
#include <asm/sections.h>

#include <asm/mips-boards/prom.h>

/*#define DEBUG*/

enum yamon_memtypes {
	yamon_dontuse,
	yamon_prom,
	yamon_free,
};
struct prom_pmemblock mdesc[PROM_MAX_PMEMBLOCKS];

#ifdef DEBUG
static char *mtypes[3] = {
	"Dont use memory",
	"Urlader data memory",
	"YAMON PROM memory",
	"Free memmory",
};
#endif

/* determined physical memory size, not overridden by command line args  */
unsigned long physical_memsize = 0L;

struct prom_pmemblock * __init prom_getmdesc(void)
{
	char *memsize_str;
	unsigned int memsize;
	char cmdline[CL_SIZE], *ptr;
	char *flashsize_str;
	unsigned int flashsize;

	/* otherwise look in the environment */
	memsize_str = prom_getenv("memsize");
	if (!memsize_str) {
		prom_printf("memsize not set in boot prom, set to default (32Mb)\n");
		physical_memsize = 0x02000000;
	} else {
#ifdef DEBUG
		prom_printf("prom_memsize = %s\n", memsize_str);
#endif
		physical_memsize = simple_strtol(memsize_str, NULL, 0);
	}

#ifdef CONFIG_CPU_BIG_ENDIAN
	/* SOC-it swaps, or perhaps doesn't swap, when DMA'ing the last
	   word of physical memory */
	physical_memsize -= PAGE_SIZE;
#endif

	/* Check the command line for a memsize directive that overrides
	   the physical/default amount */
	strcpy(cmdline, arcs_cmdline);
	ptr = strstr(cmdline, "memsize=");
	if (ptr && (ptr != cmdline) && (*(ptr - 1) != ' '))
		ptr = strstr(ptr, " memsize=");

	if (ptr)
		memsize = memparse(ptr + 8, &ptr);
	else
		memsize = physical_memsize;

	flashsize_str = prom_getenv("flashsize");
	if (!flashsize_str) {
		prom_printf("flashsize not set in boot prom, set to default (8Mb)\n");
		flashsize = 0x00800000;
	} else {
#ifdef DEBUG
		prom_printf("prom_flashsize = %s\n", flashsize_str);
#endif
		flashsize = simple_strtol(flashsize_str, NULL, 0);
	}
    prom_printf("flashsize=%u MByte\n", flashsize >> 20);


	memset(mdesc, 0, sizeof(mdesc));

#if !defined(CONFIG_MIPS_OHIO) && !defined(CONFIG_MIPS_UR8) && !defined(CONFIG_MIPS_AR7) 
	mdesc[0].type = yamon_dontuse;
	mdesc[0].base = 0x00000000;
	mdesc[0].size = 0x00001000;

	mdesc[1].type = yamon_prom;
	mdesc[1].base = 0x00001000;
	mdesc[1].size = 0x000ef000;

#ifdef CONFIG_MIPS_MALTA
	/*
	 * The area 0x000f0000-0x000fffff is allocated for BIOS memory by the
	 * south bridge and PCI access always forwarded to the ISA Bus and
	 * BIOSCS# is always generated.
	 * This mean that this area can't be used as DMA memory for PCI
	 * devices.
	 */
	mdesc[2].type = yamon_dontuse;
	mdesc[2].base = 0x000f0000;
	mdesc[2].size = 0x00010000;
#else
	mdesc[2].type = yamon_prom;
	mdesc[2].base = 0x000f0000;
	mdesc[2].size = 0x00010000;
#endif

	mdesc[3].type = yamon_dontuse;
	mdesc[3].base = 0x00100000;
	mdesc[3].size = CPHYSADDR(PFN_ALIGN((unsigned long)&_end)) - mdesc[3].base;

	mdesc[4].type = yamon_free;
	mdesc[4].base = CPHYSADDR(PFN_ALIGN(&_end));
	mdesc[4].size = memsize - mdesc[4].base;
#else /*--- #if !defined(CONFIG_MIPS_OHIO) && !defined(CONFIG_MIPS_UR8) && !defined(CONFIG_MIPS_AR7)  ---*/
    /*------------------------------------------------------------------------------------------*\
     * Memory Avail
     *
     * ROM      0x1000 0000    -  0x1040 0000      4MB
     * ROM      0x1000 0000    -  0x1080 0000      8MB
     *
     * RAM      0x1400 0000    -  0x1500 0000     16MB
     * RAM      0x1400 0000    -  0x1600 0000     32MB
     *
     * Memory Layout
     *
     * kernel   0x1400 0000    -  0x1460 0700              [0] + [1]
     * Urlader  0x1460 0700    -  0x1470 0700
     * RAM      0x1470 0700    -  0x1500 0000     16MB
     * RAM      0x1470 0700    -  0x1600 0000     32MB
     *
    \*------------------------------------------------------------------------------------------*/
    prom_printf("&_end=0x%p PFN_ALIGN(&_end)=0x%lx CPHYSADDR(PFN_ALIGN(&_end))=0x%x memsize=0x%x\n",
	    &_end, PFN_ALIGN(&_end), CPHYSADDR(PFN_ALIGN(&_end)), memsize);

#if CONFIG_MIPS_UR8_C55_MEMORY == 0
    /*--- kernel ---*/
	mdesc[0].type = yamon_free;
	mdesc[0].base = CPHYSADDR(PFN_ALIGN(&_text));
	mdesc[0].size = memsize;

	mdesc[1].type = yamon_dontuse;
	mdesc[1].base = 0;
	mdesc[1].size = 0;

	mdesc[2].type = yamon_dontuse;
	mdesc[2].base = 0;
	mdesc[2].size = 0;

    mdesc[3].type = yamon_dontuse;
    mdesc[3].base = 0;
    mdesc[3].size = 0;

#else /*--- #if CONFIG_MIPS_UR8_C55_MEMORY == 0 ---*/
    /*--------------------------------------------------------------------------------------*\
     * Der C55 benötigt einen Speicher dessen obergrenze en einer 4 MByte Grenze liegen muss
    \*--------------------------------------------------------------------------------------*/
    {
        unsigned int membase  = CPHYSADDR(PFN_ALIGN(&_text));
        unsigned int memtop   = CPHYSADDR(PFN_ALIGN(&_text)) + memsize;
        unsigned int memsegment1  = (1 << 10) * CONFIG_MIPS_UR8_C55_MEMORY;
        unsigned int memsegment2  = memsize - (memsize & ~((16 * (1 << 20)) - 1));

        prom_printf("[c55] membase = 0x%x memtop = 0x%x memsize = 0x%x\n", membase, memtop, memsize);
        prom_printf("[c55] memsegment1(size) = 0x%x memsegment2(size) = 0x%x\n", memsegment1, memsegment2);

        /*--- kernel ---*/
        mdesc[0].type = yamon_free;
        mdesc[0].base = membase;
        mdesc[0].size = memsize - memsegment1 - memsegment2;

        prom_printf("[c55] mdesc[%d]: base = 0x%x size = 0x%x\n", 0, mdesc[0].base,  mdesc[0].size);

        mdesc[1].type = yamon_dontuse;
        mdesc[1].base = membase + mdesc[0].size;
        mdesc[1].size = memsegment1;

        prom_printf("[c55] mdesc[%d]: base = 0x%x size = 0x%x\n", 1, mdesc[1].base,  mdesc[1].size);

        mdesc[2].type = yamon_free;
        mdesc[2].base = membase + mdesc[0].size + mdesc[1].size;
        mdesc[2].size = memsegment2;

        prom_printf("[c55] mdesc[%d]: base = 0x%x size = 0x%x\n", 2, mdesc[2].base,  mdesc[2].size);

        mdesc[3].type = yamon_dontuse;
        mdesc[3].base = 0;
        mdesc[3].size = 0;

        prom_printf("[c55] mdesc[%d]: base = 0x%x size = 0x%x\n", 3, mdesc[3].base,  mdesc[3].size);
    }
#endif /*--- #else ---*/ /*--- #if CONFIG_MIPS_UR8_C55_MEMORY == 0 ---*/
    mdesc[4].type = yamon_dontuse;
    mdesc[4].base = 0x10000000;
    mdesc[4].size = flashsize;
#endif /*--- #else ---*/ /*--- #if !defined(CONFIG_MIPS_OHIO) && !defined(CONFIG_MIPS_UR8)  && !defined(CONFIG_MIPS_AR7)---*/

	return &mdesc[0];
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MIPS_UR8_C55_MEMORY) && CONFIG_MIPS_UR8_C55_MEMORY != 0
int prom_c55_get_base_memory(unsigned int *base, unsigned int *len) {
    if(base)
        *base = mdesc[1].base;
    if(len)
        *len = mdesc[1].size;
    return 0;
}

EXPORT_SYMBOL(prom_c55_get_base_memory);
#endif /*--- #if defined(CONFIG_MIPS_UR8_C55_MEMORY) && CONFIG_MIPS_UR8_C55_MEMORY != 0 ---*/

static int __init prom_memtype_classify (unsigned int type)
{
	switch (type) {
	case yamon_free:
		return BOOT_MEM_RAM;
	case yamon_prom:
		return BOOT_MEM_ROM_DATA;
	default:
		return BOOT_MEM_RESERVED;
	}
}

void __init prom_meminit(void)
{
	struct prom_pmemblock *p;

#ifdef DEBUG
	prom_printf("YAMON MEMORY DESCRIPTOR dump:\n");
	p = prom_getmdesc();
	while (p->size) {
		int i = 0;
		prom_printf("[%d,%p]: base<%08lx> size<%08lx> type<%s>\n",
			    i, p, p->base, p->size, mtypes[p->type]);
		p++;
		i++;
	}
#endif
	p = prom_getmdesc();

	while (p->size) {
		long type;
		unsigned long base, size;

		type = prom_memtype_classify (p->type);
		base = p->base;
		size = p->size;

		add_memory_region(base, size, type);
                p++;
	}
}

unsigned long __init prom_free_prom_memory(void)
{
	unsigned long freed = 0;
	unsigned long addr;
	int i;

	for (i = 0; i < boot_mem_map.nr_map; i++) {
		if (boot_mem_map.map[i].type != BOOT_MEM_ROM_DATA)
			continue;

		addr = boot_mem_map.map[i].addr;
		while (addr < boot_mem_map.map[i].addr
			      + boot_mem_map.map[i].size) {
			ClearPageReserved(virt_to_page(__va(addr)));
			init_page_count(virt_to_page(__va(addr)));
			free_page((unsigned long)__va(addr));
			addr += PAGE_SIZE;
			freed += PAGE_SIZE;
		}
	}
	printk("Freeing prom memory: %ldkb freed\n", freed >> 10);

	return freed;
}
