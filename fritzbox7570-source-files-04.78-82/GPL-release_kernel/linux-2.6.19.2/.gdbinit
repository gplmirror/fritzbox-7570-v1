####################################################################################
#
####################################################################################
#
target remote 172.16.199.208:2001
monitor reset
shell sleep 3

#     DSP Memory
mem 0xA1000000 0xA1018000 rw 32 nocache
#     4 KByte internal RAM
mem 0xA0000000 0xA0001000 rw 32 nocache
#    32 MByte SDRAM
mem 0x94000800 0x95800000 rw 32 nocache
#     2 MByte Flash
mem 0x90000000 0x90200000 ro 32 nocache

set endian little

echo ready\n

####################################################################################
define eva
    delete breakpoints
    restore ar7_link_small.elf
    symbol-file ar7_link.elf
    set $pc=_start
    set $ra=_start
    break c_entry
end

####################################################################################
define restart
    set $pc=_start
    set $ra=_start
end

####################################################################################
define cause
    if $sr & 0x2
        echo exception cause is (dec): 
        p ( $cause >> 2 ) & ((1 << 5) - 1)
        if $cause & (1 << 31) 
            echo in delay slot\n
        end 
        if $cause & (1 << 9) 
            echo software interrupt 1\n
        end 
        if $cause & (1 << 8) 
            echo software interrupt 0\n
        end 
    else
        echo no exception\n
    end
end
####################################################################################
define erase_urlader
    monitor erase 0xb0000000 sector
end

####################################################################################
define erase_envspace
    monitor erase 0xb03e0000 sector
    monitor erase 0xb03f0000 sector
    monitor erase 0xb03f2000 sector
    monitor erase 0xb03f4000 sector
    monitor erase 0xb03f6000 sector
    monitor erase 0xb03f8000 sector
    monitor erase 0xb03fa000 sector
    monitor erase 0xb03fc000 sector
    monitor erase 0xb03fe000 sector
end

####################################################################################
define erase_kernel
    monitor erase 0xb0010000 sector
    monitor erase 0xb0020000 sector
    monitor erase 0xb0030000 sector
    monitor erase 0xb0040000 sector
    monitor erase 0xb0050000 sector
    monitor erase 0xb0050000 sector
    monitor erase 0xb0060000 sector
    monitor erase 0xb0070000 sector
    monitor erase 0xb0080000 sector
    monitor erase 0xb0090000 sector
    monitor erase 0xb00a0000 sector
    monitor erase 0xb00b0000 sector
end

####################################################################################
define erase_bottom_urlader
    monitor erase 0xb0000000 sector
    monitor erase 0xb0002000 sector
    monitor erase 0xb0004000 sector
    monitor erase 0xb0006000 sector
    monitor erase 0xb0008000 sector
    monitor erase 0xb000a000 sector
    monitor erase 0xb000c000 sector
    monitor erase 0xb000e000 sector
end

####################################################################################
define program_urlader
    monitor prog 0xb0000000 gdb\ar7_urlader.bin BIN
end

####################################################################################
define program_old_urlader
    monitor prog 0xb0000000 gdb/adam2.bin BIN
end

####################################################################################
define program_config
    monitor prog 0xb0000580 gdb/ar7_urlader_config.bin BIN
end

####################################################################################
define queues
    print free_Queue
    print CPMAC_Rx_Queue
    print CPMAC_Tx_Queue
    print eth_Tx_Queue
    print ip_Queue
    print tcp_Queue
    print ftp_Cmd_Queue
    print ftp_Data_Queue
    print timer_Queue
end

####################################################################################
echo possible macros are: eva, restart, queues, erase_urlader, program_urlader cause\n\n
echo ready\n
