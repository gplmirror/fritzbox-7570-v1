/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications.
*/


#include "rand.h"

int urand, randx = 0 ;

int abs(int x) { return x&0x7fffffff; }

double local_max() { return 2147483648.0; } // note: a double

int draw() { return randx  = randx*1103515245+12345; }

double fdraw( ) { return abs(draw( ))/local_max(); }
