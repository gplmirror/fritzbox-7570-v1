/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
*/

#ifndef _DEFS_H
#define _DEFS_H
//typedef unsigned int   u32; 
//typedef unsigned short u16; 
//typedef unsigned char  u8; 


typedef unsigned int   uint32;
typedef unsigned short uint16;
typedef unsigned char  uint8;

typedef int   int32;
typedef short int16;
typedef char  int8;

#define UINT32 uint32
#define INT32  int32
#define UINT16 uint16
#define INT16  int16
#define UCHAR  uint8
#define CHAR   int8
#define UINT8  uint8

/* find the required linux header file and remove the below defs */
//#define size_t  uint16
//#define uint32_t  uint32
//#define uint8_t  uint8
#include <linux/types.h>

#ifndef SUCCESS
#define SUCCESS    0
#endif

#ifndef FAILURE
#define  FAILURE  -1
#endif

#define wlan_osif_memcpy       memcpy
#define wlan_osif_memcmp       memcmp
#define wlan_osif_memset       memset
#define wlan_osif_strcpy       strcpy
#define wlan_osif_strcmp       strcmp
#define wlan_osif_strncpy      strncpy
#define wlan_osif_strlen       strlen
#define wlan_osif_sprintf      sprintf

#ifndef NULL
#define NULL   (void*)0
#endif
#endif

