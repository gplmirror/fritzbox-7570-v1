#ifndef __HMAC_PRF_H__
#define __HMAC_PRF_H__

/* Declarations of cryptographic functions used in WPA authenticated key 
   management */
#ifdef __cplusplus
extern "C" {
#endif
  void hmac_md5(unsigned char *text, int text_len, unsigned char *key, 
		int key_len, void * digest);
  
  void hmac_sha1(unsigned char *text, int text_len, unsigned char *key, 
		 int key_len, unsigned char *digest);
  /*void PRF(char *secret, int secret_len, char *label, int label_len, 
	   char *seed, int seed_len, char *out, int out_len);  */
  void PRF(unsigned char *key, int key_len, const unsigned char *prefix, 
	   int prefix_len, unsigned char *data, int data_len, 
	   unsigned char *output, int len);

#ifdef __cplusplus
}
#endif

#endif /* __HMAC_PRF_H__ */
