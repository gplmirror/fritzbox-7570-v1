
#ifndef AES_H
#define AES_H

void rijndaelKeySetupEnc(u32 rk[/*44*/], const u8 cipherKey[]);
void rijndaelKeySetupDec(u32 rk[/*44*/], const u8 cipherKey[]);
void rijndaelEncrypt(const u32 rk[/*44*/], const u8 pt[16], u8 ct[16]);
void rijndaelDecrypt(const u32 rk[/*44*/], const u8 ct[16], u8 pt[16]);
#endif
