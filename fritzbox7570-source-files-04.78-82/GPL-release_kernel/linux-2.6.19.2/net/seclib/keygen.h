#ifndef __PMK_H__
#define __PMK_H__

#ifdef __cplusplus
extern "C" {
#endif
  int PasswordHash (char *password, unsigned char *ssid, int ssidlength,
		    unsigned char *output);

#ifdef __cplusplus
}
#endif

#endif /* __PMK_H__ */
