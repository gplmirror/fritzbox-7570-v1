//#include "wlan_defs.h"
#include<linux/module.h>
#include<linux/kernel.h>
#include "def.h"
//#include "wlan_osif_api.h"
#include "hmac_prf.h"
#include "keygen.h"

#define SHA1_DIGEST_LEN 20

// F(P, S, c, i) = U1 xor U2 xor ... Uc
// U1 = PRF(P, S || Int(i)
// U2 = PRF(P, U1)
// Uc = PRF(P, Uc-1)
//
void F(char *password, unsigned char *ssid, int ssidlength, int iterations, int count, unsigned char *output)
{
  unsigned char digest[36], digest1[SHA1_DIGEST_LEN];
  int i, j;
  
  // U1 = PRF(P, S || int(i))
  wlan_osif_memcpy(digest, ssid, ssidlength);
  digest[ssidlength] = (unsigned char)((count>>24) & 0xff);
  digest[ssidlength+1] = (unsigned char)((count>>16) & 0xff);
  digest[ssidlength+2] = (unsigned char)((count>>8) & 0xff);
  digest[ssidlength+3] = (unsigned char)(count & 0xff);
  hmac_sha1(digest, ssidlength+4, 
	    (unsigned char *)password, (int)strlen(password),
	    digest1);
  
  // output = U1
  wlan_osif_memcpy(output, digest1, SHA1_DIGEST_LEN);
  
  for(i = 1; i < iterations; i++) {
    // Un = PRF(P, Un-1)
    hmac_sha1(digest1, SHA1_DIGEST_LEN, 
	      (unsigned char *)password, (int)strlen(password),
digest);
    wlan_osif_memcpy(digest1, digest, SHA1_DIGEST_LEN);
    
    // output = output xor Un
    for(j = 0; j < SHA1_DIGEST_LEN; j++) {
      output[j] ^= digest[j];
    }
  }
}

// password - ascii string up to 63 characters in length
// ssid - octet string up to 32 octets
// ssidlength - length of ssid
// output must be 40 octets in length and outputs 256 bits of key
int PasswordHash (char *password, unsigned char *ssid, int ssidlength,
		  unsigned char *output)
{
  if((strlen(password) < 8) || (strlen(password) > 63)
     || (ssidlength > 32)) return -1;
//???     || (ssidlength > 32)) return 0;
  
//  WLAN_LOG(KERN_ERROR,"SSID %s Len %d \r\n",ssid,ssidlength);

  F(password, ssid, ssidlength, 4096, 1, output);
  F(password, ssid, ssidlength, 4096, 2,
    &output[SHA1_DIGEST_LEN]);
  return 0;
}

EXPORT_SYMBOL(PasswordHash);
