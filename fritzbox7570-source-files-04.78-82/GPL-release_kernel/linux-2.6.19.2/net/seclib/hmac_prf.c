#define ULONG unsigned long
//#include "wlan_defs.h"
#include<linux/module.h>
#include<linux/kernel.h>
#include "def.h"
#include "md5.h"
#include "sha1.h"
#include "hmac_prf.h"
//#include "wpa_supplicant.h"

/*
** Function: hmac_md5
** From rfc2104
** Uses a MD5 library
*/

void
hmac_md5(unsigned char *text, int text_len, unsigned char *key, 
     int key_len, void * digest)
{
  //md5_state_t context;
  MD5_CTX context;
  unsigned char k_ipad[65];    /* inner padding -
				* key XORd with ipad
				*/
  unsigned char k_opad[65];    /* outer padding -
				* key XORd with opad
				*/
  int i;

  /* if key is longer than 64 bytes reset it to key=MD5(key) */
  if (key_len > 64) {
    //md5_state_t      tctx;
    MD5_CTX      tctx;
    
    MD5Init(&tctx);
    MD5Update(&tctx, key, key_len);
    MD5Final(digest, &tctx);
    
    key_len = 16;
  }

  /*
   * the HMAC_MD5 transform looks like:
   *
   * MD5(K XOR opad, MD5(K XOR ipad, text))
   *
   * where K is an n byte key
   * ipad is the byte 0x36 repeated 64 times
   * opad is the byte 0x5c repeated 64 times
   * and text is the data being protected
   */
  
  /* start out by storing key in pads */
  memset( k_ipad, 0, sizeof k_ipad);
  memset( k_opad, 0, sizeof k_opad);
  memcpy( k_ipad, key, key_len);
  memcpy( k_opad, key, key_len);
  
  /* XOR key with ipad and opad values */
  for (i=0; i<64; i++) {
    k_ipad[i] ^= 0x36;
    k_opad[i] ^= 0x5c;
  }
  /*
   * perform inner MD5
   */
  MD5Init(&context);                   /* init context for 1st
					* pass */
  MD5Update(&context, k_ipad, 64);      /* start with inner pad
					 */
  MD5Update(&context, text, text_len); /* then text of datagram
					*/
  MD5Final(digest, &context);		/* finish up 1st
							   pass */
  /*
   * perform outer MD5
   */
  MD5Init(&context);                   /* init context for 2nd
					* pass */
  MD5Update(&context, (const unsigned char*)k_opad, 64);     
  /* start with outer pad */
  MD5Update(&context, (const unsigned char*)digest, 16);     
  /* then results of 1st
   * hash */
  MD5Final(digest, &context);          /* finish up 2nd pass */
}


// Conversion of hmac_md5 to hmac_sha1
// using a sha1 library
//
void
hmac_sha1(unsigned char *text, int text_len, unsigned char *key, 
	  int key_len, unsigned char *digest)
{
  SHA1Context context;
  unsigned char k_ipad[65];    /* inner padding -
				* key XORd with ipad
				*/
  unsigned char k_opad[65];    /* outer padding -
				* key XORd with opad
				*/
  int i;
  /* if key is longer than 64 bytes reset it to key=SHA1(key) */
  if (key_len > 64) {
    SHA1Context      tctx;
    
    SHA1Reset(&tctx);
    SHA1Input(&tctx, key, key_len);
    SHA1Result(&tctx,key);
    
    key_len = 20;
  }
  
  /*
   * the HMAC_SHA1 transform looks like:
   *
   * SHA1(K XOR opad, SHA1(K XOR ipad, text))
   *
   * where K is an n byte key
   * ipad is the byte 0x36 repeated 64 times
   * opad is the byte 0x5c repeated 64 times
   * and text is the data being protected
   */
  
  /* start out by storing key in pads */
  memset( k_ipad, 0, sizeof k_ipad);
  memset( k_opad, 0, sizeof k_opad);
  memcpy( k_ipad, key, key_len);
  memcpy( k_opad, key, key_len);
  
  /* XOR key with ipad and opad values */
  for (i=0; i<64; i++) {
    k_ipad[i] ^= 0x36;
    k_opad[i] ^= 0x5c;
  }
  /*
   * perform inner SHA1
   */
  SHA1Reset(&context);                   /* init context for 1st
					  * pass */
  SHA1Input(&context, k_ipad, 64);     /* start with inner pad
					  */
  SHA1Input(&context, text, text_len); /* then text of datagram
					  */
  SHA1Result(&context, digest);		/* finish up 1st pass
					 */
  /*
   * perform outer SHA1
   */
  SHA1Reset(&context);                   /* init context for 2nd
					  * pass */
  SHA1Input(&context, k_opad, 64);     /* start with outer pad
					  */
  SHA1Input(&context, digest, 20);     /* then results of 1st
					  * hash */
  SHA1Result(&context, digest);          /* finish up 2nd pass */
}

// Note out_len for the prf is in bytes
/*
void PRF(char *secret, int secret_len, char *label, int label_len, char *seed, int seed_len, char *out, int out_len)
{
	int i, offset, copylen, input_len;
	unsigned char input[128];
	unsigned char buf[20];  // SHA-1 output

	offset = 0;

	// Concatenate A || '0' || B || i (which is label || '0' || seed || i)

	memcpy(input, label, label_len);
	input[label_len] = 0;
	memcpy(input + label_len + 1, seed, seed_len);
	input[label_len + 1 + seed_len] = 0;  // for first iteration
	input_len = label_len + 1 + seed_len + 1;

	for (i = 0; i < (out_len + 19) / 20; i++) {

		// Change variable in input

		input[input_len - 1] = i;

		// Do SHA-1 (160-bit output)

		hmac_sha1(input, input_len, secret, secret_len, buf);

		// Concatenate intermediate output to final output until out_len reached

		copylen = (offset + 20 < out_len) ? 20 : out_len - offset;
		memcpy(out + offset, buf, copylen);

		offset += 20;
	}
}
*/
// PRF
// Length of output is in octets rather than bits
// since length is always a multiple of 8
// output array is organized so first N octets starting from 0
// contains PRF output
//
// supported inputs are 16, 32, 48, 64
// output array must be 80 octets in size to allow for sha1 overflow
//

void PRF(unsigned char *key, int key_len, const unsigned char *prefix, 
	 int prefix_len, unsigned char *data, int data_len, 
	 unsigned char *output, int len)
{
  int i;
  unsigned char input[1024]; // concatenated input
  int currentindex = 0;
  int total_len;

#if 0
  WLAN_LOG(WLAN_WPA_DEBUG,"Key-->\r\n");
  for(i=0;i<key_len;i++)
	  WLAN_LOG(WLAN_WPA_DEBUG,"%02x ",key[i]);
  WLAN_LOG(WLAN_WPA_DEBUG,"\r\nData -->\r\n");
  for(i=0;i<data_len;i++)
	  WLAN_LOG(WLAN_WPA_DEBUG,"%02x ",data[i]);
#endif

  memcpy(input, prefix, prefix_len);
  input[prefix_len] = 0;		// single octet 0
  memcpy(&input[prefix_len+1], data, data_len);
  total_len = prefix_len + 1 + data_len;
  input[total_len] = 0;		// single octet count, starts at 0
  total_len++;
  for(i = 0; i < (len+19)/20; i++) {
    hmac_sha1(input, total_len, key, key_len,
	      &output[currentindex]);
    currentindex += 20;	// next concatenation location
    input[total_len-1]++;	// increment octet count
  }
}

EXPORT_SYMBOL(hmac_sha1);
EXPORT_SYMBOL(hmac_md5);
EXPORT_SYMBOL(PRF);
