/*
 * Copyright (C) 2006  Ikanos Communications. All rights reserved.
 * The information and source code contained herein is the property
 * of Ikanos Communications.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
*/


#ifndef __RAND_H__
#define __RAND_H__

typedef struct Randint {     // uniform distribution in the interval [0,max]
  unsigned long randx;
}randint_t;

void  randint_create(randint_t *rand) ;

void seed(randint_t *rand, long s); 


int abs(int x) ; 
double local_max(void) ;
int draw(void) ; 
double fdraw(void) ;

#endif // __RAND_H__
