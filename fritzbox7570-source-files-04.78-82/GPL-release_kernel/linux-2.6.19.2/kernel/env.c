#include <linux/autoconf.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/module.h>

#if defined(CONFIG_ARM)
#include <asm/arch/io.h>
#endif /*--- #if defined(CONFIG_ARM) ---*/
#include <linux/env.h>

#if defined(CONFIG_MIPS)
#include <asm/mips-boards/prom.h>
#define EMIF_ADDRESS
#define phys_to_virt        /*--- ist nur beim DAVINCI notwendig ---*/
#endif


/*--- #define DEBUG_PROM_INIT ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define prom_envp(index) ((char *)(long)_prom_envp[(index)])

static char env_buffer[2048];
static char *_local_envp[64 * 2];
int *_prom_envp;

void __init env_init(int *fw_arg2, enum _env_location env_location)
{
    unsigned int i;
    char *p;
    static unsigned int once = 0;
    if(once)
        return;
    once = 1;


	_prom_envp = fw_arg2;

#if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) || defined(CONFIG_MIPS_UR8)
#if defined(DEBUG_PROM_INIT)
    prom_printf("[prom_init] 0\n");
#endif /*--- #if defined(DEBUG_PROM_INIT) ---*/
#else
#if defined(DEBUG_PROM_INIT)
    printk("[prom_init] 0x%p\n", _prom_envp);
    printk("[prom_init] 0x%p [0]\n", _prom_envp[0]);
    printk("[prom_init] 0x%p [1]\n", _prom_envp[1]);
    printk("[prom_init] 0x%p [2]\n", _prom_envp[2]);
#endif /*--- #if defined(DEBUG_PROM_INIT) ---*/
    if(((_prom_envp[1] & 0xC0000000) == 0x80000000) && ((_prom_envp[2] & 0xC0000000) == 0x80000000)) {
#if defined(DEBUG_PROM_INIT)
        printk("[prom_init] switch to ram location\n");
#endif /*--- #if defined(DEBUG_PROM_INIT) ---*/
        env_location = ENV_LOCATION_PHY_RAM;
    } else {
#if defined(DEBUG_PROM_INIT)
        printk("[prom_init] switch to flash location\n");
#endif /*--- #if defined(DEBUG_PROM_INIT) ---*/
        env_location = ENV_LOCATION_FLASH;
    }
#endif /*--- #if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) || defined(CONFIG_MIPS_UR8) ---*/
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    env_buffer[0] = '\0';
    p = env_buffer;

    /*--------------------------------------------------------------------------------------*\
     * copy envp values from urlader memory to (non init) kernel memory
    \*--------------------------------------------------------------------------------------*/
    for(i = 0 ; _prom_envp && _prom_envp[i] && _prom_envp[i + 1] ; i += 2) {
#if defined(DEBUG_PROM_INIT)
#if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) || defined(CONFIG_MIPS_UR8)
        prom_printf("[prom_init] envp[%u]= \"%s\"=\"%s\" \n", i, (char *)_prom_envp[i], (char *)_prom_envp[i + 1]);
#else
        switch(env_location) {
            case ENV_LOCATION_FLASH:
                printk("[prom_init] envp[%u]= \"%s\"=\"%s\" \n", i, (char *)phys_to_virt(_prom_envp[i]), (char *)EMIF_ADDRESS(_prom_envp[i + 1]));
                break;
            case ENV_LOCATION_PHY_RAM:
                printk("[prom_init] envp[%u]= \"%s\"=\"%s\" \n", i, (char *)phys_to_virt(_prom_envp[i]), (char *)phys_to_virt(_prom_envp[i + 1]));
                break;
        }
#endif /*--- #if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) || defined(CONFIG_MIPS_UR8) ---*/
#endif /*--- #if defined(DEBUG_PROM_INIT) ---*/
        _local_envp[i] = p;
        strcat(p, (char *)phys_to_virt(_prom_envp[i]));
        p += strlen((char *)phys_to_virt(_prom_envp[i])) + 1;
        while((unsigned int)p & 0x3)  /*--- align auf n�chstes wort ---*/
            *p++ = '\0';

        _local_envp[i + 1] = p;
        switch(env_location) {
            case ENV_LOCATION_FLASH:
                strcat(p, (char *)EMIF_ADDRESS(_prom_envp[i + 1]));
                p += strlen((char *)EMIF_ADDRESS(_prom_envp[i + 1])) + 1;
                break;
            case ENV_LOCATION_PHY_RAM:
                strcat(p, (char *)phys_to_virt(_prom_envp[i + 1]));
                p += strlen((char *)phys_to_virt(_prom_envp[i + 1])) + 1;
                break;
            case ENV_LOCATION_VIRT_RAM:
                strcat(p, (char *)(_prom_envp[i + 1]));
                p += strlen((char *)(_prom_envp[i + 1])) + 1;
                break;
        }
        while((unsigned int)p & 0x3)  /*--- align auf n�chstes wort ---*/
            *p++ = '\0';
    }
    _local_envp[i] = NULL;
    _local_envp[i + 1] = NULL;
    _prom_envp = (int *)_local_envp;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *prom_getenv(char *envname)
{
	/*
	 * Return a pointer to the given environment variable.
	 * In 64-bit mode: we're using 64-bit pointers, but all pointers
	 * in the PROM structures are only 32-bit, so we need some
	 * workarounds, if we are running in 64-bit mode.
	 */
	int i, index=0;

	i = strlen(envname);

	while (prom_envp(index)) {
		if(strncmp(envname, prom_envp(index), i) == 0) {
			return (prom_envp(index+1));
		}
		index += 2;
	}

	return NULL;
}

EXPORT_SYMBOL(prom_getenv);

