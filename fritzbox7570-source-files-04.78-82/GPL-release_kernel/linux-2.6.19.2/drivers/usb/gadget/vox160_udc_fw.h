static short udc_fw_data[] = {
	/* endpoint 0: control, 16 byte, configuration 0 */
	0x00, 0x00, 0x10, 0x00, 0x00,

	/* 
	 * endpoint 1: interrupt IN, 16 byte, 
	 * configuration 1 interface/alternate 0 
	 */
	0x14, 0x38, 0x10, 0x00, 0x71,

	/* 
	 * endpoint 2: bulk IN, 64 byte, 
	 * configuration 1 interface/alternate 0
	 */
	0x24, 0x28, 0x80, 0x00, 0x52,	// 128 byte

	/* 
	 * endpoint 3: bulk OUT, 128 byte, 
	 * configuration 1 interface/alternate 0
	 */
	0x34, 0x20, 0x80, 0x00, 0x43	// 128 byte
};

long udc_fw_data_element = (sizeof (udc_fw_data) / sizeof (short));
