/*
 * EHCI HCD (Host Controller Driver) for USB.
 *
 * (C) Copyright 1999 Roman Weissgaerber <weissg@vienna.at>
 * (C) Copyright 2000-2002 David Brownell <dbrownell@users.sourceforge.net>
 * (C) Copyright 2002 Hewlett-Packard Company
 *
 * Bus Glue for ADI VOX160
 *
 * Written by Christopher Hoover <ch@hpl.hp.com>
 * Based on fragments of previous driver by Rusell King et al.
 *
 * Modified for LH7A404 from ohci-sa1111.c
 *  by Durgesh Pattamatta <pattamattad@sharpsec.com>
 * Modified for AMD Alchemy Au1xxx
 *  by Matt Porter <mporter@kernel.crashing.org>
 * Modified for ADI VOX160
 *  by Vivek Dharmadhikari <mporter@kernel.crashing.org>
 *
 * This file is licenced under the GPL.
 */

#include <linux/platform_device.h>
#include <asm/mach-adi_fusiv/vox160usb.h>

#define USBH_ENABLE_BE (1<<0)
#define USBH_ENABLE_C  (1<<1)
#define USBH_ENABLE_E  (1<<2)
#define USBH_ENABLE_CE (1<<3)
#define USBH_ENABLE_RD (1<<4)

#ifdef __LITTLE_ENDIAN
#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C)
#elif __BIG_ENDIAN
#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C | USBH_ENABLE_BE)
#else
#error not byte order defined
#endif

extern int usb_disabled(void);

/*-------------------------------------------------------------------------*/
static void vox160_start_ehci_hc(struct platform_device *dev)
{
        /* address of Clock Gate Control Register  */
        int *clock_gate_control_reg = (int *)0xb90000cc;

        /* address of Frame Length Register  */
        int *framelen_reg = (int *)0xb9230090;
        int temp;

	pr_debug(__FILE__ ": starting VX160 EHCI USB Controller\n");

        /* program DDR register */
        *((int *)0xb9180020) = 0x00100060;
        udelay(1000);

        /* Enable Clock to USB host controller Phy */
        *((int *)0xb90000dc) = 0x00000001;
        udelay(1000);

        /* Reset USB host controller Phy */
        *((int *)0xb90000dc) = 0x00000005;
        udelay(1000);

        /* Release USB host controller Phy reset */
        *((int *)0xb90000dc) = 0x00000001;
        udelay(1000);

        /* Release USB host controller Phy suspend  */
        *((int *)0xb90000dc) = 0x00000003;
        udelay(1000);

        *clock_gate_control_reg |= (1 << 13);
       
	pr_debug(__FILE__ ": Clock to USB host has been enabled \n");

        /*=================================
         * 29th bit-->usb2_desc_endian_conv
         * 30th bit-->usb2_data_endian_conv
         * 31st bit--> usb2_csr_endian_conv
         *=================================
	 */
        temp = *((int *)0xb9000088);
        temp |= 0x60000000;
        *((int *)0xb9000088) = temp;
        udelay(1000);

        /* turn HC on (run/stop) bit
         * Refer 2.3.1 section of ehci spec
         * for each bit explanation
         * 11th bit = Async schedule enable
         */
        *((int *)0xb9230010) = 0x00080b01;
        udelay(1000);


        /* program frame length register with value 0x00001d4d */
        *framelen_reg = 0x00001d4d;
}
static void vox160_stop_ehci_hc(struct platform_device *dev)
{
        /* address of Clock Gate Control Register  */
        /* int *clock_gate_control_reg = (int *)0xb90000cc; */

	pr_debug(__FILE__ ": stopping VOX160 EHCI USB Controller\n");

	/* Disable clock */
	/* disable USBH_CLK_EN bit in clock gate control register */
        /* *clock_gate_control_reg &= ~(1 << 13); */
}


/*-------------------------------------------------------------------------*/

/* configure so an HC device and id are always provided */
/* always called with process context; sleeping is OK */


/**
 * usb_hcd_vox160_probe - initialize Au1xxx-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 *
 */
int usb_hcd_vox160_ehci_probe (const struct hc_driver *driver,
			  struct platform_device *dev)
{
	int retval;
	struct usb_hcd *hcd;
	struct ehci_hcd *ehci;

	pr_debug(__FILE__ ": Probing USB EHCI  \n");

        vox160_start_ehci_hc(dev);

        /* offset 1 contains IRQ info */
	if(dev->resource[1].flags != IORESOURCE_IRQ) {
		pr_debug ("resource[2] is not IORESOURCE_IRQ");
		return -ENOMEM;
	}

	hcd = usb_create_hcd(driver, &dev->dev, "vox160 ehci");
	if (!hcd)
		return -ENOMEM;

        /* offset 0 contains EHCI info */
	hcd->rsrc_start = dev->resource[0].start;
	hcd->rsrc_len = dev->resource[0].end - dev->resource[0].start + 1;

	if (!request_mem_region(hcd->rsrc_start, hcd->rsrc_len, hcd_name)) {
		pr_debug("request_mem_region failed");
		retval = -EBUSY;
		goto err1;
	}

	hcd->regs = ioremap(hcd->rsrc_start, hcd->rsrc_len);
	if (!hcd->regs) {
		pr_debug("ioremap failed");
		retval = -ENOMEM;
		goto err2;
	}

        //vox160_start_ehci_hc(dev);
	ehci = hcd_to_ehci(hcd);
	ehci->caps = hcd->regs;
	ehci->regs = hcd->regs + HC_LENGTH(readl(&ehci->caps->hc_capbase));
	/* cache this readonly data; minimize chip reads */
	ehci->hcs_params = readl(&ehci->caps->hcs_params);

	retval = usb_add_hcd(hcd, dev->resource[1].start, IRQF_DISABLED | IRQF_SHARED);
	if (retval == 0)
		return retval;
        vox160_stop_ehci_hc(dev);
	iounmap(hcd->regs);
 err2:
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
 err1:
	usb_put_hcd(hcd);
	return;
}


/* may be called without controller electrically present */
/* may be called with controller, bus, and devices active */

/**
 * usb_hcd_au1xxx_remove - shutdown processing for Au1xxx-based HCDs
 * @dev: USB Host Controller being removed
 * Context: !in_interrupt()
 *
 * Reverses the effect of usb_hcd_vox160_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 *
 */
void usb_hcd_vox160_echi_remove (struct usb_hcd *hcd, struct platform_device *dev)
{
	usb_remove_hcd(hcd);
        pr_debug("stopping VOX160 EHCI USB Controller\n");
	vox160_stop_ehci_hc(dev);
	iounmap(hcd->regs);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ehci_vx160_hc_driver = {
	.description = hcd_name,
	.product_desc = "Ikanos On-Chip EHCI Host Controller",
	.hcd_priv_size = sizeof(struct ehci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq = ehci_irq,
	.flags = HCD_MEMORY | HCD_USB2,

	/*
	 * basic lifecycle operations
	 */
	.reset = ehci_init,
	.start = ehci_run,
	.stop = ehci_stop,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue = ehci_urb_enqueue,
	.urb_dequeue = ehci_urb_dequeue,
	.endpoint_disable = ehci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number = ehci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data = ehci_hub_status_data,
	.hub_control = ehci_hub_control,
#ifdef	CONFIG_PM
	.hub_suspend = ehci_hub_suspend,
	.hub_resume = ehci_hub_resume,
#endif
};


/*-------------------------------------------------------------------------*/
static int ehci_hcd_vox160_drv_probe(struct platform_device *pdev)
{
	pr_debug("In ehci_hcd_vox160_drv_probe\n");

	if (usb_disabled())
		return -ENODEV;

	return usb_hcd_vox160_ehci_probe(&ehci_vx160_hc_driver, pdev);
}

static int ehci_hcd_vox160_drv_remove(struct platform_device *pdev)
{
	struct usb_hcd *hcd = platform_get_drvdata(pdev);

	pr_debug ("In ehci_hcd_vox160_drv_remove\n");
        usb_hcd_vox160_echi_remove(hcd, pdev);
	return 0;
}
	/*TBD*/
/*static int ehci_hcd_vox160_drv_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	return 0;
}
static int ehci_hcd_vox160_drv_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	return 0;
}
*/

MODULE_ALIAS("vox160-ehci-hcd");
static struct platform_driver ehci_hcd_vox160_driver = {
	.probe		= ehci_hcd_vox160_drv_probe,
	.remove		= ehci_hcd_vox160_drv_remove,
	/*.suspend	= ehci_hcd_vox160_drv_suspend, */
	/*.resume	= ehci_hcd_vox160_drv_resume, */
	.driver = {
		.name		= "vox160-ehci-hcd",
		.bus		= &platform_bus_type,
	}
};
