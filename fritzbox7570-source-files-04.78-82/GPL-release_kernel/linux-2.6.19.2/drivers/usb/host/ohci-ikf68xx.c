/*
 * OHCI HCD (Host Controller Driver) for USB.
 *
 * (C) Copyright 1999 Roman Weissgaerber <weissg@vienna.at>
 * (C) Copyright 2000-2002 David Brownell <dbrownell@users.sourceforge.net>
 * (C) Copyright 2002 Hewlett-Packard Company
 *
 * Bus Glue for AMD Alchemy Au1xxx
 * Bus Glue for IKANOS's IKF68XX
 *
 * Written by Christopher Hoover <ch@hpl.hp.com>
 * Based on fragments of previous driver by Rusell King et al.
 *
 * Modified for LH7A404 from ohci-sa1111.c
 *  by Durgesh Pattamatta <pattamattad@sharpsec.com>
 * Modified for AMD Alchemy Au1xxx
 *  by Matt Porter <mporter@kernel.crashing.org>
 * Modified for IKANOS's IKF68XX 
 *  by Vivek Dharmadhikari<vivek.dharmadhikari@analog.com>
 *
 * This file is licenced under the GPL.
 */

#include <linux/platform_device.h>
#include <asm/mach-ikan_mips/ikf68xx_usb.h>

#define USBH_ENABLE_BE (1<<0)
#define USBH_ENABLE_C  (1<<1)
#define USBH_ENABLE_E  (1<<2)
#define USBH_ENABLE_CE (1<<3)
#define USBH_ENABLE_RD (1<<4)

#ifdef __LITTLE_ENDIAN
#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C)
#elif __BIG_ENDIAN
#define USBH_ENABLE_INIT (USBH_ENABLE_CE | USBH_ENABLE_E | USBH_ENABLE_C | USBH_ENABLE_BE)
#else
#error not byte order defined
#endif

extern int usb_disabled(void);

//*-------------------------------------------------------------------------*/
static void ikf68xx_start_ohci_hc(struct platform_device *dev)
{
        /* address of Clock Gate Control Register  */
        int *clock_gate_control_reg = (int *)0xb90000cc;
        int temp;

	printk(KERN_DEBUG __FILE__
		": starting IKF68XX OHCI USB Host Controller\n");

		/* == AVM/WK 20081215 ==
		 * This register address is reserved on VX180.
		 */
#if 0 
        /* program DDR register */
        *((int *)0xb9180020) = 0x00100060;
        udelay(1000);
#endif

        /* Enable Clock to USB host controller Phy */
        *((int *)0xb90000dc) = 0x00000001;
        udelay(1000);

        /* Reset both USB host controller Phys */
		/* == AVM/WK 20090505 ==
		 * Value corrected for VX180, avoiding commom block reset.
           *((int *)0xb90000dc) = 0x00000009;
		 */
        *((int *)0xb90000dc) = 0x00000031;
        udelay(1000);

        /* Release USB host controller Phy reset */
        *((int *)0xb90000dc) = 0x00000001;
        udelay(1000);

        /* Release USB host controller Phy suspend  */
        *((int *)0xb90000dc) = 0x00000007;
        udelay(1000);

        *clock_gate_control_reg |= (1 << 13);
       
	printk(KERN_DEBUG __FILE__
	": Clock to USB host has been enabled \n");

        /*=================================
         * 29th bit-->usb2_desc_endian_conv
         * 30th bit-->usb2_data_endian_conv
         * 31st bit--> usb2_csr_endian_conv
         *=================================
         */
        temp = *((int *)0xb9000088);
        temp |= 0x60000000;
        *((int *)0xb9000088) = temp;
         udelay(1000);
}

static void ikf68xx_stop_ohci_hc(struct platform_device *dev)
{
        /* address of Clock Gate Control Register  */
        /* int *clock_gate_control_reg = (int *)0xb90000cc;*/

	printk(KERN_DEBUG __FILE__ ": stopping IKF68XX OHCI USB Controller\n");

	/* Disable clock */
	/* disable USBH_CLK_EN bit in clock gate control register */
        /* *clock_gate_control_reg &= ~(1 << 13); */
}

/*-------------------------------------------------------------------------*/

/* configure so an HC device and id are always provided */
/* always called with process context; sleeping is OK */


/**
 * usb_hcd_ikf68xx_ohci_probe - initialize ikf68xx-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 *
 */
int usb_hcd_ikf68xx_ohci_probe (const struct hc_driver *driver,
			  struct platform_device *dev)
{
	int retval;
	struct usb_hcd *hcd;

	printk(KERN_DEBUG __FILE__": Probing USB OHCI  \n");

        
        /* offset 1 contains IRQ info */
	if(dev->resource[1].flags != IORESOURCE_IRQ) {
		pr_debug ("resource[1] is not IORESOURCE_IRQ");
		return -ENOMEM;
	}

	hcd = usb_create_hcd(driver, &dev->dev, "ikf68xx ohci");
	if (!hcd)
		return -ENOMEM;

        /* offset 0 contains OHCI info */
	hcd->rsrc_start = dev->resource[0].start;
	hcd->rsrc_len = dev->resource[0].end - dev->resource[0].start + 1;

	if (!request_mem_region(hcd->rsrc_start, hcd->rsrc_len, hcd_name)) {
		pr_debug("request_mem_region failed");
		retval = -EBUSY;
		goto err1;
	}

	hcd->regs = ioremap(hcd->rsrc_start, hcd->rsrc_len);
	if (!hcd->regs) {
		pr_debug("ioremap failed");
		retval = -ENOMEM;
		goto err2;
	}

        ikf68xx_start_ohci_hc(dev);
	ohci_hcd_init(hcd_to_ohci(hcd));

	retval = usb_add_hcd(hcd, dev->resource[1].start, IRQF_DISABLED | IRQF_SHARED);
	if (retval == 0)
		return retval;

	ikf68xx_stop_ohci_hc(dev);
	iounmap(hcd->regs);
 err2:
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
 err1:
	usb_put_hcd(hcd);
	return retval;
}


/* may be called without controller electrically present */
/* may be called with controller, bus, and devices active */

/**
 * usb_hcd_ikf68xx_remove - shutdown processing for ikf68xx-based HCDs
 * @dev: USB Host Controller being removed
 * Context: !in_interrupt()
 *
 * Reverses the effect of usb_hcd_ikf68xx_ohci_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 *
 */
void usb_hcd_ikf68xx_remove (struct usb_hcd *hcd, struct platform_device *dev)
{
	usb_remove_hcd(hcd);
        pr_debug("stopping IKF68XX USB OHCI Controller\n");
	ikf68xx_stop_ohci_hc(dev);
	iounmap(hcd->regs);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
}

/*-------------------------------------------------------------------------*/

static int __devinit
ohci_ikf68xx_start (struct usb_hcd *hcd)
{
	struct ohci_hcd	*ohci = hcd_to_ohci (hcd);
	int		ret;

	ohci_dbg (ohci, "ohci_ikf68xx_start, ohci:%p", ohci);

	if ((ret = ohci_init (ohci)) < 0)
		return ret;

	if ((ret = ohci_run (ohci)) < 0) {
		err ("can't start %s", hcd->self.bus_name);
		ohci_stop (hcd);
		return ret;
	}

	return 0;
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ohci_ikf68xx_hc_driver = {
	.description =		hcd_name,
	.product_desc =		"Ikanos On-Chip OHCI Host Controller",
	.hcd_priv_size =	sizeof(struct ohci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq =			ohci_irq,
	.flags =		HCD_USB11 | HCD_MEMORY,

	/*
	 * basic lifecycle operations
	 */
	.start =		ohci_ikf68xx_start,
#ifdef	CONFIG_PM
	/* suspend:		ohci_ikf68xx_suspend,  -- tbd */
	/* resume:		ohci_ikf68xx_resume,   -- tbd */
#endif /*CONFIG_PM*/
	.stop =			ohci_stop,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue =		ohci_urb_enqueue,
	.urb_dequeue =		ohci_urb_dequeue,
	.endpoint_disable =	ohci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number =	ohci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data =	ohci_hub_status_data,
	.hub_control =		ohci_hub_control,
};

/*-------------------------------------------------------------------------*/

static int ohci_hcd_ikf68xx_drv_probe(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	int ret;

	pr_debug ("In ohci_hcd_ikf68xx_drv_probe");

	if (usb_disabled())
		return -ENODEV;

	ret = usb_hcd_ikf68xx_ohci_probe(&ohci_ikf68xx_hc_driver, pdev);
	return ret;
}

static int ohci_hcd_ikf68xx_drv_remove(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	pr_debug ("In ohci_hcd_ikf68xx_drv_remove");
	usb_hcd_ikf68xx_remove(hcd, pdev);
	return 0;
}
	/*TBD*/
/*static int ohci_hcd_ikf68xx_drv_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	return 0;
}
static int ohci_hcd_ikf68xx_drv_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	return 0;
}
*/

static struct device_driver ohci_hcd_ikf68xx_driver = {
	.name		= "ikf68xx-ohci-hcd",
	.bus		= &platform_bus_type,
	.probe		= ohci_hcd_ikf68xx_drv_probe,
	.remove		= ohci_hcd_ikf68xx_drv_remove,
	/*.suspend	= ohci_hcd_ikf68xx_drv_suspend, */
	/*.resume	= ohci_hcd_ikf68xx_drv_resume, */
};

static int __init ohci_hcd_ikf68xx_init (void)
{
	pr_debug (DRIVER_INFO " (IKF68XX OHCI)\n");
	pr_debug ("block sizes: ed %d td %d\n",
		sizeof (struct ed), sizeof (struct td));

	return driver_register(&ohci_hcd_ikf68xx_driver);
}

static void __exit ohci_hcd_ikf68xx_cleanup (void)
{
	driver_unregister(&ohci_hcd_ikf68xx_driver);
}

module_init (ohci_hcd_ikf68xx_init);
module_exit (ohci_hcd_ikf68xx_cleanup);
