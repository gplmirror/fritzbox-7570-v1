/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2006,2007,2008,2009 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef _CPPHY_TYPES_H_
#define _CPPHY_TYPES_H_

#include <linux/workqueue.h>
#include <linux/wait.h>
#include <linux/avm_cpmac.h>
#include "cpmac_priority.h"

typedef struct {
  struct net_device *p_dev;
  cpmac_phy_handle_t phy_handle;
  cpmac_service_funcs_t service_funcs;
} dev_desc_t;

typedef enum {
    CPPHY_HW_ST_INITIALIZED = 1,
    CPPHY_HW_ST_OPENED
} cpphy_hw_state_t;

typedef enum {
    CPPHY_MDIO_ST_FINDING    = 2,
    CPPHY_MDIO_ST_FOUND      = 3,
    CPPHY_MDIO_ST_NWAY_START = 4,
    CPPHY_MDIO_ST_NWAY_WAIT  = 5,
    CPPHY_MDIO_ST_LINK_WAIT  = 6,
    CPPHY_MDIO_ST_LINKED     = 7,
    CPPHY_MDIO_ST_LOOPBACK   = 8,
    CPPHY_MDIO_ST_PDOWN      = 9
} cpphy_mdio_state_t;

typedef enum {
    CPPHY_SWITCH_MODE_16BIT = (1 << 0),
    CPPHY_SWITCH_MODE_READ  = (1 << 1),
    CPPHY_SWITCH_MODE_WRITE = (1 << 2)
} cpphy_switch_mode_t;

struct avm_new_switch_struct {
    struct {
        unsigned char  written;           /* Flag, if info is already written */
        unsigned char  keep_tag_outgoing; /* Tag outgoing packets from this port */
        unsigned short vid;               /* Tag untagged incoming packets with this VID */
        unsigned short port_vlan_mask;    /* Configured ports for port VLAN */
    } port[AVM_CPMAC_MAX_PORTS];
    struct {
        unsigned char  written : 1;       /* Flag, if info is already written */
        unsigned char  active  : 1;       /* Flag, if info is already written */
        unsigned char  target_mask;       /* Mask for the ports that should receive tagged packets */
        unsigned short vid;               /* ID of the VLAN group (preferrably 2-15) */
        union {
            struct {
                unsigned short VID : 12;
                unsigned short VP  :  3;    /*--- Vlan-Priority ---*/
                unsigned short VV  :  1;    /*--- Vlan-Valid ---*/
            } Bits;
            unsigned short Register;
        } VFL;
        union {
            struct {
                unsigned short M   : 7;     /*--- Member ---*/
                unsigned short TM  : 7;     /*--- Tagged-Member ---*/
                unsigned short FID : 2;     /*--- Filter ID ---*/
            } Bits;
            unsigned short Register;
        } VFH;
    } vlan[AVM_CPMAC_MAX_VLAN_GROUPS];
    unsigned char cpu_port;
};

typedef enum {
    CPPHY_POWER_GLOBAL_NONE = 0,
    CPPHY_POWER_GLOBAL_OFF  = 1,
    CPPHY_POWER_GLOBAL_ON   = 2
} cpphy_power_mode_t;

typedef enum {
    CPPHY_POWER_MODE_OFF = 0,
    CPPHY_POWER_MODE_ON,
    CPPHY_POWER_MODE_ON_IDLE_MDI,
    CPPHY_POWER_MODE_ON_IDLE_MDIX,
    CPPHY_POWER_MODE_ON_REDUCE_EMI,
    CPPHY_POWER_MODE_ON_TESTMODE_10,
    CPPHY_POWER_MODE_ON_TESTMODE_100,
    CPPHY_POWER_MODE_ON_STOP_XOVER,
    CPPHY_POWER_MODE_LINK_LOST,
    CPPHY_POWER_MODE_ON_MDI,
    CPPHY_POWER_MODE_ON_MDI_MLT3,
    CPPHY_POWER_MODE_ON_MDI_NLP,
    CPPHY_POWER_MODE_ON_MDIX,
    CPPHY_POWER_MODES
} cpphy_switch_powermodes_t;

typedef struct {
    unsigned int  timeout;
    unsigned char current_on;
    unsigned char current_mode;
    unsigned char is_mdix;
    unsigned char current_port;
    unsigned int  linked_ports;
    unsigned int  roundrobin;
    adm_phy_power_setup_t setup;
    unsigned char port_on[AVM_CPMAC_MAX_PORTS];
    unsigned char port_mode[AVM_CPMAC_MAX_PORTS];
    unsigned long time_to_wait_to[AVM_CPMAC_MAX_PORTS];
} cpphy_adm_power;

typedef enum {
    CPMAC_WORK_TOGGLE_VLAN = 0,
    CPMAC_WORK_TICK,
    CPMAC_WORK_UPDATE_MAC_TABLE, 
    CPMAC_WORK_SWITCH_DUMP,
    CPMAC_WORK_TOGGLE_PPPOA,
    CPMAC_WORK_MAX_ITEMS
} cpmac_work_ident_t;

typedef struct cpphy_mdio_t_struct *cpphy_mdio_t_struct_ptr;
typedef unsigned long (*cpmac_work_function) (cpphy_mdio_t_struct_ptr mdio);
typedef struct {
    unsigned int        active;
    cpmac_work_function item;
    unsigned long       next_run;
} cpmac_work_item_t;

typedef struct cpphy_mdio_t_struct {
    cpphy_mdio_state_t             state;
    unsigned int                   testvalue; /* FIXME for testing */
    unsigned int                   nway_mode;
    unsigned int                   control;
    unsigned int                   timeout;
    unsigned int                   phy_num;
    unsigned int                   inst;
    cpmac_priv_t                  *cpmac_priv;
    cpmac_work_item_t              work[CPMAC_WORK_MAX_ITEMS];
    struct work_struct             cpmac_work;
    volatile unsigned int          stop_work;
    volatile unsigned int          work_to_do;
    wait_queue_head_t              waitqueue;
    unsigned int                   switch_dump_value;
    volatile unsigned int          wan_tagging_enable;
    unsigned int                   run_ar_queue_without_removal_counter;
    struct semaphore               semaphore;
    struct semaphore               semaphore_switch;
    unsigned int                   cpmac_switch;
    unsigned int                   switch_ports;
    unsigned int                   switch_keep_tagging;
    unsigned int                   is_vinax;
    cpphy_adm_power                adm_power;
    cpphy_power_mode_t             global_power;
    cpphy_power_mode_t             global_power_off;
    unsigned int                   linked;
    unsigned int                   half_duplex;
    unsigned int                   slow_speed;
    unsigned int                   mdix;
    unsigned int                   power_down;
    void                          *power_handle;
    cpphy_switch_mode_t            Mode;
    unsigned int                   cpmac_irq;
    struct avm_cpmac_config_struct cpmac_config;
    struct avm_new_switch_struct   switch_status;
    cpmac_switch_configuration_t  *switch_config;
    unsigned int                   switch_use_EMV;
    unsigned int                   mode_pppoa;
    unsigned int                   event_update_needed;
    struct cpmac_event_struct      event_data;
} cpphy_mdio_t;

/* Defined in cpmac_priority.h */
/*--- typedef struct { ...  } cpphy_tcb_t; ---*/
/*--- typedef struct { ...  } cpphy_rcb_t; ---*/

typedef struct cpphy_cppi_struct {
    int                     RxTeardownPending;
    cpphy_rcb_t            *RxPrevEnqueue;
    cpphy_rcb_t            *RxCurrDequeue;
    cpphy_rcb_t            *RxFirst;
    cpphy_rcb_t            *RxLast;
    char                   *RcbStart;
    cpphy_tcb_t            *TxPrevEnqueue;
    cpphy_tcb_t            *TxCurrDequeue;
    volatile cpphy_tcb_t   *TxFirst;
    volatile cpphy_tcb_t   *TxLast;
    volatile cpphy_tcb_t   *TxFirstFree;
    volatile cpphy_tcb_t   *TxLastFree;
    cpphy_tcb_prio_queues_t TxPrioQueues;
    unsigned int            NeededDMAtcbs;
    struct {
        unsigned int        tcbs_alloced;
        unsigned int        tcbs_freed;
        unsigned int        tcbs_alloced_dynamic;
        unsigned int        tcbs_freed_dynamic;
    } support;
    atomic_t                dequeue_running;
    atomic_t                dma_send_running;
    volatile unsigned int   TxDmaActive;
    int                     TxTeardownPending;
    char                   *TcbStart;
    int                     TxOpen;
    int                     RxOpen;
    unsigned char           TxChannel;
    unsigned int            MaxNeedCount;
    unsigned int            CurrNeedCount;
    cpphy_hw_state_t        hw_state;
    unsigned int            hash1;
    unsigned int            hash2;
    cpmac_priv_t           *cpmac_priv;
    cpphy_mdio_t           *mdio;
} cpphy_cppi_t;

typedef struct {
    cpmac_priv_t *cpmac_priv;
    unsigned int  instance;
    unsigned int  high_phy;
    unsigned int  cpmac_switch;
    unsigned int  is_vinax;
    unsigned int  switch_use_EMV;
    cpphy_cppi_t  cppi;
    cpphy_mdio_t  mdio;
    unsigned int  is_used;
} cpphy_global_t;

typedef struct {
    cpphy_global_t           cpphy[CPMAC_MAX_PHY];
    struct workqueue_struct *workqueue;
    unsigned int             timer_count;
    struct timer_list        timer;
} cpmac_global_t;

extern cpphy_global_t cpmac_cpphy_global[];
extern cpmac_global_t cpmac_global;

#endif /*--- #ifndef _CPPHY_TYPES_H_ ---*/

