/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2006,2007,2008,2009 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef _CPMAC_DEBUG_H_
#define _CPMAC_DEBUG_H_

#undef  CPMAC_DMA_RX_PRIOQUEUE_DEBUG
#undef  CPMAC_DMA_TX_PRIOQUEUE_DEBUG
#undef  CPMAC_SCHED_PRIOQUEUE_DEBUG
#undef  CPMAC_EXTERNAL_COMMAND_DEBUG /* Enable command to call test functions */

#if defined(DEB_ERR)
#undef DEB_ERR
#endif
#if defined(DEB_WARN)
#undef DEB_WARN
#endif
#if defined(DEB_INFO)
#undef DEB_INFO
#endif
#if defined(DEB_TRC)
#undef DEB_TRC
#endif
#define DEB_SUPPORT(a...) printk(KERN_ERR "[cpmac] " a);
/*--- #define DEB_SUPPORT(a...) ---*/
#define DEB_ERR(a...) printk(KERN_ERR "[cpmac] " a);
#define DEB_WARN(a...) printk(KERN_WARNING "[cpmac] " a);
/*--- #define DEB_INFO(a...) printk (KERN_ERR "[cpmac] " a); ---*/
#define DEB_INFO(a...)
/*--- #define DEB_INFOTRC(a...) printk(KERN_ERR "[cpmac] " a); ---*/
#define DEB_INFOTRC(a...) 
/*--- #define DEB_TRC(a...) printk(KERN_ERR "[cpmac] " a); ---*/
#define DEB_TRC(a...)
/*--- #define DEB_TEST(a...) DEB_ERR("[test] " a); ---*/
#define DEB_TEST(a...)

#define assert(i)  BUG_ON(!(i))
/*--- #define assert(exp) ---*/

#endif
