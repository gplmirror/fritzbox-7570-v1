/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2006,2007,2008 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef _CPPHY_ADM6996_H_
#define _CPPHY_ADM6996_H_

#include <linux/avm_cpmac.h>
#include "cpmac_main.h"

/* Interprete four bits of the VID as VID group (FID) starting from bit given here */
#define ADM_6996_TAGSHIFT 8
#define ADM_GET_TAG_GROUP(x) (((x) >> ADM_6996_TAGSHIFT) & 0xf)

#define ADM_DEVICE_ADDRESS 0x100
#define ADM_SERIAL_OFFSET 0x200
#define ADM_GET_SERIAL_REG(mdio, addr) adm_read_32Bit_cached((mdio), (addr))
#define ADM_GET_EEPROM_REG(mdio, addr) adm_read((mdio), (addr))
/*--- #define ADM_GET_EEPROM_REG(mdio, addr) adm_safe_read((mdio), (addr)) ---*/
#define ADM_PUT_EEPROM_REG(mdio, addr, dat) adm_write((mdio), (addr), (dat))

void adm_init(cpphy_mdio_t *mdio);
unsigned int adm_read_32Bit_cached(cpphy_mdio_t *mdio, unsigned int addr);
unsigned int adm_read(cpphy_mdio_t *mdio, unsigned int addr);
unsigned short tantos_read(cpphy_mdio_t *mdio, unsigned short *ptr);
unsigned long switch_dump(cpphy_mdio_t *mdio);
int adm_write(cpphy_mdio_t *mdio, unsigned int addr, unsigned int dat);
void adm_update_cache(cpphy_mdio_t *mdio);
void adm_update_error_status(cpphy_mdio_t *mdio, unsigned int port);
cpmac_err_t adm_set_mode_pppoa(cpphy_mdio_t *mdio, unsigned int enable_pppoa);
void adm_vlan_fbox_reset(cpphy_mdio_t *mdio);
void adm_prepare_reboot(cpphy_mdio_t *mdio);
unsigned int adm_check_link(cpphy_mdio_t *mdio);
void adm_switch_port_power(cpphy_mdio_t *mdio, unsigned int port, unsigned int power_on);
cpmac_err_t adm_switch_port_power_config(cpphy_mdio_t          *mdio,
                                         adm_phy_power_setup_t *setup,
                                         unsigned char          set);
#if defined(CONFIG_AVM_POWER)
int adm_power_config(int state);
#endif /*--- #if defined(CONFIG_AVM_POWER) ---*/

void set_map_in(cpphy_mdio_t *mdio,
                unsigned short vid,
                unsigned short device,
                unsigned short port);
struct net_device *find_or_register_device(char *name,
                                           cpmac_priv_t *cpmac_priv,
                                           unsigned short VID);
cpmac_err_t adm_configure_cpmac(cpphy_mdio_t *mdio, struct avm_cpmac_config_struct *config);
cpmac_err_t adm_configure_special(cpphy_mdio_t *mdio, cpmac_switch_configuration_t *config);
cpmac_err_t adm_get_config_cpmac(cpphy_mdio_t *mdio, struct avm_cpmac_config_struct *config);
cpmac_err_t adm_get_configured(cpphy_mdio_t *mdio, cpmac_switch_configuration_t *config);
unsigned long adm_set_wan_keep_tagging(cpphy_mdio_t *mdio);

#endif
