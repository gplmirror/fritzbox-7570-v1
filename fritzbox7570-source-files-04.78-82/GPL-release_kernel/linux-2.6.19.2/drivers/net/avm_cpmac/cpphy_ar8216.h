/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2006,2007,2008,2009 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef _CPPHY_AR8216_H_
#define _CPPHY_AR8216_H_

#include <linux/avm_cpmac.h>

int ar_init(cpphy_mdio_t *mdio);
void ar_vlan_fbox_reset (cpphy_mdio_t *mdio);
unsigned int ar_check_link_orig(cpphy_mdio_t *mdio);
unsigned int ar_check_link_emv(cpphy_mdio_t *mdio);
unsigned int ar_switch_test_link(cpphy_mdio_t *mdio, unsigned int port);
void ar_switch_port_power_orig(cpphy_mdio_t *mdio, unsigned int port, unsigned int power_on);
void ar_switch_port_power_emv(cpphy_mdio_t *mdio, unsigned int port, unsigned int power_on);
void ar_boot_power_on_emv(cpphy_mdio_t *mdio);
void ar_switch_port_power_on_boot_orig(cpphy_mdio_t *mdio, unsigned int portset);
void ar8216_mdio_write16(cpphy_mdio_t *mdio, unsigned int regadr, unsigned int data);
void ar8216_mdio_write32(cpphy_mdio_t *mdio, unsigned int regadr, unsigned int data);
unsigned int ar8216_mdio_read32(cpphy_mdio_t *mdio, unsigned int regadr);
enum ath_display_regs { ATH_PORT_CONTROL, ATH_CONTROL, ATH_MII };
void display_ath_regs(cpphy_mdio_t *mdio, enum ath_display_regs control);

unsigned long ar8216_ar_work_item(cpphy_mdio_t *mdio);
unsigned int ar8216_get_phy_port(cpphy_mdio_t *mdio, struct sk_buff *skb);

cpmac_err_t ar_configure_cpmac(cpphy_mdio_t *mdio, struct avm_cpmac_config_struct *config);

#define ATH_GET_TAG_GROUP(x) (((x) - 0x10) & 0xf)

#endif /*--- #ifndef _CPPHY_AR8216_H_ ---*/

