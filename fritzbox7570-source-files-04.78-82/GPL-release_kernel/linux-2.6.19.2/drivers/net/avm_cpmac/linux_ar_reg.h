/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2008,2009 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

/****************************************************************************
**      AR8216 support
*****************************************************************************/
#ifndef _INC_AR_REG
#define _INC_AR_REG

/*------------------------------------------------------------------------------------------*\
 * global Register 32bit
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_DEVICE_ID         0x00
#define AR8216_DEVICE_ID                0x101
#define AR8316_DEVICE_ID                0x1001

#define AR8216_GLOBAL_INTERRUPT         0x10
#define AR8216_GLOBAL_INTERRUPTMASK     0x14
#define AR8216_GLOBAL_MAC_ADDRESS_HIGH  0x20
#define AR8216_GLOBAL_MAC_ADDRESS_LOW   0x24
#define AR8216_GLOBAL_CONTROL           0x30

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_VLAN_TABLE_1      0x40
#define VLAN_TABLE_1_VT_FUNC_SHIFT          0
#define VLAN_TABLE_1_VT_FUNC_MASK           (((1<<2)-1)<< VLAN_TABLE_1_VT_FUNC_SHIFT)               
#define VLAN_TABLE_1_VT_FUNC_VALUE(value)   (((value)<< VLAN_TABLE_1_VT_FUNC_SHIFT) & VLAN_TABLE_1_VT_FUNC_MASK)           
  #define VLAN_TABLE_FUNC_NOOP              0
  #define VLAN_TABLE_FUNC_FLUSH_ALL         1
  #define VLAN_TABLE_FUNC_LOAD_ENTRY        2
  #define VLAN_TABLE_FUNC_PURGE_ENTRY       3
  #define VLAN_TABLE_FUNC_REMOVE_PORT       4
#define VLAN_TABLE_1_VT_BUSY                (1 << 3)
#define VLAN_TABLE_1_VT_FULL_VIO            (1 << 4)
#define VLAN_TABLE_1_VT_PORT_NUM_SHIFT      8
#define VLAN_TABLE_1_VT_PORT_NUM_MASK       (((1<<4)-1)<< VLAN_TABLE_1_VT_PORT_NUM_SHIFT)               
#define VLAN_TABLE_1_VT_PORT_NUM_VALUE(value) (((value)<< VLAN_TABLE_1_VT_PORT_NUM_SHIFT) & VLAN_TABLE_1_VT_PORT_NUM_MASK)           
#define VLAN_TABLE_1_VID_SHIFT              16
#define VLAN_TABLE_1_VID_MASK               (((1<<12)-1)<< VLAN_TABLE_1_VID_SHIFT)               
#define VLAN_TABLE_1_VID_VALUE(value)       (((value)<< VLAN_TABLE_1_VID_SHIFT) & VLAN_TABLE_1_VID_MASK)           
#define VLAN_TABLE_1_VT_PRI_SHIFT           28
#define VLAN_TABLE_1_VT_PRI_MASK            (((1<<3)-1)<< VLAN_TABLE_1_VT_PRI_SHIFT)               
#define VLAN_TABLE_1_VT_PRI_VALUE(value)    (((value)<< VLAN_TABLE_1_VT_PRI_SHIFT) & VLAN_TABLE_1_VT_PRI_MASK)           
#define VLAN_TABLE_1_VT_PRI_EN              (1U << 31)

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_VLAN_TABLE_2      0x44
#define VLAN_TABLE_2_VID_MEM_SHIFT          0
#define VLAN_TABLE_2_VID_MEM_MASK           (((1<<10)-1)<< VLAN_TABLE_2_VID_MEM_SHIFT)               
#define VLAN_TABLE_2_VID_MEM_VALUE(value)   (((value)<< VLAN_TABLE_2_VID_MEM_SHIFT) & VLAN_TABLE_2_VID_MEM_MASK)           
#define VLAN_TABLE_2_VT_VALID               (1 << 11)

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_AR_1              0x50
#define AR_1_AT_ADDR_BYTE4_SHIFT          24
#define AR_1_AT_ADDR_BYTE4_MASK           (((1 << 8)-1)<< AR_1_AT_ADDR_BYTE4_SHIFT)               
#define AR_1_AT_ADDR_BYTE4_VALUE(value)   (((value)<< AR_1_AT_ADDR_BYTE4_SHIFT) & AR_1_AT_ADDR_BYTE4_MASK)           

#define AR_1_AT_ADDR_BYTE5_SHIFT          16
#define AR_1_AT_ADDR_BYTE5_MASK           (((1 << 8)-1)<< AR_1_AT_ADDR_BYTE5_SHIFT)               
#define AR_1_AT_ADDR_BYTE5_VALUE(value)   (((value)<< AR_1_AT_ADDR_BYTE5_SHIFT) & AR_1_AT_ADDR_BYTE5_MASK)           

#define AR_1_AR_FULL_VIO                  (1 << 12)

#define AR_1_AT_PORT_NUM_SHIFT            8
#define AR_1_AT_PORT_NUM_MASK             (((1<<4)-1)<< AR_1_AT_PORT_NUM_SHIFT)               
#define AR_1_AT_PORT_NUM_VALUE(value)     (((value)<< AR_1_AT_PORT_NUM_SHIFT) & AR_1_AT_PORT_NUM_MASK)           

#define AR_1_AT_BUSY                      (1 << 3)

#define AR_1_AT_FUNC_SHIFT                0
#define AR_1_AT_FUNC_MASK                 (((1<<3)-1)<< AR_1_AT_FUNC_SHIFT)               
#define AR_1_AT_FUNC_VALUE(value)         (((value)<< AR_1_AT_FUNC_SHIFT) & AR_1_AT_FUNC_MASK)           

/*--- #define _SHIFT          0 ---*/
/*--- #define _MASK           (((1<<10)-1)<< _SHIFT) ---*/               
/*--- #define _VALUE(value)   (((value)<< _SHIFT) & _MASK) ---*/           

/*--- #define _SHIFT          0 ---*/
/*--- #define _MASK           (((1<<10)-1)<< _SHIFT) ---*/               
/*--- #define _VALUE(value)   (((value)<< _SHIFT) & _MASK) ---*/           

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_AR_2              0x54
#define AR_2_AT_ADDR_BYTE0_SHIFT          24
#define AR_2_AT_ADDR_BYTE0_MASK           (((1 << 8)-1) << AR_2_AT_ADDR_BYTE0_SHIFT)               
#define AR_2_AT_ADDR_BYTE0_VALUE(value)   (((value) << AR_2_AT_ADDR_BYTE0_SHIFT) & AR_2_AT_ADDR_BYTE0_MASK)           

#define AR_2_AT_ADDR_BYTE1_SHIFT          16 
#define AR_2_AT_ADDR_BYTE1_MASK           (((1 << 8)-1) << AR_2_AT_ADDR_BYTE1_SHIFT)               
#define AR_2_AT_ADDR_BYTE1_VALUE(value)   (((value) << AR_2_AT_ADDR_BYTE1_SHIFT) & AR_2_AT_ADDR_BYTE1_MASK)           

#define AR_2_AT_ADDR_BYTE2_SHIFT          8
#define AR_2_AT_ADDR_BYTE2_MASK           (((1 << 8)-1) << AR_2_AT_ADDR_BYTE2_SHIFT)               
#define AR_2_AT_ADDR_BYTE2_VALUE(value)   (((value) << AR_2_AT_ADDR_BYTE2_SHIFT) & AR_2_AT_ADDR_BYTE2_MASK)           

#define AR_2_AT_ADDR_BYTE3_SHIFT          0
#define AR_2_AT_ADDR_BYTE3_MASK           (((1 << 8)-1) << AR_2_AT_ADDR_BYTE3_SHIFT)               
#define AR_2_AT_ADDR_BYTE3_VALUE(value)   (((value) << AR_2_AT_ADDR_BYTE3_SHIFT) & AR_2_AT_ADDR_BYTE3_MASK)           

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_AR_3              0x58
#define AR_3_DES_PORT_SHIFT             0
#define AR_3_DES_PORT_MASK              (((1<<10)-1)<< AR_3_DES_PORT_SHIFT)               
#define AR_3_DES_PORT_VALUE(value)      (((value)<< AR_3_DES_PORT_SHIFT) & _MASK)           

#define AR_3_AT_PRIORITY_SHIFT          10
#define AR_3_AT_PRIORITY_MASK           (((1<<2)-1)<< AR_3_AT_PRIORITY_SHIFT)               
#define AR_3_AT_PRIORITY_VALUE(value)   (((value)<< AR_3_AT_PRIORITY_SHIFT) & AR_3_AT_PRIORITY_MASK)           

#define AR_3_AT_PRIORITY_EN             (1 << 12)
#define AR_3_AT_MIRROR_EN               (1 << 13)

#define AR_3_AT_STATUS_SHIFT            14
#define AR_3_AT_STATUS_MASK             (((1<<2)-1)<< AR_3_AT_STATUS_SHIFT)               
#define AR_3_AT_STATUS_VALUE(value)     (((value)<< AR_3_AT_STATUS_SHIFT) & AR_3_AT_STATUS_MASK)           

#define AR_3_SA_DROP_EN              (1 << 16)
#define AR_3_REDIRECT_TO_CPU         (1 << 25)
#define AR_3_COPY_TO_CPU             (1 << 26)

/*--- #define _SHIFT          0 ---*/
/*--- #define _MASK           (((1<<10)-1)<< _SHIFT) ---*/               
/*--- #define _VALUE(value)   (((value)<< _SHIFT) & _MASK) ---*/           

/*--- #define _SHIFT          0 ---*/
/*--- #define _MASK           (((1<<10)-1)<< _SHIFT) ---*/               
/*--- #define _VALUE(value)   (((value)<< _SHIFT) & _MASK) ---*/           

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_GLOBAL_AR_CONTROL        0x5C

#define AR_CONTROL_AGE_EN            (1 << 17)
#define AR_CONTROL_AGE_TIME_SHIFT    0
#define AR_CONTROL_AGE_TIME_MASK     (((1 << 16) - 1) << AR_CONTROL_AGE_TIME_SHIFT)               
#define AR_CONTROL_AGE_TIME(value)   (((value) << AR_CONTROL_AGE_TIME_SHIFT) & AR_CONTROL_AGE_TIME_MASK)           

#define AR8216_GLOBAL_IP_PRIORITY_1     0x60
#define AR8216_GLOBAL_IP_PRIORITY_2     0x64
#define AR8216_GLOBAL_IP_PRIORITY_3     0x68
#define AR8216_GLOBAL_IP_PRIORITY_4     0x6C
#define AR8216_GLOBAL_TAG_PRIORITY      0x70
#define AR8216_GLOBAL_CPUPORT           0x78
#define AR8216_GLOBAL_MIB               0x80
#define AR8216_GLOBAL_MDIO_HIGH_ADDR    0x94
#define AR8216_GLOBAL_DEST_IP           0x98

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define AR8216_PORT0_CONTROL_BASIS      0x100
#define AR8216_PORT1_CONTROL_BASIS      0x200
#define AR8216_PORT2_CONTROL_BASIS      0x300
#define AR8216_PORT3_CONTROL_BASIS      0x400
#define AR8216_PORT4_CONTROL_BASIS      0x500
#define AR8216_PORT5_CONTROL_BASIS      0x600

/*--- PORT-STATUS ---*/
#define AR8216_PORT_STATUS_OFFSET       0x00
#define ATH_PORT_STATUS_SPEED_10M           (0<<0)
#define ATH_PORT_STATUS_SPEED_100M          (1<<0)
#define ATH_PORT_STATUS_TXMACEN             (1<<2)
#define ATH_PORT_STATUS_RXMACEN             (1<<3)
#define ATH_PORT_STATUS_TXFLOWEN            (1<<4)
#define ATH_PORT_STATUS_RXFLOWEN            (1<<5)
#define ATH_PORT_STATUS_FULLDUPLEX          (1<<6)
#define ATH_PORT_STATUS_PHY_LINKUP          (1<<8)
#define ATH_PORT_STATUS_LINKEN              (1<<9)
#define ATH_PORT_STATUS_LINK_PAUSE          (1<<10)
#define ATH_PORT_STATUS_ASYNC_PAUSE         (1<<11)

/*--- PORT-CONTROL ---*/
#define AR8216_PORT_CONTROL_OFFSET      0x04
#define ATH_PORT_CTRL_PORTSTATE_DISABLE     (0<<0)
#define ATH_PORT_CTRL_PORTSTATE_BLOCKING    (1)
#define ATH_PORT_CTRL_PORTSTATE_LISTEN      (2)
#define ATH_PORT_CTRL_PORTSTATE_LERNING     (3)
#define ATH_PORT_CTRL_PORTSTATE_FORWARD     (4)
#define ATH_PORT_CTRL_LERN_ONE_LOCK         (1<<7)
#define ATH_PORT_CTRL_EG_VLAN_MODE(x)       ((x)<<8)
#define ATH_PORT_CTRL_IGMP_MLD_EN           (1<<10)
#define ATH_PORT_CTRL_HEAD_EN               (1<<11)
#define ATH_PORT_CTRL_MAC_LOOP_BACK         (1<<12)
#define ATH_PORT_CTRL_SINGLE_VLAN_EN        (1<<13)
#define ATH_PORT_CTRL_LEARN_EN              (1<<14)
#define ATH_PORT_CTRL_EG_MIRROR_EN          (1<<16)
#define ATH_PORT_CTRL_ING_MIRROR_EN         (1<<17)

/*--- PORT-VLAN ---*/
#define AR8216_PORT_VLAN_OFFSET            0x08
#define ATH_PORT_VLAN_VID_SHIFT             0
#define ATH_PORT_VLAN_VID_MASK              (((1<<12)-1)<<ATH_PORT_VLAN_VID_SHIFT)               
#define ATH_PORT_VLAN_VID_VALUE(value)      (((value)<<ATH_PORT_VLAN_VID_SHIFT)&ATH_PORT_VLAN_VID_MASK)           
#define ATH_PORT_VLAN_VID_MEM_SHIFT         16
#define ATH_PORT_VLAN_VID_MEM_MASK          (((1<<10)-1)<<ATH_PORT_VLAN_VID_MEM_SHIFT)               
#define ATH_PORT_VLAN_VID_MEM_VALUE(value)  (((value)<<ATH_PORT_VLAN_VID_MEM_SHIFT)&ATH_PORT_VLAN_VID_MEM_MASK)           
#define ATH_PORT_VLAN_EG_PRIO_0             (1<<27)
#define ATH_PORT_VLAN_ING_PRI_SHIFT         28
#define ATH_PORT_VLAN_ING_PRI_MASK          (((1<<2)-1)<<ATH_PORT_VLAN_ING_PRI_SHIFT)               
#define ATH_PORT_VLAN_ING_PRI_VALUE(value)  (((value)<<ATH_PORT_VLAN_ING_PRI_SHIFT)&ATH_PORT_VLAN_ING_PRI_MASK)           
#define ATH_PORT_VLAN_8021_QMODE_SHIFT      30
#define ATH_PORT_VLAN_8021_QMODE_MASK          (((1<<2)-1)<<ATH_PORT_VLAN_8021_QMODE_SHIFT)               
#define ATH_PORT_VLAN_8021_QMODE_VALUE(value)  (((value)<<ATH_PORT_VLAN_8021_QMODE_SHIFT)&ATH_PORT_VLAN_8021_QMODE_MASK)           
  #define ATH_PORT_VLAN_8021_QMODE_DISABLE  0
  #define ATH_PORT_VLAN_8021_QMODE_FALLBACK 1
  #define ATH_PORT_VLAN_8021_QMODE_CHECK    2
  #define ATH_PORT_VLAN_8021_QMODE_SECURE   3

/*--- PORT-RATE ---*/
#define AR8216_PORT_RATE_LIMIT_OFFSET      0x0C
/*--- PORT-PRIORITY ---*/
#define AR8216_PORT_PRIORITY_OFFSET        0x10

struct ar_port_status { 
    enum _link_speed { 
        link_speed_10M   = 0,
        link_speed_100M  = 1,
        link_speed_1000M = 2,
        link_speed_error = 3
    } link_speed : 2;
    unsigned int txmac_enable : 1;
    unsigned int rxmac_enable : 1;
    unsigned int tx_flow_enable : 1;
    unsigned int rx_flow_enable : 1;
    unsigned int full_duplex : 1;
    unsigned int reserved1 : 1;
    unsigned int link_status : 1;
    unsigned int link_mode_enable : 1;
    unsigned int link_pause_enable : 1;
    unsigned int link_async_pause_enable : 1;
    unsigned int reserved2 : (31 - 11);
};

struct ar_port_control {
    unsigned int port_state : 3;
    unsigned int reserved1  : 1;
    unsigned int learn_one_lock : 1;
    unsigned int egress_VLAN_mode : 1;
    unsigned int igmp_mld_enable : 1;
    unsigned int mac_loop_back : 1;
    unsigned int single_VLAN_enable : 1;
    unsigned int learn_enable : 1;
    unsigned int reserved2 : 1;
    unsigned int egress_mirror_enable : 1;
    unsigned int ingress_mirror_enable : 1;
    unsigned int reserved : (31-18);
};

struct ar_port_VLAN {
    unsigned int port_VID : 12;
    unsigned int reserved1 : 4;
    unsigned int port_VID_mem : (25 - 15);
    unsigned int reserved2 : 1;
    unsigned int ingress_port_prio : 2;
    unsigned int _802_1Q_mode : 2;
};

enum _ar_rate_limit {
    ar_rate_limit_128kbits,
    ar_rate_limit_256kbits,
    ar_rate_limit_512kbits,
    ar_rate_limit_1mbits,
    ar_rate_limit_2mbits,
    ar_rate_limit_4mbits,
    ar_rate_limit_8mbits,
    ar_rate_limit_16mbits,
    ar_rate_limit_32mbits,
    ar_rate_limit_64mbits
};

struct ar_rate_limit {
    enum _ar_rate_limit ingree_rate : 4;
    unsigned int reserved1 : (15 - 4);
    enum _ar_rate_limit egree_rate : 4;
    unsigned int reserved2 : (23 - 19);
    unsigned int ingress_rate_limit_enable : 1;
    unsigned int egress_rate_limit_enable : 1;
    unsigned int reserved3 : (31 - 25);
};

struct ar_priority_control {
    unsigned int port_pri_sel : 2;
    unsigned int ip_pri_sel : 2;
    unsigned int vlan_pri_sel : 2;
    unsigned int da_pri_sel : 2;
    unsigned int reserved1 : (15 - 7);
    unsigned int ip_pri_en : 1;
    unsigned int vlan_pri_en : 1;
    unsigned int da_pri_en : 1;
    unsigned int port_pri_en : 1;
    unsigned int reserved2 : (31 - 20);
};

struct ar_struct {
    struct ar_port {  /* offsets 0x100, 0x200, 0x300 .... */
        struct ar_port_status status;
        struct ar_port_control control;
        struct ar_port_VLAN port_VLAN;
        struct ar_rate_limit rate_limit;
        struct ar_priority_control priority_control;
    } part[6];
};


/*------------------------------------------------------------------------------------------*\
 * MII-Register 16bit
\*------------------------------------------------------------------------------------------*/
#define AR8216_MAX_PHYS                 4

#define AR8216_MII_CONTROL              0x00

/*--- ATHR_PHY_CONTROL fields ---*/
#define AR8216_PHYCTRL_SW_RESET        (1<<15)  
#define AR8216_PHYCTRL_LOOPBACK        (1<<14)  
#define AR8216_PHYCTRL_SPEEDSEL_LSB    (1<<13) 
#define AR8216_PHYCTRL_AUTONEG_EN      (1<<12)
#define AR8216_PHYCTRL_POWERDOWN       (1<<11)
#define AR8216_PHYCTRL_ISOLATE         (1<<10)
#define AR8216_PHYCTRL_AUTONEG_RESTART (1<<9)
#define AR8216_PHYCTRL_FULLDUPLEX      (1<<8)
#define AR8216_PHYCTRL_COLLISION_TEST  (1<<7)
#define AR8216_PHYCTRL_SPEEDSEL_MSB    (1<<6)

#define AR8216_MII_STATUS               0x01
#define AR8216_MII_PHY_ID1              0x02
#define AR8216_MII_PHY_ID2              0x03
#define AR8216_MII_AUTO_NEG_ADV         0x04

/*--- Advertisement register. ---*/
#define ATHR_ADVERTISE_NEXT_PAGE              0x8000
#define ATHR_ADVERTISE_ASYM_PAUSE             0x0800
#define ATHR_ADVERTISE_PAUSE                  0x0400
#define ATHR_ADVERTISE_100FULL                0x0100
#define ATHR_ADVERTISE_100HALF                0x0080  
#define ATHR_ADVERTISE_10FULL                 0x0040  
#define ATHR_ADVERTISE_10HALF                 0x0020  

#define ATHR_ADVERTISE_ALL (ATHR_ADVERTISE_10HALF | ATHR_ADVERTISE_10FULL | \
                            ATHR_ADVERTISE_100HALF | ATHR_ADVERTISE_100FULL | \
			                ATHR_ADVERTISE_ASYM_PAUSE | ATHR_ADVERTISE_PAUSE)


#define AR8216_MII_LINK_PARTNER         0x05
#define AR8216_MII_AUTO_NEG_EXP         0x06
#define AR8216_MII_FUNCTION_CTRL        0x10
#define AR8216_MII_PHY_STATUS           0x11

/*--- PHY-Specific Status ---*/
#define ATHR_PHY_STATUS_JABBER              (1<<0)
#define ATHR_PHY_STATUS_POLARITY            (1<<1)
#define ATHR_PHY_STATUS_RX_PAUSE_EN         (1<<2)
#define ATHR_PHY_STATUS_TX_PAUSE_EN         (1<<3)
#define ATHR_PHY_STATUS_ENERGY_DETECT       (1<<4)
#define ATHR_PHY_STATUS_SMART_SPEED_DOWN    (1<<5)
#define ATHR_PHY_STATUS_MDI_CROSSOVER       (1<<6)
#define ATHR_PHY_STATUS_LINK                (1<<10)
#define ATHR_PHY_STATUS_SPEED_DUPLEX        (1<<11)
#define ATHR_PHY_STATUS_PAGE_RECEIVED       (1<<12)
#define ATHR_PHY_STATUS_DUPLEX              (1<<13)
#define ATHR_PHY_STATUS_SPEED_SHIFT         14
#define ATHR_PHY_STATUS_SPEED_MASK          (3<<14)


#define AR8216_MII_INT_ENABLE           0x12
#define AR8216_MII_INT_STATUS           0x13
#define AR8216_MII_EXT_PHY_CTRL         0x14
#define AR8216_MII_RX_ERROR_CNT         0x15
#define AR8216_MII_VIRT_CABLE_TST       0x16
#define AR8216_MII_LED_CTRL             0x17
#define AR8216_MII_LED_MANUAL           0x18
#define AR8216_MII_VIRT_CABLE_STATUS    0x1C
#define AR8216_MII_DEBUG_ADDR           0x1D
#define AR8216_MII_DEBUG_DATA           0x1E

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- extern struct ar_struct ar_serial_register; ---*/

#endif /*--- #define _INC_AR_REG ---*/
