/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2006,2007,2008 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#ifndef _CPMAC_PRIORITY_H
#define _CPMAC_PRIORITY_H

enum cpmac_enum_prio_queues {
    CPPHY_PRIO_QUEUE_LAN = 0,
    CPPHY_PRIO_QUEUE_WAN,
    CPPHY_PRIO_QUEUES
};

typedef union BUFFER {
    volatile unsigned int Dword;
    volatile struct _BUFF_PARAM {
        volatile unsigned int length :16;
        volatile unsigned int offset :16;
    } Bits;
} BUFFER;

typedef union PACKET {
    volatile unsigned int Dword;
    volatile struct _PACKET_CTRL {
        volatile unsigned int  buffer_length :16;
        volatile unsigned int  protocol_spec :4;
        volatile unsigned int  region_offset :3;
        volatile unsigned int  packet_error  :1;
        volatile unsigned int  add_buff_cnt  :3;
        volatile unsigned int  descr_type    :1;
        volatile unsigned int  packet_type   :4;
    } Bits;
} PACKET;

typedef union PACKET_TAGS {
    volatile unsigned int Dword;
    volatile struct _BUFF_TAGS {
        volatile unsigned int dest        :16;
        volatile unsigned int sub_channel :4;
        volatile unsigned int channel     :6;
        volatile unsigned int port        :6;
    } Bits;
} PACKET_TAGS;

/* TODO Unite rcb and tcb? */
typedef struct {
#   if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7)
    unsigned int    HNext;
    unsigned int    BufPtr;
    unsigned int    Off_BLen;
    unsigned int    mode;
#   elif defined(CONFIG_MIPS_UR8) /*--- #if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) ---*/
    volatile void          *NextPacket;
    PACKET_TAGS             Tags;
    PACKET                  Packet;
    unsigned int            pData;
    BUFFER                  Buffer;
    volatile void          *NextDescr;
#   else /*--- #elif defined(CONFIG_MIPS_UR8) ---*/
#   error "No rcb definition available!"
#   endif /*--- #else ---*/ /*--- #elif defined(CONFIG_MIPS_UR8) ---*/
    void           *Next;
    struct sk_buff *skb;
    void           *KMallocPtr;
} cpphy_rcb_t;

typedef struct {
#   if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7)
    unsigned int HNext;
    unsigned int BufPtr;
    unsigned int Off_BLen;
    unsigned int mode;
#   elif defined(CONFIG_MIPS_UR8) /*--- #if defined(CONFIG_MIPS_OHIO) || defined(CONFIG_MIPS_AR7) ---*/
    volatile void          *NextPacket;
    PACKET_TAGS             Tags;
    PACKET                  Packet;
    unsigned int            pData;
	BUFFER                  Buffer;
    volatile void          *NextDescr;
#   else /*--- #elif defined(CONFIG_MIPS_UR8) ---*/
#   error "No tcb definition available!"
#   endif /*--- #else ---*/ /*--- #elif defined(CONFIG_MIPS_UR8) ---*/
    volatile struct sk_buff *skb;
    volatile void           *Next;
    volatile void           *KMallocPtr;
    volatile unsigned int    IsDynamicallyAllocated;
} cpphy_tcb_t;

typedef struct {
    volatile unsigned int  Free;
    atomic_t               DMAFree;
    spinlock_t             lock;
             unsigned int  MaxSize;
             unsigned int  MaxDMAFree;
             unsigned int  BytesEnqueued;
             unsigned int  BytesDequeued;
             unsigned int  Pause;
             unsigned int  PauseInit;
    volatile cpphy_tcb_t  *First;
    volatile cpphy_tcb_t  *Last;
} cpphy_tcb_queue_t;

typedef struct {
  unsigned int NumberOfPrioQueues;
  cpphy_tcb_queue_t q[CPPHY_PRIO_QUEUES];
} cpphy_tcb_prio_queues_t;

#endif /*--- #ifndef _CPMAC_PRIORITY_H ---*/

