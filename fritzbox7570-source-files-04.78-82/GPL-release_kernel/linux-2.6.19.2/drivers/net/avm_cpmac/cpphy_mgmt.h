/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2008,2009 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
\*------------------------------------------------------------------------------------------*/

#if !defined(_CPPHY_MGMT_H_)
#define _CPPHY_MGMT_H_

void cpphy_mgmt_power_init(cpphy_mdio_t *mdio);
unsigned int cpphy_mgmt_check_link(cpphy_mdio_t *mdio);
void cpphy_mgmt_event_dataupdate(cpphy_mdio_t *mdio);
void cpphy_mgmt_power(cpphy_mdio_t *mdio);
cpmac_err_t cpphy_mgmt_work_add(cpphy_mdio_t        *mdio, 
                                cpmac_work_ident_t   ident,
                                cpmac_work_function  function,
                                unsigned long        initial_timeout);
cpmac_err_t cpphy_mgmt_work_del(cpphy_mdio_t *mdio, cpmac_work_ident_t ident);
void cpphy_mgmt_work_start(cpphy_mdio_t *mdio);
void cpphy_mgmt_work_schedule(cpphy_mdio_t *mdio, 
                              cpmac_work_ident_t ident, 
                              unsigned long timeout);
void cpphy_mgmt_work_stop(cpphy_mdio_t *mdio);
cpmac_err_t cpphy_mgmt_init(cpphy_mdio_t *mdio);
void cpphy_mgmt_deinit(cpphy_mdio_t *mdio);
void cpphy_mgmt_support_data(cpphy_mdio_t *mdio);

#endif /*--- #if !defined(_CPPHY_MGMT_H_) ---*/

