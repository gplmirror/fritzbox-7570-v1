#
# ISDN device configuration
#

menu "ISDN subsystem"

config ISDN
	tristate "ISDN support"
	depends on NET
	---help---
	  ISDN ("Integrated Services Digital Networks", called RNIS in France)
	  is a special type of fully digital telephone service; it's mostly
	  used to connect to your Internet service provider (with SLIP or
	  PPP).  The main advantage is that the speed is higher than ordinary
	  modem/telephone connections, and that you can have voice
	  conversations while downloading stuff.  It only works if your
	  computer is equipped with an ISDN card and both you and your service
	  provider purchased an ISDN line from the phone company.  For
	  details, read <http://www.alumni.caltech.edu/~dank/isdn/> on the WWW.

	  Select this option if you want your kernel to support ISDN.


menu "Old ISDN4Linux"
	depends on NET && ISDN

config ISDN_I4L
	tristate "Old ISDN4Linux (obsolete)"
	---help---
	  This driver allows you to use an ISDN-card for networking
	  connections and as dialin/out device.  The isdn-tty's have a built
	  in AT-compatible modem emulator.  Network devices support autodial,
	  channel-bundling, callback and caller-authentication without having
	  a daemon running.  A reduced T.70 protocol is supported with tty's
	  suitable for German BTX.  On D-Channel, the protocols EDSS1
	  (Euro-ISDN) and 1TR6 (German style) are supported.  See
	  <file:Documentation/isdn/README> for more information.

	  ISDN support in the linux kernel is moving towards a new API,
	  called CAPI (Common ISDN Application Programming Interface).
	  Therefore the old ISDN4Linux layer is becoming obsolete. It is 
	  still usable, though, if you select this option.

if ISDN_I4L
source "drivers/isdn/i4l/Kconfig"
endif

endmenu


comment "CAPI subsystem"
	depends on NET && ISDN

config ISDN_CAPI
	tristate "CAPI2.0 support"
	depends on ISDN
	help
	  This provides the CAPI (Common ISDN Application Programming
	  Interface, a standard making it easy for programs to access ISDN
	  hardware, see <http://www.capi.org/>.  This is needed for AVM's set
	  of active ISDN controllers like B1, T1, M1.

config CAPI_OSLIB
	tristate "split CAPI2.0 oslib"
	depends on ISDN
	help
        split oslib for fbox isdn drivers

config CAPI_OSLIB_DEBUG
    bool "capi oslib debug version"
    depends on CAPI_OSLIB
	help
        debug version of oslib 

config CAPI_CODEC
    tristate "capi driver codec support"
    depends on ISDN

config CAPI_CODEC_SPEEX
    bool "capi driver codec SPEEX"
    depends on CAPI_CODEC

config CAPI_CODEC_ILBC
    bool "capi driver codec ILBC"
    depends on CAPI_CODEC

config CAPI_CODEC_G729
    bool "capi driver codec G729"
    depends on CAPI_CODEC

config CAPI_CODEC_G711
    bool "capi driver codec G711"
    depends on CAPI_CODEC

config CAPI_CODEC_G726
    bool "capi driver codec G726"
    depends on CAPI_CODEC

config CAPI_CODEC_VAD
    bool "capi driver codec VAD"
    depends on CAPI_CODEC

config CAPI_CODEC_CNG
    bool "capi driver codec CNG (comfort noise generator)"
    depends on CAPI_CODEC

config CAPI_CODEC_FAX
    bool "capi driver codec FAX (T30/T38)"
    depends on CAPI_CODEC

config AVM_DECT
	tristate "AVM DECT Stack"

config AVM_DECT_DEBUG
    bool "AVM DECT debug version"
    depends on AVM_DECT

source "drivers/isdn/capi/Kconfig"

source "drivers/isdn/hardware/Kconfig"

endmenu

