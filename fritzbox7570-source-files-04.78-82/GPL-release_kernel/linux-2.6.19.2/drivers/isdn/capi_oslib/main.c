#include <linux/autoconf.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/string.h>
#include <asm/semaphore.h>
#include <linux/new_capi.h>
#include <linux/capi_oslib.h>
#include <linux/interrupt.h>
#include <linux/env.h>
#include "debug.h"
#include "host.h"
#include "file.h"
#include "capi_oslib.h"
#include "ca.h"
#include "capi_pipe.h"
#include "socket_if.h"

/*---------------------------------------------------------------------------*\
\*---------------------------------------------------------------------------*/
int		io_addr		= 0;
int		irq_num		= 0;
static int		dect_hw		= 0;
static int		dect_on		= 1;
static int		local_ec	= 0;
static int		debug_mode	= 0;
static int		trace_mode	= 0;
int		capi_oslib_dect_hw		= 0;
int		capi_oslib_dect_on		= 1;
int		capi_oslib_local_ec    = 0;
int		capi_oslib_debug_mode	= 0;
int		capi_oslib_trace_mode	= 0;

EXPORT_SYMBOL(capi_oslib_dect_hw);
EXPORT_SYMBOL(capi_oslib_dect_on);
EXPORT_SYMBOL(capi_oslib_debug_mode);
EXPORT_SYMBOL(capi_oslib_trace_mode);
EXPORT_SYMBOL(capi_oslib_local_ec);

module_param (io_addr, int, 0 );
module_param (irq_num, int, 0 );
module_param (dect_hw, int, 0 );
module_param (dect_on, int, 1 );
module_param (local_ec, int, 0 );
module_param (debug_mode, int, 0 );
module_param (trace_mode, int, 0 );

MODULE_PARM_DESC (io_addr, "io_addr: I/O address");
MODULE_PARM_DESC (irq_num, "irq_num: IRQ number");
MODULE_PARM_DESC (dect_hw, "dect_hw: DECT_HW_Version");
MODULE_PARM_DESC (dect_on, "dect_on: DECT activation");
MODULE_PARM_DESC (local_ec, "local_ec: 1: echo-canceler 2: dtmf-scanner 3: all");
MODULE_PARM_DESC (debug_mode, "debug mode");
MODULE_PARM_DESC (trace_mode, "trace mode");

struct _stack_init_params  *capi_oslib_init_params;
struct _stack_interrupt_library *capi_oslib_interrupt_library;
lib_callback_t *capi_oslib_stack;

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int __init capi_oslib_init(void) {
    return capi_oslib_file_init();
}
module_init(capi_oslib_init)

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_CAPI_OSLIB_MODULE)
int __exit capi_oslib_exit(void) {
    capi_oslib_file_cleanup();
    return 0;
}
module_exit(capi_oslib_exit);
#endif /*--- #if defined(CONFIG_CAPI_OSLIB_MODULE) ---*/


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int avm_stack_detach(lib_callback_t *stack_library) {

    DEB_INFO("[avm_stack_detach] stack_library->cm_exit\n");
    if(stack_library && stack_library->cm_exit)
        stack_library->cm_exit();
    os_disable_scheduler();
    capi_oslib_remove_card(capi_oslib_init_params);
    capi_oslib_socket_release();
    CA_MEM_EXIT( );
    return 0;
}
EXPORT_SYMBOL(avm_stack_detach);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int avm_stack_attach(lib_callback_t *stack_library, struct _stack_interrupt_library *irq_library, struct _stack_init_params *p_params) {
    char *hwrev;
    char *Version;

    DEB_INFO("avm_stack_attach\n");

    capi_oslib_init_params        = p_params;
    capi_oslib_interrupt_library  = irq_library;
    capi_oslib_stack              = stack_library;

    if(p_params) {
        printk(KERN_INFO "driver params overwritten io_addr=0x%x irq_num=%u\n", p_params->io_addr, p_params->irq_num);
        io_addr = p_params->io_addr;
        irq_num = p_params->irq_num;
        dect_hw = p_params->dect_hw;
        dect_on = p_params->dect_on;
        local_ec = p_params->local_ec;
        debug_mode = p_params->debug_mode;
        trace_mode = p_params->trace_mode;

        capi_oslib_dect_hw	  = dect_hw;
        capi_oslib_dect_on	  = dect_on;
        capi_oslib_local_ec   = local_ec;
        capi_oslib_debug_mode = debug_mode;
        capi_oslib_trace_mode = trace_mode;
    }

    Capi_Pipe_Init();

    HOST_INIT(SOURCE_UNKNOWN, 25 /* max APPLs */, 100 /* max NCCIs */, 0 /* CAPI_INDEX */);

    DEB_INFO("[avm_stack_attach] capi_oslib_install_card \n");
    capi_oslib_install_card (capi_oslib_init_params);

    DEB_INFO("[avm_stack_attach] capi_oslib_file_activate\n");
    capi_oslib_file_activate();

    hwrev = prom_getenv("HWRevision");
    if(hwrev) {
        if ((strcmp(hwrev, "103") == 0) || (strcmp(hwrev, "104") == 0)) {
            /* 5144 || 5188 */
            DEB_INFO("[avm_stack_attach] capi_oslib_socket_init\n");
            capi_oslib_socket_init();
        }
    }

    DEB_INFO("[avm_stack_attach] CAPI_INIT\n");
    CAPI_INIT(); /*--- kernel capi init ---*/

    DEB_INFO("[avm_stack_attach] stack_library->cm_start\n");
    stack_library->cm_start();

    DEB_INFO("[avm_stack_attach] CA_MALLOC(256 * %u)\n", stack_library->controllers);
    CAPI_Version = (char *)CA_MALLOC(256 * stack_library->controllers);

    DEB_INFO("[avm_stack_attach] stack_library->cm_init(0x%08X, %u)\n", io_addr, irq_num);
    Version = stack_library->cm_init(io_addr, irq_num);

    if(Version) {
        DEB_INFO("[avm_stack_attach] memcpy(Version ...)\n");
        memcpy(CAPI_Version, Version, 256 * stack_library->controllers);
    } else {
        DEB_ERR("ERROR no version information\n");
        avm_stack_detach(stack_library);
        return 1;
    }

    if(capi_oslib_init_params && capi_oslib_init_params->irq_num)
	    disable_irq (capi_oslib_init_params->irq_num);
    DEB_INFO("[avm_stack_attach] stack_library->cm_activate\n");
    stack_library->cm_activate();
	stack_library->cm_handle_events();
    if(capi_oslib_init_params && capi_oslib_init_params->irq_num)
	    enable_irq (capi_oslib_init_params->irq_num);

    os_enable_scheduler();
    os_trigger_scheduler();

    DEB_INFO("[avm_stack_attach] done\n");
    return 0;
} /* fritz_init */
EXPORT_SYMBOL(avm_stack_attach);

