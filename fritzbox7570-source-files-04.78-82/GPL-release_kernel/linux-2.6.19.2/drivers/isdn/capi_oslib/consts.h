#ifndef _consts_h_
#define _consts_h_


/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
#define MAX_CAPI_MESSAGE_SIZE               2048U
#define MAX_CONTROLLERS                     10
/*--- #define NEED_PAGE_LOCK ---*/

/*--- #define CAPI_OSLIB_USE_LOCAL_BUFFERS ---*/        /* copy_from_user und copy_to_user fuer messages verwenden */

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define USE_WORKQUEUES
/*--- #define USE_TASKLETS ---*/


#endif /*--- #ifndef _consts_h_ ---*/
