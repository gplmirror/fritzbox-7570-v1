/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_AVM_WATCHDOG)
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/fcntl.h>
#include <asm/ioctl.h>
#include <asm/errno.h>
#include <asm/uaccess.h>
#include <linux/ar7wdt.h>
#include "avm_sammel.h"
#include "ikan_wdt.h"

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
/*--- #define AVM_WATCHDOG_DEBUG ---*/

#if defined(AVM_WATCHDOG_DEBUG)
#define DBG_ERR(...)   printk(KERN_ERR __VA_ARGS__)
#define DBG_INFO(...)  printk(KERN_INFO __VA_ARGS__)
#else /*--- #if defined(AVM_WATCHDOG_DEBUG) ---*/
#define DBG_ERR(...)  
#define DBG_INFO(...)  
#endif /*--- #else ---*/ /*--- #if defined(AVM_WATCHDOG_DEBUG) ---*/

/*-----------------------------------------------------------------------------------------------*\
 * System Clock: ca. 166 MHZ 
\*-----------------------------------------------------------------------------------------------*/
void ar7wdt_hw_init(void) {
    DBG_ERR( "[vx180:watchdog] start ...\n");
    *FUSIV_WDT_CTRL  = FUSIV_WDT_CTRL_DISABLE | FUSIV_WDT_CTRL_RELOAD(0xFFFC); 
    *FUSIV_WDT_CTRL  = FUSIV_WDT_CTRL_SERVICE | FUSIV_WDT_CTRL_RELOAD(0xFFFC); 
    DBG_ERR( "[vx180:watchdog] status: 0x%x ...\n", *FUSIV_WDT_STAT);
    return;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
void ar7wdt_hw_deinit(void) {
    DBG_ERR( "[vx180:watchdog] stop ...\n");
    *FUSIV_WDT_CTRL = FUSIV_WDT_CTRL_DISABLE | FUSIV_WDT_CTRL_SERVICE;
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_reboot(void) {
    DBG_ERR("ar7wdt_hw_reboot!!\n");
    panic("ar7wdt_hw_reboot: watchdog expired\n");
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_trigger(void) {
    unsigned int status;
    status = *(volatile unsigned int *)FUSIV_WDT_STAT;
    DBG_ERR( "[vx180:watchdog] before trigger (status=0x%x) timer 0x%x %s%s%s (errors %d)\n", 
            status,
            status >> 16,
            status & (1 << 0) ? "warned " : "",
            status & (1 << 1) ? "disabled " : "",
            status & (1 << 2) ? "overflow" : "",
            (status >> 3) & 0x3);

    *FUSIV_WDT_CTRL  = FUSIV_WDT_CTRL_SERVICE; 
    /*--- *FUSIV_WDT_CTRL  = FUSIV_WDT_CTRL_SERVICE | FUSIV_WDT_CTRL_RELOAD(0xFFFC); ---*/ 
    status = *(volatile unsigned int *)FUSIV_WDT_STAT;
    DBG_ERR( "[vx180:watchdog] after trigger (status=0x%x) timer 0x%x %s%s%s (errors %d)\n", 
            status,
            status >> 16,
            status & (1 << 0) ? "warned " : "",
            status & (1 << 1) ? "disabled " : "",
            status & (1 << 2) ? "overflow" : "",
            (status >> 3) & 0x3);

}

#endif /*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/


