/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#ifndef _avm_profile_h_
#define _avm_profile_h_

#define AVM_PROFILING_VERSION   2

#if defined(CONFIG_AVM_SIMPLE_PROFILING)
#if defined(CONFIG_ARM)
#define avm_profile_counter()   davinci_get_check_idle_timer();
#define avm_profile_sdramacess()      0
#define avm_profile_sdramactivate()   0
#endif
#if defined(CONFIG_MIPS)
#define avm_profile_counter()   read_c0_count()
#include <linux/skbuff.h>
#include <asm/mips-boards/ur8.h>
#include <asm/mach-ur8/hw_emif.h>
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int avm_profile_sdramacess(void) {
    struct EMIF_register_memory_map *UR8_EMIF_register_memory_map = (struct EMIF_register_memory_map *)UR8_EMIF_BASE;
    return UR8_EMIF_register_memory_map->TotalAccesses; 
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int avm_profile_sdramactivate(void) {
    struct EMIF_register_memory_map *UR8_EMIF_register_memory_map = (struct EMIF_register_memory_map *)UR8_EMIF_BASE;
    return UR8_EMIF_register_memory_map->TotalActivate; 
}
#endif

enum _avm_profile_data_type {
    avm_profile_data_type_free,
    avm_profile_data_type_text,
    avm_profile_data_type_code_address_info,
    avm_profile_data_type_data_address_info,
    avm_profile_data_type_trace_skb,
    avm_profile_data_type_hw_irq_begin,
    avm_profile_data_type_hw_irq_end,
    avm_profile_data_type_sw_irq_begin,
    avm_profile_data_type_sw_irq_end,
    avm_profile_data_type_timer_begin,
    avm_profile_data_type_timer_end,
    avm_profile_data_type_tasklet_begin,
    avm_profile_data_type_tasklet_end,
    avm_profile_data_type_hi_tasklet_begin,
    avm_profile_data_type_hi_tasklet_end,
    avm_profile_data_type_workitem_begin,
    avm_profile_data_type_workitem_end,
    avm_profile_data_type_cpphytx_begin,
    avm_profile_data_type_cpphytx_end,
    avm_profile_data_type_cpphyrx_begin,
    avm_profile_data_type_cpphyrx_end,
    avm_profile_data_type_func_begin,
    avm_profile_data_type_func_end,
    avm_profile_data_type_unknown
};

struct _avm_profile_data {
    /* offset 0x00 */ struct task_struct *curr;
    /* offset 0x04 */ enum _avm_profile_data_type type : 16;
    /* offset 0x06 */ unsigned int id : 16;
    /* offset 0x08 */ unsigned int addr;  /* obersten 3 bit geben die quelle an */
    /* offset 0x0C */ unsigned int time;
    /* offset 0x10 */ unsigned int total_access;
    /* offset 0x14 */ unsigned int total_activate;
    /* length 0x18 */
};

#define profile_DataSetsPerBlock        ((1 << PAGE_SHIFT) / sizeof(struct _avm_profile_data))
/*--- #define profile_DataSetsPerBlock        170 ---*/  /*--- 4096 / 0x18 ==> 170... */
#define profile_BlockNeeded             (CONFIG_AVM_PROFILING_TRACE_MODE * 10)
/*--- #define PROFILE_BUFFER_LEN              (profile_BlockNeeded * (1 << PAGE_SHIFT)) ---*/

#if defined(AVM_PROFILING_VERSION)
struct _simple_profiling {
    void *data[profile_BlockNeeded];
    atomic_t pos;
    unsigned int len;
    unsigned int enabled;
    unsigned int mode;
    unsigned long start_time;
    unsigned long end_time;
    unsigned int wraparround;
    spinlock_t lock;
};
#endif/*--- #if defined(AVM_PROFILING_VERSION) ---*/

extern struct _simple_profiling simple_profiling;


void avm_simple_profiling_text(const  char *text);
void avm_simple_profiling_skb(unsigned int addr, unsigned int where, struct sk_buff *skb);
void avm_simple_profiling_log(enum _avm_profile_data_type type, unsigned int addr, unsigned int id);
unsigned int avm_simple_profiling(struct pt_regs *regs, unsigned int irq_num);
void avm_simple_profiling_enable(unsigned int on, unsigned int *count, unsigned long *timediff);
struct _avm_profile_data *avm_simple_profiling_by_idx(unsigned int idx);

#else /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
#define avm_simple_profiling_text(text)
#define avm_simple_profiling_log(addr, id)
#define avm_simple_profiling(regs)
#define avm_simple_profiling_enable(on, count, timediff)
#endif /*--- #else ---*/ /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/

#endif /*--- #ifndef _avm_profile_h_ ---*/
