/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------*\
 * In dieser Datei werden die moeglichen Blinkmodi definiert bzw. erzeugt.                  *
\*------------------------------------------------------------------------------------------*/

#include <linux/version.h>
#include <linux/timer.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/avm_led.h>
#include <linux/avm_profile.h>
#include <linux/avm_debug.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 0)
#include <linux/kallsyms.h>
#endif
#include <asm/uaccess.h>
#include "avm_sammel.h"
#include "avm_led.h"

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 0)
#include <asm/mach_avm.h>
#else
#endif

/*------------------------------------------------------------------------------------------*\
 * avm_led_profile.c
\*------------------------------------------------------------------------------------------*/
int avm_led_profile_init(unsigned int table_index, struct _avm_virt_led *V) {
    switch(table_index) {
        case 0:  /* off */
            printk("[avm_led] profile_init: disable\n");
            return 1;
        case 1: /* on */
            printk("[avm_led] profile_init: enable\n");
            return 2;
        case 2: /* write CSV File */
            printk("[avm_led] profile_init: write CSV file\n");
            return 3;
        case 3: /* write ASCII File */
            printk("[avm_led] profile_init: write ASCII file\n");
            return 4;
        case 4: /* display ASCII*/
            printk("[avm_led] profile_init: display ASCII\n");
            return 5;
        case 5: /* on (wraparround-mode) */
            printk("[avm_led] profile_init: enable (wraparround-mode)\n");
            return 6;
        case 6: /* on skb trace */
            printk("[avm_led] profile_init: enable skb trace\n");
            return 7;
        case 7: /* on skb trace (wraparround-mode) */
            printk("[avm_led] profile_init: enable skb trace (wraparround-mode)\n");
            return 8;
        default:
            printk("[avm_led] profile_init: other %u\n", table_index);
            return 33;
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void avm_led_profile_exit(int handle) {
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void avm_led_profile_stop(int handle) {
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void avm_led_profile_timer(unsigned long handle) {
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
char *avm_profile_data_short_names[] = {
    "FREE", /*--- avm_profile_data_type_free, ---*/
    "TEXT", /*--- avm_profile_data_type_text, ---*/
    "CODE", /*--- avm_profile_data_type_code_address_info, ---*/
    "DATA", /*--- avm_profile_data_type_data_address_info, ---*/
    "SKB",  /*--- avm_profile_data_type_trace_skb, ---*/
    "BIRQ", /*--- avm_profile_data_type_hw_irq_begin, ---*/
    "EIRQ", /*--- avm_profile_data_type_hw_irq_end, ---*/
    "BSWI", /*--- avm_profile_data_type_sw_irq_begin, ---*/
    "ESWI", /*--- avm_profile_data_type_sw_irq_end, ---*/
    "BTIM", /*--- avm_profile_data_type_timer_begin, ---*/
    "ETIM", /*--- avm_profile_data_type_timer_end, ---*/
    "BLET", /*--- avm_profile_data_type_tasklet_begin, ---*/
    "ELET", /*--- avm_profile_data_type_tasklet_end, ---*/
    "BLHT", /*--- avm_profile_data_type_hi_tasklet_begin, ---*/
    "ELHT", /*--- avm_profile_data_type_hi_tasklet_end, ---*/
    "BWRK", /*--- avm_profile_data_type_workitem_begin, ---*/
    "EWRK", /*--- avm_profile_data_type_workitem_end, ---*/
    "BCPT", /*--- avm_profile_data_type_cpphytx_begin, ---*/
    "ECPT", /*--- avm_profile_data_type_cpphytx_end, ---*/
    "BCPR", /*--- avm_profile_data_type_cpphyrx_begin, ---*/
    "ECPR", /*--- avm_profile_data_type_cpphyrx_end, ---*/
    "BFUN", /*--- avm_profile_data_type_func_begin, ---*/
    "EFUN", /*--- avm_profile_data_type_func_end, ---*/
    "ERROR", /*--- avm_profile_data_type_unknown ---*/
    NULL
};

char *avm_profile_data_long_names[] = {
    "free", /*--- avm_profile_data_type_free, ---*/
    "text", /*--- avm_profile_data_type_text, ---*/
    "code", /*--- avm_profile_data_type_code_address_info, ---*/
    "data", /*--- avm_profile_data_type_data_address_info, ---*/
    "skb",  /*--- avm_profile_data_type_trace_skb, ---*/
    "begin hw irq", /*--- avm_profile_data_type_hw_irq_begin, ---*/
    "end hw irq", /*--- avm_profile_data_type_hw_irq_end, ---*/
    "begin sw irq", /*--- avm_profile_data_type_sw_irq_begin, ---*/
    "end sw irq", /*--- avm_profile_data_type_sw_irq_end, ---*/
    "begin timer", /*--- avm_profile_data_type_timer_begin, ---*/
    "end timer", /*--- avm_profile_data_type_timer_end, ---*/
    "begin tasklet", /*--- avm_profile_data_type_tasklet_begin, ---*/
    "end tasklet", /*--- avm_profile_data_type_tasklet_end, ---*/
    "begin hitasklet", /*--- avm_profile_data_type_hi_tasklet_begin, ---*/
    "end hitasklet", /*--- avm_profile_data_type_hi_tasklet_end, ---*/
    "begin workitem", /*--- avm_profile_data_type_workitem_begin, ---*/
    "end workitem", /*--- avm_profile_data_type_workitem_end, ---*/
    "begin cpphy_tx", /*--- avm_profile_data_type_cpphytx_begin, ---*/
    "end cpphy_tx", /*--- avm_profile_data_type_cpphytx_end, ---*/
    "begin cpphy_rx", /*--- avm_profile_data_type_cpphyrx_begin, ---*/
    "end cpphy_rx", /*--- avm_profile_data_type_cpphyrx_end, ---*/
    "begin func", /*--- avm_profile_data_type_func_begin, ---*/
    "end func", /*--- avm_profile_data_type_func_end, ---*/
    "unknown", /*--- avm_profile_data_type_unknown ---*/
    NULL
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void avm_led_profile_action(int handle, unsigned int param1, unsigned int param2) {
    /*--- extern struct _avm_profile_data **avm_simple_profiling_enable(unsigned int on, unsigned int *count, unsigned long *timediff); ---*/
    unsigned int len, i, ii = 0;
    unsigned long timediff;
    static unsigned char Buffer[512], Symbols[256];
    static struct file *fp;
    mm_segment_t  oldfs;  
    size_t bytesWritten;
    char *p = Buffer;

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    void open_output_file(char *name) {
        fp = filp_open(name, O_WRONLY | O_CREAT | O_TRUNC, 0666);
        if(IS_ERR(fp)) {
            printk(KERN_ERR "[avm_led] Failed: Could not open \"/var/profile.log\"\n");
            return;
        }
        /* Lesezugriff auf File(system) erlaubt? */
        if (fp->f_op->write == NULL) {
            printk(KERN_ERR "[avm_led] Failed: Could not write \"/var/profile.log\"\n");
            return;
        }

        /* Von Anfang an lesen */
        fp->f_pos = 0;
    }

    void close_output_file(void) {
        filp_close(fp, NULL);
    }

    printk("[avm_led_profile_action] handle %u\n", handle);

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    switch(handle) {
        default:
            break;
        case 1: /* disable */
            avm_simple_profiling_enable(0, &len, &timediff);
            printk("[avm_led] profile_action: disable, %u entries recorded (output: 2: CSV, 3: ASCII)\n", len);
            return;
        case 2: /* enable */
        case 6: /* enable wraparound */
        case 7: /* enable skb trace */
        case 8: /* enable skb trace wraparound */
            avm_simple_profiling_enable(handle, &len, NULL);
            printk("[avm_led] profile_action: enable\n");
            return;
        case 3: /* write CSV file */
            avm_simple_profiling_enable(0, &len, &timediff);
            printk("[avm_led] profile_action: %u entries recorded, write CSV file\n", len);
            open_output_file("/var/profile.csv");
            sprintf(Buffer, "# measure time %lu msec\n", (timediff * 1000) / HZ);
            p = &Buffer[strlen(Buffer)];
            goto write_file;
            return;
        case 4: /* write ASCII file */
            avm_simple_profiling_enable(0, &len, &timediff);
            printk("[avm_led] profile_action: %u entries recorded, write ASCII file\n", len);
            open_output_file("/var/profile.txt");
            sprintf(Buffer, "# measure time %lu msec\n", (timediff * 1000) / HZ);
            p = &Buffer[strlen(Buffer)];
write_file:

            /*------------------------------------------------------------------------------*\
            \*------------------------------------------------------------------------------*/
            for(i = 0 ; i < len ; i++) {
                unsigned int _addr;
                unsigned int len;
                char *_text;
                char *description = "";
                struct _avm_profile_data *data;

                data = avm_simple_profiling_by_idx(i);
                if(data == NULL) {
                    break;
                }
                /*--------------------------------------------------------------------------*\
                \*------------------------------------------------------------------------------*/
                if((data->type < avm_profile_data_type_free) || (data->type > avm_profile_data_type_func_end)) {
                    printk("[profiling:%d] internal error data type %d unknown\n", i, data->type);
                    continue;
                }

                /*--------------------------------------------------------------------------*\
                \*------------------------------------------------------------------------------*/
                /*--- if((data->curr < (struct task_struct *)0x94000000) || (data->curr > (struct task_struct *)0x96000000)) { ---*/
                    /*--- if((data->curr < (struct task_struct *)0xC0000000) || (data->curr > (struct task_struct *)0xE0000000)) { ---*/
                        /*--- printk("[profiling:%d %s] invallid current pointer 0x%x\n", i, ---*/ 
                                /*--- avm_profile_data_short_names[data->type], ---*/ 
                                /*--- (void *)data->curr); ---*/
                        /*--- continue; ---*/
                    /*--- } ---*/
                /*--- } ---*/

                /*--------------------------------------------------------------------------*\
                \*------------------------------------------------------------------------------*/
                /*--- if((data->addr < 0x94000000) || (data->addr > 0x96000000)) { ---*/
                    /*--- printk("[profiling:%d %s] invallid addr pointer 0x%x\n", i, ---*/ 
                            /*--- avm_profile_data_short_names[data->type], ---*/ 
                            /*--- data->addr); ---*/
                    /*--- continue; ---*/
                /*--- } ---*/

                /*--------------------------------------------------------------------------*\
                \*------------------------------------------------------------------------------*/
                switch(data->type) {
                    case avm_profile_data_type_free:
                    case avm_profile_data_type_unknown:
                    default:
                        /*--- printk("x"); ---*/
                        break;
                    case avm_profile_data_type_code_address_info:
                        __sprint_symbol(Symbols, data->addr);
                        switch(handle) {
                            case 3:
                                sprintf(p, "0x%08X;0x%08X;0x%08X;CODE;0x%08x;%s;%.*s;%u\n", 
                                        data->time, 
                                        data->total_access, 
                                        data->total_activate,
                                        data->addr, Symbols, TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                printk("c");
                                break;
                            case 4:
                                sprintf(p, "0x%08X: CODE 0x%08x %s (%.*s interrupted by irq %u)\n", 
                                        data->time, 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                printk("c");
                                break;
                            case 5:
                                printk("0x%08X: CODE 0x%08x %s (%.*s interrupted by irq %u)\n", 
                                        data->time, 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                break;
                        }
                        break;
                    case avm_profile_data_type_data_address_info:
                        __sprint_symbol(Symbols, data->addr);
                        switch(handle) {
                            case 3:
                                sprintf(p, "0x%08X;0x%08X;0x%08X;DATA;0x%08x;%s;%.*s;%u\n", 
                                        data->total_access, 
                                        data->total_activate, 
                                        data->time, 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                printk("d");
                                break;
                            case 4:
                                sprintf(p, "0x%08X: DATA 0x%08x %s (%.*s interrupted by irq %u)\n", 
                                        data->time, 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                printk("d");
                                break;
                            case 5:
                                printk("0x%08X: DATA 0x%08x %s (%.*s interrupted by irq %u)\n", 
                                        data->time, 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                break;
                        }
                        break;
                    case avm_profile_data_type_trace_skb:
                        printk("s");
                        goto print_work_trace;
                    case avm_profile_data_type_text:
                        /*--- printk("t"); ---*/
                        break;
                    case avm_profile_data_type_hw_irq_begin:
                    case avm_profile_data_type_hw_irq_end:
                        description = "interrupted by irq";
                        goto print_work_trace;
                    case avm_profile_data_type_sw_irq_begin:
                    case avm_profile_data_type_sw_irq_end:
                        description = "id:";
                        goto print_work_trace;
                    case avm_profile_data_type_timer_begin:
                    case avm_profile_data_type_timer_end:
                        description = "id:";
                        goto print_work_trace;
                    case avm_profile_data_type_tasklet_begin:
                    case avm_profile_data_type_tasklet_end:
                        description = "id:";
                        goto print_work_trace;
                    case avm_profile_data_type_hi_tasklet_begin:
                    case avm_profile_data_type_hi_tasklet_end:
                        description = "id:";
                        goto print_work_trace;
                    case avm_profile_data_type_workitem_begin:
                    case avm_profile_data_type_workitem_end:
                        description = "id:";
                        goto print_work_trace;
                    case avm_profile_data_type_cpphyrx_begin:
                    case avm_profile_data_type_cpphyrx_end:
                    case avm_profile_data_type_cpphytx_begin:
                    case avm_profile_data_type_cpphytx_end:
                    case avm_profile_data_type_func_begin:
                    case avm_profile_data_type_func_end:
                        description = "id:";
                        goto print_work_trace;

print_work_trace:
                        __sprint_symbol(Symbols, data->addr);
                        switch(handle) {
                            case 3:
                                sprintf(p, "0x%08X;0x%08X;0x%08X;%s;0x%08x;%s;%.*s;%u;\n", 
                                        data->time, 
                                        data->total_access, 
                                        data->total_activate, 
                                        avm_profile_data_short_names[data->type], 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, 
                                        data->id);
                                printk("d");
                                break;
                            case 4:
                                sprintf(p, "0x%08X:%s 0x%08x %s (%.*s %s %u);\n", 
                                        data->time, 
                                        avm_profile_data_short_names[data->type], 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, description, 
                                        data->id);
                                printk("d");
                                break;
                            case 5:
                                printk("0x%08X: %s 0x%08x %s (%.*s %s %u)\n", 
                                        data->time, 
                                        avm_profile_data_long_names[data->type], 
                                        data->addr, 
                                        Symbols, 
                                        TASK_COMM_LEN, 
                                        data->curr->comm, description, 
                                        data->id);
                                break;
                        }
                }
                if(handle != 5) {
                    len = strlen(Buffer);
                    oldfs = get_fs();
                    set_fs(KERNEL_DS);
                    bytesWritten = fp->f_op->write(fp, Buffer, len, &fp->f_pos);
                    if(bytesWritten != len) {
                        printk(KERN_ERR "memory full (abort writing file)\n");
                        set_fs(oldfs);
                        break;
                    }
                    p = Buffer;
                    set_fs(oldfs);
                }
            }
            if(handle != 5) {
                close_output_file();
                printk("\n[avm_led] file closed\n");
            }
            return;
    }
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void avm_led_profile_sync(int virt_led_handle) {
    return;
}

#if defined(CONFIG_AVM_DEBUG) || defined(CONFIG_AVM_DEBUG_MODULE)
static void *profiler_DbgHandle;
#define SKIP_SPACE(a)    while(*(a) && (*(a) == ' ' || *(a) == '\t')) (a)++ 
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void cmdlineparse(char *cmdline, void *dummy) {
    char *p = cmdline; 
    unsigned int  val;
    SKIP_SPACE(p);
    if(*p) {
        sscanf(p, "%x", &val);
        avm_led_profile_action(val+1, 0, 0);
    } else {
        printk(KERN_ERR"0-disable,1-enable,2 write csv,3-write ascii,4-display ascii,5-enable+wraparound,6-enable skb,7-skb+wraparound\n");
    }
}
/*--------------------------------------------------------------------------------*\
 * Das Led-Modul ist komplett ausgelagert worden, so ist es nun notwendig
 * den Profiler ebenfalls anders anzusteuern:
 * echo profiler <val> >/dev/debug
\*--------------------------------------------------------------------------------*/
void __init avm_profiler_init(void) {
    profiler_DbgHandle = avm_DebugCallRegister("profiler", cmdlineparse, NULL);
}
#if defined(CONFIG_AVM_DEBUG_MODULE) && (CONFIG_AVM_DEBUG_MODULE == 1)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_profiler_exit(void) {
    if(profiler_DbgHandle) {
        avm_DebugCallUnRegister(profiler_DbgHandle);
        profiler_DbgHandle = NULL;
    }
}
#endif/*--- #if defined(CONFIG_AVM_DEBUG_MODULE) && (CONFIG_AVM_DEBUG_MODULE == 1) ---*/
#endif/*--- #if defined(CONFIG_AVM_DEBUG) || defined(CONFIG_AVM_DEBUG_MODULE) ---*/
