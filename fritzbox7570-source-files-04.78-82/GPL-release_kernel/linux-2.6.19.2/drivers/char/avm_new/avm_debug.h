/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#ifndef _AVM_DEBUG_H_
#define _AVM_DEBUG_H_

#include "avm_sammel.h"
#include "avm_debug.h"

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int __init avm_debug_init(void);
void avm_debug_cleanup(void);

/*--------------------------------------------------------------------------------*\
 * Das Led-Modul ist komplett ausgelagert worden, so ist es nun notwendig
 * den Profiler ebenfalls anders anzusteuern:
 * echo profiler <val> >/dev/debug
\*--------------------------------------------------------------------------------*/
void __init avm_profiler_init(void);
void avm_profiler_exit(void);


/*--------------------------------------------------------------------------------*\
 * fuer Testzwecke
\*--------------------------------------------------------------------------------*/
unsigned int avm_DebugAddWatchBuffer(unsigned char *pa, unsigned long size, const char *file, unsigned int line_nmb);
int avm_DebugWatchBuffer(unsigned char *p, unsigned int idx, const char *file, unsigned int line_nmb);
void avm_DebugDelWatchBuffer(unsigned int idx);
#endif/*--- #ifndef _AVM_DEBUG_H_ ---*/
