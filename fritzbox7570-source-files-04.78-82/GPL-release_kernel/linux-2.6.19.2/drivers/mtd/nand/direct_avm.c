/*
 * drivers/mtd/nand/direct_avm.c
 *
 * Updated, and converted to generic GPIO based driver by Russell King.
 * Updated, and converted to generic adresspamming based driver by Martin Pommerenke
 *
 * Written by Martin Pommerenke <m.pommerenke@avm.de>
 *   Based on Ben Dooks <ben@simtec.co.uk>
 *   Based on 2.4 version by Mark Whittaker
 *
 * © 2004 Simtec Electronics
 * © 2009 AVM Berlin
 *
 * Device driver for NAND connected via GPIO and Addressmapping
 *
 * modified vor special AVM NAND interface
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/nand-direct-avm.h>
#include <asm/mach_avm.h>

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if !defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) && !defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
#error "CONFIG_MTD_NAND_DIRECT_AVM_GPIO or CONFIG_MTD_NAND_DIRECT_AVM_ADDR must be defined."
#endif /*--- #if !defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) && !defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- #define DEBUG_DIRECT_NAND ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifdef DEBUG_DIRECT_NAND
#define DBG_ERR(...)      printk(KERN_ERR __VA_ARGS__)
#define DBG_WARN(...)     printk(KERN_WARNING __VA_ARGS__)
/*--- #define DBG_DEBUG(...)    printk(KERN_DEBUG __VA_ARGS__) ---*/
#define DBG_DEBUG(...)    printk(KERN_WARNING __VA_ARGS__)
#define DBG_PRINTK(...)   printk(__VA_ARGS__)
#else
#define DBG_ERR(...)      printk(KERN_ERR __VA_ARGS__)
#define DBG_WARN(...)
#define DBG_DEBUG(...)
#define DBG_PRINTK(...) 
#endif


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
#define DIRECT_NAND_MAX_GPIO_RESOURCES      1
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
#define DIRECT_NAND_MAX_GPIO_RESOURCES      5
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/
static struct resource direct_nand_gpioressource[DIRECT_NAND_MAX_GPIO_RESOURCES];

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static uint8_t bbt_pattern[] = { 'A', 'V', 'M', ' ' };
static uint8_t mirror_pattern[] = { ' ', 'M', 'V', 'A' };

static struct nand_bbt_descr direct_avm_nand_bbt_main_descr = {
	.options = NAND_BBT_SEARCH | NAND_BBT_CREATE | NAND_BBT_WRITE
		| NAND_BBT_8BIT | NAND_BBT_VERSION | NAND_BBT_PERCHIP | NAND_BBT_SCANALLPAGES | NAND_BBT_SCANEMPTY,
	.offs = 8,
	.len = sizeof(bbt_pattern),
	.veroffs = 8 + sizeof(mirror_pattern),
	.maxblocks = 4,
	.pattern = bbt_pattern
};

static struct nand_bbt_descr direct_avm_nand_bbt_mirror_descr = {
	.options = NAND_BBT_SEARCH | NAND_BBT_CREATE | NAND_BBT_WRITE
		| NAND_BBT_8BIT | NAND_BBT_VERSION | NAND_BBT_PERCHIP | NAND_BBT_SCANALLPAGES | NAND_BBT_SCANEMPTY,
	.offs = 8,
	.len = sizeof(mirror_pattern),
	.veroffs = 8 + sizeof(mirror_pattern),
	.maxblocks = 4,
	.pattern = mirror_pattern
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct direct_avm_nand {
	void __iomem		*io_ctrl;
	struct mtd_info		mtd_info;
	struct nand_chip	nand_chip;
	struct direct_avm_nand_platdata plat;
    unsigned int ctrl_addr;
};

#define direct_avm_nand_getpriv(x) container_of(x, struct direct_avm_nand, mtd_info)

#define value_is_valid(x)      (((signed int)(x) == -1) ? 0 : 1)

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
static __inline void direct_avm_nand_write_protect(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s write protect ", yes ? "set" : "reset");
    DBG_PRINTK("CHANGE: addr 0x%x => ", direct_avm_nand->ctrl_addr);
    if(yes) {
        DBG_PRINTK("NWP == 0 ");
        direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_nwp;
    } else {
        DBG_PRINTK("NWP == 1 ");
        direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_nwp;
    }
    *((volatile unsigned int *)direct_avm_nand->ctrl_addr) = 0; /* dummy write auf diese Addresse */
    DBG_PRINTK(" %x\n", direct_avm_nand->ctrl_addr);
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
static __inline void direct_avm_nand_write_protect(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s write protect ", yes ? "set" : "reset");
    if(yes) {
        DBG_PRINTK("NWP == 0 ");
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_nwp, 0);
    } else {
        DBG_PRINTK("NWP == 1 ");
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_nwp, 1);
    }
    DBG_PRINTK("\n");
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
static __inline void direct_avm_nand_chip_select(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s chip select \n", yes ? "set" : "reset");
    if(yes) {
        direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_nce;
    } else {
        direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_nce;
    }
    *((volatile unsigned int *)direct_avm_nand->ctrl_addr) = 0; /* dummy write auf diese Addresse */
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
static __inline void direct_avm_nand_chip_select(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s chip select \n", yes ? "set" : "reset");
    if(yes) {
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_nce, 0);
    } else {
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_nce, 1);
    }
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
static __inline void direct_avm_nand_command_latch_enable(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s command latch enable\n", yes ? "set" : "reset");
    if(yes) {
        direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_cle;
    } else {
        direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_cle;
    }
    *((volatile unsigned int *)direct_avm_nand->ctrl_addr) = 0; /* dummy write auf diese Addresse */
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
static __inline void direct_avm_nand_command_latch_enable(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s command latch enable\n", yes ? "set" : "reset");
    if(yes) {
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_cle, 1);
    } else {
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_cle, 0);
    }
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
static __inline void direct_avm_nand_address_latch_enable(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s address latch enable\n", yes ? "set" : "reset");
    if(yes) {
        direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_ale;
    } else {
        direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_ale;
    }
    *((volatile unsigned int *)direct_avm_nand->ctrl_addr) = 0; /* dummy write auf diese Addresse */
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
static __inline void direct_avm_nand_address_latch_enable(struct direct_avm_nand *direct_avm_nand, unsigned int yes) {
    DBG_DEBUG("[DIRECT_NAND] %s address latch enable\n", yes ? "set" : "reset");
    if(yes) {
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_ale, 1);
    } else {
        avm_gpio_out_bit(direct_avm_nand->plat.gpio_ale, 0);
    }
}
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void direct_avm_nand_cmd_ctrl(struct mtd_info *mtd, int cmd, unsigned int ctrl) {
	struct direct_avm_nand *direct_avm_nand = direct_avm_nand_getpriv(mtd);

	if (ctrl & NAND_CTRL_CHANGE) {
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
        DBG_DEBUG("[DIRECT_NAND] CHANGE: 0x%x addr 0x%x => ", ctrl, direct_avm_nand->ctrl_addr);
        if(ctrl & NAND_NCE) {
            DBG_PRINTK("NCE == 1 ");
            direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_nce;
        } else {
            DBG_PRINTK("NCE == 0 ");
            direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_nce;
        }
        if(ctrl & NAND_CLE) {
            DBG_PRINTK("CLE == 1 ");
            direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_cle;
        } else {
            DBG_PRINTK("CLE == 0 ");
            direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_cle;
        }
        if(ctrl & NAND_ALE) {
            DBG_PRINTK("ALE == 1 ");
            direct_avm_nand->ctrl_addr |= direct_avm_nand->plat.addr_ale;
        } else {
            DBG_PRINTK("ALE == 0 ");
            direct_avm_nand->ctrl_addr &= ~direct_avm_nand->plat.addr_ale;
        }
        DBG_PRINTK(" %x\n", direct_avm_nand->ctrl_addr);
        *((volatile unsigned short *)direct_avm_nand->ctrl_addr) = 0; /* dummy write auf diese Addresse */
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
        DBG_DEBUG("[DIRECT_NAND] CHANGE: 0x%x ", ctrl);
        if(ctrl & NAND_NCE) {
            DBG_PRINTK("NCE == 1 ");
            avm_gpio_out_bit(direct_avm_nand->plat.gpio_nce, 0);
        } else {
            DBG_PRINTK("NCE == 0 ");
            avm_gpio_out_bit(direct_avm_nand->plat.gpio_nce, 1);
        }
        if(ctrl & NAND_CLE) {
            DBG_PRINTK("CLE == 1 ");
            avm_gpio_out_bit(direct_avm_nand->plat.gpio_cle, 1);
        } else {
            DBG_PRINTK("CLE == 0 ");
            avm_gpio_out_bit(direct_avm_nand->plat.gpio_cle, 0);
        }
        if(ctrl & NAND_ALE) {
            DBG_PRINTK("ALE == 1 ");
            avm_gpio_out_bit(direct_avm_nand->plat.gpio_ale, 1);
        } else {
            DBG_PRINTK("ALE == 0 ");
            avm_gpio_out_bit(direct_avm_nand->plat.gpio_ale, 0);
        }
        DBG_PRINTK(" \n");
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/
	}
	if (cmd == NAND_CMD_NONE) {
        DBG_DEBUG("[DIRECT_NAND] CMD NONE\n");
		return;
    }

    DBG_DEBUG("[DIRECT_NAND] write 1 byte 0x%x to 0x%x\n", cmd & 0xFF, (unsigned int)direct_avm_nand->nand_chip.IO_ADDR_W);
	/*--- writeb(cmd, direct_avm_nand->nand_chip.IO_ADDR_W); ---*/
	/*--- *((volatile unsigned short * )direct_avm_nand->nand_chip.IO_ADDR_W) = ((cmd & 0xFF) << 8) | (cmd & 0xFF); ---*/
	*((volatile unsigned short * )direct_avm_nand->nand_chip.IO_ADDR_W) = (cmd & 0xFF);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void direct_avm_nand_writebuf(struct mtd_info *mtd, const u_char *buf, int len) {
	struct nand_chip *this = mtd->priv;
    DBG_DEBUG("[DIRECT_NAND] write %d bytes to 0x%x\n", len, (unsigned int)this->IO_ADDR_W);
    /*--- DBG_ERR("[DIRECT_NAND] write %d bytes to 0x%x\n", len, (unsigned int)this->IO_ADDR_W); ---*/
    while(len--) {
	    *((volatile unsigned short * )this->IO_ADDR_W) = (*buf++ & 0xFF);
    }
	/*--- writesb(this->IO_ADDR_W, buf, len); ---*/
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static unsigned char direct_avm_nand_read(struct mtd_info *mtd) {
	struct nand_chip *this = mtd->priv;
	unsigned short ch = *((volatile unsigned short *)this->IO_ADDR_R);
    /*--- DBG_ERR("[DIRECT_NAND] read 1 byte 0x%x: 0x%x\n", (unsigned int)this->IO_ADDR_R, ch); ---*/
    DBG_DEBUG("[DIRECT_NAND] read 1 byte 0x%x: 0x%x\n", (unsigned int)this->IO_ADDR_R, ch);
    return ch;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void direct_avm_nand_readbuf(struct mtd_info *mtd, u_char *buf, int len) {
	struct nand_chip *this = mtd->priv;
    /*--- int dump = 0, all_ff = 1; ---*/
    DBG_DEBUG("[DIRECT_NAND] read %d bytes from 0x%x\n", len, (unsigned int)this->IO_ADDR_W);
    /*--- DBG_ERR("[DIRECT_NAND] read %d bytes from 0x%x\n", len, (unsigned int)this->IO_ADDR_W); ---*/
	/*--- readsb(this->IO_ADDR_R, buf, len); ---*/
    /*--- if(len == 218) ---*/ 
        /*--- dump = len; ---*/
    while(len--) {
	    *buf++ = (unsigned char)*((volatile unsigned short * )this->IO_ADDR_R);
        /*--- if(buf[-1] != 0xFF) all_ff = 0; ---*/
    }
    /*--- if(dump && !all_ff) ---*/
        /*--- printk(KERN_ERR "[NAND] 218 bytes: % *B\n", dump, buf - dump); ---*/
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int direct_avm_nand_verifybuf(struct mtd_info *mtd, const u_char *buf, int len) {
	struct nand_chip *this = mtd->priv;
	unsigned char read, *p = (unsigned char *) buf;
	int i, err = 0;

    DBG_DEBUG("[DIRECT_NAND] verify %d bytes\n", len);
    /*--- DBG_ERR("[DIRECT_NAND] verify %d bytes\n", len); ---*/

	for (i = 0; i < len; i++) {
	    read = (unsigned char)*((volatile unsigned short * )this->IO_ADDR_R);
		/*--- read = readb(this->IO_ADDR_R); ---*/
		if (read != p[i]) {
			DBG_WARN("%s: err at %d (read %04x vs %04x)\n", __func__, i, read, p[i]);
			err = -EFAULT;
		}
	}
	return err;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if 0
static void direct_avm_nand_writebuf16(struct mtd_info *mtd, const u_char *buf, int len) {
	struct nand_chip *this = mtd->priv;

	if (IS_ALIGNED((unsigned long)buf, 2)) {
		writesw(this->IO_ADDR_W, buf, len>>1);
	} else {
		int i;
		unsigned short *ptr = (unsigned short *)buf;

		for (i = 0; i < len; i += 2, ptr++) {
			writew(*ptr, this->IO_ADDR_W);
        }
	}
}
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if 0
static void direct_avm_nand_readbuf16(struct mtd_info *mtd, u_char *buf, int len) {
	struct nand_chip *this = mtd->priv;

	if (IS_ALIGNED((unsigned long)buf, 2)) {
		readsw(this->IO_ADDR_R, buf, len>>1);
	} else {
		int i;
		unsigned short *ptr = (unsigned short *)buf;

		for (i = 0; i < len; i += 2, ptr++)
			*ptr = readw(this->IO_ADDR_R);
	}
}
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if 0
static int direct_avm_nand_verifybuf16(struct mtd_info *mtd, const u_char *buf, int len) {
	struct nand_chip *this = mtd->priv;
	unsigned short read, *p = (unsigned short *) buf;
	int i, err = 0;
	len >>= 1;

	for (i = 0; i < len; i++) {
		read = readw(this->IO_ADDR_R);
		if (read != p[i]) {
			pr_debug("%s: err at %d (read %04x vs %04x)\n",
			       __func__, i, read, p[i]);
			err = -EFAULT;
		}
	}
	return err;
}
#endif


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int direct_avm_nand_devready(struct mtd_info *mtd) {
	struct direct_avm_nand *direct_avm_nand = direct_avm_nand_getpriv(mtd);
    int ret;
	ret = avm_gpio_in_bit(direct_avm_nand->plat.gpio_rdy);
    DBG_DEBUG("[DIRECT_NAND] device %sready\n", ret ? "" : "not ");
    return ret;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __exit direct_avm_nand_remove(struct platform_device *dev) {
	struct direct_avm_nand *direct_avm_nand = platform_get_drvdata(dev);
	struct resource *res;

	nand_release(&direct_avm_nand->mtd_info);
    direct_avm_nand_chip_select(direct_avm_nand, 0);
    direct_avm_nand_write_protect(direct_avm_nand, 1);

	res = platform_get_resource(dev, IORESOURCE_MEM, 1);
	/*--- iounmap(direct_avm_nand->io_ctrl); ---*/
	if (res)
		release_mem_region(res->start, res->end - res->start + 1);

	res = platform_get_resource(dev, IORESOURCE_MEM, 0);
	/*--- iounmap(direct_avm_nand->nand_chip.IO_ADDR_R); ---*/
	release_mem_region(res->start, res->end - res->start + 1);
    
	release_resource(&direct_nand_gpioressource[0]);

#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
	release_resource(&direct_nand_gpioressource[1]);
	release_resource(&direct_nand_gpioressource[2]);
	release_resource(&direct_nand_gpioressource[3]);
	release_resource(&direct_nand_gpioressource[4]);
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

	kfree(direct_avm_nand);

	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void __iomem *request_and_remap(struct resource *res, size_t size, const char *name, int *err) {
	void __iomem *ptr;

	if (!request_mem_region(res->start, res->end - res->start + 1, name)) {
		*err = -EBUSY;
		return NULL;
	}

    ptr = (void __iomem *)res->start;

#if 0
	ptr = ioremap(res->start, size);
	if (!ptr) {
		release_mem_region(res->start, res->end - res->start + 1);
		*err = -ENOMEM;
	}
#endif
	return ptr;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __devinit direct_avm_nand_probe(struct platform_device *dev)
{
	struct direct_avm_nand *direct_avm_nand;
	struct nand_chip *this;
	struct resource *res0;
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
	struct resource *res1;
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/
	int ret = 0;

    DBG_ERR("[direct_avm_nand_probe] error output\n");
    DBG_WARN("[direct_avm_nand_probe] warning output\n");
    DBG_DEBUG("[direct_avm_nand_probe] debug output\n");

	if (!dev->dev.platform_data)
		return -EINVAL;

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
	res0 = platform_get_resource(dev, IORESOURCE_MEM, 0);
	if (!res0)
		return -EINVAL;

    DBG_DEBUG("[direct_avm_nand_probe] got resource values 0 (data_addr %x)\n", res0->start);

	direct_avm_nand = kmalloc(sizeof(struct direct_avm_nand), GFP_KERNEL);
	if (direct_avm_nand == NULL) {
		DBG_ERR("[direct_avm_nand_probe] failed to create NAND MTD\n");
		return -ENOMEM;
	}
    memset(direct_avm_nand, 0, sizeof(struct direct_avm_nand));

	this = &direct_avm_nand->nand_chip;
	this->IO_ADDR_R = request_and_remap(res0, 2, "NAND Data", &ret);
	if (!this->IO_ADDR_R) {
		DBG_ERR("[direct_avm_nand_probe] unable to map NAND Data\n");
		goto err_map;
	}

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
	res1 = platform_get_resource(dev, IORESOURCE_MEM, 1);
	if (!res1) {
		DBG_ERR("[direct_avm_nand_probe] no NAND control resource found\n");
		goto err_map;
    }

    direct_avm_nand->io_ctrl = request_and_remap(res1, 4, "NAND Ctrl", &ret);
    if (!direct_avm_nand->io_ctrl) {
        DBG_ERR("[direct_avm_nand_probe] unable to map NAND Ctrl\n");
        goto err_ctrl;
    }
    direct_avm_nand->ctrl_addr = (typeof(direct_avm_nand->ctrl_addr))res1->start;

    DBG_DEBUG("[direct_avm_nand_probe] got resource values 1 (ctrl_addr %x)\n", direct_avm_nand->ctrl_addr);
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

	memcpy(&direct_avm_nand->plat, dev->dev.platform_data, sizeof(direct_avm_nand->plat));
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    direct_nand_gpioressource[0].start = direct_avm_nand->plat.gpio_rdy;
    direct_nand_gpioressource[0].end   = direct_avm_nand->plat.gpio_rdy;
    direct_nand_gpioressource[0].name  = "NAND Ready";
    direct_nand_gpioressource[0].flags = IORESOURCE_IO;
	if(request_resource(&gpio_resource, &direct_nand_gpioressource[0])) {
        DBG_ERR("[direct_avm_nand_probe] unable to get GPIO resource\n");
        goto err_gpio;
    }
    DBG_DEBUG("[direct_avm_nand_probe] use GPIO %d as Ready\n", direct_avm_nand->plat.gpio_rdy);
    ikan_gpio_ctrl(direct_avm_nand->plat.gpio_rdy, GPIO_PIN, GPIO_INPUT_PIN);

#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    direct_nand_gpioressource[1].start = direct_avm_nand->plat.gpio_nce;
    direct_nand_gpioressource[1].end   = direct_avm_nand->plat.gpio_nce;
    direct_nand_gpioressource[1].name  = "NAND /CE";
    direct_nand_gpioressource[1].flags = IORESOURCE_IO;
	if(request_resource(&gpio_resource, &direct_nand_gpioressource[1])) {
        DBG_ERR("[direct_avm_nand_probe] unable to get GPIO resource\n");
        goto err_gpio1;
    }
    ikan_gpio_ctrl(direct_avm_nand->plat.gpio_nce, GPIO_PIN, GPIO_OUTPUT_PIN);

    direct_nand_gpioressource[2].start = direct_avm_nand->plat.gpio_nwp;
    direct_nand_gpioressource[2].end   = direct_avm_nand->plat.gpio_nwp;
    direct_nand_gpioressource[2].name  = "NAND /WP";
    direct_nand_gpioressource[2].flags = IORESOURCE_IO;
	if(request_resource(&gpio_resource, &direct_nand_gpioressource[2])) {
        DBG_ERR("[direct_avm_nand_probe] unable to get GPIO resource\n");
        goto err_gpio2;
    }
    ikan_gpio_ctrl(direct_avm_nand->plat.gpio_nwp, GPIO_PIN, GPIO_OUTPUT_PIN);

    direct_nand_gpioressource[3].start = direct_avm_nand->plat.gpio_ale;
    direct_nand_gpioressource[3].end   = direct_avm_nand->plat.gpio_ale;
    direct_nand_gpioressource[3].name  = "NAND ALE";
    direct_nand_gpioressource[3].flags = IORESOURCE_IO;
	if(request_resource(&gpio_resource, &direct_nand_gpioressource[3])) {
        DBG_ERR("[direct_avm_nand_probe] unable to get GPIO resource\n");
        goto err_gpio3;
    }
    ikan_gpio_ctrl(direct_avm_nand->plat.gpio_ale, GPIO_PIN, GPIO_OUTPUT_PIN);

    direct_nand_gpioressource[4].start = direct_avm_nand->plat.gpio_cle;
    direct_nand_gpioressource[4].end   = direct_avm_nand->plat.gpio_cle;
    direct_nand_gpioressource[4].name  = "NAND CLE";
    direct_nand_gpioressource[4].flags = IORESOURCE_IO;
	if(request_resource(&gpio_resource, &direct_nand_gpioressource[4])) {
        DBG_ERR("[direct_avm_nand_probe] unable to get GPIO resource\n");
        goto err_gpio4;
    }
    ikan_gpio_ctrl(direct_avm_nand->plat.gpio_cle, GPIO_PIN, GPIO_OUTPUT_PIN);

    DBG_ERR("[NAND] direct NAND: GPIO usage: %d: /CE %d: /WP %d: CLE %d: ALE (0x%x)\n", 
        direct_avm_nand->plat.gpio_nce,
        direct_avm_nand->plat.gpio_nwp,
        direct_avm_nand->plat.gpio_cle,
        direct_avm_nand->plat.gpio_ale,
        *((volatile unsigned int *)0xB90000D4));
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

	this->IO_ADDR_W  = this->IO_ADDR_R;
	this->ecc.mode   = NAND_ECC_SOFT;
	this->options    = direct_avm_nand->plat.options;
	this->chip_delay = direct_avm_nand->plat.chip_delay;

	this->bbt_td = &direct_avm_nand_bbt_main_descr;
	this->bbt_md = &direct_avm_nand_bbt_mirror_descr;

	/* install our routines */
	this->cmd_ctrl   = direct_avm_nand_cmd_ctrl;
	this->dev_ready  = direct_avm_nand_devready;

	if (this->options & NAND_BUSWIDTH_16) {
#if 0
		this->read_buf   = direct_avm_nand_readbuf16;
		this->write_buf  = direct_avm_nand_writebuf16;
		this->verify_buf = direct_avm_nand_verifybuf16;
#endif
	} else {
		this->read_buf   = direct_avm_nand_readbuf;
		this->write_buf  = direct_avm_nand_writebuf;
		this->verify_buf = direct_avm_nand_verifybuf;
	}

    this->read_byte      = direct_avm_nand_read;

	/* set the mtd private data for the nand driver */
	direct_avm_nand->mtd_info.priv  = this;
	direct_avm_nand->mtd_info.owner = THIS_MODULE;

    DBG_DEBUG("[direct_avm_nand_probe] start nand_scan ...\n");
    direct_avm_nand_write_protect(direct_avm_nand, 0);
	if (nand_scan(&direct_avm_nand->mtd_info, 1)) {
		DBG_ERR("[direct_avm_nand_probe] no nand chips found?\n");
		ret = -ENXIO;
		goto err_wp;
	}
    DBG_DEBUG("[direct_avm_nand_probe] adjust_parts ...\n");

	if (direct_avm_nand->plat.adjust_parts)
		direct_avm_nand->plat.adjust_parts(&direct_avm_nand->plat, direct_avm_nand->mtd_info.size);

    DBG_DEBUG("[direct_avm_nand_probe] add_mtd_partitions %d partitions ...\n",  direct_avm_nand->plat.num_parts);

	add_mtd_partitions(&direct_avm_nand->mtd_info, direct_avm_nand->plat.parts, direct_avm_nand->plat.num_parts);

    DBG_DEBUG("[direct_avm_nand_probe] platform_set_drvdata ...\n");
	platform_set_drvdata(dev, direct_avm_nand);
    DBG_DEBUG("[direct_avm_nand_probe] success !\n");
	return 0;

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
err_wp:
    direct_avm_nand_write_protect(direct_avm_nand, 1);

err_gpio4:
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
	release_resource(&direct_nand_gpioressource[3]);
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

err_gpio3:
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
	release_resource(&direct_nand_gpioressource[2]);
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

err_gpio2:
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
	release_resource(&direct_nand_gpioressource[1]);
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/

err_gpio1:
	release_resource(&direct_nand_gpioressource[0]);

err_gpio:
	/*--- iounmap(direct_avm_nand->io_ctrl); ---*/
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
	if (res1)
		release_mem_region(res1->start, res1->end - res1->start + 1);
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/

err_ctrl:
	/*--- iounmap(direct_avm_nand->nand_chip.IO_ADDR_R); ---*/
	release_mem_region(res0->start, res0->end - res0->start + 1);
err_map:
	kfree(direct_avm_nand);
	return ret;
}

static struct platform_driver direct_avm_nand_driver = {
	.probe		= direct_avm_nand_probe,
	.remove		= direct_avm_nand_remove,
	.driver		= {
		.name	= "direct-avm-nand",
	},
};

static int __init direct_avm_nand_init(void) {
	printk(KERN_INFO "AVM Direct NAND driver, © 2008 AVM Berlin\n");
	return platform_driver_register(&direct_avm_nand_driver);
}

static void __exit direct_avm_nand_exit(void) {
	platform_driver_unregister(&direct_avm_nand_driver);
}

module_init(direct_avm_nand_init);
module_exit(direct_avm_nand_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("AVM Direct NAND Driver");
