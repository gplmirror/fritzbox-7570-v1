/*
 * $Id: pagedmap.c,v 1.39 2005/11/29 14:49:36 gleixner Exp $
 *
 * Mappings of paged chips in physical memory
 *
 * Copyright (C) 2003 MontaVista Software Inc.
 * Copyright (C) 2007 AVM Berlin
 * Author: Jun Sun, jsun@mvista.com or jsun@junsun.net
 *
 * 031022 - [jsun] add run-time configure and partition setup
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/pagedmap.h>
#include <asm/io.h>

struct pagedmap_flash_info {
	struct mtd_info		*mtd;
	struct map_info		map;
	struct resource		*res;
#ifdef CONFIG_MTD_PARTITIONS
	int			nr_parts;
	struct mtd_partition	*parts;
#endif
};


static int pagedmap_flash_remove(struct platform_device *dev)
{
	struct pagedmap_flash_info *info;
	struct pagedmap_flash_data *pagedmap_data;

	info = platform_get_drvdata(dev);
	if (info == NULL)
		return 0;
	platform_set_drvdata(dev, NULL);

	pagedmap_data = dev->dev.platform_data;

	if (info->mtd != NULL) {
#ifdef CONFIG_MTD_PARTITIONS
		if (info->nr_parts) {
			del_mtd_partitions(info->mtd);
			kfree(info->parts);
		} else if (pagedmap_data->nr_parts) {
			del_mtd_partitions(info->mtd);
		} else {
			del_mtd_device(info->mtd);
		}
#else
		del_mtd_device(info->mtd);
#endif
		map_destroy(info->mtd);
	}

	if (info->map.virt != NULL)
		iounmap(info->map.virt);

	if (info->res != NULL) {
		release_resource(info->res);
		kfree(info->res);
	}

	return 0;
}

static const char *rom_probe_types[] = { "cfi_probe", "jedec_probe", "map_rom", NULL };
#ifdef CONFIG_MTD_PARTITIONS
static const char *part_probe_types[] = { "cmdlinepart", "RedBoot", NULL };
#endif

static int pagedmap_flash_probe(struct platform_device *dev)
{
	struct pagedmap_flash_data *pagedmap_data;
	struct pagedmap_flash_info *info;
	const char **probe_type;
	int err;

printk("[%s, %u] \n", __FILE__, __LINE__);

	pagedmap_data = dev->dev.platform_data;
	if (pagedmap_data == NULL)
		return -ENODEV;

printk("[%s, %u] \n", __FILE__, __LINE__);
    /*--- dev->resource->start = CONFIG_MTD_PAGEDMAP_START; ---*/
    /*--- dev->resource->end   = dev->resource->start + CONFIG_MTD_PAGEDMAP_LEN; ---*/

printk("[%s, %u] \n", __FILE__, __LINE__);
    printk(/* KERN_NOTICE */ KERN_ERR "pagedmap platform flash device: %.8llx at %.8llx\n",
	    (unsigned long long)dev->resource->end - dev->resource->start + 1,
	    (unsigned long long)dev->resource->start);

printk("[%s, %u] \n", __FILE__, __LINE__);
	info = kmalloc(sizeof(struct pagedmap_flash_info), GFP_KERNEL);
	if (info == NULL) {
		err = -ENOMEM;
printk("[%s, %u] \n", __FILE__, __LINE__);
		goto err_out;
	}
printk("[%s, %u] \n", __FILE__, __LINE__);
	memset(info, 0, sizeof(*info));

printk("[%s, %u] \n", __FILE__, __LINE__);
	platform_set_drvdata(dev, info);

printk("[%s, %u] \n", __FILE__, __LINE__);
	info->res = request_mem_region(CONFIG_MTD_PAGEDMAP_START, CONFIG_MTD_PAGEDMAP_LEN, dev->dev.bus_id);
	/*--- info->res = request_mem_region(dev->resource->start, ---*/
			/*--- dev->resource->end - dev->resource->start + 1, ---*/
			/*--- dev->dev.bus_id); ---*/

	if (info->res == NULL) {
		dev_err(&dev->dev, "Could not reserve memory region\n");
		err = -ENOMEM;
		goto err_out;
	}
printk("[%s, %u] \n", __FILE__, __LINE__);

	info->map.name = dev->dev.bus_id;
    printk("info->map.name = %s\n", info->map.name);
	info->map.phys = dev->resource->start;
	info->map.size = dev->resource->end - dev->resource->start + 1;
	info->map.bankwidth = pagedmap_data->width;
	info->map.set_vpp = pagedmap_data->set_vpp;

	info->map.virt = ioremap(CONFIG_MTD_PAGEDMAP_START, CONFIG_MTD_PAGEDMAP_LEN);
	if (info->map.virt == NULL) {
		dev_err(&dev->dev, "Failed to ioremap flash region\n");
		err = EIO;
		goto err_out;
	}

printk("[%s, %u] \n", __FILE__, __LINE__);
	paged_map_init(&info->map);

printk("[%s, %u] \n", __FILE__, __LINE__);
	probe_type = rom_probe_types;
	for (; info->mtd == NULL && *probe_type != NULL; probe_type++)
		info->mtd = do_map_probe(*probe_type, &info->map);
	if (info->mtd == NULL) {
		dev_err(&dev->dev, "map_probe failed\n");
		err = -ENXIO;
		goto err_out;
	}
	info->mtd->owner = THIS_MODULE;

printk("[%s, %u] \n", __FILE__, __LINE__);
#ifdef CONFIG_MTD_PARTITIONS
	err = parse_mtd_partitions(info->mtd, part_probe_types, &info->parts, 0);
	if (err > 0) {
		add_mtd_partitions(info->mtd, info->parts, err);
		return 0;
	}

    if (pagedmap_data->probes) {
        err = parse_mtd_partitions(info->mtd, pagedmap_data->probes, &info->parts, 0);
        if (err > 0) {
            add_mtd_partitions(info->mtd, info->parts, err);
            return 0;
        }
    }

printk("[%s, %u] \n", __FILE__, __LINE__);
	if (pagedmap_data->nr_parts) {
		printk(KERN_NOTICE "Using pagedmap partition information\n");
		add_mtd_partitions(info->mtd, pagedmap_data->parts,
						pagedmap_data->nr_parts);
		return 0;
	}
#endif

printk("[%s, %u] \n", __FILE__, __LINE__);
	add_mtd_device(info->mtd);
	return 0;

err_out:
	pagedmap_flash_remove(dev);
	return err;
}

#ifdef CONFIG_PM
static int pagedmap_flash_suspend(struct platform_device *dev, pm_message_t state)
{
	struct pagedmap_flash_info *info = platform_get_drvdata(dev);
	int ret = 0;

	if (info)
		ret = info->mtd->suspend(info->mtd);

	return ret;
}

static int pagedmap_flash_resume(struct platform_device *dev)
{
	struct pagedmap_flash_info *info = platform_get_drvdata(dev);
	if (info)
		info->mtd->resume(info->mtd);
	return 0;
}

static void pagedmap_flash_shutdown(struct platform_device *dev)
{
	struct pagedmap_flash_info *info = platform_get_drvdata(dev);
	if (info && info->mtd->suspend(info->mtd) == 0)
		info->mtd->resume(info->mtd);
}
#endif

static struct platform_driver pagedmap_flash_driver = {
	.probe		= pagedmap_flash_probe,
	.remove		= pagedmap_flash_remove,
#ifdef CONFIG_PM
	.suspend	= pagedmap_flash_suspend,
	.resume		= pagedmap_flash_resume,
	.shutdown	= pagedmap_flash_shutdown,
#endif
	.driver		= {
		.name	= "pagedmap-flash",
	},
};


#ifdef CONFIG_MTD_PAGEDMAP_LEN
#if CONFIG_MTD_PAGEDMAP_LEN != 0
#ifndef CONFIG_ARCH_DAVINCI
#warnig using PAGEDMAP compat code
#error using PAGEDMAP compat code
#define PAGEDMAP_COMPAT
#endif /*--- #ifndef CONFIG_ARCH_DAVINCI ---*/
#endif
#endif

#ifdef PAGEDMAP_COMPAT
static struct pagedmap_flash_data pagedmap_flash_data = {
	.width		= CONFIG_MTD_PAGEDMAP_BANKWIDTH,
};

static struct resource pagedmap_flash_resource = {
	.start		= CONFIG_MTD_PAGEDMAP_START,
	.end		= CONFIG_MTD_PAGEDMAP_START + CONFIG_MTD_PAGEDMAP_LEN - 1,
	.flags		= IORESOURCE_MEM,
};

static struct platform_device pagedmap_flash = {
	.name		= "pagedmap-flash",
	.id		= 0,
	.dev		= {
		.platform_data	= &pagedmap_flash_data,
	},
	.num_resources	= 1,
	.resource	= &pagedmap_flash_resource,
};

void pagedmap_configure(unsigned long addr, unsigned long size,
		int bankwidth, void (*set_vpp)(struct map_info *, int))
{
	pagedmap_flash_resource.start = addr;
	pagedmap_flash_resource.end = addr + size - 1;
	pagedmap_flash_data.width = bankwidth;
	pagedmap_flash_data.set_vpp = set_vpp;
}

#ifdef CONFIG_MTD_PARTITIONS
void pagedmap_set_partitions(struct mtd_partition *parts, int num_parts)
{
	pagedmap_flash_data.nr_parts = num_parts;
	pagedmap_flash_data.parts = parts;
}
#endif
#endif

static int __init pagedmap_init(void)
{
	int err;

	err = platform_driver_register(&pagedmap_flash_driver);
#ifdef PAGEDMAP_COMPAT
	if (err == 0)
		platform_device_register(&pagedmap_flash);
#endif

	return err;
}

static void __exit pagedmap_exit(void)
{
#ifdef PAGEDMAP_COMPAT
	platform_device_unregister(&pagedmap_flash);
#endif
	platform_driver_unregister(&pagedmap_flash_driver);
}

module_init(pagedmap_init);
module_exit(pagedmap_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("David Woodhouse <dwmw2@infradead.org>, Martin Pommmerenke <m.pommerenke@avm.de>");
MODULE_DESCRIPTION("Generic configurable MTD map driver for paged flash");
