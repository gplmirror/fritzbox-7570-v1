/*
 *
 * Handle mapping of the flash on Any ADI Boards
 *
 * Author:	Leo @ Analog Devices
 * Copyright:	(C) 2005 Analog Devices
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>


extern int board_flash_start,board_flash_size;

unsigned int WINDOW_ADDR, WINDOW_SIZE;

//#define NUM_PARTITIONS 2

#define BOOT_SIZE	256
#define KERNEL_SIZE 1024


/* partition_info gives details on the logical partitions that the split the
 * single flash device into. If the size if zero we use up to the end of the
 * device. */
static struct mtd_partition partition_info_8MB16MB[]={
	{ .name = "Ikanos Flash OS Partition",
	.offset = 0,
	.size =   (BOOT_SIZE+KERNEL_SIZE)*1024 },
	{ .name = "Ikanos Flash FS Partition",
	.offset = (BOOT_SIZE+KERNEL_SIZE)*1024 }
};

static struct mtd_partition partition_info_32MB[]={
        { .name = "Ikanos Flash Custom Partition",
        .offset = 0x1e0000,
        .size =   0xe20000 },
        { .name = "Ikanos Flash OS Partition",
        .offset = 16*1024*1024,
        .size =   ((BOOT_SIZE+KERNEL_SIZE)*1024) },
        { .name = "Ikanos Flash FS Partition",
        .offset = ((BOOT_SIZE+KERNEL_SIZE)*1024) }
};
				   
static unsigned int myfs_start = 0;

static int __init setfs_start(char *str)
{
	get_option(&str, &myfs_start);
	return 1;
}

static struct mtd_info *mymtd;

struct map_info mb_map = {
	.name = "Ikanos flash",
	.phys = 0,
	.size = 0,
	.bankwidth = 2,
};

int __init init_mb(void)
{
	struct mtd_partition *partition_info = NULL;
	u8  num_partitions = 0;
	u32 tmp;
	WINDOW_ADDR = board_flash_start & 0x1FFFFFFF;
	WINDOW_SIZE = board_flash_size;
	tmp =	myfs_start & 0x1FFFFFFF;
	if(((tmp)<(WINDOW_ADDR+0x40000)) || (tmp>(WINDOW_ADDR+WINDOW_SIZE))) 
		panic("!!! File system start address(myfs_start) setting 0x%x is wrong. \n", myfs_start);

	mb_map.phys = WINDOW_ADDR;
	mb_map.size = WINDOW_SIZE;

	switch(board_flash_start)
	{
	    case 0xbf800000:
	    case 0xbf000000:
	        partition_info_8MB16MB[1].offset = (myfs_start &0x1FFFFFFF) - WINDOW_ADDR;
	        partition_info_8MB16MB[0].size = partition_info_8MB16MB[1].offset;
	        partition_info = partition_info_8MB16MB;
		num_partitions = 2;
	        break;
	    case 0xbe000000:
                partition_info_32MB[2].offset = (myfs_start &0x1FFFFFFF) - WINDOW_ADDR;
                partition_info_32MB[1].size = (myfs_start &0x1FFFFFFF)-0x1f000000;
	        partition_info = partition_info_32MB;
		num_partitions = 3;
                break;
	}

	printk(KERN_NOTICE "On Board flash device: 0x%x at 0x%x\n", WINDOW_SIZE, WINDOW_ADDR);
	mb_map.virt = ioremap(WINDOW_ADDR, WINDOW_SIZE);
	if (!mb_map.virt) {
		printk("Failed to ioremap\n");
		return -EIO;
	}
	simple_map_init(&mb_map);

	mymtd = do_map_probe("cfi_probe", &mb_map);
	if (mymtd) {
		mymtd->owner = THIS_MODULE;
		add_mtd_device(mymtd);
        add_mtd_partitions(mymtd, partition_info, num_partitions);
		return 0;
	}

	iounmap((void *)mb_map.virt);
	return -ENXIO;
}

static void __exit cleanup_mb(void)
{
	if (mymtd) {
		del_mtd_device(mymtd);
		map_destroy(mymtd);
	}
	if (mb_map.virt) {
		iounmap((void *)mb_map.virt);
		mb_map.virt = 0;
	}
}

module_init(init_mb);
module_exit(cleanup_mb);
__setup("myfs_start=", setfs_start);

MODULE_AUTHOR("Leo @ Analog Devices");
MODULE_DESCRIPTION("MTD map driver Ikanos Fusiv vx180 board");
MODULE_LICENSE("GPL");
