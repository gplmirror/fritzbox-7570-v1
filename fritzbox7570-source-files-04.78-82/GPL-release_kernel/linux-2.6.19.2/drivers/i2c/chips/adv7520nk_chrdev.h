/*
 * adv7520nk_chrdev.h
 *
 *  Created on: 22.10.2008
 *      Author: sschroeder
 */

#ifndef ADV7520NK_CHRDEV_H_
#define ADV7520NK_CHRDEV_H_

struct adv7520PrivData;

int hdmictrl_chrdrv_register( struct adv7520PrivData* pHandle );
int hdmictrl_chrdev_unregister( struct adv7520PrivData* pHandle );

#endif /* ADV7520NK_CHRDEV_H_ */
