/*
 * adv7520nk_hdmi.h
 *
 *  Created on: 21.10.2008
 *      Author: sschroeder
 */

#ifndef ADV7520NK_HDMI_H_
#define ADV7520NK_HDMI_H_

typedef struct adv7520PrivData {
    int                         has_resource;
    int                         has_irq;
    int                         hdcp_request;
    char                        hdmi_registers[256];
    char                        edid_register[256];
    struct workqueue_struct*    wq;
    dev_t                       n_cdev;
    struct cdev*                s_cdev;
} sAdv7520PrivData;

#define SET_BIT(REG, BIT)       ( REG |= (1<<BIT) )
#define CLEAR_BIT(REG, BIT)     ( REG &= (~(1<<BIT)) )
#define ISSET_BIT(REG, BIT)     ( REG & ( 1<<BIT ) )


/* IRQ BIT */
#define BIT_IRQ_HOT_PLUG_DETECT            	7
#define BIT_IRQ_MONITOR_SENSE              	6
#define BIT_IRQ_ACTIVE_VSYNC_EDGE          	5
#define BIT_IRQ_AUDIO_FIFO_FULL            	4
#define BIT_IRQ_EMBEDDED_SYNC_PARITY_ERROR 	3
#define BIT_IRQ_EDID_READY                 	2
#define BIT_IRQ_HDCP_AUTHENTICATED         	1

/* IRQ Handling Registers 0x97 */
#define BIT_IRQ_HDCP_ERROR					7
#define BIT_IRQ_BKSV_FLAG					6
#define BIT_IRQ_CEC_TX_READY				5
#define BIT_IRQ_CEC_TX_ARBITRATION_LOST		4
#define BIT_IRQ_CEC_TX_RETRY_TIEMOUT		3
#define BIT_IRQ_CEC_RX_READY				2

/* HDCP BIT DEFINITION */
#define BIT_HDCP_ERROR_FLAG					7 /* HDCP Error occured: Read HDCP ERROR Codes */
#define BIT_HDCP_DISIRED					7 /* HDCP Encryption */
#define BIT_HDCP_FRAME_ENCRYPTION			4 /* Frame Encryption */
#define BIT_HDCP_REQUEST_FLAG				3 /* Status of HDCP Desired */
#define BIT_HDCP_HDMI_DVI_SELECT			1
#define BIT_HDCP_SUPPORT					1
#define BIT_HDCP_FAST_HDCP					0


/* HDCP Register Addresses */
#define HDMI_REG_ADDRESS_RX_SENSE				0x42
#define HDMI_REG_ADDRESS_HDCP_DESIRED			0xAF
#define HDMI_REG_ADDRESS_HDCP_FRAME_ENCRYPTION	0xAF
#define HDMI_REG_ADDRESS_HDCP_HDMI_DVI_SELECT	0xAF
#define HDMI_REG_ADDRESS_HDCP_ERROR_FLAG		0xC5
#define HDMI_REG_ADDRESS_HDCP_SUPPORT			0xBE
#define HDMI_REG_ADDRESS_HDCP_FAST				0xBE
#define HDMI_REG_ADDRESS_HDCP_BSSV1				0xBF
#define HDMI_REG_ADDRESS_HDCP_BSSV2				0xC0
#define HDMI_REG_ADDRESS_HDCP_BSSV3				0xC1
#define HDMI_REG_ADDRESS_HDCP_BSSV4				0xC2
#define HDMI_REG_ADDRESS_HDCP_BSSV5				0xC3
#define HDMI_REG_ADDRESS_HDCP_REQUEST_FLAG		0xC6
#define HDMI_REG_ADDRESS_HDCP_BKSV_COUNT		0xC7
#define HDMI_REG_ADDRESS_HDCP_BKSV_FLAG			0xC7
#define HDMI_REG_ADDRESS_HDCP_CONTROLLER_STATE	0xC8
#define HDMI_REG_ADDRESS_HDCP_CONTROLLER_ERROR	0xC8

/* IRQ Handling Registers 0x96 */
#define IRQ_HOT_PLUG_DETECT(REG)            ISSET_BIT(REG, BIT_IRQ_HOT_PLUG_DETECT)
#define IRQ_MONITOR_SENSE(REG)              ISSET_BIT(REG, BIT_IRQ_MONITOR_SENSE)
#define IRQ_ACTIVE_VSYNC_EDGE(REG)          ISSET_BIT(REG, BIT_IRQ_ACTIVE_VSYNC_EDGE)
#define IRQ_AUDIO_FIFO_FULL(REG)            ISSET_BIT(REG, BIT_IRQ_AUDIO_FIFO_FULL)
#define IRQ_EMBEDDED_SYNC_PARITY_ERROR(REG) ISSET_BIT(REG, BIT_IRQ_EMBEDDED_SYNC_PARITY_ERROR)
#define IRQ_EDID_READY(REG)                 ISSET_BIT(REG, BIT_IRQ_EDID_READY)
#define IRQ_HDCP_AUTHENTICATED(REG)         ISSET_BIT(REG, BIT_IRQ_HDCP_AUTHENTICATED)

/* IRQ Handling Registers 0x97 */
#define IRQ_HDCP_ERROR(REG)					ISSET_BIT(REG, BIT_IRQ_HDCP_ERROR)
#define IRQ_BKSV_FLAG(REG)					ISSET_BIT(REG, BIT_IRQ_BKSV_FLAG)
#define IRQ_CEC_TX_READY(REG)				ISSET_BIT(REG, BIT_IRQ_CEC_TX_READY)
#define IRQ_CEC_TX_ARBITRATION_LOST(REG)	ISSET_BIT(REG, BIT_IRQ_CEC_TX_ARBITRATION_LOST)
#define IRQ_CEC_TX_RETRY_TIEMOUT(REG)		ISSET_BIT(REG, BIT_IRQ_CEC_TX_RETRY_TIEMOUT)
#define IRQ_CEC_RX_READY(REG)				ISSET_BIT(REG, BIT_IRQ_CEC_RX_READY)

/* HDCP Controller Status: R0xC8[3:0] */
#define HDCP_CTRLSTATE_InReset 				0
#define	HDCP_CTRLSTATE_ReadingEdid			1
#define	HDCP_CTRLSTATE_Idle					2
#define	HDCP_CTRLSTATE_Initializing			3
#define	HDCP_CTRLSTATE_HdcpEnabled			4
#define	HDCP_CTRLSTATE_InitRepeater			5


/* HDCP Error Codes (R0xC5[7] is set!): R0xC8[7:4] */
#define	HDCP_ERRORCODE_NoError				0
#define	HDCP_ERRORCODE_BadReceiverBKSV		1
#define	HDCP_ERRORCODE_RiMismatch			2
#define	HDCP_ERRORCODE_PjMismatch			3
#define	HDCP_ERRORCODE_I2CError				4
#define	HDCP_ERRORCODE_TimeoutRepeater		5
#define	HDCP_ERRORCODE_MaxCascadeExceed		6
#define	HDCP_ERRORCODE_SHA1HashCheckFailed	7

/* HDCP ErrorCode */
typedef union	hdcpDebugRegister
{
	char	byte;
	struct
	{
		char	state:4;
		char	errorCode:4;
	}		bits;
} uHdcpDebugRegister;

#endif /* ADV7520NK_HDMI_H_ */
