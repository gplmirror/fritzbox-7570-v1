#include <linux/init.h>
#include <linux/module.h>
#include <linux/autoconf.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/env.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>

#include <asm/irq.h>
#include <asm/io.h>
//#include <asm/uaccess.h>

#include <asm/arch/hardware.h>

#include <asm/arch/gpio.h>
#include <asm/arch/hw_irq.h>
#include <asm/arch/i2c-client.h>

#include "adv7520nk_core.h"
#include "adv7520nk_chrdev.h"

#ifdef CONFIG_I2C_DEBUG_CHIP
#define _D printk
static void hdmictrl_print_array( char* pBuffer, unsigned int size );
#else
#define _D 1?(void)0:(void)
#define hdmictrl_print_array( B, S)
#endif

#define _I printk
#define _E printk

#define HDMI_GPIO_IRQ_PIN        gpio
#define IRQ_HDMI DAVINCI_GPIO_IRQ(HDMI_GPIO_IRQ_PIN)

/* Private Data of the module! */
static sAdv7520PrivData s_privData;


#define GET_MAIN_REGISTER( REG )     s_privData.hdmi_registers[REG]

#define HDCP_ERROR_FLAG                     ISSET_BIT(GET_MAIN_REGISTER(HDMI_REG_ADDRESS_HDCP_ERROR_FLAG), BIT_HDCP_ERROR_FLAG)
#define HDCP_REQUEST_FLAG                   ISSET_BIT(GET_MAIN_REGISTER(HDMI_REG_ADDRESS_HDCP_REQUEST_FLAG), BIT_HDCP_REQUEST_FLAG)
#define HDCP_FAST_AUTHENTIFICATION_FLAG     ISSET_BIT(GET_MAIN_REGISTER(HDMI_REG_ADDRESS_HDCP_FAST), BIT_HDCP_FAST_HDCP)
#define HDCP_SUPPORT_FLAG                   ISSET_BIT(GET_MAIN_REGISTER(HDMI_REG_ADDRESS_HDCP_SUPPORT), BIT_HDCP_SUPPORT)
#define HDCP_RX_SENSE_STATE                 ISSET_BIT(GET_MAIN_REGISTER( HDMI_REG_ADDRESS_RX_SENSE), 5)
#define HDCP_BKSV_FLAG                      ISSET_BIT(GET_MAIN_REGISTER(HDMI_REG_ADDRESS_HDCP_BKSV_FLAG), 7)


static unsigned int hdmictrlval = 0;
static unsigned int gpio = CONFIG_SENSORS_ADV7520NK_HDMI_AVM_GPIO;

static struct proc_dir_entry *proc_file;

static int  hdmictrl_registers_refresh( void );
static char hdmictrl_register_get( unsigned char addr, char refresh );
static int  hdmictrl_register_set( char addr, char val );
static int  hdmictrl_irqs_config( void );
static int  hdmictrl_clr_irq( char irq_req1, char irq_req2 );
static int 	hdmictrl_read_edid( void );

static int hdmictrl_hdcp_enable( int enable );
static int hdmictrl_hdcp_debug( void );
static int hdmictrl_hdcp_errorHandling( void );



static int hdmictrl_proc_read( char *buf, char **start, off_t offset,
                int size, int *eof, void *data)
{
    int bytes_written=0, ret;

    _D("[hdmictrl_proc_read]\n");
    hdmictrlval = 0;

    ret=snprintf(buf+bytes_written, size-bytes_written, "0x%08x\n",hdmictrlval);
    bytes_written += (ret>(size-bytes_written)) ? (size-bytes_written):ret;

    *eof = 1;
    return bytes_written;
}

static unsigned int parseval(unsigned char *p, int base)
{
	unsigned int v=0;
	int count=0;
	while(*p && count<8) {
		unsigned char c = *p++;
		unsigned int  x=0;
		switch(c) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				x=c-'0'; break;
			case 'a': case 'A': x=0x0a; break;
            case 'b': case 'B': x=0x0b; break;
			case 'c': case 'C': x=0x0c; break;
			case 'd': case 'D': x=0x0d; break;
			case 'e': case 'E': x=0x0e; break;
			case 'f': case 'F': x=0x0f; break;
			default:goto stop;
		}
		v*=base;
		v+=x;
		count++;
	}
stop:
	return v;
}

static int hdmictrl_proc_write( struct file *instanz, const char __user *userbuffer,
        unsigned long count, void *data )
{
    char *kernel_buffer;
    int not_copied;
    unsigned int oldval;

    _D("[hdmictrl_proc_write]\n");
    kernel_buffer = kmalloc( count+1, GFP_KERNEL );
    if( !kernel_buffer ) {
        return -ENOMEM;
    }
    not_copied = copy_from_user( kernel_buffer, userbuffer, count );
    kernel_buffer[count]='\0';
    _D("(%s)",kernel_buffer);

    oldval = hdmictrlval;

    if     ( strncmp( "0x", kernel_buffer, 2 )==0 ) {
	    hdmictrlval=parseval(kernel_buffer+2,16);
    }

    _I("0x%08x modified to 0x%08x\n", oldval, hdmictrlval);
nowrite:
    kfree( kernel_buffer );
    return count-not_copied;
}

static int hdmictrl_clr_irq( char irq_reg1, char irq_reg2 )
{
    int rc=0;

    if( irq_reg1 )
    	rc=hdmictrl_register_set(0x96, irq_reg1);

    if( irq_reg2 )
		rc=hdmictrl_register_set(0x97, irq_reg2);

    return rc;
}

static int hdmictrl_registers_refresh( void )
{
    s_privData.hdmi_registers[0]=0;

    return davinci_i2c_read(255, s_privData.hdmi_registers, 0x3d);
}

static int hdmictrl_register_set( char addr, char val )
{
    char cmd[2];

    cmd[0] = addr;
    cmd[1] = val;

    return davinci_i2c_write( 2, cmd, 0x3d );
}

/** Return register value of hdmi chip and refresh the registers if requested.
 *
 * @param addr      register address of ADV7520
 * @param refresh   if 1, read all registers over i2c
 * @return          register value
 */
static char hdmictrl_register_get( unsigned char addr, char refresh )
{
    if( refresh )
        hdmictrl_registers_refresh();

    return s_privData.hdmi_registers[addr];
}


static int hdmictrl_hdcp_enable( int enable )
{
    int rc=0;
    char hdcp_reg;

    hdcp_reg = hdmictrl_register_get( HDMI_REG_ADDRESS_HDCP_DESIRED, 0 );

    if( enable )
    {
        /* Set bit7(HDCP_DESIRE) to 1 */
        SET_BIT(hdcp_reg, BIT_HDCP_DISIRED);

        hdmictrl_register_set( 0xAF, hdcp_reg );

        /* Set bit4(FRAME_ENCRYPT) to 1 */
        SET_BIT(hdcp_reg, BIT_HDCP_FRAME_ENCRYPTION);

        hdmictrl_register_set( 0xAF, hdcp_reg );

        /* SET Mode to HDMI */
        //SET_BIT(hdcp_reg, BIT_HDCP_HDMI_DVI_SELECT );
    }
    else
    {
        /* Clear bit7(HDCP_DESIRE) to 1 */
        CLEAR_BIT(hdcp_reg, BIT_HDCP_DISIRED);
        /* Clear bit4(FRAME_ENCRYPT) to 1 */
        CLEAR_BIT(hdcp_reg, BIT_HDCP_FRAME_ENCRYPTION);

        hdmictrl_register_set( 0xAF, hdcp_reg );
    }

    return rc;
}

static int hdmictrl_get_hotplug(void)
{
    return hdmictrl_register_get( 0x42, 0) & 0x40;
}

struct i2c_cmd {
    char addr;
    char value;
};

static struct i2c_cmd enable_sequence[] = {
    { 0x41, 0x10 },
    { 0x98, 0x03 },
    { 0x9c, 0x38 },
    { 0x9d, 0x61 },
    { 0xa2, 0x25 },
    { 0xa3, 0x25 },
    { 0xde, 0x88 },
    { 0xaf, 0x16 },
    { 0x4b, 0x00 },
    { 0x01, 0x00 },
    { 0x02, 0x18 },
    { 0x03, 0x80 },
    { 0x07, 0x00 },
    { 0x08, 0x00 },
    { 0x09, 0x00 },
    { 0x14, 0x0b },
    { 0x0a, 0x53 },
    { 0x0b, 0x8e },
    { 0x0c, 0xbc },
    { 0xBA, 0x70 }	/* Clock Delay[7-5]: No Delay; ADV7520 Selection: internal keys */
};


static int hdmictrl_enable(int enable)
{
    // program hdmi via i2c (s.init.sh)
    int rv, i;
    int size = sizeof(enable_sequence)/sizeof(enable_sequence[0]);
    for( i=0 ; i<size ; i++ ) {
        rv = davinci_i2c_write( 2, (char*)&enable_sequence[i], 0x3d );
    }
    return 0;
}

static struct resource my_irq_gpio_resource = {
    .name = "hdmi irq",
    .flags = IORESOURCE_IO
};

static int EnableGpioIrq(int gpio) {
    struct gpio_controller *GPIO = (struct gpio_controller *)IO_ADDRESS(DAVINCI_GPIO_BASE + 0x10);
    volatile unsigned int *gpio_bank_irq_enable = (volatile unsigned int *)IO_ADDRESS(DAVINCI_GPIO_BASE + 0x08);

    _D("[EnableGpioIrq]\n");

    my_irq_gpio_resource.start = my_irq_gpio_resource.end = gpio;
    if(request_resource(&gpio_resource, &my_irq_gpio_resource)) {
        _E(KERN_ERR "ERROR: gpio %u is not avalible as irq pin\n", gpio);
        return -EFAULT;
    }

    /*--- board_setup_gpio("msp430", gpio); ---*/
#if 1
    davinci_gpio_ctrl(gpio, GPIO_PIN, GPIO_INPUT_PIN);
#else
    //GPIO->set_rising  = 1 << gpio;
    GPIO->clr_rising  = 1 << gpio;
    GPIO->set_falling = 1 << gpio;
    //GPIO->clr_falling = 1 << gpio;

    *gpio_bank_irq_enable |= 1;
    GPIO->intstat = (1 << gpio); /* clear pending irqs */
#endif
    return 0;
}

static void wq_function( void *data );

static DECLARE_WORK( wq_object, wq_function, NULL );

static void wq_function( void *data )
{
    char irq_reg1 = hdmictrl_register_get( 0x96 , 1);
    char irq_reg2 = hdmictrl_register_get( 0x97 , 0);

    _D("START reg1=0x%x reg2=0x%x!\n", irq_reg1, irq_reg2);

    /* Look, which interrupt was triggered! */
    /* HOT Plug Detect */
    if(IRQ_HOT_PLUG_DETECT(irq_reg1))
    {
        _D("HOT Plug Detect: ");
        if( hdmictrl_get_hotplug() )
        {
        	_D("HDMI Plugged in!\n");
            hdmictrl_enable(1);
        }
        else
        	_D("HDMI Plugged out!\n");
    }
    /* Monitor Sense */
    if(IRQ_MONITOR_SENSE(irq_reg1))
    {
		_D("Monitor Sense: ");

		if( hdmictrl_get_hotplug() )
		{
			_D("HDMI Plugged in!\n");
			hdmictrl_enable(1);
		}
		else
			_D("HDMI Plugged out!\n");
	}
    /* Active Vsync Sense */
    if( IRQ_ACTIVE_VSYNC_EDGE(irq_reg1) )
    {
        _D("Active Vsync Edge!\n");

    }
    /* Audio FIFO full */
    if( IRQ_AUDIO_FIFO_FULL(irq_reg1) )
    {
        _D("Audio FIFO full!\n");
    }
    /* Embedded Sync Parity Error */
    if( IRQ_EMBEDDED_SYNC_PARITY_ERROR(irq_reg1) )
    {
        _D("Embedded Sync Parity Error!\n");
    }
    /* EDID Ready */
    if( IRQ_EDID_READY(irq_reg1) )
    {
        _D("EDID Ready!\n");
        hdmictrl_read_edid();
        /*hdmictrl_hdcp_enable(1);*/
    }
    /* HDCP Authenticated */
    if ( IRQ_HDCP_AUTHENTICATED(irq_reg1) )
    {
        _D("HDCP Authenticated!\n");
    }
    /* HDCP Error */
    if ( IRQ_HDCP_ERROR(irq_reg2 ) )
    {
    	_D("HDCP Error!\n");
    	hdmictrl_hdcp_debug( );

    	if( HDCP_ERROR_FLAG )
    		hdmictrl_hdcp_errorHandling();
    	//CLEAR_BIT( irq_reg2, BIT_IRQ_HDCP_ERROR );
    }
    /* BKSV FLAG */
    if( IRQ_BKSV_FLAG(irq_reg2) )
    {
    	_D("BKSV Flag!\n");
    }

    /* Clear all Interrupts */
    hdmictrl_clr_irq( irq_reg1, irq_reg2 );

    _D("OK\n");
    return;
error:
    _D("STOP\n");
    return;
}

#ifdef CONFIG_I2C_DEBUG_CHIP
static void hdmictrl_print_array( char* pBuffer, unsigned int size )
{
	int colums,rows=size/16, r, c;
	char* p = pBuffer;

	for( r=0; r<rows; r++)
	{
		_D("0x%04x: ", r*16);
		for( c=0; c<16; c++ )
		{
			_D("0x%02x ", *p++);
		}
		_D("\n");
	}

	if( size % 16 )
	{
		_D("0x%04x: ", r*16);
		for( c=0; c<(size%16); c++ )
		{
			_D("0x%02x ", *p++);
		}
		_D("\n");
	}
	_D("\n");

}
#endif

static int hdmictrl_read_edid( void )
{
	int rc = 0;
	char edid_dev_addr = hdmictrl_register_get( 0x43, 0);

	edid_dev_addr>>=1;

	_D("read edid from dev=0x%02x\n", edid_dev_addr);
	rc=davinci_i2c_read(255, s_privData.edid_register, edid_dev_addr);

	if( rc )
		_E("Error reading edid!\n");

	hdmictrl_print_array( s_privData.edid_register, 256 );

	return rc;
}

static int hdmictrl_hdcp_debug( void )
{
	uHdcpDebugRegister hdcpDebugRegister;

	hdcpDebugRegister.byte = hdmictrl_register_get( HDMI_REG_ADDRESS_HDCP_CONTROLLER_STATE, 0);

	_D("HDCP: request=%s state=0x%x!\n", (HDCP_REQUEST_FLAG ? "yes" : "no"), hdcpDebugRegister.bits.state);
	_D("HDCP: HDCP1.2Support=%s FastHdcpAuthentication=%s!\n", ( HDCP_SUPPORT_FLAG ? "yes" : "no"), ( HDCP_FAST_AUTHENTIFICATION_FLAG ? "yes" : "no"));
	_D("HDCP: Rx Sense State: %u!\n", ( HDCP_RX_SENSE_STATE ? 1: 0));
	_D("HDCP: BKSV Flag: %u\n", (HDCP_BKSV_FLAG ? 1 : 0) );

	return 0;
}

static int hdmictrl_hdcp_errorHandling( void )
{
	int rc=0;
	uHdcpDebugRegister hdcpDebugRegister;

	hdcpDebugRegister.byte = hdmictrl_register_get( HDMI_REG_ADDRESS_HDCP_CONTROLLER_ERROR, 0 );

	switch( hdcpDebugRegister.bits.errorCode )
	{
	case HDCP_ERRORCODE_NoError:
		_E("[HDCP Error: %s!\n", "HDCP_ERRORCODE_NoError" );
		break;
	case HDCP_ERRORCODE_BadReceiverBKSV:
		_E("[HDCP Error: %s!\n", "BadReceiverBKSV");
		break;
	case HDCP_ERRORCODE_RiMismatch:
		_E("[HDCP Error: %s!\n", "RiMismatch");
		/*hdmictrl_hdcp_enable( 1 );*/
		break;
	case HDCP_ERRORCODE_PjMismatch:
		_E("[HDCP Error: %s!\n", "PjMismatch");
		break;
	case HDCP_ERRORCODE_I2CError:
		_E("[HDCP Error: %s!\n", "I2CError");
		break;
	case HDCP_ERRORCODE_TimeoutRepeater:
		_E("[HDCP Error: %s!\n", "TimeoutRepeater");
		break;
	case HDCP_ERRORCODE_MaxCascadeExceed:
		_E("[HDCP Error: %s!\n", "MaxCascadeExceed");
		break;
	case HDCP_ERRORCODE_SHA1HashCheckFailed:
		_E("[HDCP Error: %s!\n", "SHA1HashCheckFailed");
		break;

	}
	return rc;
}
static int hdmi_irq_param=0;

static irqreturn_t hdmi_isr(int irq, void *dummy, struct pt_regs *fp)
{
	int handled=1;

	_D("[hdmi_isr]\n");

#if 1
    if( !queue_work( s_privData.wq, &wq_object ) ) {
        _D("ERROR: queue_work failed !\n");
    }
#endif
	return IRQ_RETVAL(handled);
}

static void hdmictrl_cleanup( void )
{
    _D("[hdmictrl_cleanup]\n");
    if( s_privData.has_resource>0 ) {
		release_resource(&my_irq_gpio_resource);
	}
    if( s_privData.has_irq>0 ) {
		free_irq(IRQ_HDMI, &hdmi_irq_param);
	}

    if( proc_file ) remove_proc_entry( "hdmictrl", NULL);

   	if( s_privData.wq ) {
		destroy_workqueue( s_privData.wq );
	}

   	hdmictrl_chrdev_unregister( &s_privData );

}

static int hdmictrl_irqs_config( void )
{
	int rc;
	char irq_mask_1=0;
	char irq_mask_2=0;

	/* Configure irq Mask 1 */
	SET_BIT( irq_mask_1, BIT_IRQ_HOT_PLUG_DETECT );
	SET_BIT(irq_mask_1, BIT_IRQ_MONITOR_SENSE );
	SET_BIT(irq_mask_1, BIT_IRQ_EDID_READY);
	//SET_BIT(irq_mask_1, 4);
	//SET_BIT(irq_mask_1, 3);

	/* Configure irq Mask 2*/
	SET_BIT(irq_mask_2, BIT_IRQ_HDCP_ERROR );
	SET_BIT(irq_mask_2, BIT_IRQ_BKSV_FLAG );

	_D("Set IrqMask 0x94=0x%x 0x95=0x%x!\n", irq_mask_1, irq_mask_2);
	/* Enable IRQs R0x96: Set Interruptmask in R0x94 */
	if( (rc=hdmictrl_register_set( 0x94, irq_mask_1)) )
	{
		_E("Error setting IrqMask 0x94 0x%x!\n", irq_mask_1);
		return rc;
	}

	/* Enable IRQs R0x97: Set Interruptmask in R0x95*/
	if( (rc=hdmictrl_register_set( 0x95, irq_mask_2)))
	{
		_E("Error setting IrqMask 0x95 0x%x!\n", irq_mask_2);
	}
	return rc;
}

static int __init hdmictrl_init(void)
{
    int err=0;
    char reg;
    _D("[hdmictrl_init gpio:%d]\n",gpio);

    memset( &s_privData, 0, sizeof(s_privData) );
   	s_privData.wq = create_workqueue("irqueue");

   	/* Enable IRQs on CHIP */
   	hdmictrl_irqs_config();

#if 1
    hdmictrl_register_get( 0x42, 1);
    // initial status update
    if( hdmictrl_get_hotplug() )
        hdmictrl_enable(1);
#else
    wq_function( NULL );
#endif

    // get gpio12 on 8160
    // register isr
    if( EnableGpioIrq(HDMI_GPIO_IRQ_PIN) ) {
		return -EFAULT;
	}
    s_privData.has_resource++;

    if (request_irq(IRQ_HDMI, hdmi_isr, IRQF_SHARED | IRQF_TRIGGER_FALLING, "hdmi hotplug", &hdmi_irq_param )) {
	    _E(KERN_ERR "Can't allocate irq %d\n", IRQ_HDMI);
	    return -EBUSY;
    }
	s_privData.has_irq++;

    proc_file=create_proc_entry( "hdmictrl", S_IRUSR | S_IWUSR, NULL );
    if( proc_file ) {
        proc_file->read_proc  = hdmictrl_proc_read;
        proc_file->write_proc = hdmictrl_proc_write;
        proc_file->data = NULL;
    }

    hdmictrl_chrdrv_register( &s_privData );

    return 0;
error:
    hdmictrl_cleanup();
    return err;
}

static void __exit hdmictrl_exit(void)
{
    _D("[hdmictrl_exit]\n");
    hdmictrl_cleanup();

    /* Reset Chip */
    hdmictrl_register_set( 0x41, (1<<6));
}

module_init(hdmictrl_init);
module_exit(hdmictrl_exit);

module_param( gpio, uint, 0 );
MODULE_PARM_DESC(gpio,"GPIO Pin of HDMIINT (defaults to 9)");

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Module for hdmi control");
MODULE_AUTHOR("avm.de");

