/*
 * adv7520nk_chrdev.c
 *
 *  Created on: 22.10.2008
 *      Author: sschroeder
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>


#include "adv7520nk_core.h"

#define _E( ARGS... )  printk("[ADV7520] CDEV Error:" ARGS )

#define _D( ARGS... )  printk("[ADV7520] CDEV Debug:" ARGS )

static int hdmictrl_chrdev_open(struct inode* pInode, struct file* pFile);
static int hdmictrl_chrdev_release(struct inode* pInode, struct file* pFile);
static int hdmictrl_chrdev_ioctl(struct inode* pInode, struct file* pFile, unsigned int cmd, unsigned long arg);

static struct file_operations adv7520nk_fops = { .owner = THIS_MODULE, .open = hdmictrl_chrdev_open,
        .release= hdmictrl_chrdev_release, .ioctl = hdmictrl_chrdev_ioctl };

int hdmictrl_chrdrv_register(sAdv7520PrivData* pHandle)
{
    int rc = 0;
    int err;

    if ((err = alloc_chrdev_region(&pHandle->n_cdev, 0, 1, "adv7520nk")) != 0)
    {
        _E("allocating chrdev (rc=%d)!\n", err);
        rc = 1;

        goto hdmictrl_chrdrv_register_return;
    }

    pHandle->s_cdev = cdev_alloc();

    if (pHandle->s_cdev == NULL)
    {
        _E("Allocating struct cdev!");
        rc = 2;

        goto hdmictrl_chrdrv_register_return;
    }
    /* Init chrdev functions */
    cdev_init(pHandle->s_cdev, &adv7520nk_fops);
    pHandle->s_cdev->owner = THIS_MODULE;

    /* Assoziate dev_t with cdev */
    if ((err = cdev_add(pHandle->s_cdev, pHandle->n_cdev, 1)) < 0)
    {
        _E("Adding cdev to kernel (EC=%d)!\n", err);
        cdev_del(pHandle->s_cdev);
        rc = 3;
    }
    hdmictrl_chrdrv_register_return:

    return rc;
}

int hdmictrl_chrdev_unregister(sAdv7520PrivData* pHandle)
{
    int rc = 0;

    if( pHandle->s_cdev )
        cdev_del( pHandle->s_cdev );

    if( pHandle->n_cdev )
        unregister_chrdev_region(pHandle->n_cdev, 1);

    return rc;
}

static int hdmictrl_chrdev_open(struct inode* pInode, struct file* pFile)
{
    int rc=0;
    sAdv7520PrivData* pPrivData;

    _D("open chrdev!\n");

    pPrivData = container_of(pInode->i_cdev, sAdv7520PrivData, s_cdev );

    if( pPrivData )
        pFile->private_data = pPrivData;
    else
    {
        _E("no private data found!");
        rc = 1;
    }

    return rc;
}

static int hdmictrl_chrdev_release(struct inode* pInode, struct file* pFile)
{
    int rc=0;

    _D("release chrdev!\n");

    return rc;
}

static int hdmictrl_chrdev_ioctl(struct inode* pInode, struct file* pFile, unsigned int cmd, unsigned long arg)
{
    int rc=0;

    _D("ioctl chrdev!\n");

    return rc;
}

