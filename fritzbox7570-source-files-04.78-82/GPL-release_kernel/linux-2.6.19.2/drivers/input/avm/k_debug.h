#ifndef K_DEBUG_INCLUDED
#define K_DEBUG_INCLUDED

/*--- #define DEBUG ---*/

#ifdef DEBUG
#define _D printk
#else
#define _D 1?(void)0:(void)
#endif
#define _I printk
#define _E printk

#endif
