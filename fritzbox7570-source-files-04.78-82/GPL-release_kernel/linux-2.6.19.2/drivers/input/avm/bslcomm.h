#ifndef _BSL_COMM_H_
#define _BSL_COMM_H_

/*-------------------------------------------------------------------------------------*\
 * declaration of Functions
\*-------------------------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

int Init_Com(char *comport, unsigned int baud); 
void CloseCom(void);

int bslTxRx(unsigned char cmd, unsigned short addr, unsigned short len, unsigned char* blkout, unsigned char* blkin);

#ifdef __cplusplus
}
#endif

#endif
