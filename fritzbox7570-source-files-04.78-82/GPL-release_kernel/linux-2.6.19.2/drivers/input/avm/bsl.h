/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
#ifndef _BSL_H_
#define _BSL_H_

/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
#define ACTION_PROGRAM      0x01    /*--- Mask: program data: ---*/
#define ACTION_VERIFY       0x02    /*--- Mask: verify data: ---*/
#define ACTION_ERASE_CHECK  0x04    /*--- Mask: erase check: ---*/
#define ACTION_PASSWD       0x08    /*--- Mask: transmit password: ---*/
#define ACTION_ERASE        0x10    /*--- Mask: erase while erase check ---*/
#define ACTION_ONEPASS      0x03    /*--- Mask: erase while erase check ---*/

#define BSL_HDR_OFF     0
#define BSL_CMD_OFF     1
#define BSL_L1_OFF      2
#define BSL_L2_OFF      3
#define BSL_AL_OFF      4
#define BSL_AH_OFF      5
#define BSL_LL_OFF      6
#define BSL_LH_OFF      7
#define BSL_DATA_OFF    8

/*-------------------------------------------------------------------------------------*\
 * BSL-Commands
\*-------------------------------------------------------------------------------------*/
#define BSL_CMD_SYNC        0x80 /* sync Char */
#define BSL_CMD_TXBLK       0x12 /* Transmit block    to boot loader: */
#define BSL_CMD_TXPWORD     0x10 /* Transmit password to boot loader: */
#define BSL_CMD_ERASE       0x16 /* Erase one segment:                */
#define BSL_CMD_MERASE      0x18 /* Erase complete FLASH memory:      */
#define BSL_CMD_ERASE_CHK   0x1C /* Erase check FLASH memory:      */
#define BSL_CMD_BAUD        0x20 /* change Baudrate */
#define BSL_CMD_SET_OFF     0x21 /* set memory offset */
#define BSL_CMD_LOADPC      0x1A /* Load PC and start execution:      */
#define BSL_CMD_RXBLK       0x14 /* Receive block  from boot loader: */
#define BSL_CMD_VERSION     0x1E /* Receive Version of BSL, FW, FW_Checksum */

/*-------------------------------------------------------------------------------------*\
 * Header Definitions 
\*-------------------------------------------------------------------------------------*/
#define BSL_DATA_FRAME      0x80
#define BSL_DATA_ACK        0x90
#define BSL_DATA_NAK        0xA0

/*-------------------------------------------------------------------------------------*\
 * special Definitions
\*-------------------------------------------------------------------------------------*/
#define BSL_LEN_ERASE_SEGMENT   0xA502
#define BSL_LEN_ERASE_INFO      0xA504

/*-------------------------------------------------------------------------------------*\
 * Errors
\*-------------------------------------------------------------------------------------*/
#define ERR_NONE                    0
#define ERR_OPEN_COMM               2 /* OpenComm failed:                 */
#define ERR_OPEN_COMM_INIT          21
#define ERR_OPEN_COMM_NO_HANDLE     22
#define ERR_FWFILE_INVALID          30

#define ERR_SEND_COMMAND       5 /* Unspecific error concerning transmission of command: */
#define ERR_RX_HDR_TIMEOUT     6 /* Timeout while receiving header */
#define ERR_RX_NAK             7 /* NAK received:               */
#define ERR_CMD_NOT_COMPLETED  8 /* Command did not send ACK: indicates that it didn't complete correctly: */
#define ERR_CMD_FAILED         9 /* Command failed, is not defined or is not allowed: */
#define ERR_CLOSE_COMM        10 /* CloseComm failed:           */
#define ERR_RX_UNDEF          11  

#define ERR_TIMEOUT             95  /*--- Error: unable to open input file: ---*/
#define ERR_FILE_OPEN           96  /*--- Error: unable to open input file: ---*/
#define ERR_BSL_ERASE_CHECK     97  /*--- Error: erase check failed: ---*/
#define ERR_BSL_VERIFY          98  /*--- Error: verification failed: ---*/
#define ERR_BSL_SYNC            99  /*--- Bootstrap loader syncronization error: ---*/

#define MAX_DATA_BYTES          64  /*--- mehr Daten kann der Bootloader nicht speichern ---*/
/*--- maximale Anzahl �bertragbarer Zeichen DATA+BSL_HEADER+CRC ---*/
#define MAX_FRAME_SIZE          (MAX_DATA_BYTES+BSL_DATA_OFF+2) 
/* Upper limit of address range that might be modified by 
 * "BSL checksum bug".
 */
#define BSL_CRITICAL_ADDR 0x0A00

#endif /*--- _BSL_H_ ---*/
