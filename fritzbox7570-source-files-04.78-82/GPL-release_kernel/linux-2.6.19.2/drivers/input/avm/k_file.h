#ifndef K_FILE_INCLUDED
#define K_FILE_INCLUDED

#include <linux/fs.h>

#define K_OPEN(name,flags,mode) k_open(name,flags,mode)
#define K_CLOSE(kfp) ((kfp)->ops->close(kfp))
#define K_SEEK(kfp,offset,mode) ((kfp)->ops->seek(kfp,offset,mode))
#define K_READ(kfp,buf,len) ((kfp)->ops->read(kfp,buf,len))
#define K_WRITE(kfp,buf,len) ((kfp)->ops->write(kfp,buf,len))
#define K_IOCTL(kfp,cmd,data) ((kfp)->ops->ioctl(kfp,cmd,(unsigned long)(data)))

struct k_file {
	struct file *filp;
	struct k_file_ops *ops;
	size_t size;
};

struct k_file_ops {
	void    ( *close)(struct k_file *kfp);
	loff_t  (  *seek)(struct k_file *kfp, loff_t offset, int mode);
	ssize_t (  *read)(struct k_file *kfp, char __user *buffer, size_t size);
	ssize_t ( *write)(struct k_file *kfp, char __user *buffer, size_t size);
	int     (*select)(struct k_file *kfp);
	int     ( *ioctl)(struct k_file *kfp, unsigned int cmd, unsigned long data); 
};

extern struct k_file *k_open( char *name, int, int mode );
extern int SetBaudrate(struct k_file *kfp, int rate);

#endif
