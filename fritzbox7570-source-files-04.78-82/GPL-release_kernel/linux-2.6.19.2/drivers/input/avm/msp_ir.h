#ifndef MSP_IR_H_INCLUDED
#define MSP_IR_H_INCLUDED

int parseIR( unsigned char *cmd, int len, unsigned int custcode, unsigned int mode, unsigned int *code );

#endif
