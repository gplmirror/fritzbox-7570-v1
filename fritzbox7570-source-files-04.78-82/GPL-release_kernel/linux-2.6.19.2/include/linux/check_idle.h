#ifndef _linux_check_idle
#define _linux_check_idle

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if CONFIG_CHECK_IDLE
void init_check_idle(int shift, int schwelle, void (*report_function)(unsigned long, unsigned long), signed int (*timer_function)(void));
void end_check_idle(void);
void begin_check_idle(void);
signed int davinci_get_check_idle_timer(void);
#endif /*--- #if CONFIG_CHECK_IDLE ---*/

#endif /*--- #ifndef _linux_check_idle ---*/
