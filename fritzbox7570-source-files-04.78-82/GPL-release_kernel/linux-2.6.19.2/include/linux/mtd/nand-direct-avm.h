#ifndef __LINUX_MTD_NAND_DIRECT_AVM_H
#define __LINUX_MTD_NAND_DIRECT_AVM_H

#include <linux/mtd/nand.h>

struct direct_avm_nand_platdata {
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR)
	int	addr_nce;
	int	addr_nwp;
	int	addr_cle;
	int	addr_ale;
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_ADDR) ---*/
	int	gpio_rdy;
#if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO)
	int	gpio_nwp;
	int	gpio_nce;
	int	gpio_ale;
	int	gpio_cle;
#endif /*--- #if defined(CONFIG_MTD_NAND_DIRECT_AVM_GPIO) ---*/
	void	(*adjust_parts)(struct direct_avm_nand_platdata *, size_t);
	struct mtd_partition *parts;
	unsigned int num_parts;
	unsigned int options;
	int	chip_delay;
};

#endif /*--- #ifndef __LINUX_MTD_NAND_DIRECT_AVM_H ---*/
