#ifndef __DSL_AVM_EVENT__
#define __DSL_AVM_EVENT__

#include <linux/avm_event.h>

/* *****************************************************************************************
 * Andrea: Versionsmummer der Datei. 
 * BITTE HOCHZ�HLEN VOR JEDEM CHECK IN. DIE APPLIKATIONEN UND DIE DRIVERS, DIE DIESE DATEI 
 * INCLUDIEREN, SOLLEN DEN WERT DIESES DEFINES BEIM HOCHFAHREN AUSGEBEN, UM SICHERZUSTELLEN, 
 * DASS SIE GEGEN DIE SELBE VERSION DES HEADERS KOMPILIERT WORDEN SIND.
 * *****************************************************************************************/
#define DSL_AVM_EVENT_FILE_VERSION	1


//! This driver version supports the datapump version in ctlmgr interface
#define SUPPORT_DSL_AVM_EVENT_DATAPUMPVERSION   1

/*! DSL driver name */
#define AVM_TIATM_CHRDEV                        "/dev/tiatm"

/* For TI */
#define MAX_CMSGPCB2_LENGTH   12 /* must be same as in dev_host_interface.h */

/*! This defines the structure version of DSL_SMALL_STAT_EVENT */
#define AVM_SMALL_STAT_EVENT_VERSION            1

/* Set this define to a valid DSL parametrization interface.
   Note: the parametrization interface version must match to that of the DSL pump!
*/
#define AVM_DSL_PARAM_INTERFACE_VERSION         1

#if AVM_DSL_PARAM_INTERFACE_VERSION
/* Type used for setting and getting the DSL parameters */
typedef struct TIOIDDSL_PARAMS
{
    unsigned int    InterfaceVersion;   /* Version number of this interface */
    unsigned int    ControlBits;        /* Control bit field.
                                            bit0     RFI detection off
                                            bit1-2   0: automatic delay path selection
                                                     1: fast
                                                     2: interleaved
                                                     3: reserved
                                        */
    unsigned int    MaxDsRate;          /* [kbit/s] Only ADSL2(+) */
    unsigned int    MaxUsRate;          /* [kbit/s] Only ADSL2(+) */
    unsigned short  RFI_f;              /* Carrier Index */
    unsigned short  UnusedBand_fu;      /* Carrier Index */
    unsigned short  UnusedBand_fo;      /* Carrier Index */
    signed char     OffsetDsMargin;     /* [dB/10] steps */
    signed char     OffsetDsPcb;        /* [dB] ADSL2(+) only */
    signed char     OffsetUsPcb;        /* [dB] ADSL2(+) only */
    unsigned char   _reserved[3];       /* Reserved field */
} TIOIDDSL_PARAMS;

/* Type used for setting and getting the DSL features on UR8 */
typedef struct TIOIDDSL_FEATURES
{
    /**
    * Target channel, one of the following enums:
        0: DSL_PHY_FEATURELIST0
        1: DSL_PHY_FEATURELIST1
        2: INTEROP_FEATURELIST0
        3: INTEROP_FEATURELIST1
    */
    unsigned int    Channel;
    
    /**
    * Feature bits. See the TI Feature bit documentation for more details.
    */
    unsigned int    FeatureBitField;
} TIOIDDSL_FEATURES;

#endif /* AVM_DSL_PARAM_INTERFACE_VERSION */

//#define AVM_TIATM_IOCTL_GET_STATUS    _IOR(0xF8, 1, TIOIDINFO)
//bk: fixed value because TIOIDINFO has a different length in kernel and in user mode.
#define AVM_TIATM_IOCTL_GET_STATUS        0x4638F801

//! Stop the trace service
#define AVM_TIATM_IOCTL_STOP_TRACE        _IO(0xA0, 1)
//! Start the trace service
//#define AVM_TIATM_IOCTL_START_TRACE       _IO(0xA0, 2)  //Not supported yet on 2.6.19 kernel layouts
//! Retrain the modem, use this when parameters changed
#define AVM_TIATM_IOCTL_RETRAIN_MODEM     _IO(0xA0, 3)

//! Set DSL parameters; new parameters take effect after the next modem retrain
#define AVM_TIATM_IOCTL_DSLPARAMS_SET     _IOW(0xB0, 1, TIOIDDSL_PARAMS)
//! Get DSL parameters
#define AVM_TIATM_IOCTL_DSLPARAMS_GET     _IOR(0xB0, 2, TIOIDDSL_PARAMS)

/*! Set feature bits for given channel. */
#define AVM_TIATM_IOCTL_SET_FEATURES      _IOW(0xA5, 1, TIOIDDSL_FEATURES)
/*! Get current feature bits for given channel. */
#define AVM_TIATM_IOCTL_GET_FEATURES      _IOR(0xA5, 2, TIOIDDSL_FEATURES)

enum
{
  RSTATE_NO_CABLE,
  RSTATE_IDLE,
  RSTATE_INIT,
  RSTATE_HS,
  RSTATE_RTDL,
  RSTATE_SHOWTIME,
};

enum
{
  DSLTRAIN_NO_MODE,
  DSLTRAIN_MULTI_MODE,
  DSLTRAIN_T1413_MODE,
  DSLTRAIN_GDMT_MODE,
  DSLTRAIN_GLITE_MODE,
  DSLTRAIN_ADSL2_MODE,
  DSLTRAIN_ADSL2PLUS_MODE,
  DSLTRAIN_VDSL2_MODE
};

typedef struct
{
	unsigned short first;
	unsigned short last;
} US_BandIndex_t;


typedef struct
{
	unsigned char NumBandsUS;
	US_BandIndex_t band[5]; 
}US_BandConfig_t;



typedef union expertInformation {
	struct {
		unsigned short US_Option;					/*choosen upstream Option (0..3)*/
		unsigned short DS_Option;					/*choosen downstream Option (0..3)*/
		unsigned short USConRate[4];				/**/
		unsigned short DSConRate[4];				/**/
		unsigned short US_Net_DR[4];				/**/
		unsigned short DS_Net_DR[4];				/**/
		unsigned char  US_Path[4];					/**/
		unsigned char  DS_Path[4];
		unsigned short usDataBytesPerSymbol[4];		/**/
		unsigned short dsDataBytesPerSymbol[4];
		unsigned short usParityBytesPerSymbol[4];	/**/
		unsigned short dsParityBytesPerSymbol[4];
		unsigned short usSymbolsPerCodeWord[4];		/**/
		unsigned short dsSymbolsPerCodeWord[4];
		unsigned short usInterleaverDepth[4];		/**/
		unsigned short dsInterleaverDepth[4];
		unsigned char OLR_Bitswap_DS;
		unsigned char OLR_Bitswap_US;
	} adsl;
	struct {
		unsigned char US_B;
		unsigned char DS_B;
		unsigned char US_MSGc;
		unsigned char DS_MSGc;
		unsigned char US_T;
		unsigned char DS_T;
		unsigned char US_M;
		unsigned char DS_M;
		unsigned char US_R;
		unsigned char DS_R;
		unsigned char US_D;
		unsigned char DS_D;
		unsigned short US_L;
		unsigned short DS_L;
		unsigned short US_MaxConRate;
		unsigned short DS_MaxConRate;
		unsigned short US_MinConRate;
		unsigned short DS_MinConRate;
		unsigned short US_msg_rate;
		unsigned short DS_msg_rate;
		unsigned short US_net_res;
		unsigned short DS_net_res;
		unsigned short US_delay_max;
		unsigned short DS_delay_max;
		unsigned short US_error_max;
		unsigned short DS_error_max;
		unsigned short US_inp_min;
		unsigned short DS_inp_min;
		unsigned short US_ifft_fill;
		unsigned short DS_ifft_fill;
		short US_nom_psd;
		short DS_nom_psd;
		short US_max_nom_psd;
		short DS_max_nom_psd;
		short US_max_nom_atp;
		short DS_max_nom_atp;

        unsigned short DS_inp_act;          /* only ADSL2(+) */
        unsigned short US_inp_act;          /* only ADSL2(+) */
		unsigned char OLR_Bitswap_DS;
		unsigned char OLR_Bitswap_US;
		unsigned char OLR_SeamlessRA_DS;    /* only ADSL2(+) */
		unsigned char OLR_SeamlessRA_US;    /* only ADSL2(+) */
		unsigned char PM_Mode_L2;           /* only ADSL2(+) */
		unsigned char PM_Mode_L2_now;       /* only ADSL2(+) */
        unsigned short MaxFECsPerSymbol;               /* only ADSL2(+) */

	} adsl2;
} EXPERT_INFORMATION;

//bk for ur8
//copied typedef tagT143Info from TIs ur8 driver (dsl_hal_api.h)  

typedef struct tagT1413Info
{
  unsigned char  VendorId[2];
  unsigned char  t1413Revision;
  unsigned char  VendorRevision;
} T1413INFO;
//end bk

typedef struct tagTIOIDINFO
{
  unsigned char  bState;				/* addr->bDSPATURState    */
  unsigned char  TrainedMode;			/* Status of the mode in which the modem is trained (G.dmt, T1.413, etc) */
  unsigned char  Annex;					/* 0-> Unknown, 1-> Annex A, 2-> Annex B */
  unsigned char  avmFW;					/* 0 -> not an avm firmware */

  unsigned int	 US_Att_Net_DR;			/* Attainable Net Data Rate */
  unsigned int	 DS_Att_Net_DR;			/* Attainable Net Data Rate */
  unsigned int	 USConRate;				/* US Conection Rates */
  unsigned int	 DSConRate;				/* DS Connection Rates */
  unsigned int 	 US_Net_DR;				/* Net Data Rate */
  unsigned int	 DS_Net_DR;				/* Net Data Rate */
  unsigned char  US_TrainedPath;		/* Fast = 0 Interleave = 1 */
  unsigned char  DS_TrainedPath;		/* Fast = 0 Interleave = 1 */
  unsigned char  US_Adaptive;			/* 0-> fixed rate 1-> adaptive rate*/
  unsigned char  DS_Adaptive;			/* 0-> fixed rate 1-> adaptive rate*/
  unsigned short US_delay;				/* delay of Latency Path */
  unsigned short DS_delay;				/* delay of Latency Path */
  unsigned short US_Frame_OR;			/* Frame Overhead Rate */
  unsigned short DS_Frame_OR;			/* Frame Overhead Rate */
  unsigned short US_RS_Coding_OR;		/* RS Coding Overhead Rate */
  unsigned short DS_RS_Coding_OR;		/* RS Coding Overhead Rate */
  unsigned short US_Trellis_Coding_OR;	/* Trellis Coding Overhead Rate */
  unsigned short DS_Trellis_Coding_OR;	/* Trellis Coding Overhead Rate */
  unsigned int   US_Status;
  unsigned int   DS_Status;	

  unsigned short dsLineAttn;			/* DS Line Attenuation */
           short dsMargin;				/* Measured DS MArgin */
  unsigned short usLineAttn;
           short usMargin;

  /* LOS & SEF Stats */
  unsigned int   LOS_errors;			/* Num of ADSL frames where loss-of-sync */
  unsigned int   SEF_errors;			/* Num of severly errored ADSL frames - LOS > MAXBADSYNC ADSL frames */
  unsigned int   coLosErrors;			/* CO LOS Defects */
  unsigned int   coRdiErrors;			/* CO RDI defects */
  unsigned int   usCRC_errors;			/* Num of Upstream CRC errored ADSL frames */
  unsigned int   dsCRC_errors;			/* Num of Downstream CRC errored ADSL frames */
  unsigned int   usFEC_errors;			/* Num of Upstream FEC errored (corrected) ADSL frames */
  unsigned int   dsFEC_errors;			/* Num of Downstream FEC errored (corrected) ADSL frames */
  unsigned int   usNCD_error;			/* UpStream No Cell Delineation */
  unsigned int   dsNCD_error;			/* Downstream No Cell Delineation */
  unsigned int   usLCD_errors;			/* UpStream Loss of Cell Delineation (within the same connection) */
  unsigned int   dsLCD_errors;			/* Downstream Loss of Cell Delineation (within the same connection) */
  unsigned int   usHEC_errors;			/* Num of Upstream HEC errored ADSL frames */
  unsigned int   dsHEC_errors;			/* Num of Downstream HEC errored ADSL frames */

  /* Upstream ATM Stats */
  unsigned int   usAtm_count;			/* Upstream Good Cell Count */
  unsigned int   usIdle_count;			/* Upstream Idle Cell Count */

  /* Downstream ATM Stats */
  unsigned int   dsGood_count;			/* Downstream Good Cell Count */
  unsigned int   dsIdle_count;			/* Downstream Idle Cell Count */
  unsigned int   dsBadHec_count;		/* Downstream Bad Hec Cell Count */
  
  unsigned char  BitAllocTbl[512];		/*TODO : use only one table + limit*/
  unsigned int   lastUSChannel;
    signed char  snrTblDstrm[512];  /*rename snr, unsigned*/
  unsigned int   dsPilot;
           int   usTxPower;
           int   dsTxPower;
		   int	 usPowerCutBack;
		   int	 dsPowerCutBack;

  unsigned int   atucVendorId;
  unsigned char  atucGhsRevisionNum;
  unsigned char  atucVersion[2]; 
  unsigned char  currentHybridNum;

  unsigned int   StdMode;	/* ???*/
  unsigned int   FrmMode;				/* addr->atur_msg.framing_mode*/
  unsigned int   MaxFrmMode;
  unsigned int   trainFails;
  unsigned int   trainFailStates[30];
  unsigned int   idleTick;
  unsigned int   initTick;
  unsigned int   showtimeTick;
           int   showtimeCount;

  /* Sar stat*/
  unsigned int   sar_txErrors;
  unsigned int   sar_rxErrors;
  unsigned int   sar_rxPktCnt;
  unsigned int   sar_txPktCnt;
  unsigned int   sar_rxBytes;
  unsigned int   sar_txBytes;
 
  /* OAM loop back*/
           int   nearEndF4count;
           int   nearEndF5count;
           int   farEndF4count;
           int   farEndF5count;


  EXPERT_INFORMATION expert;

  /* Identification of the DSALAM */
  unsigned char DslamVendorID[8];
  unsigned char DslamVersionNumber[16];
  unsigned char DslamSerialNumber[32];
  
  /*Hochtakten infos*/
	/*chip infos*/
  unsigned short dsp_chip_name;
  unsigned char rev_number;
  unsigned char sangam_chip_type; /*7300C oder nicht!!!*/
  unsigned int time_code; /*seriennummer*/
  unsigned short overclock_possible;

	/*dynamische infos*/
  unsigned short dsp_chip_freq;

  /*Sascha: for TI*/
#ifdef AVM_CLEAR_EOC
  unsigned int   clear_eoc;        /* 1: clearEOC msg indicator; 0: otherwise */
#endif
#ifdef AVM_USE_BITFIELD /*Sascha: Umgebungsvariable wird von uns nicht gesetzt*/
  unsigned int   useBitField;   /* Use of bitfields without translation for backward compatibility. */
#endif
  
  US_BandConfig_t  US_BandConfig; /*VDSL2!*/
  unsigned char vdsl_profile;  /*1:8a, 2:8b, 3:8c, 4:8d, 5:12a, 6:12b, 7:17a, 8:30a, 0:unknown*/ 
  unsigned char vdsl_transfer_mode;  /*1: ATM, 2: PTM, 0: unknown*/ 

#if SUPPORT_DSL_AVM_EVENT_DATAPUMPVERSION
  /* Data pump version. This field is used when
     the pump version should not be determined by /proc/avalanche/avsar_ver
     If the string is empty (i.e. [0] == '\0' ) then the version must be fetched from the proc file!
     For maintaining the backward compatibility, this field must be cleared (see above) by ioctl 
     caller before the call.
  */ 
  char             datapumpVersion[64];
#endif //SUPPORT_DSL_AVM_EVENT_DATAPUMPVERSION

//  
//bk added fields from TI UR8 driver
//
#ifdef __KERNEL__
  //bk the __KERNEL__ part of TIOIDINFO must start with TIOIDINFO_kenrelOffset as it is used to get the size of TIOIDINFO in user mode in dda_common.c
  unsigned int TIOIDINFO_kernelOffset;

  unsigned int   dsOVFDrop_count;  /* Downstream Overflow Dropped Cell Count */
  unsigned int   dsPdu_count;      /* Downstream PDU Count */

  unsigned int   usPdu_count;      /* UpStream PDU Count */

#ifndef AVM_CLEAR_EOC
  unsigned int   clear_eoc;        /* 1: clearEOC msg indicator; 0: otherwise */
#endif
#ifndef AVM_USE_BITFIELD /*Sascha: Umgebungsvariable wird von uns nicht gesetzt*/
  unsigned int   useBitField;   /* Use of bitfields without translation for backward compatibility. */
#endif

  unsigned int   USPayload;        /* ennic_tx_pullup*/
  unsigned int   DSPayload;        /* ennic_indicate_receive_packet*/
  //unsigned int   TrainedPath;      /* Status of the Modem in which trained (Fast or Interleaved Path) */
  unsigned short annex_selected;
  unsigned short psd_mask_qualifier;
  unsigned int   max_ds_tones;
  unsigned int   max_us_tones;
  unsigned int   SRA;
  unsigned int   usSuperFrmCnt;    /* Num of US Superframes */
  unsigned int   dsSuperFrmCnt;    /* Num of DS Superframes */
  unsigned int   LOF_errors;       /* Number of times Loss Of Framing happened */
  unsigned int   coLofErrors;      /* Number of times Loss Of Framing happened in CO */
  unsigned int   LOF_f;              /* Loss Of Framing flag. 1=set, 0=clear */
  unsigned int   coLOF_f;            /* CO Loss Of Framing flag. 1=set, 0=clear */

  /* CRC Stats */
  unsigned int   usICRC_errors;    /* Num of Upstream CRC errored ADSL frames on Interleaved Path */
  unsigned int   usFCRC_errors;    /* Num of Upstream CRC errored ADSL frames on Fast Path */
  unsigned int   dsICRC_errors;    /* Num of Downstream CRC errored ADSL frames on Interleaved Path */
  unsigned int   dsFCRC_errors;    /* Num of Downstream CRC errored ADSL frames on Fast Path */
  unsigned int   erroredSeconds;   /* Downstream errored seconds. Currently support only downstream and ADSL2/ADSL2+. 11/2005 */
  unsigned int   severelyerrsecs;  /* Downstream severely errored seconds. Currently support only downstream and ADSL2/ADSL2+.*/
  /* FEC Stats */
  unsigned int   usIFEC_errors;    /* Num of Upstream FEC errored (corrected) ADSL frames on Interleaved Path */
  unsigned int   usFFEC_errors;    /* Num of Upstream FEC errored (corrected) ADSL frames on Fast Path */
  unsigned int   dsIFEC_errors;    /* Num of Downstream FEC errored (corrected) ADSL frames on Interleaved Path */
  unsigned int   dsFFEC_errors;    /* Num of Downstream FEC errored (corrected) ADSL frames on Fast Path */

  /* NCD Stats */
  unsigned int   usINCD_error;     /* UpStream No Cell Delineation on Interleaved Path */
  unsigned int   usFNCD_error;     /* UpStream No Cell Delineation on Fast Path */
  unsigned int   dsINCD_error;     /* Downstream No Cell Delineation on Interleaved Path */
  unsigned int   dsFNCD_error;     /* Downstream No Cell Delineation on Fast Path */

  /* LCD Stats */
  unsigned int   usILCD_errors;    /* UpStream Loss of Cell Delineation (within the same connection) on Interleaved Path */
  unsigned int   usFLCD_errors;    /* UpStream Loss of Cell Delineation (within the same connection) on Fast Path */
  unsigned int   dsILCD_errors;    /* Downstream Loss of Cell Delineation (within the same connection) on Interleaved Path */
  unsigned int   dsFLCD_errors;    /* Downstream Loss of Cell Delineation (within the same connection) on Fast Path */

  /* HEC Stats */
  unsigned int   usIHEC_errors;    /* Num of Upstream HEC errored ADSL frames on Interleaved Path */
  unsigned int   usFHEC_errors;    /* Num of Upstream HEC errored ADSL frames on Fast Path */
  unsigned int   dsIHEC_errors;    /* Num of Downstream HEC errored ADSL frames on Interleaved Path */
  unsigned int   dsFHEC_errors;    /* Num of Downstream HEC errored ADSL frames on Fast Path */

  unsigned char    bCMsgs1[12];  // (was 6) used by both cMsgs1 & cMsg-PCB now, sync from dev_host_interface.h
  unsigned char    bRMsgs1[6];
  unsigned char    bCRates2;
  unsigned char    bRRates2;
  unsigned char    bCMsgs2[4];
  unsigned char    bRMsgs2[4];

  unsigned char currentExtHybridNum;
  unsigned char atucRevisionNum;

  unsigned char  dsFastParityBytesPerSymbol;
  unsigned char  dsIntlvParityBytesPerSymbol;
  unsigned char  dsSymbolsPerCodeWord;
  unsigned int   dsInterleaverDepth;
  unsigned char  usFastParityBytesPerSymbol;
  unsigned char  usIntlvParityBytesPerSymbol;
  unsigned char  usSymbolsPerCodeWord;
  unsigned int   usInterleaverDepth;
  unsigned int   atmBertBitCountLow;
  unsigned int   atmBertBitCountHigh;
  unsigned int   atmBertBitErrorCountLow;
  unsigned int   atmBertBitErrorCountHigh;
  unsigned int   lineLength;
  unsigned int   grossGain;
           int   trellis;
  //unsigned int   useBitField;
  unsigned int   LPR;
 
#if 1
  // added for TR69
  unsigned char  ghsATUCVendorId[8];  /* country code(2) + VID(4) + Vendor Specific(2) */
  unsigned char  ghsATURVendorId[8];  /* country code(2) + VID(4) + Vendor Specific(2) */
  T1413INFO      t1413ATUC;
  T1413INFO      t1413ATUR;
#endif

//UR8_MERGE_START CQ11709 Tim
  unsigned short dsHlogMT;    //Downstream Hlog measurement time
  unsigned short dsSNRMT;     //Downstream SNR measurement time
  unsigned short dsQLNMT;     //Downstream QLN measurement time
//UR8_MERGE_END CQ11709 Tim

    /* DSP_CRATES1 */
  unsigned char    bCRates1[4][30];
  unsigned char    bRRates1[4][11];

  /* DSP_SNR */
  short   rxSnrPerBin0[512];    /* Band plan */
  short   rxSnrPerBin1[512];    /* Band plan */
  short   rxSnrPerBin2[512];    /* Band plan */
  short   rxSnrPerBin3[512];    /* Band plan */

  /* Statistics */

  /* DEV_HOST_dspWrSharedTables_t */
  unsigned char  rBng[1024];    /* Band plan */
  unsigned char  cBng[128];     /* Band plan */
  unsigned char  BitAllocTblDstrm[512];    /* Band plan */
  unsigned char  BitAllocTblUstrm[128];    /* Band plan */
  signed char  marginTblDstrm[512];      /* Band plan */

  int   dsNoise[512];  /* Band plan */

  /* ADSL1 only */
  unsigned char  rMsgRa[12];
  unsigned char  cMsgRa[6];

  unsigned char  usBitSwap;
  unsigned char  dsBitSwap;
  signed char    uncancelledecho;
  unsigned char  smartmargin;

#ifdef  DHALAPP_LINUX
  unsigned int   CurDslState;
  unsigned int   retrain_count;
#endif
#endif /* #ifdef __KERNEL__ */
//end bk

}TIOIDINFO,*PTIOIDINFO;


typedef struct dsl_stat_event {
	struct _avm_event_header	header;
	TIOIDINFO info;
} DSL_STAT_EVENT;

typedef struct dsl_small_stat_event {
    struct _avm_event_header  header;
    unsigned char   bState;         /* addr->bDSPATURState */
    unsigned char   vdsl_available; /* is_vdsl=0 => ADSL; is_vdsl=1 => VDSL */
    unsigned short  reserved;
    unsigned int    USConRate;      /* US Conection Rates  */
    unsigned int    DSConRate;      /* DS Connection Rates */

/*! boto: Extentions for different structure versions */
#if ( AVM_SMALL_STAT_EVENT_VERSION > 0 )

    /*! This field contains flags indicating a combination of reasons for a
        loss of DSL syncronization.
        The flags are defined as follows (see dsl driver's dev_host_interface.h for the enum definition).
        
            REASON_LOS              = 0x00000001, // Loss of Sync
            REASON_DYING_GASP       = 0x00000002, // Dyning Gasp
            REASON_USCRCERR         = 0x00000004, // Too much upstream CRC errors
            REASON_MARGIN_DROP      = 0x00000008, // Sudden margin drop
            AVM_REASON_PM_L2_TO_L0  = 0x04000000, // Status: Change from L2 to L0 mode
            AVM_REASON_PM_L0_TO_L2  = 0x08000000, // Status: Change from L0 to L2 mode
            AVM_REASON_NO_CABLE     = 0x10000000, // DSL cable was unplugged
            AVM_REASON_HOST_RETRAIN = 0x20000000, // A forced retrain triggered by host
            AVM_REASON_DSCRCERR     = 0x40000000  // Too much downstream CRC errors
    */
    unsigned int    ReasonFlags;
    
#endif /* ( AVM_SMALL_STAT_EVENT_VERSION > 0 ) */
// end boto

} DSL_SMALL_STAT_EVENT;


/*struct for ioctl ATM_AVM_GET_STAT*/
struct avmAtmIoctlInfo {
	unsigned int dsl_us_crc;
	unsigned int dsl_ds_crc;
	unsigned int dsl_us_hec;
	unsigned int dsl_ds_hec;
	unsigned int atm_us_good;
	unsigned int atm_ds_good;
	unsigned int atm_us_idle;
	unsigned int atm_ds_idle;
	unsigned int atm_ds_bad_hec;
	unsigned int atm_ds_ovflw;		/*overflow between DSP and SAR*/
	unsigned int sar_CrcErrors;
	unsigned int sar_LenErrors;
	unsigned int sar_DmaLenErrors; /*Incremented when the port DMA's more data than expected*/
	unsigned int sar_AbortErrors;
	unsigned int sar_StarvErrors;
	unsigned int sar_TxMisQCnt;    /*Incremented when host queues a packet for transmission as the port finishes transmitting the previous last packet in the queue*/
	unsigned int sar_RxMisQCnt;	/*Incremented when host queues adds buffers to a queue as the port finished the reception of the previous last packet in the queue*/
	unsigned int sar_TxEOQCnt; /*Number of times the port has reached the end of the transmit queue*/
	unsigned int sar_RxEOQCnt; /*Number of times the port has reached the end of the receive queue*/
	unsigned int sar_rcbDropped; /*when NeedsCount >= MAX_NEEDS*/
};


				/*************************/
				/* VINAX SPECIFIC IOCTLs */
				/*************************/
				
/** magic number */
#define VINAX_IOC_MAGIC 'Q'
				
/** This service requests the data about line state and frequences graph. 
 * Ioctl is sent by the ctlmgr.

   \param TIOIDINFO* The parameter points to a
          \ref TIOIDINFO structure (see avm_vinax_event.h)

   \return 
      - 0 if successful, otherwise 
      - -1

   \remarks This call is only valid after a previous init call.
            Unknown or still not available Cfg values are returned with 
            default settings.

   \code
	TIOIDINFO vdsl_data;
    memset(&vdsl_data, 0x00, sizeof(TIOIDINFO));
     ret = ioctl(fd, FIO_VINAX_AVM_STATS, &vdsl_data)
   \endcode
   \ingroup VINAX_COMMON              */

/*DIESER IOCTL IST TEIL DER SCHNITTSTELLE ZUM CTLMGR UND WIRD DESHALB IN 
 * avm_vinax_event.h DEFINIERT!*/
 
 /*SIEHE drv_vinax_interface.h ZUM VERGLEICH*/

#define FIO_VINAX_AVM_STATS             _IO(VINAX_IOC_MAGIC, 49)


#endif // __DSL_AVM_EVENT__

