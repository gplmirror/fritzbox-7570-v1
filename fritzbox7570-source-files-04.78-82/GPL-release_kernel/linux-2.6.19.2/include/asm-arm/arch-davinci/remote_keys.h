#ifndef _asm_arch_remote_keys_h_
#define _asm_arch_remote_keys_h_

enum _remote_keys {

    remote_key_power = 0x000c,

    remote_key_1 = 0x0001,
    remote_key_2 = 0x0002,
    remote_key_3 = 0x0003,

    remote_key_4 = 0x0004,
    remote_key_5 = 0x0005,
    remote_key_6 = 0x0006,

    remote_key_7 = 0x0007,
    remote_key_8 = 0x0008,
    remote_key_9 = 0x0009,

    remote_key_CROSS = 0x001d,
    remote_key_0 = 0x0000,
    remote_key_HASH = 0x001c,

    remote_key_BACK = 0x000b,
    remote_key_HOME = 0x0086,

    remote_key_UP = 0x0058,

    remote_key_LEFT = 0x005a,
    remote_key_OK = 0x005c,
    remote_key_RIGHT = 0x005b,

    remote_key_DOWN = 0x0059,

    remote_key_GRAY = 0x0075,
    remote_key_PINK = 0x0076,

    remote_key_RED = 0x006d,
    remote_key_GREEN = 0x006e,
    remote_key_YELLOW = 0x006f,
    remote_key_BLUE = 0x0070,

    remote_key_VOL_UP = 0x0010,
    remote_key_CHN_UP = 0x0020,

    remote_key_MUTE = 0x000d,

    remote_key_VOL_DOWN = 0x0011,
    remote_key_CHN_DOWN = 0x0021,

    remote_key_MENU = 0x0054,
    remote_key_STOP = 0x0031,
    remote_key_PAUSE = 0x0030,

    remote_key_REW = 0x0029,
    remote_key_PLAY = 0x002c,
    remote_key_FWD = 0x0028
};

#endif /*--- #ifndef _asm_arch_remote_keys_h_ ---*/
