#ifndef _system_module_h_
#define _system_module_h_

#if 0
#define PINMUX0_REG             *(volatile unsigned int *)(SYSTEM_BASE + 0x00)
#define PINMUX4_REG             *(volatile unsigned int *)(SYSTEM_BASE + 0x04) 
#define DSPBOOTADDR_REG         *(volatile unsigned int *)(SYSTEM_BASE + 0x08) 
#define SUSPSRC_REG             *(volatile unsigned int *)(SYSTEM_BASE + 0x0C) 
#define INTGEN_REG              *(volatile unsigned int *)(SYSTEM_BASE + 0x10) 
#define BOOTCFG_REG             *(volatile unsigned int *)(SYSTEM_BASE + 0x14) 

#define DEVICE_ID_REG           *(volatile unsigned int *)(SYSTEM_BASE + 0x28) 

#define USBPHY_CTL_REG          *(volatile unsigned int *)(SYSTEM_BASE + 0x34) 
#define CHIP_SHRTSW_REG         *(volatile unsigned int *)(SYSTEM_BASE + 0x38) 
#define MSTPRI0_REG             *(volatile unsigned int *)(SYSTEM_BASE + 0x3C) 
#define MSTPRI1_REG             *(volatile unsigned int *)(SYSTEM_BASE + 0x40) 
#define VPSS_CLKCTL_REG         *(volatile unsigned int *)(SYSTEM_BASE + 0x44) 
#define VDD3P3V_PWDN_REG        *(volatile unsigned int *)(SYSTEM_BASE + 0x48) 
#define DRRVTPER_REG            *(volatile unsigned int *)(SYSTEM_BASE + 0x4C) 
#endif

union _pinmux0_ {
    volatile unsigned int Reg;
    struct __pinmux0 {
        unsigned int aeaw           : 5;
        unsigned int reserved59     : 5;
        unsigned int aecs4          : 1;
        unsigned int aecs5          : 1;
        unsigned int vlynqwd        : 2;
        unsigned int vlscren        : 1;
        unsigned int vlynqen        : 1;
        unsigned int hdiren         : 1;
        unsigned int ataen          : 1;
        unsigned int reserved1821   : 4;
        unsigned int rgb666         : 1;
        unsigned int rgb888         : 1;
        unsigned int loeen          : 1;
        unsigned int lflden         : 1;
        unsigned int cwen           : 1;
        unsigned int cflden         : 1;
        unsigned int reserved2830   : 3;
        unsigned int emacen         : 1;
    } Bits;
};

union _pinmux1_ {
    volatile unsigned int Reg;
    struct __pinmux1 {
        unsigned int uart0en        : 1;
        unsigned int uart1en        : 1;
        unsigned int uart2en        : 1;
        unsigned int u2floen        : 1;
        unsigned int pwm0en         : 1;
        unsigned int pwm1en         : 1;
        unsigned int pwm2en         : 1;
        unsigned int i2cen          : 1;
        unsigned int spien          : 1;
        unsigned int reserved9      : 1;
        unsigned int aspen          : 1;
        unsigned int reserved1115   : 5;
        unsigned int clk0en         : 1;
        unsigned int clk1en         : 1;
        unsigned int timinen        : 1;
        unsigned int reserved1931   : 13;
    } Bits;
};

struct system_regs {
    union _pinmux0_         pinmux0;
    union _pinmux1_         pinmux1;
    volatile unsigned int   dspbootaddr;
    volatile unsigned int   suspsrc;
    volatile unsigned int   intgen;
    volatile unsigned int   bootcfg;
    volatile unsigned int   reserved1[4];
    volatile unsigned int   device_id;
    volatile unsigned int   reserved2[2];
    volatile unsigned int   usbphy_ctl;
    volatile unsigned int   chp_shrtsw;
    volatile unsigned int   mstpri0;
    volatile unsigned int   mstpri1;
    volatile unsigned int   vpss_clkctl;
    volatile unsigned int   vdd3p3v_pwdn;
    volatile unsigned int   drrvtper;
};
        
    

#endif
