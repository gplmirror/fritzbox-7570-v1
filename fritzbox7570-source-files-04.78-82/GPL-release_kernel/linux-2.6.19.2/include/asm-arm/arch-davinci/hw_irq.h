#ifndef _arm_arch_hw_irq_h_
#define _arm_arch_hw_irq_h_


struct irq_controller {
    /* 0x00 */ volatile unsigned int FIQ0; /* Fast Irq Status Register 0 */
    /* 0x04 */ volatile unsigned int FIQ1; /* Fast Irq Status Register 1 */
    /* 0x08 */ volatile unsigned int IRQ0; /* Irq Status Register 0 */
    /* 0x0C */ volatile unsigned int IRQ1; /* Irq Status Register 1 */
    /* 0x10 */ volatile unsigned int FIQENTRY;
    /* 0x14 */ volatile unsigned int IRQENTRY;
    /* 0x18 */ volatile unsigned int EINT0;
    /* 0x1C */ volatile unsigned int EINT1;
    /* 0x20 */ volatile unsigned int INTCTL;
    /* 0x24 */ volatile unsigned int EABASE;
    /* 0x28 */ volatile unsigned int reserved0;
    /* 0x2C */ volatile unsigned int reserved1;
    /* 0x30 */ volatile unsigned int INTPRI0;
    /* 0x34 */ volatile unsigned int INTPRI1;
    /* 0x38 */ volatile unsigned int INTPRI2;
    /* 0x3C */ volatile unsigned int INTPRI3;
    /* 0x40 */ volatile unsigned int INTPRI4;
    /* 0x44 */ volatile unsigned int INTPRI5;
    /* 0x48 */ volatile unsigned int INTPRI6;
    /* 0x4C */ volatile unsigned int INTPRI7;
};







#endif /*--- #ifndef _arm_arch_hw_irq_h_ ---*/
