/*------------------------------------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------------------------------------*/
#ifndef _hw_clock_h_
#define _hw_clock_h_

#define PLLCTL_OFFSET       0x100
#define PLLM_OFFSET         0x110
#define PLLDIV1_OFFSET      0x118
#define PLLDIV2_OFFSET      0x11C
#define PLLDIV3_OFFSET      0x120
#define PLLDIV4_OFFSET      0x160
#define PLLDIV5_OFFSET      0x164
#define POSTDIV_OFFSET      0x128
#define BPDIV_OFFSET        0x12C
#define PLLCMD_OFFSET       0x138
#define PLLSTAT_OFFSET      0x13C

#define PLL1_BASE           DAVINCI_PLL_CNTRL0_BASE            
#define PLL2_BASE           DAVINCI_PLL_CNTRL1_BASE            

#define PLL1CTL_REG         (*(volatile unsigned int *)(PLL1_BASE + PLLCTL_OFFSET))
#define PLL1M_REG           (*(volatile unsigned int *)(PLL1_BASE + PLLM_OFFSET))
#define PLL1DIV1_REG        (*(volatile unsigned int *)(PLL1_BASE + PLLDIV1_OFFSET))
#define PLL1DIV2_REG        (*(volatile unsigned int *)(PLL1_BASE + PLLDIV2_OFFSET))
#define PLL1DIV3_REG        (*(volatile unsigned int *)(PLL1_BASE + PLLDIV3_OFFSET))
#define PLL1DIV4_REG        (*(volatile unsigned int *)(PLL1_BASE + PLLDIV4_OFFSET))
#define PLL1DIV5_REG        (*(volatile unsigned int *)(PLL1_BASE + PLLDIV5_OFFSET))
#define PLL1POSTDIV_REG     (*(volatile unsigned int *)(PLL1_BASE + POSTDIV_OFFSET))
#define PLL1BPDIV_REG       (*(volatile unsigned int *)(PLL1_BASE + BPDIV_OFFSET))
#define PLL1CMD_REG         (*(volatile unsigned int *)(PLL1_BASE + PLLCMD_OFFSET))
#define PLL1STAT_REG        (*(volatile unsigned int *)(PLL1_BASE + PLLSTAT_OFFSET))

#define PLL2CTL_REG         (*(volatile unsigned int *)(PLL2_BASE + PLLCTL_OFFSET))
#define PLL2M_REG           (*(volatile unsigned int *)(PLL2_BASE + PLLM_OFFSET))
#define PLL2DIV1_REG        (*(volatile unsigned int *)(PLL2_BASE + PLLDIV1_OFFSET))
#define PLL2DIV2_REG        (*(volatile unsigned int *)(PLL2_BASE + PLLDIV2_OFFSET))
#define PLL2DIV3_REG        (*(volatile unsigned int *)(PLL2_BASE + PLLDIV3_OFFSET))
#define PLL2DIV4_REG        (*(volatile unsigned int *)(PLL2_BASE + PLLDIV4_OFFSET))
#define PLL2DIV5_REG        (*(volatile unsigned int *)(PLL2_BASE + PLLDIV5_OFFSET))
#define PLL2POSTDIV_REG     (*(volatile unsigned int *)(PLL2_BASE + POSTDIV_OFFSET))
#define PLL2BPDIV_REG       (*(volatile unsigned int *)(PLL2_BASE + BPDIV_OFFSET))
#define PLL2CMD_REG         (*(volatile unsigned int *)(PLL2_BASE + PLLCMD_OFFSET))
#define PLL2STAT_REG        (*(volatile unsigned int *)(PLL2_BASE + PLLSTAT_OFFSET))

/*--- PLLCTL ---*/
#define PLLEN       (1 << 0)
#define PLLPWRDN    (1 << 1)
#define PLLRST      (1 << 3)
#define PLLDIS      (1 << 4)
#define PLLENSRC    (1 << 5)
#define CLKMODE     (1 << 8)

union _pll_ctl_ {
    volatile unsigned int Reg;
    volatile struct __pll_ctl__ {
        unsigned int pllen       : 1;
        unsigned int pllpwrdn    : 1;
        unsigned int reserved2   : 1;
        unsigned int pllrst      : 1;
        unsigned int plldis      : 1;
        unsigned int pllensrc    : 1;
        unsigned int reserved67  : 2;
        unsigned int clkmode     : 1;
        unsigned int reserved931 : 22;
    } Bits;
};

union _pll_div_ {
    volatile unsigned int Reg;
    volatile struct __pll_div__ {
        unsigned int ratio        : 5;
        unsigned int reserved514  : 9;
        unsigned int diven        : 1;
        unsigned int reserved1431 : 17;
    } Bits;
};

union _pll_cmd_ {
    volatile unsigned int Reg;
    volatile struct __pll_cmd__ {
        unsigned int goset        : 1;
        unsigned int reserved     : 31;
    } Bits;
};

union _pll_stat_ {
    volatile unsigned int Reg;
    volatile struct __pll_stat__ {
        unsigned int gostat       : 1;
        unsigned int reserved1    : 1;
        unsigned int stable       : 1;
        unsigned int reserved331  : 29;
    } Bits;
};


#endif /*--- #ifndef _hw_clock_h_ ---*/
