/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _HW_TIMER_H_
#define _HW_TIMER_H_

union emu_mgt {
    volatile unsigned int Register;
    struct __emu_mgt {
        volatile unsigned int free       :1;
        volatile unsigned int soft       :1;
        volatile unsigned int reserved   :30; 
    } Bits;
};

union timer_tcr {
    volatile unsigned int Register;
    struct __timer_tcr {
        volatile unsigned int reserved1  :6;
        volatile unsigned int enable12   :2;
        volatile unsigned int clksrc     :1;
        volatile unsigned int reserved2  :13; 
        volatile unsigned int enable34   :2; 
        volatile unsigned int reserved3  :8; 
    } Bits;
};

union timer_tgcr {
    volatile unsigned int Register;
    struct __timer_tgcr {
        volatile unsigned int unreset12  :1;
        volatile unsigned int unreset34  :1;
        volatile unsigned int timmode    :2;
        volatile unsigned int reserved1  :4; 
        volatile unsigned int psc34      :4; 
        volatile unsigned int tddr34     :4; 
        volatile unsigned int reserved2  :16; 
    } Bits;
};

union timer_wdtcr {
    volatile unsigned int Register;
    struct __timer_wdtcr {
        volatile unsigned int reserved       :12;
        volatile unsigned int reservedzero   :2;
        volatile unsigned int wdt_enable     :1;
        volatile unsigned int wdt_flag       :1; 
        volatile unsigned int wdt_key        :16; 
    } Bits;
};

struct timer {
    volatile unsigned int   pid12;
    union emu_mgt           emumgt_clkspd;
    volatile unsigned int   reserved1;
    volatile unsigned int   reserved2;
    volatile unsigned int   tim12;
    volatile unsigned int   tim34;
    volatile unsigned int   prd12;
    volatile unsigned int   prd34;
    union timer_tcr         tcr;
    union timer_tgcr        tgcr;
    union timer_wdtcr       wdtcr;
};


#endif /*--- #ifndef _HW_TIMER_H_ ---*/

