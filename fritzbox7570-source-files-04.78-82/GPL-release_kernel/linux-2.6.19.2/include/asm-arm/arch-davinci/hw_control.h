#ifndef _asm_arch_hw_control_h_
#define _asm_arch_hw_control_h_

#ifdef CONFIG_DAVINCI_GPIO_SHIFT_REGISTER
enum _hw_control_bits {
    ephy_reset = 0x01,
    enable_5Volt = 0x02,
    ephy_power_down = 0x04,
    scart_sig_on = 0x08,
    scart_signal_rgb = 0x10,
    scart_signal_16_9 = 0x20,
    audio_reset = 0x40,
    non_wlan_reset = 0x80
};

void gpio_shift_register_load(unsigned int mask, unsigned int values);
#define gpio_control_hw(was, wie)   gpio_shift_register_load((unsigned int)(was) << 16, (wie) ? (unsigned int)(was) << 16 : 0)
#else/*--- #ifdef CONFIG_DAVINCI_GPIO_SHIFT_REGISTER ---*/
#define gpio_control_hw(was, wie)   (0)
#endif /*--- #else ---*//*--- #ifdef CONFIG_DAVINCI_GPIO_SHIFT_REGISTER ---*/
#endif /*--- #ifndef _asm_arch_hw_control_h_ ---*/
