/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifndef _ikan_clk
#define _ikan_clk_h

#include <asm/mach_avm.h>
#include <asm-mips/mach-ikan_mips/vx180.h>

unsigned int ikan_get_clock(enum _avm_clock_id clock_id);
#endif /*--- #ifndef _ikan_clk_h ---*/
