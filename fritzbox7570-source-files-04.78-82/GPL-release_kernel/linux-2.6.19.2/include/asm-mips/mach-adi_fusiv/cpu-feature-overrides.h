/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2003 Ralf Baechle
 */
#ifndef __ASM_MACH_GENERIC_CPU_FEATURE_OVERRIDES_H
#define __ASM_MACH_GENERIC_CPU_FEATURE_OVERRIDES_H

#define cpu_has_tlb		1
#define cpu_has_3k_cache		1
#define cpu_has_4kex	0
#define cpu_has_4ktlb	0
#define cpu_has_fpu		0
/* #define cpu_has_32fpr	? */
#define cpu_has_counter		0
/* #define cpu_has_watch	? */
#define cpu_has_divec		0
#define cpu_has_vce		0
/* #define cpu_has_cache_cdex_p	? */
/* #define cpu_has_cache_cdex_s	? */
/* #define cpu_has_prefetch	? */
#define cpu_has_mcheck		0
/* #define cpu_has_ejtag	? */
#define cpu_has_llsc		0
/* #define cpu_has_vtag_icache	? */
/* #define cpu_has_dc_aliases	? */
/* #define cpu_has_ic_fills_f_dc ? */
#define cpu_has_nofpuex		1
/* #define cpu_has_64bits	? */
/* #define cpu_has_64bit_zero_reg ? */
/* #define cpu_has_subset_pcaches ? */


#endif /* __ASM_MACH_GENERIC_CPU_FEATURE_OVERRIDES_H */
