//
// Debugger Test program
//
// PDSP Example
//
// pdsp.h
//
#define PTR_RAM_EXT         0x00000000
#define PTR_RAM_INT1        0x00200000
#define PTR_RAM_INT2        0x00210000
#define PTR_RAM_INT3        0x00220000

#define SRCDATA_ADDR        0x00000000
#define SRCDATA_SIZE        32
#define DSTDATA_ADDR1       0x00000020
#define DSTDATA_ADDR2       0x00000040
#define DSTDATA_ADDR3       0x00000060
#define DSTDATA_ADDR4       0x00000080

