//
// Debugger Test program
//
// LMDB Example
// 
// lmdb_subs.p
//

//
// Benchmark Subroutines
//

//
// Subroutine:
//      ADD_BYTE_VALUE
//
// Called with 
//      REG8_BITNUMBER = Index
// 
ADD_BYTE_VALUE:
        LSL     REG32_RETURN_VALUE, REG32_RETURN_VALUE, #8
        MOV     REG32_RETURN_VALUE.b0, REG8_BITNUMBER
        .ret





