//=========================================================================
// PDSP Example Code
//-------------------------------------------------------------------------
//
// test1.p - Generic Structure Test
//
//
//
// Copyright 2006 by Texas Instruments Inc.
//=========================================================================

        .entrypoint Start

        
        // Should Fail (not 16 bits)
//        .setcallreg r30
        
        // Should Fail (not r0 - r30)
//        .setcallreg r31.w0
        
        // Should Fail (not 16 bits)
//        .setcallreg r30.b0

        // Should Work
        .setcallreg r14.w2

        // Should Work (fail if redef)
//        .setcallreg r30.w2

        // Should Work (fail if redef)
//        .setcallreg r30.w1

        .origin     0

//
// Test Starts
//

Start:
        call    fun1
        call    fun2

        // Should Fail (after code)
//        .setcallreg r29.w1
        
end:    jmp     end

fun1:   
//        .ret
        ret

fun2:   
        ret

