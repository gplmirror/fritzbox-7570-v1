//
// macro.p
//
// This program tests the assembler macro.
//
// We will use structures and register scoping to validate proper 
// macro expansion
//
//

// Define a test structure
.struct struct_test
    .u32    x
    .u32    y
    .u32    sum
.ends


// Define 2 scopes using identical structure names, but with 
// different register ranges
.enter scope1
.assign struct_test, r3, r5, test
.leave scope1

.enter scope2
.assign struct_test, r6, r8, test
.leave scope2


//
// Define some macros to test
//

//
// mov32 : Move a 32bit value to a register
//
// Usage:
//     mov32   dst, src    
//
// Sets dst = src. Src must be a 32 bit immediate value.
//
.macro  mov32               
.mparam dst, src
        mov     dst.w0, src & 0xFFFF
        mov     dst.w2, src >> 16
.endm       


//
// qbir : Quick branch in range
//
// Usage:
//     qbir    label, test, low, high  
//
// Jumps to label if (low <= test <= high).
// Test must be a register. Low and High can be reisters
// or a 8 bit immediate value.
//
.macro  qbir
.mparam label, test, low, high
        qbgt    out_of_range, test, low
        qbge    label, test, high
out_of_range:
.endm


//
// mult13 : Multiply by 13
//
// Usage:
//     mult13  dst, src               
//
// Sets dst = src * 13. Src must be a resiter field and can
// not overlap dst.
//
.macro  mult13      
.mparam dst
.mparam src
        lsl     dst, src, 1     // dst = src * 2
        add     dst, dst, src   // dst = src * 3
        lsl     dst, dst, 2     // dst = src * 12
        add     dst, dst, src   // dst = src * 13
.endm        


//
// addxy : Add the x and components
//
// Usage:
//     addxy               
//
// Sets test.sum = test.x + test.y
//
.macro  addxy
        add     test.sum, test.x, test.y
.endm


        .entrypoint Start
        .origin 0
    
Start:  
        mov     r0, 0x1234
        mov32   r1, 0x1234
        mov32   r2, 0x12345678

//        mult13  r1                          // Should be error
//        mult13  r1, r2, r3                  // Should be error

.using scope1
        mov32   test.x, 0x10002             // r3 = 0x10002
        mult13  test.y, test.x              // r4 = 0xD001A
        addxy                               // r5 = 0xE001C
.leave scope1        
        
.using scope2
        mov32   test.x, 2                   // r6 = 2
        mult13  test.y, test.x              // r7 = 26
        addxy                               // r8 = 28
.leave scope2

.using scope1
        qbir    error, test.sum, 1, 100
.leave scope1        

.using scope2
        qbir    noerror, test.sum, 1, 100
        jmp     error
noerror:
.leave scope2

        //
        // Check our work
        //

        qbne    error, r0, r1

        mov     r0.w0, 0x1234
        qbne    error, r2.w2, r0.w0
        mov     r0.w0, 0x5678
        qbne    error, r2.w0, r0.w0

        mov     r0.w0, 0x1
        qbne    error, r3.w2, r0.w0
        mov     r0.w0, 0x2
        qbne    error, r3.w0, r0.w0

        mov     r0.w0, 0xd
        qbne    error, r4.w2, r0.w0
        mov     r0.w0, 0x1a
        qbne    error, r4.w0, r0.w0

        mov     r0.w0, 0xe
        qbne    error, r5.w2, r0.w0
        mov     r0.w0, 0x1c
        qbne    error, r5.w0, r0.w0

        qbne    error, r6, 2
        qbne    error, r7, 26
        qbne    error, r8, 28
    
done:   halt       
        
error:  halt        
    
        
        





    
    
    