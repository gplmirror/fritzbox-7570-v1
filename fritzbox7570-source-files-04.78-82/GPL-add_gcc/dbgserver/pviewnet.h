/*===========================================================================
// pView::DebugEngine - Debugger/Simulator Engine Component
//---------------------------------------------------------------------------
//
// File     : pViewNet.h
// Author   : Michael Denio
//
// Description:
//     pView Network Communications Specification
//
// Author/Comments:
//     Michael Denio (miked@ti.com)
//
//---------------------------------------------------------------------------
// Copyright 2006 by Texas Instrument Incorporated
============================================================================*/

//
// Message Format
//
// All pView messages are 40 bytes in length. They consist of the
// following (in little endian format):
//
// UINT32   Version;
// UINT32   NetCommand;
// UINT32   Aux[8]
//
// Some commands will specify "more data size" in one or more of
// the Aux fields. When this occurs, the raw additional data is
// sent immediately following the command record.
//


#define PNET_VERSION_NUMBER         0x00000005

#define PNET_VERSION_VARMASK        0x0000FFFF
#define PNET_VERSION_FIXED          0x14920000
#define PNET_VERSION_FIXEDMASK      0xFFFF0000
#define PNET_VERSION                (PNET_VERSION_FIXED|(PNET_VERSION_NUMBER&PNET_VERSION_VARMASK))
#define IS_PNET(x)                  ((x&PNET_VERSION_FIXEDMASK)==PNET_VERSION_FIXED)

#define PNET_PORT                   1900

//
// PNET_SERVER_READY
//
// Sent by a PVIEW server when it is ready to accept debugger
// commands. The message is typically sent immediately upon
// connection (when free) or when the client sends a
// PNET_CLIENT_FORCECONNECT message.
//
// Version    = PNET_VERSION
// NetCommand = PNET_SERVER_READY
//
#define PNET_SERVER_READY           1000

//
// PNET_SERVER_BUSY
//
// Sent by a PVIEW server when it is busy with another client.
// The message is typically sent immediately upon connection.
// Note that the client must immediately respond with a
// PNET_CLIENT_FORCECONNECT message, or the connection will
// be terminated by the sever.
//
// Version    = PNET_VERSION
// NetCommand = PNET_SERVER_BUSY
//
#define PNET_SERVER_BUSY            1001

//
// PNET_SERVER_CMDDONE
//
// Sent by a PVIEW server when it completes execution
// of a command.
//
// Version    = PNET_VERSION
// NetCommand = PNET_SERVER_CMDDONE
// Aux[0]     = Return Code
// Aux[1]     = Original Command
// Aux[2..7]  = Return Data (command specific)
//
#define PNET_SERVER_CMDDONE         1002
#define PNET_SERVER_CMDDONE_SUCCESS         0 // Succcess executing command
#define PNET_SERVER_CMDDONE_ERROR           1 // Error executing command

//
// PNET_SERVER_ERROR
//
// Sent by a PVIEW server when it encounters an error in the
// communications stream. This is indicative of a bad or out
// of synch connection.
//
// Version    = PNET_VERSION
// NetCommand = PNET_SERVER_ERROR
// Aux[0]     = Error Code
// Aux[1]     = Offending Data (command, version, or parameter)
//
#define PNET_SERVER_ERROR           1003
#define PNET_SERVER_ERROR_BADSYNC           1 // Unexpeced VERSION code
#define PNET_SERVER_ERROR_BADVERSION        2 // VERSION recognized but not supported
#define PNET_SERVER_ERROR_UNKNOWNCOMMAND    3 // Good VERSION code, but bad command
#define PNET_SERVER_BAD_PARAMETER           4 // Bad command parameter





//
// PNET_CLIENT_FORCECONNECT
//
// Sent by a PVIEW client when the the server reports busy and
// the client wishes to take control of the server.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_FORCECONNECT
//
// Server Response: PNET_SERVER_READY
//
#define PNET_CLIENT_FORCECONNECT    1

//
// PNET_CLIENT_QUERY
//
// Sent by a PVIEW client when the client becomes confused
// as to the state of the server.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_QUERY
//
// Server Response: PNET_SERVER_READY or PNET_SERVER_BUSY
//
#define PNET_CLIENT_QUERY           2

//
// PNET_CLIENT_ENUMERATE
//
// Sent by a PVIEW client to tell the server to enumerate
// its known devices.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_ENUMERATE
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = Number of CPU devices
//      Aux[3] = Number of Memory devices
//
#define PNET_CLIENT_ENUMERATE       3

//
// PNET_CLIENT_GETCPUINFO
//
// Sent by a PVIEW client to request information on the
// specified CPU.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_GETCPUINFO
// Aux[0]     = CPU Index
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = CPU Big Endian Flag
//      Aux[3] = Number of CPU register banks
//      Aux[4] = Base Address of CPU IRAM (usually 0)
//      Aux[5] = Length of CPU IRAM
//      Aux[6] = Length of Tab Text (w/o NULL term)
//      Aux[7] = Length of Tool Tip Text (w/o NULL term)
//      Record followed immediately by Tab Text and Tool Tip Text
//
#define PNET_CLIENT_GETCPUINFO      4

//
// PNET_CLIENT_GETMEMINFO
//
// Sent by a PVIEW client to request information on the
// specified memory unit.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_GETMEMINFO
// Aux[0]     = Memory Range Index
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = Base Address of RAM
//      Aux[3] = Length of RAM
//      Aux[4] = Length of Tab Text (w/o NULL term)
//      Aux[5] = Length of Tool Tip Text (w/o NULL term)
//      Record followed immediately by Tab Text and Tool Tip Text
//
#define PNET_CLIENT_GETMEMINFO      5

//
// PNET_CLIENT_GETCPUSTATUS
//
// Sent by a PVIEW client to request the status of the
// specified CPU.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_GETCPUSTATUS
// Aux[0]     = CPU Index
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = CPU Instruction Pointer
//      Aux[3] = RunMode
#define PNET_CPU_RUNMODE_HALT           0
#define PNET_CPU_RUNMODE_SINGLESTEP     1
#define PNET_CPU_RUNMODE_RUN            2
#define PNET_CPU_RUNMODE_RUNFREE        3
//      Aux[4] = Status
#define PNET_CPU_STATUS_READY           0
#define PNET_CPU_STATUS_BUSY            1
//      Aux[5] = Cycle Count (when available)
//      Aux[6] = Cycle Stall Count (when available)
//
#define PNET_CLIENT_GETCPUSTATUS    6

//
// PNET_CLIENT_CLEARCPUSTATUS
//
// Sent by a PVIEW client to tell the server to clear
// the status of the specified CPU, being the cycle
// count and the cycle stall count.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_CLEARCPUSTATUS
// Aux[0]     = CPU Index
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_CLEARCPUSTATUS  7

//
// PNET_CLIENT_GETCPUREGISTER
//
// Sent by a PVIEW client to tell the server to get
// the specified CPU register.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_GETCPUREGISTER
// Aux[0]     = CPU Index
// Aux[1]     = Register Bank Index
// Aux[2]     = Register Index
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = Register Value
//
#define PNET_CLIENT_GETCPUREGISTER  8

//
// PNET_CLIENT_SETCPUREGISTER
//
// Sent by a PVIEW client to tell the server to set
// the specified CPU register.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_SETCPUREGISTER
// Aux[0]     = CPU Index
// Aux[1]     = Register Bank Index
// Aux[2]     = Register Index
// Aux[3]     = Register Value
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_SETCPUREGISTER  9

//
// PNET_CLIENT_GETCPUREGISTERS
//
// Sent by a PVIEW client to tell the server to get
// the specified CPU registers.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_GETCPUREGISTERS
// Aux[0]     = CPU Index
// Aux[1]     = Register Bank Index
// Aux[2]     = Base Register Index
// Aux[3]     = Number of Registers
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = Number of registers
// Data of length NumRegs*4 follows immediately
//
#define PNET_CLIENT_GETCPUREGISTERS 10

//
// PNET_CLIENT_MEMREAD
//
// Sent by a PVIEW client to tell the server to read
// a memory block.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_MEMREAD
// Aux[0]     = Address
// Aux[1]     = Length
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = Length of bytes read
// Data of length "Length" follows immediately
//
#define PNET_CLIENT_MEMREAD         11

//
// PNET_CLIENT_MEMWRITE
//
// Sent by a PVIEW client to tell the server to write
// a memory block.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_MEMWRITE
// Aux[0]     = Address
// Aux[1]     = Length
// Data of length "Length" follows immediately
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_MEMWRITE        12

//
// PNET_CLIENT_IRAMREAD
//
// Sent by a PVIEW client to tell the server to read
// from CPU IRAM.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_IRAMREAD
// Aux[0]     = CPU Index
// Aux[1]     = Instruction Address
// Aux[2]     = Length (in units of intruction word)
//
// Server Response: PNET_SERVER_CMDDONE
//      Aux[2] = Length of bytes read
// Data of length "Length" follows immediately
//
#define PNET_CLIENT_IRAMREAD        13

//
// PNET_CLIENT_IRAMWRITE
//
// Sent by a PVIEW client to tell the server to write
// to CPU IRAM.
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_IRAMWRITE
// Aux[0]     = CPU Index
// Aux[1]     = Instruction Address
// Aux[2]     = Length (in units of intruction word)
// Data of length "Length" follows immediately
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_IRAMWRITE       14

//
// PNET_CLIENT_RESETCPU
//
// Sent by a PVIEW client to reset CPU
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_RESETCPU
// Aux[0]     = CPU Index
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_RESETCPU        15

//
// PNET_CLIENT_SETCPUIP
//
// Sent by a PVIEW client to set CPU IP
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_SETCPUIP
// Aux[0]     = CPU Index
// Aux[1]     = CPU IP
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_SETCPUIP        16

//
// PNET_CLIENT_SETCPURUNMODE
//
// Sent by a PVIEW client to set CPU run mode
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_SETCPURUNMODE
// Aux[0]     = CPU Index
// Aux[1]     = CPU Run Mode (see PNET_CLIENT_GETCPUSTATUS)
// Aux[2]     = "Run to" address for temp breakpoint (0xFFFFFFFF for none)
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_SETCPURUNMODE   17

//
// PNET_CLIENT_SETCPUBREAKPOINT
//
// Sent by a PVIEW client to set CPU break point
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_SETCPUBREAKPOINT
// Aux[0]     = CPU Index
// Aux[1]     = Breakpoint Address
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_SETCPUBREAKPOINT        18

//
// PNET_CLIENT_CLEARCPUBREAKPOINT
//
// Sent by a PVIEW client to clear CPU break point
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_CLEARCPUBREAKPOINT
// Aux[0]     = CPU Index
// Aux[1]     = Breakpoint Address
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_CLEARCPUBREAKPOINT      19

//
// PNET_CLIENT_CLEARALLCPUBREAKPOINTS
//
// Sent by a PVIEW client to clear all CPU break points
//
// Version    = PNET_VERSION
// NetCommand = PNET_CLIENT_CLEARCPUBREAKPOINT
// Aux[0]     = CPU Index
//
// Server Response: PNET_SERVER_CMDDONE
//
#define PNET_CLIENT_CLEARALLCPUBREAKPOINTS  20

