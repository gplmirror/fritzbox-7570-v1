//-------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------
// dbgserver.c
//
//
// Original Author: Michael Denio
//
// Copyright 2008 by Texas Instruments Inc.
//-------------------------------------------------------------------------------

#include "pdsp_config.h"
#include "dbgserver.h"
#include "pviewnet.h"

#ifdef _DOS_
#define fdClose(x) closesocket(x)
#define fdError()  WSAGetLastError()
#else
#define fdClose(x) close(x)
#define fdError()  errno
#endif

#define  TRANSACTION_SIMULATION 0

typedef struct _PDSP {
    uint Mode;
    uint Cycles;
    uint Stalls;
    uint RunFree;
    uint DebugBase;
    uint RegBase;
    uint IRAMBase;
} PDSP;

typedef struct _MEMORY {
    uint Base;
    uint Length;
    char *ShortName;
    char *LongName;
} MEMORY;

typedef struct _BREAKPT {
    uint InUse;
    uint Addr;
    uint OpCode;
} BREAKPT;


// Read a word from the supplied address. Return NULL on error
uint pnet_ReadWord( uint addr )
{
    uint size,data;
    size = MemRead( addr, &data, 4, 1 );
    if( size!=1 )
        return 0;
    else
        return data;
}

// Write the supplied word to the supplied address
void pnet_WriteWord( uint addr, uint data )
{
    uint size;
    size = MemWrite( addr, &data, 4, 1 );
}

#define GET_PDSP_CONTROL(x)     pnet_ReadWord(pdsp[x].DebugBase)
#define GET_PDSP_IP(x)          pnet_ReadWord(pdsp[x].DebugBase+4)
#define GET_PDSP_CYCLES(x)      pnet_ReadWord(pdsp[x].DebugBase+12)
#define GET_PDSP_STALLS(x)      pnet_ReadWord(pdsp[x].DebugBase+16)
#define SET_PDSP_CONTROL(x,y)   pnet_WriteWord(pdsp[x].DebugBase,y)
#define SET_PDSP_IP(x,y)        pnet_WriteWord(pdsp[x].DebugBase+4,y)
#define SET_PDSP_CYCLES(x,y)    pnet_WriteWord(pdsp[x].DebugBase+12,y)
#define SET_PDSP_STALLS(x,y)    pnet_WriteWord(pdsp[x].DebugBase+16,y)
#define READ_PDSP_IRAM(x,a)     pnet_ReadWord(pdsp[x].IRAMBase+((a)*4))
#define WRITE_PDSP_IRAM(x,a,d)  pnet_WriteWord(pdsp[x].IRAMBase+((a)*4),d)
#define READ_PDSP_REG(x,b,r)    pnet_ReadWord(pdsp[x].RegBase+((b)*128)+((r)*4));
#define WRITE_PDSP_REG(x,b,r,v) pnet_WriteWord(pdsp[x].RegBase+((b)*128)+((r)*4),v);

#define CONTROL_NRESET          0x0001
#define CONTROL_ENABLE          0x0002
#define CONTROL_CCOUNT          0x0008
#define CONTROL_SSTEP           0x0100
#define CONTROL_BIGENDIAN       0x4000
#define CONTROL_BUSY            0x8000

#define OPCODE_BREAK            0x2a000000

uint serverDebug = 0;
uint endianLocal = 0;
uint endianPDSP  = 0;

//
// PDSP/RAM Configuration
//
#define NUM_CPU     3
#define NUM_MEM     3
#define NUM_BP      16
#define CPU_IRAM    1024
#define MAX_MEMOP   8192

PDSP pdsp[NUM_CPU] = { { 0, 0, 0, 0, PDSP1_BASE, PDSP1_DEBUG, PDSP1_IRAM },
                       { 0, 0, 0, 0, PDSP2_BASE, PDSP2_DEBUG, PDSP2_IRAM },
                       { 0, 0, 0, 0, PDSP3_BASE, PDSP3_DEBUG, PDSP3_IRAM } };

/* Memory Regions we Report to Client */
MEMORY mem[NUM_MEM] = { { LRAM1_BASE, LRAM1_SIZE, "CPDSP", "CPDSP Local Memory" },
                        { LRAM2_BASE, LRAM2_SIZE, "MPDSP", "MPDSP Local Memory" },
                        { LRAM3_BASE, LRAM3_SIZE, "QPDSP", "QPDSP Local Memory" } };

BREAKPT bp[NUM_CPU][NUM_BP];
BREAKPT bpRunTo[NUM_CPU];

char *pdspNames[NUM_CPU] = { "CPDSP", "MPDSP", "QPDSP" };

//
// Server Communications
//
typedef struct _PNET_COMMAND {
    uint   Version;
    uint   NetCommand;
    uint   Aux[8];
} PNET_COMMAND;

PNET_COMMAND rxCmd;
PNET_COMMAND txCmd;


char scrap[33024];
uint progbuf[CPU_IRAM];

#define CPSF_TEMPHALT       1
#define CPSF_NOCOUNTS       2

uint CheckPDSPState( uint idx, uint flags )
{
    int i;
    uint oldctrl;
    uint stalls,cycles;

    // Get the run status
    oldctrl = GET_PDSP_CONTROL(idx) & 0xFFFF;

    if( oldctrl & CONTROL_ENABLE )
    {
        if( flags & CPSF_TEMPHALT )
        {
            // We are running and need to halt
            SET_PDSP_CONTROL(idx,oldctrl & ~CONTROL_ENABLE);
        }
        else
        {
            // We are running and don't need to halt
            return( oldctrl );
        }
    }

    // At this point, we know we are halted

    // Wait for idle
    for( i=0; i<1000; i++ )
        if( !(GET_PDSP_CONTROL(idx) & CONTROL_BUSY ) )
            break;
    if( i==1000 )
        printf("*** Ready Timeout Error ***\n");

    // If we were halted coming in (not temp halt) and in RunFree,
    // then re-install the breakpoints
    if( !(oldctrl & CONTROL_ENABLE) && pdsp[idx].RunFree )
    {
        pdsp[idx].RunFree=0;
        for( i=0; i<NUM_BP; i++ )
        {
            if( bp[idx][i].InUse )
                WRITE_PDSP_IRAM(idx,bp[idx][i].Addr,OPCODE_BREAK);
        }
    }

    // Update cycle counts
    if( !(flags & CPSF_NOCOUNTS) )
    {
        cycles = GET_PDSP_CYCLES(idx);
        stalls = GET_PDSP_STALLS(idx);
        SET_PDSP_CYCLES(idx,1);
        if( cycles && cycles != 0xFFFFFFFF )
        {
            // Apply the counts
            pdsp[idx].Cycles += cycles;
            pdsp[idx].Stalls += stalls;
        }
    }

    // Return "original" state
    return( oldctrl );
}


void SuspendBP( uint idx )
{
    int i;

    pdsp[idx].RunFree=1;
    for( i=0; i<NUM_BP; i++ )
    {
        if( bp[idx][i].InUse )
            WRITE_PDSP_IRAM(idx,bp[idx][i].Addr,bp[idx][i].OpCode);
    }
}

//
// Main Debugger Command Processing
//
void ProcessCommand( SOCKET s )
{
    char tmpstr1[128];
    char tmpstr2[128];
    uint idx,tmp1;
    int i;
    uint relAddr;

    txCmd.NetCommand = PNET_SERVER_CMDDONE;
    txCmd.Aux[0]     = PNET_SERVER_CMDDONE_SUCCESS;
    txCmd.Aux[1]     = rxCmd.NetCommand;

    switch( rxCmd.NetCommand )
    {
    case PNET_CLIENT_ENUMERATE:
        txCmd.Aux[2] = NUM_CPU;
        txCmd.Aux[3] = NUM_MEM;
        pnet_SendMessage( s );
        break;

    case PNET_CLIENT_GETCPUINFO:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        sprintf(tmpstr1,"%s",pdspNames[idx]);
        sprintf(tmpstr2,"%s Instruction Memory",pdspNames[idx]);

        if( GET_PDSP_CONTROL(idx) & CONTROL_BIGENDIAN )
            txCmd.Aux[2] = 1;           // Big Endian Flag
        else
            txCmd.Aux[2] = 0;           // Big Endian Flag

        txCmd.Aux[3] = 2;               // Number of CPU register banks
        txCmd.Aux[4] = 0;               // Base Address of CPU IRAM (usually 0)
        txCmd.Aux[5] = CPU_IRAM;        // Length of CPU IRAM
        txCmd.Aux[6] = (uint)strlen(tmpstr1); // Length of Tab Text (w/o NULL term)
        txCmd.Aux[7] = (uint)strlen(tmpstr2); // Length of Tool Tip Text (w/o NULL term)
        pnet_SendMessage( s );
        send( s, tmpstr1, txCmd.Aux[6], 0 );
        send( s, tmpstr2, txCmd.Aux[7], 0 );
        break;

    case PNET_CLIENT_GETMEMINFO:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_MEM )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        txCmd.Aux[2] = mem[idx].Base;               // Base Address of RAM
        txCmd.Aux[3] = mem[idx].Length;             // Length of RAM
        txCmd.Aux[4] = (uint)strlen(mem[idx].ShortName);  // Length of Tab Text (w/o NULL term)
        txCmd.Aux[5] = (uint)strlen(mem[idx].LongName);   // Length of Tool Tip Text (w/o NULL term)
        pnet_SendMessage( s );
        send( s, mem[idx].ShortName, txCmd.Aux[4], 0 );
        send( s, mem[idx].LongName, txCmd.Aux[5], 0 );
        break;

    case PNET_CLIENT_GETCPUSTATUS:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // If the IP is out of range, lock the PDSP at the end of RAM
        // This will actually reset the PDSP
        if( GET_PDSP_IP(idx) >= CPU_IRAM )
            SET_PDSP_CONTROL(idx,(CPU_IRAM-1) << 16);

        // Get the run status
        tmp1 = CheckPDSPState(idx,0);
        if( tmp1 & CONTROL_ENABLE )
        {
            // Here we say we are running
            if( pdsp[idx].Mode == PNET_CPU_RUNMODE_HALT )
                pdsp[idx].Mode = PNET_CPU_RUNMODE_RUN;
        }
        else
            pdsp[idx].Mode = PNET_CPU_RUNMODE_HALT;

        txCmd.Aux[2] = GET_PDSP_IP(idx);            // CPU Instruction Pointer
        txCmd.Aux[3] = pdsp[idx].Mode;              // RunMode
        if( tmp1 & CONTROL_BUSY )
            txCmd.Aux[4] = PNET_CPU_STATUS_BUSY;    // Status
        else
            txCmd.Aux[4] = PNET_CPU_STATUS_READY;   // Status
        txCmd.Aux[5] = pdsp[idx].Cycles;            // Cycle Count
        txCmd.Aux[6] = pdsp[idx].Stalls;            // Cycle Stall Count
        pnet_SendMessage( s );
        break;

    case PNET_CLIENT_CLEARCPUSTATUS:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // Temporarily halt CPU to clear its counts
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT);
        pdsp[idx].Cycles = 0;
        pdsp[idx].Stalls = 0;

        pnet_SendMessage( s );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_GETCPUREGISTER:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( rxCmd.Aux[1] >= 2 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }
        if( rxCmd.Aux[2] >= 32 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 2 );
            break;
        }

        // Temporarily halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        // Get the register
        txCmd.Aux[2] = READ_PDSP_REG(idx,rxCmd.Aux[1],rxCmd.Aux[2]);

        pnet_SendMessage( s );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_SETCPUREGISTER:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( rxCmd.Aux[1] >= 2 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }
        if( rxCmd.Aux[2] >= 32 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 2 );
            break;
        }

        // Temporarily halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        // Set the register
        WRITE_PDSP_REG(idx,rxCmd.Aux[1],rxCmd.Aux[2],rxCmd.Aux[3]);

        pnet_SendMessage( s );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_GETCPUREGISTERS:
        idx = rxCmd.Aux[0];
        if( rxCmd.Aux[0] >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( rxCmd.Aux[1] >= 2 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }
        if( rxCmd.Aux[2] >= 32 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 2 );
            break;
        }
        if( (rxCmd.Aux[2] + rxCmd.Aux[3]) > 32 )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 2 );
            break;
        }

        // Temporarily halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        // Copy the data from register into byte accessible RAM
        i = MemRead(pdsp[idx].RegBase+((rxCmd.Aux[1])*128)+(rxCmd.Aux[2]*4),progbuf,4,rxCmd.Aux[3]);

        txCmd.Aux[2] = rxCmd.Aux[3];                // Return length
        pnet_SendMessage( s );
        send( s, (char *)progbuf, (rxCmd.Aux[3]-rxCmd.Aux[2])*4, 0 );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_MEMREAD:
        if( rxCmd.Aux[1] > MAX_MEMOP )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }

        {
            uint xferAddr, xferSize, data;

            xferAddr = rxCmd.Aux[0] & ~3;
            xferSize = ((rxCmd.Aux[0]+rxCmd.Aux[1]+3)&~3)-xferAddr;
            i = MemRead( xferAddr, (uint *)scrap, 4, xferSize/4 );

            if( endianLocal != endianPDSP )
            {
                for( i=0; i<(int)xferSize; i+=4 )
                {
                    data = *(uint *)(scrap+i);
                    data = (data<<24) | (data>>24) | ((data&0xFF00)<<8) | ((data&0xFF0000)>>8);
                    *(uint *)(scrap+i) = data;
                }
            }
        }

        // Send the data
        txCmd.Aux[2] = rxCmd.Aux[1];
        pnet_SendMessage( s );
        send( s, scrap+(rxCmd.Aux[0]&3), txCmd.Aux[2], 0 );
        break;

    case PNET_CLIENT_MEMWRITE:
        if( rxCmd.Aux[1] > MAX_MEMOP )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }

        // Get the data
        pnet_Recv( s, scrap, rxCmd.Aux[1] );

        // Try to move 32 bit values as 32 bit values
        if( !(rxCmd.Aux[1]&3) && !(rxCmd.Aux[0]&3) )
        {
            // ** Move as words **
            uint data;

            if( endianLocal != endianPDSP )
            {
                for( i=0; i<(int)rxCmd.Aux[1]; i+=4 )
                {
                    data = *(uint *)(scrap+i);
                    data = (data<<24) | (data>>24) | ((data&0xFF00)<<8) | ((data&0xFF0000)>>8);
                    *(uint *)(scrap+i) = data;
                }
            }

            i = MemWrite( rxCmd.Aux[0], (uint *)scrap, 4, rxCmd.Aux[1]/4 );
        }
        else
        {
            // ** Move as bytes **
            if( endianLocal != endianPDSP )
            {
                for( i=0; i<(int)rxCmd.Aux[1]; i++ )
                {
                    uint cnvAddr = 3-((rxCmd.Aux[0]+i)&3)+((rxCmd.Aux[0]+i)&~3);
                    i = MemWrite( cnvAddr, (uint *)(scrap+i), 1, 1 );
                }
            }
            else
            {
                i = MemWrite( rxCmd.Aux[0], (uint *)scrap, 1, rxCmd.Aux[1] );
            }
        }
        pnet_SendMessage( s );
        break;

    case PNET_CLIENT_IRAMREAD:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( (rxCmd.Aux[1]+rxCmd.Aux[2]) > CPU_IRAM )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // Temporarily halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        // Copy the data from IRAM into byte accessible RAM
        i = MemRead( pdsp[idx].IRAMBase+(rxCmd.Aux[1]*4), progbuf, 4, rxCmd.Aux[2] );

        // Hide our opcode used for BP
        for( i=0; i<NUM_BP; i++ )
        {
            if( bp[idx][i].InUse )
            {
                relAddr = bp[idx][i].Addr - rxCmd.Aux[1];
                if( relAddr < rxCmd.Aux[2] )
                    progbuf[relAddr] = bp[idx][i].OpCode;
            }
        }
        if( bpRunTo[idx].InUse )
        {
            relAddr = bpRunTo[idx].Addr - rxCmd.Aux[1];
            if( relAddr < rxCmd.Aux[2] )
                progbuf[relAddr] = bpRunTo[idx].OpCode;
        }

        txCmd.Aux[2] = rxCmd.Aux[2];                // Return length
        pnet_SendMessage( s );
        send( s, (char *)progbuf, rxCmd.Aux[2]*4, 0 );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_IRAMWRITE:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( (rxCmd.Aux[1]+rxCmd.Aux[2]) > CPU_IRAM )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // Halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        pnet_Recv( s, (char *)progbuf, rxCmd.Aux[2]*4 );
        pnet_SendMessage( s );

        // Copy the data from byte accessible RAM to IRAM
        i = MemWrite( pdsp[idx].IRAMBase+(rxCmd.Aux[1]*4), progbuf, 4, rxCmd.Aux[2] );

        break;

    case PNET_CLIENT_RESETCPU:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        SET_PDSP_CONTROL(idx,0);
        // Wait for idle
        for( i=0; i<1000; i++ )
            if( !(GET_PDSP_CONTROL(idx) & CONTROL_BUSY ) )
                break;
        for( i=0; i<32; i++ )
            WRITE_PDSP_REG(idx,0,i,0);
        for( i=0; i<CPU_IRAM; i++ )
            WRITE_PDSP_IRAM(idx,i,0);

        // We don't want to restore the temp breakpt
        bpRunTo[idx].InUse = 0;

        // Clear the cycle counters
        SET_PDSP_CYCLES(idx,1);
        pdsp[idx].Cycles = 0;
        pdsp[idx].Stalls = 0;

        pnet_SendMessage( s );
        break;

    case PNET_CLIENT_SETCPUIP:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // Halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT);

        tmp1 = rxCmd.Aux[1]<<16;
        SET_PDSP_CONTROL(idx,tmp1);
        pnet_SendMessage( s );
        break;

    case PNET_CLIENT_SETCPURUNMODE:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // Halt the PDSP for a clean mode change
        // Clearing the enable befor calling CheckPDSPState() will
        // allow CheckPDSPState() to perform proper clean up.
        tmp1 = GET_PDSP_CONTROL(idx) & 0xFFFF;
        SET_PDSP_CONTROL(idx,tmp1 & ~CONTROL_ENABLE);
        CheckPDSPState(idx,0);

        // Remove any old "run to" address
        if( bpRunTo[idx].InUse )
        {
            bpRunTo[idx].InUse = 0;
            WRITE_PDSP_IRAM(idx,bpRunTo[idx].Addr,bpRunTo[idx].OpCode);
        }

        // Now just handle the run modes
        switch( rxCmd.Aux[1] )
        {
        case PNET_CPU_RUNMODE_HALT:
            break;
        case PNET_CPU_RUNMODE_SINGLESTEP:
            SuspendBP(idx);
            SET_PDSP_CONTROL(idx,CONTROL_NRESET|CONTROL_ENABLE|CONTROL_SSTEP|CONTROL_CCOUNT);
            break;
        case PNET_CPU_RUNMODE_RUN:
            // Suspend BP for one step, then run with BP
            SuspendBP(idx);
            SET_PDSP_CONTROL(idx,CONTROL_NRESET|CONTROL_ENABLE|CONTROL_SSTEP|CONTROL_CCOUNT);
            do
            {
                tmp1 = CheckPDSPState(idx,CPSF_NOCOUNTS);
            } while( tmp1 & CONTROL_ENABLE );
            // Check for "run to" address
            if( rxCmd.Aux[2]!=0xFFFFFFFF && rxCmd.Aux[2]<CPU_IRAM )
            {
                // Install "run to" address
                bpRunTo[idx].InUse  = 1;
                bpRunTo[idx].Addr   = rxCmd.Aux[2];
                bpRunTo[idx].OpCode = READ_PDSP_IRAM(idx,bpRunTo[idx].Addr);
                WRITE_PDSP_IRAM(idx,bpRunTo[idx].Addr,OPCODE_BREAK);
            }
            SET_PDSP_CONTROL(idx,CONTROL_NRESET|CONTROL_ENABLE|CONTROL_CCOUNT);
            break;
        case PNET_CPU_RUNMODE_RUNFREE:
            SuspendBP(idx);
            SET_PDSP_CONTROL(idx,CONTROL_NRESET|CONTROL_ENABLE|CONTROL_CCOUNT);
            break;
        }
        pnet_SendMessage( s );
        break;

    case PNET_CLIENT_SETCPUBREAKPOINT:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( rxCmd.Aux[1] >= CPU_IRAM )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }

        // Halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        for( i=0; i<NUM_BP; i++ )
        {
            if( bp[idx][i].InUse && bp[idx][i].Addr==rxCmd.Aux[1] )
               break;
        }
        if( i==NUM_BP )
        {
            for( i=0; i<NUM_BP; i++ )
            {
                if( !bp[idx][i].InUse )
                {
                    bp[idx][i].InUse  = 1;
                    bp[idx][i].Addr   = rxCmd.Aux[1];
                    bp[idx][i].OpCode = READ_PDSP_IRAM(idx,bp[idx][i].Addr);
                    WRITE_PDSP_IRAM(idx,bp[idx][i].Addr,OPCODE_BREAK);
                    break;
                }
            }
        }

        pnet_SendMessage( s );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_CLEARCPUBREAKPOINT:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }
        if( rxCmd.Aux[1] >= CPU_IRAM )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 1 );
            break;
        }

        // Halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        for( i=0; i<NUM_BP; i++ )
        {
            if( bp[idx][i].InUse && bp[idx][i].Addr==rxCmd.Aux[1] )
            {
                WRITE_PDSP_IRAM(idx,bp[idx][i].Addr,bp[idx][i].OpCode);
                bp[idx][i].InUse = 0;
            }
        }

        pnet_SendMessage( s );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    case PNET_CLIENT_CLEARALLCPUBREAKPOINTS:
        idx = rxCmd.Aux[0];
        if( idx >= NUM_CPU )
        {
            pnet_SendError( s, PNET_SERVER_BAD_PARAMETER, 0 );
            break;
        }

        // Halt CPU (if needed)
        tmp1 = CheckPDSPState(idx,CPSF_TEMPHALT|CPSF_NOCOUNTS);

        // Remove breakpoints
        for( i=0; i<NUM_BP; i++ )
        {
            if( bp[idx][i].InUse )
            {
                WRITE_PDSP_IRAM(idx,bp[idx][i].Addr,bp[idx][i].OpCode);
                bp[idx][i].InUse = 0;
            }
        }

        pnet_SendMessage( s );

        // Start up again if we were running
        if( tmp1 & CONTROL_ENABLE )
            SET_PDSP_CONTROL(idx,tmp1);
        break;

    default:
        pnet_SendError( s, PNET_SERVER_ERROR_UNKNOWNCOMMAND, rxCmd.NetCommand );
        break;
    }
}


void pnet_SendMessage( SOCKET s )
{
#if 0
        printf("\nSend CMD=%d\nPARAM=[0x%08x, 0x%08x, 0x%08x, 0x%08x,\n       0x%08x, 0x%08x, 0x%08x, 0x%08x]\n",
                   txCmd.NetCommand,txCmd.Aux[0],txCmd.Aux[1],txCmd.Aux[2],
                   txCmd.Aux[3],txCmd.Aux[4],txCmd.Aux[5],txCmd.Aux[6],txCmd.Aux[7]);
#endif
    send( s, (const char *)&txCmd, sizeof(txCmd), 0 );
}

void pnet_SendError( SOCKET s, uint ErrorType, uint BadData )
{
    txCmd.NetCommand = PNET_SERVER_ERROR;
    txCmd.Aux[0]     = ErrorType;
    txCmd.Aux[1]     = BadData;
    pnet_SendMessage( s );
}

int pnet_RecvMessage( SOCKET s )
{
    int i;

    i = recv( s, (char *)&rxCmd, sizeof(rxCmd), MSG_PEEK );
    if( i<=0 )
        return RECVMSG_SOCKERR;
    if( i < sizeof(rxCmd) )
        return RECVMSG_PENDING;

    i = recv( s, (char *)&rxCmd, sizeof(rxCmd), 0 );
    if( i != sizeof(rxCmd) )
        return RECVMSG_SOCKERR;

    if( rxCmd.Version != PNET_VERSION )
    {
        if( IS_PNET(rxCmd.Version) )
        {
            txCmd.NetCommand = PNET_SERVER_ERROR;
            txCmd.Aux[0]     = PNET_SERVER_ERROR_BADVERSION;
            txCmd.Aux[1]     = rxCmd.Version;
            pnet_SendMessage( s );
        }
        else
        {
            txCmd.NetCommand = PNET_SERVER_ERROR;
            txCmd.Aux[0]     = PNET_SERVER_ERROR_BADSYNC;
            txCmd.Aux[1]     = rxCmd.Version;
            pnet_SendMessage( s );
        }
        return RECVMSG_CMDERR;
    }
#if 0
        printf("\nRecv CMD=%d PARAM=[0x%08x, 0x%08x, 0x%08x]\n",rxCmd.NetCommand,rxCmd.Aux[0],rxCmd.Aux[1],rxCmd.Aux[2]);
#endif
    return RECVMSG_OK;
}

int pnet_Recv( SOCKET s, char *pBuffer, uint length )
{
    struct  timeval timeout;
    fd_set  ibits;
    int     i,off;

    off = 0;

RECV_AGAIN:
    FD_ZERO(&ibits);
    FD_SET(s, &ibits);

    timeout.tv_sec  = 2;
    timeout.tv_usec = 0;

    if( select( 32, &ibits, 0, 0, &timeout ) )
    {
        i = recv( s, pBuffer+off, length-off, 0 );
        if( i<=0 )
            goto RECV_ERROR;
        if( i < (int)length-off )
        {
            off += i;
            goto RECV_AGAIN;
        }
        return length;
    }

RECV_ERROR:
    return -1;
}


int main()
{
#ifdef _DOS_
    WORD    wVersionRequested;
    WSADATA wsaData;
    time_t  tn,ts_busy;
#endif
    SOCKET  s       = INVALID_SOCKET;
    SOCKET  sudp    = INVALID_SOCKET;
    SOCKET  sactive = INVALID_SOCKET;
    SOCKET  sbusy   = INVALID_SOCKET;
    struct  sockaddr_in sin;
    struct  timeval timeout;
    fd_set  ibits;
    int     badMessage = 0;
    int     i,j;
    uint    data1,data2,PDSPCount;

    printf("\n\nPDSP Debug Server 0.04\n");
    printf("PDSP CPU Count   : %d\n", NUM_CPU);
    printf("Mem Region Count : %d\n", NUM_MEM);

    //
    // Verify we can see our PDSP cores, and record Endian mode
    //
    data1 = 0x12340000;
    PDSPCount = 0;
    for( i=0; i<NUM_CPU; i++ )
        SET_PDSP_CONTROL(i,data1);
    for( i=0; i<NUM_CPU; i++ )
    {
        data1 = data2 = 0;
        data1 = GET_PDSP_CONTROL(i);
        data2 = GET_PDSP_IP(i);
        if( (data1&0xFFFF3FFF)==0x12340001 && data2==0x1234 )
        {
            PDSPCount++;
            if( data1 & (1<<14) )
                endianPDSP = 1;
        }
        else
        {
            printf("ERROR: Read 0x%08x from GET_PDSP_CONTROL(%d)\n",data1,i);
            printf("ERROR: Read 0x%08x from GET_PDSP_IP(%d)\n",data2,i);
            return;
        }
    }
    data1 = 0;
    for( i=0; i<NUM_CPU; i++ )
        SET_PDSP_CONTROL(i,data1);

    endianLocal = 0;
    *(unsigned char *)&endianLocal = 1;
    if( endianLocal != 1 )
        endianLocal = 1;
    else
        endianLocal = 0;

    if( endianLocal != endianPDSP )
    {
        if( TRANSACTION_SIMULATION == 1 )
        {
            printf("\nNote: Endian conversion disabled in transaction simulation.\n");
            endianLocal = endianPDSP;
        }
        else
            printf("Endian conversion supplied\n");
    }

    if( PDSPCount != NUM_CPU )
        printf("ERROR: Expected %d PDSP cores, found %d cores\n",NUM_CPU,PDSPCount);

    // Initialize State Data
    for( i=0; i<NUM_CPU; i++ )
    {
        bpRunTo[i].InUse = 0;
        for( j=0; j<NUM_BP; j++ )
            bp[i][j].InUse = 0;
    }

#ifdef _DOS_
    wVersionRequested = MAKEWORD(1, 1);
    i = WSAStartup(wVersionRequested, &wsaData);
    if (i != 0)
    {
        printf("\r\nUnable to initialize WinSock for host info");
        exit(0);
    }
#endif

    sudp = socket( AF_INET, SOCK_DGRAM, 0 );
    if( sudp==INVALID_SOCKET )
    {
        printf("failed socket (%d)\n",fdError());
        goto leave;
    }

    sin.sin_family      = AF_INET;
    sin.sin_addr.s_addr = 0;
    sin.sin_port        = htons(PNET_PORT);

    if ( bind( sudp, (struct sockaddr *)&sin, sizeof(sin) ) < 0 )
    {
        printf("failed bind (%d)\n",fdError());
        goto leave;
    }

    s = socket( AF_INET, SOCK_STREAM, 0 );
    if( s==INVALID_SOCKET )
    {
        printf("failed socket (%d)\n",fdError());
        goto leave;
    }

    sin.sin_family      = AF_INET;
    sin.sin_addr.s_addr = 0;
    sin.sin_port        = htons(PNET_PORT);

    if ( bind( s, (struct sockaddr *)&sin, sizeof(sin) ) < 0 )
    {
        printf("failed bind (%d)\n",fdError());
        goto leave;
    }

    if( (listen( s, 5 )) < 0 )
    {
        printf("failed listen (%d)\n",fdError());
        goto leave;
    }

    txCmd.Version = PNET_VERSION;

    // Run until task is destroyed by the system
#ifdef _DOS_
    while(!_kbhit())
#else
    for(;;)
#endif
    {
        // Clear the select flags
        FD_ZERO(&ibits);

        FD_SET(s, &ibits);
        FD_SET(sudp, &ibits);
        if( sactive != INVALID_SOCKET )
            FD_SET(sactive, &ibits);
        if( sbusy != INVALID_SOCKET )
            FD_SET(sbusy, &ibits);

        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        i = select( 32, &ibits, 0, 0, &timeout );
        if( i<0 )
            goto leave;

        if( i )
        {
            if( FD_ISSET(sudp, &ibits) )
            {
                char scrap[4];
#ifdef _DOS_
                int tmp;
#else
                socklen_t tmp;
#endif
                struct sockaddr from;

                tmp = sizeof(from);
                i = recvfrom( sudp, scrap, 4, 0, &from, &tmp );

                if( i==4 && !strncmp( scrap, "Hi?", 4 ) )
                {
                    strcpy( scrap, "Hi!" );
                    sendto( sudp, scrap, 4, 0, &from, sizeof(from) );
                }
            }

            if( (sactive==INVALID_SOCKET || sbusy==INVALID_SOCKET) && FD_ISSET(s, &ibits) )
            {
                // We have a new connection. Assign it so sbusy at
                // first...
                sbusy = accept( s, 0, 0 );

                // If the active socket is free use it, else print out
                // a busy message
                if( sactive == INVALID_SOCKET )
                {
                    sactive = sbusy;
                    sbusy = INVALID_SOCKET;
                    txCmd.NetCommand = PNET_SERVER_READY;
                    pnet_SendMessage( sactive );
                    badMessage = 0;
                }
                else
                {
                    txCmd.NetCommand = PNET_SERVER_BUSY;
                    pnet_SendMessage( sbusy );
#ifdef _DOS_
                    ts_busy = time(0);
#endif
                }
            }

            if( sbusy!=INVALID_SOCKET )
            {
                if( FD_ISSET(sbusy, &ibits) )
                {
                    i=pnet_RecvMessage( sbusy );
                    if( i == RECVMSG_PENDING )
                        goto NO_BUSY_COMMAND;
                    if( i != RECVMSG_OK )
                    {
                        fdClose( sbusy );
                        sbusy = INVALID_SOCKET;
                    }
                    else
                    {
#ifdef _DOS_
                        ts_busy = time(0);
#endif
                        if( rxCmd.NetCommand == PNET_CLIENT_FORCECONNECT )
                        {
                            FD_ZERO(&ibits);
                            fdClose( sactive );
                            sactive = sbusy;
                            sbusy = INVALID_SOCKET;
                            txCmd.NetCommand = PNET_SERVER_READY;
                            pnet_SendMessage( sactive );
                            badMessage = 0;
                        }
                        else if( rxCmd.NetCommand == PNET_CLIENT_QUERY )
                        {
                            txCmd.NetCommand = PNET_SERVER_BUSY;
                            pnet_SendMessage( sbusy );
                        }
                        else
                        {
                            fdClose( sbusy );
                            sbusy = INVALID_SOCKET;
                        }
                    }
                }
                else
                {
NO_BUSY_COMMAND:
#ifdef _DOS_
                    tn = time(0);
                    if( (tn - ts_busy)>2 )
                    {
                        fdClose( sbusy );
                        sbusy = INVALID_SOCKET;
                    }
#else
                    fdClose( sbusy );
                    sbusy = INVALID_SOCKET;
#endif
                }
            }

            if( sactive!=INVALID_SOCKET && FD_ISSET(sactive, &ibits) )
            {
                i=pnet_RecvMessage( sactive );
                if( i!=RECVMSG_PENDING && i!=RECVMSG_OK )
                {
                    if( ++badMessage > 9 )
                    {
                        fdClose( sactive );
                        if( sbusy != INVALID_SOCKET )
                        {
                            sactive = sbusy;
                            txCmd.NetCommand = PNET_SERVER_READY;
                            pnet_SendMessage( sactive );
                            badMessage = 0;
                        }
                        else
                        {
                            sactive = INVALID_SOCKET;
                        }
                    }
                }
                else if( i==RECVMSG_OK )
                {
                    ProcessCommand( sactive );
                }
            }
        }
    }

leave:
    printf("\nPDSP Debug Server Exiting...\n");
    if( sactive != INVALID_SOCKET )
        fdClose(sactive);
    if( sbusy != INVALID_SOCKET )
        fdClose(sbusy);
    if( s != INVALID_SOCKET )
        fdClose( s );
    if( sudp != INVALID_SOCKET )
        fdClose( sudp );
#ifdef _DOS_
    WSACleanup();
#endif

    return(0);
}


uint MemRead( uint Address, uint *pData, uint WordSize, uint WordCnt )
{
    int i;
    uint *pSource = (uint *)Address;

    if( WordSize != 4 )
    {
        printf("Bad MemRead assumption!\n");
        return 0;
    }

    for( i=0; i<WordCnt; i++ )
        *pData++ = *pSource++;

    return(WordCnt);
}

uint MemWrite( uint Address, uint *pData, uint WordSize, uint WordCnt )
{
    int i;
    uint *pDestination = (uint *)Address;

    if( WordSize != 4 )
    {
        printf("Bad MemWrite assumption!\n");
        return 0;
    }

    for( i=0; i<WordCnt; i++ )
        *pDestination++ = *pData++;

    return(WordCnt);
}

