
//
// PDSP Cores
//

#define PDSP_ENDIAN     1

#define PDSP1_BASE      0x300d000
#define PDSP1_DEBUG     0x300e000
#define PDSP1_IRAM      0x3080000

#define PDSP2_BASE      0x300d100
#define PDSP2_DEBUG     0x300e100
#define PDSP2_IRAM      0x3088000

#define PDSP3_BASE      0x300d200
#define PDSP3_DEBUG     0x300e200
#define PDSP3_IRAM      0x3090000

//
// PDSP Scratch RAM
//

#define LRAM1_BASE      0x200000
#define LRAM1_SIZE      0x20000
#define LRAM1_RD        0
#define LRAM1_WD        0

#define LRAM2_BASE      0x220000
#define LRAM2_SIZE      0x20000
#define LRAM2_RD        0
#define LRAM2_WD        0

#define LRAM3_BASE      0x240000
#define LRAM3_SIZE      0x20000
#define LRAM3_RD        0
#define LRAM3_WD        0

//
// Queue Manager
//
#define QMGR1_BASE      0x3100000
#define QMGR1_DESCBASE  0x25C000

//
// Status Bits and Constants Notes
//

//
// PDSP1 Status In Bit 0 = QMgr1 Bit 0
// PDPS1 Constant 0      = QMGR1_BASE
//
// PDSP2 Status In Bit 0 = QMgr1 Bit 1
// PDSP2 Constant 0      = QMGR1_BASE
//
// PDSP3 Status In Bit 0 = QMgr1 Bit 2
// PDSP3 Constant 0      = QMGR1_BASE
//

