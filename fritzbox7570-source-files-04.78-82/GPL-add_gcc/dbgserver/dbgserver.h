//-------------------------------------------------------------------------------
// pView Modeling
//-------------------------------------------------------------------------------
// pm_dbgserver.h
//
// This file contains the class definition of the pView Model PDSP debugger
// network server device
//
// Original Author: Michael Denio
//
// Copyright 2008 by Texas Instruments Inc.
//-------------------------------------------------------------------------------

#ifndef PM_DBGSERVER_INCLUDED_
#define PM_DBGSERVER_INCLUDED_

#include <stdio.h>

#ifdef _DOS_
#include <time.h>
#include <conio.h>
#include <winsock.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#define SOCKET int
#define INVALID_SOCKET -1
#endif

void SuspendBP( uint idx );
void ProcessCommand( SOCKET s );
uint MemRead( uint Address, uint *pData, uint WordSize, uint WordCnt );
uint MemWrite( uint Address, uint *pData, uint WordSize, uint WordCnt );
uint pnet_ReadWord( uint addr );
void pnet_WriteWord( uint addr, uint data );
void pnet_SendMessage( SOCKET s );
void pnet_SendError( SOCKET s, uint ErrorType, uint BadData );
int  pnet_RecvMessage( SOCKET s );
#define RECVMSG_OK          1
#define RECVMSG_PENDING     0
#define RECVMSG_CMDERR      -1
#define RECVMSG_SOCKERR     -2
int pnet_Recv( SOCKET s, char *pBuffer, uint length );
uint CheckPDSPState( uint idx, uint flags );

#endif
