

/* 'stolen' from uclibc */

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STDIO_H
#include <stdio.h>
#endif


#ifdef __UCLIBC_HAS_WCHAR__
#ifdef HAVE_WCHAR_H
#include <wchar.h>
#endif
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif



#include "attrib.h"
#include "types.h"
#include "unistr.h"
#include "debug.h"
#include "logging.h"
#include "misc.h"


#ifndef __UCLIBC_HAS_WCHAR__

#include "my_wchar.h"

#ifdef __UCLIBC_HAS_WCHAR__
#error  __UCLIBC_HAS_WCHAR__ defined 
// size_t _stdlib_mb_cur_max (void) // UDO - DIRTY HACK
// {
// 	return 2;
// }
#endif

#if 0
#define __restrict 

static size_t mbsrtowcs(wchar_t *__restrict dst, const char **__restrict src,
                                 size_t len, mbstate_t *__restrict ps)
{
        static mbstate_t mbstate;       /* Rely on bss 0-init. */

        return __mbsnrtowcs(dst, src, SIZE_MAX, len,
                                                ((ps != NULL) ? ps : &mbstate));
}

static size_t wcrtomb(register char *__restrict s, wchar_t wc,
                           mbstate_t *__restrict ps)
{
#ifdef __UCLIBC_MJN3_ONLY__
#warning TODO: Should wcsnrtombs nul-terminate unconditionally?  Check glibc.
#endif /* __UCLIBC_MJN3_ONLY__ */
        wchar_t wcbuf[1];
        const wchar_t *pwc;
        size_t r;
        char buf[MB_LEN_MAX];

        if (!s) {
                s = buf;
                wc = 0;
        }

        pwc = wcbuf;
        wcbuf[0] = wc;

        r = __wcsnrtombs(s, &pwc, 1, MB_LEN_MAX, ps);
        return (r != 0) ? r : 1;
}

static size_t mbrtowc(wchar_t *__restrict pwc, const char *__restrict s,
                           size_t n, mbstate_t *__restrict ps)
{
        static mbstate_t mbstate;       /* Rely on bss 0-init. */
        wchar_t wcbuf[1];
        const char *p;
        size_t r;
        char empty_string[1];           /* Avoid static to be fPIC friendly. */

        if (!ps) {
                ps = &mbstate;
        }

        if (!s) {
                pwc = (wchar_t *) s;    /* NULL */
                empty_string[0] = 0;    /* Init the empty string when necessary. */
                s = empty_string;
                n = 1;
        } else if (!n) {
                /* TODO: change error code? */
                return (ps->__mask && (ps->__wc == 0xffffU))
                        ? ((size_t) -1) : ((size_t) -2);
        }

        p = s;

#ifdef __CTYPE_HAS_UTF_8_LOCALES
        /* Need to do this here since mbsrtowcs doesn't allow incompletes. */
        if (ENCODING == __ctype_encoding_utf8) {
                if (!pwc) {
                        pwc = wcbuf;
                }
                r = _wchar_utf8sntowcs(pwc, 1, &p, n, ps, 1);
                return (r == 1) ? (p-s) : r; /* Need to return 0 if nul char. */
        }
#endif

#ifdef __UCLIBC_MJN3_ONLY__
#warning TODO: This adds a trailing nul!
#endif /* __UCLIBC_MJN3_ONLY__ */

        r = __mbsnrtowcs(wcbuf, &p, SIZE_MAX, 1, ps);

        if (((ssize_t) r) >= 0) {
                if (pwc) {
                        *pwc = *wcbuf;
                }
        }
        return (size_t) r;
}



// -------------------------------------------------------------------

int wctomb(register char *__restrict s, wchar_t swc)
{
        return (!s)
                ?
#ifdef __CTYPE_HAS_UTF_8_LOCALES
                (ENCODING == __ctype_encoding_utf8)
#else
                0                                               /* Encoding is stateless. */
#endif
                : ((ssize_t) wcrtomb(s, swc, NULL));
}


int mbtowc(wchar_t *__restrict pwc, register const char *__restrict s, size_t n)
{
        static mbstate_t state;
        size_t r;

        if (!s) {
                state.__mask = 0;
#ifdef __CTYPE_HAS_UTF_8_LOCALES
                return ENCODING == __ctype_encoding_utf8;
#else
                return 0;
#endif
        }

        if ((r = mbrtowc(pwc, s, n, &state)) == (size_t) -2) {
                /* TODO: Should we set an error state? */
                state.__wc = 0xffffU;   /* Make sure we're in an error state. */
                return (size_t) -1;             /* TODO: Change error code above? */
        }
        return r;
}


size_t mbstowcs(wchar_t * __restrict pwcs, const char * __restrict s, size_t n)
{
        mbstate_t state;
        const char *e = s;                      /* Needed because of restrict. */

        state.__mask = 0;                       /* Always start in initial shift state. */
        return mbsrtowcs(pwcs, &e, n, &state);
}

#else  /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */

/* This implementation is for sizeof(wchar_t) == 4 and MAX_CUR_MB == 1.
 * MAX_CUR_MB == 1 implicates the ISO8859-1 encoding for the multi byte char encoding 
 * (which is in fact a single byte encoding in this case).
 * All unicode codepoints >= 256  cant be encoded in ISO8859-1, so these filename can't be used at all!
 * Using a FALLBACK_CODEPOINT does not work (e.g. error message "ls: /var/mp/C_mit_Akut__.txt: No such file or directory").
 */

// #define FALLBACK_CODEPOINT ((unsigned long)'_')


static size_t ConvertSingleUnicodeCodepointTo_ISO8859_1(unsigned long codepoint, unsigned char *buf, size_t max_buf, /*OUT*/int *pbFailed)
{
	if (max_buf < 1) { *pbFailed = 1; return 0; }
#ifdef FALLBACK_CODEPOINT
	if (codepoint > 0xff) codepoint = FALLBACK_CODEPOINT;
#else
	if (codepoint > 0xff) { *pbFailed = 1; return 0; }
#endif

	*buf = (unsigned char)codepoint;
	*pbFailed = 0;
	return 1;
}

static unsigned long ConvertSingle_ISO8859_1_ToUnicodeCodepoint(unsigned char *buf, size_t max_buf, /*out */size_t *pBytesConverted)
{
	*pBytesConverted = 0;
	if (0 == max_buf) return 0;

	*pBytesConverted = 1;
	return (unsigned long)*buf;
}

#if 0 
static unsigned long ConvertSingle_UCS2LE_ToUnicodeCodepoint(unsigned char *buf, size_t max_buf, /*out */size_t *pBytesConverted)
{
	unsigned long codepoint;
	if (max_buf < 2) {
		*pBytesConverted = 0;
		return 0;
	}
	codepoint = *buf + (*(buf+1) << 8);
	*pBytesConverted = 2;
	return codepoint;
}
#endif

static size_t ConvertSingleUnicodeCodepointTo_UCS2LE(unsigned long codepoint, unsigned char *buf, size_t max_buf)
{
	if (max_buf < 2) return 0;

	if (codepoint > 0xffff) {
		// can't represent in UCS2
#ifdef FALLBACK_CODEPOINT
		codepoint = FALLBACK_CODEPOINT;
#else
		return 0;
#endif
	}
	*buf++ = codepoint & 0xff;
	*buf = (codepoint >> 8) & 0xff;
	return 2;
}

/*
DESCRIPTION

The wctomb() function converts wchar into a multibyte character and stores the result, up to MB_CUR_MAX bytes, in the array object pointed to by s. The behavior of this function is affected by the LC_CTYPE category for the current locale. When wctomb() is called with s as a NULL pointer, if the encoding has a state dependency a non-zero value is returned and the function is placed into its initial state, otherwise 0 is returned. Subsequent calls with s as other than a NULL pointer cause the internal state of the function to be altered as necessary. Changing the LC_CTYPE category causes the shift state of this function to be indeterminate.
PARAMETERS

s 

    Is the address of the multibyte character.
wchar 

    Is the wide character to convert.

RETURN VALUES

The wctomb() function returns the number of bytes, never greater than MB_CUR_MAX, in the wide character wchar. If wchar is the NULL character then wctomb() returns 1. If the conversion of wchar is not possible in the current locale, wctomb() returns -1. 
*/
int wctomb(register char *__restrict s, wchar_t swc)
{
//	fprintf(stderr, "wctomb swc=%u s=%p\n", swc, s); fflush(stderr);
	if (!s) return 0;
	size_t BytesConverted;
	unsigned long codepoint = (unsigned long)swc;
	int bFailed;
	BytesConverted = ConvertSingleUnicodeCodepointTo_ISO8859_1(codepoint, (unsigned char *)s, MB_CUR_MAX, &bFailed);
	if (bFailed || BytesConverted > MB_CUR_MAX) {
fprintf(stderr, "wctomb failed to convert unicode codepoint=0x%lx to mb (%s)\n", codepoint, "ISO-8859"); fflush(stderr);
		return -1;
	}
	return BytesConverted;
}

/*
DESCRIPTION

The mbtowc() function converts the multibyte character addressed by s into the corresponding UNICODE character.
PARAMETERS

pwc 
    Is the address of a wide character, type wchar_t, to receive the UNICODE equivalent of s.
s 
    Points to the multibyte character to be converted to UNICODE.
n 
    Is the maximum width, in bytes, for which to scan s for a valid multibyte sequence. Regardless of the value of n, no more than MB_CUR_MAX bytes are examined.

RETURN VALUES

If successful, mbtowc() returns the length, in bytes, of the multibyte character for which it found a UNICODE equivalent.
If s is null or points to a null character, mbtowc() returns zero.
If no valid multibyte sequence is found at s, mbtowc() returns -1. 
*/
int mbtowc(wchar_t *__restrict pwc, register const char *__restrict s, size_t n)
{
	static char debug = 1;
	if (debug) {
		debug = 0;
fprintf(stderr, "my_wchar debug sizeof(wchar_t)=%u MB_CUR_MAX=%u\n", sizeof(wchar_t), MB_CUR_MAX); fflush(stderr);
	}

	if (!s || !*s) return 0;
	size_t BytesConverted;
	if (n > MB_CUR_MAX) n = MB_CUR_MAX;
	unsigned long codepoint = ConvertSingle_ISO8859_1_ToUnicodeCodepoint((unsigned char *)s, n, &BytesConverted);
	if (0 == BytesConverted) return -1;
	unsigned char buf[2];
	size_t BytesConverted2 = ConvertSingleUnicodeCodepointTo_UCS2LE(codepoint, buf, 2);
	if (2 != BytesConverted2) {
fprintf(stderr, "mbtowc failed to convert unicode codepoint=0x%lx to wchar_t\n", codepoint); fflush(stderr);
		return -1;
	}
	*pwc = (wchar_t)*((unsigned short *)buf);

	return BytesConverted;
}


/*
DESCRIPTION

The mbstowcs() function converts the multibyte string addressed by s into the corresponding UNICODE string. It stores up to n wide characters in pwcs. It stops conversion after encountering and storing a null character.
PARAMETERS

pwcs 
    Is the address of an array of wide characters, type wchar_t, to receive the UNICODE equivalent of multibyte string s.
s 
    Points to a null-terminated multibyte string to be converted to UNICODE.
n 
    Is the maximum number of characters to convert and store in pwcs.

RETURN VALUES
If successful, mbstowcs() returns the number of multibyte characters it converted, not including the terminating null character.
If s is a null pointer or points to a null character, mbstowcs() returns zero. 
If mbstowcs() encounters an invalid multibyte sequence, it returns -1. 
*/
size_t mbstowcs(wchar_t * __restrict pwcs, const char * __restrict s, size_t n)
{
	size_t nchar = 0;
	if (!s || !*s) return 0;
	while (*s && nchar < n) {
		size_t BytesConverted;
		nchar++;
		unsigned long codepoint = ConvertSingle_ISO8859_1_ToUnicodeCodepoint((unsigned char *)s, MB_CUR_MAX, &BytesConverted);
		if (0 == BytesConverted) {
fprintf(stderr, "mbstowcs failed to convert mb to unicode\n"); fflush(stderr);
			return -1;
		}
		s += BytesConverted;
		*pwcs++ = (wchar_t)codepoint;
	}
	// TODO - don't konwn if when n == nchar its allowed/required to add the 0-wchar to terminate the string.
	if (nchar < n) *pwcs = (wchar_t)0;
// fprintf(stderr, "mbstowcs ret %u\n", nchar); fflush(stderr);
	return nchar;
}	

#endif


#endif /* __UCLIBC_HAS_WCHAR__ */

