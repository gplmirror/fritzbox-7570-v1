
#ifdef __UCLIBC_HAS_WCHAR__
#error  __UCLIBC_HAS_WCHAR__ defined 
#endif

#define MB_CUR_MAX 1

// typedef unsigned short wchar_t ;

int wctomb(register char *s, wchar_t swc);
int mbtowc(wchar_t *pwc, register const char *s, size_t n);
size_t mbstowcs(wchar_t *pwcs, const char *s, size_t n);

