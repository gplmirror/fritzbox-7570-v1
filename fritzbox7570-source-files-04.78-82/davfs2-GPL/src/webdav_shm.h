/*  webdav_shm.h: shared memory structure.
    Copyright (C) 2009 AVM 

    This file is part of davfs2.

    davfs2 is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    davfs2 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with davfs2; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA. */


//shared memory infos
struct webdav_shm_infos{
    unsigned int is_running;
    unsigned int connection_state;
    unsigned int cache_speed;
    __uint64_t cache_storage_avail;
    __uint64_t cache_storage_used;
    unsigned int dirty_files;
    unsigned int running_uploads;
    unsigned int running_downloads;
    __uint64_t finished_uploads;
    __uint64_t failed_uploads;
    __uint64_t sum_failed_uploads;
    __uint64_t finished_downloads;
    __uint64_t failed_downloads;
    __uint64_t storage_quota_avail;
    __uint64_t storage_quota_used;
    __uint64_t upload_quota_avail;
    __uint64_t upload_quota_used;
    __uint64_t download_quota_avail;
    __uint64_t download_quota_used;
    __uint64_t traffic_quota_avail;
    __uint64_t traffic_quota_used;
    __uint64_t avg_upload_speed;
    __uint64_t avg_download_speed;
    unsigned int low_upload_speed;
    __uint64_t size_downloads;
    __uint64_t size_uploads;
    char uploading_file1[2048];
    char uploading_file2[2048];
    char downloading_file1[2048];
    char downloading_file2[2048];
    __uint64_t place_holder_1;
    __uint64_t place_holder_2;
    __uint64_t place_holder_3;
    time_t alive_time;
};


//shared memory id
#define WEBDAV_SHM_ID 334455
